> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1-rc3 - [urls pangu]<br/>

# Tasks

本文用于汇总 RISC-V 的相关任务和图文视频等分析成果。

大家可在相应任务后追加 `@your-gitee-id` 来认领任务，也可以提交 PR 新增或修订任务。

相关图文或直播回放成果在发布后将于第一时间更新进来。

## QuickStart

- RISC-V Linux Analyse Howto
  - [如何分析 Linux 内核 RISC-V 架构相关代码][027] @falcon

- RISC-V Linux Distributions
  - [两分钟内极速体验 RISC-V Linux 系统发行版][025] @falcon
  - [5 秒内跨架构运行 RISC-V Ubuntu 22.04 + xfce4 桌面系统][038] @falcon

- Pull Request Howto
  - [直播回放：提交 PR 须知 —— 中英文排版等][044] @falcon
  - [直播回放：RISC-V Linux 技术调研暑期实习启动会暨入职培训][040] @falcon
  - [直播回放：RISC-V Linux 技术调研暑期实习宣讲会][058] @falcon

- Linux Upstream Howto
  - [直播回放：开放实习与兼职岗位并启动 Linux Upstream 计划][053] @falcon

- RISC-V Linux Activity Report
  - [直播回放：半年度简报][099] @tinylab

## Experiments

- RISC-V Assembly
  - [视频演示：快速上手 RISC-V 汇编语言实验][046] @tinylab

- RISC-V OS Basics
  - [视频演示：无门槛开展 RVOS 课程实验][047] @tinylab

- RISC-V Linux Kernel Development
  - [视频演示：极速开展 RISC-V Linux v5.17 内核实验][045] @tinylab

- RISC-V Application Development
  - [RISC-V Lab][068] @tinylab

## Hardware

- RISC-V CPU Design
  - [大学生 RISC-V 处理器设计实践][041] @谭源，@魏人
  - [从零开始设计 RISC-V CPU][075] @Fajie.WangNiXi

- RISC-V Board Design
  - [开源之夏 2022 - RISC-V 硬件产品开发实录][002] @taotieren, @LucasXu
  - [直播回放：I2C & SPI 用法][060] @taotieren
  - [直播回放：原理图简介 & SDIO 用法][061] @taotieren
  - [直播回放：MIPI+USB+GPIO 用法][101] @taotieren

- RISC-V Board Usage
  - D1 Board
    - [D1-H 开发板入门][016] @iOSDevLog
    - [为哪吒 D1 开发板安装 ArchLinux RISC-V rootfs][015] @tieren
  - Longan Nano
    - [在 Linux 下制作 rv-link 调试器][039] @tieren

## Specs

- ISA
  - [RISC-V ISA 简介][019], [幻灯][004], [直播回放][043] @iOSDevLog
  - [RISC-V 特权指令][033] @pingbo

- psABI
- SBI

- Extensions
  - [如何因应不断增加的硬件扩展？][078] @郑鈜壬 From PLCT

## Simulation, Emulation & Virtulization

- rv8
- Spike

- QEMU
  - [QEMU 模拟目标平台的各种启动方式分析][074] @Bin Meng, @yjmstr

- [JuiceVm][008]

- [KVM][013] @walimis
  - [直播回放：GPU Virtulization][059] @walimis
  - [RISC-V 虚拟化技术调研与分析][072] @walimis, @XiakaiPan

- Other
  - [Writing a simple RISC-V emulator in plain C (Base integer, multiplication and csr instructions)][001] @yjmstr
  - [renode](https://github.com/renode/renode)


## Basic support

- Build Infrastructure
- User-facing API
- Generic library routines and assembly
  - FPU
  - non FPU
  - [non M-extension][012]
  - futex

## Bootloader

- BBL (Berkeley Boot Loader)

- OpenSBI
  - [OpenSBI 快速上手][031] @falcon

- RustSBI
  - RISC-V 引导程序环境：应用&规范，[幻灯][005]，[直播回放][049] @luojia65

- U-Boot

- UEFI
  - [RISC-V UEFI 架构支持详解，第 1 部分 - OpenSBI/U-Boot/UEFI 简介][037] @Jacob

## Boot, Init & Shutdown

- Booting
  - RISC-V Linux 内核启动流程分析，[直播回放][051] @通天塔
  - [RISC-V Linux 启动流程分析][029] @通天塔

- DTS @iOSDevLog
  - [直播回放：RISC-V Linux 设备树——语法与格式详解][062] @iOSDevLog
  - [直播回放：RISC-V Linux 设备树——源码分析][100] @iOSDevLog

- UEFI support
- Compressed images
- Early Boot
- earlycon @iOSDevLog
- Soc Init
- setup_arch
- poweroff

- printk
  - 内核调试基础设施之 printk 初探，[直播回放][054] @iOSDevLog

## Memory Management

- Paging and MMU
  - [RISC-V MMU 概述][030], [直播回放][042] @pwl999

- memblock
  - [memblock 内存分配器原理和代码分析][091] @tjytimi

- setup_vm
- fixed mapping @tjytimi
- ioremap @tjytimi
- buddy @tjytimi
- per_cpu_cache @tjytimi
- vmalloc @tjytimi
- process's address space @tjytimi
- mmap @tjytimi
- page_fault @tjytimi
- Barriers

- Sparsemem
  - [RISC-V Linux SPARSEMEM 介绍与分析][092] @Jack Y.

- CMA
- DMA
- Hugetlbfs
- memtest
- NUMA

- noMMU
  - [RISC-V NoMMU Linux 相关支持调研与分析][069]

- msharefs

## Device, IRQs, Traps and the SBI

- SBI
- Device
- IRQs
  - [Linux IRQ 子系统分析][073] @通天塔
  - [RISC-V 中断子系统分析——硬件及其初始化][088] @通天塔
  - [RISC-V 中断子系统分析——PLIC 中断处理][089] @通天塔
  - [RISC-V 中断子系统分析——CPU 中断处理][090] @通天塔

- Traps
  - [RISC-V Trap 机制分析][076] @envestcc

- swiotlb

## Timers

- Timers
  - [RISC-V timer 在 Linux 中的实现][036], [直播回放][057] @yuliao

- swtimer
- udelay

## Synchronization

- Atomics
  - [RISC-V 原子指令与用法介绍][017], [直播回放][048] @pingbo
  - RISC-V v.s. Aarch64 atomic

- [Generic Ticket Spinlock][010]
- [Mutex][084]
- [Restartable Sequence (RSEQ)][011]
- [Qspinlock][014]
- [A Memory Model for RISC-V][086]

## Multi-core

- SMP support
  - [RISC-V Linux SMP 技术调研与分析][082]

- IPI
- Hotplug
- CPUIdle
- Membarrier sync core @通天塔

## Scheduling

- Multi-tasking
  - [RISC-V Linux __schedule() 分析][028] @Jack Y.

- Context switch
  - RISC-V Linux 内核调度详解，[直播回放][055] @Jack Y.
  - [RISC-V Linux 上下文切换分析][018] @Jack Y.

- Task implementation
  - [RISC-V Linux 进程创建与执行流程代码分析][035] @tjytimi
  - [RISC-V 架构下内核线程返回函数探究][024] @tjytimi

- EAS

## User-space Support

- Syscall
  - RISC-V Linux 系统调用详解，[直播回放][056] @envestcc
  - [RISC-V Syscall 系列 1：什么是 Syscall ?][093] @envestcc
  - [RISC-V Syscall 系列 2：Syscall 过程分析][094] @envestcc

- ELF support
- module implementation
- Multi-threads
- binfmt_flat

## Power Management

- Suspend To RAM
- Suspend To Disk
- Wakeup Source Support
- Clock Gating
- Regulator

## Thermal Management

## Tracing

- Stacktrace
  - [RISC-V Linux Stacktrace 详解][034], [直播回放][052] @zhao305149619

- Tracepoint

- Ftrace
  - [RISC-V ftrace 相关技术调研与分析][070] @sugarfillet

- [Fprobe][063]
- Kprobes
- Uprobes
- [User Events][064]

- eBPF
  - [eBPF 领域技术调研与分析][071]

## Debugging

- Ptrace
- Crash
- KGDB
- Kexec & Kdump
- Ltrace

## Detection

- KCSAN
- KASAN

- KMSAN
  - [Try to Support KMSAN for RISC-V][081] @tjytimi

- KFENCE
  - [Linux Kfence 详解][026] @pwl999

- Kmemleak

## Profiling

- Perf
- PMU (Performance Monitor Unit)
- kcov

## Tuning

- vdso
  - [RISC-V Syscall 系列 3：什么是 vDSO？][095] @envestcc
  - [RISC-V Syscall 系列 4：vDSO 实现原理分析][096] @envestcc

- Jump Label
  - [RISC-V Linux jump_label 详解，第 1 部分：技术背景][020] @falcon
  - [RISC-V Linux jump_label 详解，第 2 部分：指令编码][021], [直播回放][050] @falcon
  - [RISC-V jump_label 详解，第 3 部分：核心实现][022] @falcon
  - [RISC-V jump_label 详解，第 4 部分：运行时代码改写][023] @falcon
  - [RISC-V jump_label 详解，第 5 部分：优化案例][087] @falcon

- Static call

## Security

- Stackprotector
- OP-TEE for RISC-V
- PengLai for RISC-V
- KataOS for RISC-V

## Fast boot

- XIP

## Real Time

- Preemption
  - [RISC-V RealTime 分析、优化与 CPU 设计建议][077] @falcon

## Benchmarking

- Microbench
  - [RISC-V 处理器指令级性能评测尝试][032]
  - [开源之夏 2022 - 处理器指令级 Microbench 开发实录][003]

- [martinus/nanobench][009]
- [andreas-abel/nanoBench][007]

## Testing

- Autotest frameworks

## AI

- RISC-V + AI
  - [D1 开发板 AI 应用开发实践][079] @zjw

## Embedded Linux Systems

- Busybox
- Openembedded
- Buildroot
- Yocto

## Linux Distributions

- [Ubuntu][098]
- [Fedora][085]
- [openEuler][067]
- [Debian][097]
- [ArchLinux][065]
- [AIpine][066]
- [Deepin][083]

## More

- Missing Features & Tools
  - [Missing Features/Tools for RISC-V][080] @通天塔，@iOSDevLog

## Latest development

- [News][006]，已新建独立的 News 发布页

[001]: https://fmash16.github.io/content/posts/riscv-emulator-in-c.html
[002]: https://gitee.com/tinylab/cloud-lab/issues/I56CKU
[003]: https://gitee.com/tinylab/cloud-lab/issues/I57CM0
[004]: https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-isa.pptx
[005]: https://gitee.com/tinylab/riscv-linux/raw/master/ppt/riscv-rustsbi.pdf
[006]: https://gitee.com/tinylab/riscv-linux/tree/master/news
[007]: https://github.com/andreas-abel/nanoBench
[008]: https://github.com/juiceRv/JuiceVm
[009]: https://github.com/martinus/nanobench
[010]: https://lore.kernel.org/linux-riscv/11364105.8ZH9dyz9j6@diego/T/#t
[011]: https://lore.kernel.org/linux-riscv/20220308083253.12285-1-vincent.chen@sifive.com/T/#t
[012]: https://lore.kernel.org/linux-riscv/20220402101801.GA9428@amd/T/#t
[013]: https://lore.kernel.org/linux-riscv/CAAhSdy3JwLyOoNOubAS2VusNdj6O-CJJNSs+mM8+NvKErtAdmw@mail.gmail.com/T/#u
[014]: https://lore.kernel.org/linux-riscv/CAJF2gTT9YHgTzPaBN4ekYS7UcBOj_9k9xEcrsoXgW6PCZc8x3Q@mail.gmail.com/T/#t
[015]: https://tinylab.org/nezha-d1-archlinux/
[016]: https://tinylab.org/nezha-d1-quickstart/
[017]: https://tinylab.org/riscv-atomics/
[018]: https://tinylab.org/riscv-context-switch/
[019]: https://tinylab.org/riscv-isa-intro
[020]: https://tinylab.org/riscv-jump-label-part1/
[021]: https://tinylab.org/riscv-jump-label-part2/
[022]: https://tinylab.org/riscv-jump-label-part3/
[023]: https://tinylab.org/riscv-jump-label-part4/
[024]: https://tinylab.org/riscv-kthread-ret/
[025]: https://tinylab.org/riscv-linux-distros/
[026]: https://tinylab.org/riscv-linux-kfence/
[027]: https://tinylab.org/riscv-linux-quickstart/
[028]: https://tinylab.org/riscv-linux-schedule/
[029]: https://tinylab.org/riscv-linux-startup/
[030]: https://tinylab.org/riscv-mmu/
[031]: https://tinylab.org/riscv-opensbi-quickstart/
[032]: https://tinylab.org/riscv-perf-benchmark/
[033]: https://tinylab.org/riscv-privileged/
[034]: https://tinylab.org/riscv-stacktrace/
[035]: https://tinylab.org/riscv-task-implementation/
[036]: https://tinylab.org/riscv-timer/
[037]: https://tinylab.org/riscv-uefi-part1/
[038]: https://tinylab.org/run-riscv-ubuntu-over-x86/
[039]: https://tinylab.org/rv-link-debugger/
[040]: https://www.bilibili.com/video/BV1GV4y1H77p
[041]: https://www.bilibili.com/video/BV1L54y1o7Fs
[042]: https://www.cctalk.com/v/16477623213273
[043]: https://www.cctalk.com/v/16484079493695
[044]: https://www.cctalk.com/v/16484083661860
[045]: https://www.cctalk.com/v/16484095223067
[046]: https://www.cctalk.com/v/16484116944868
[047]: https://www.cctalk.com/v/16484116944869
[048]: https://www.cctalk.com/v/16489499392383
[049]: https://www.cctalk.com/v/16495466863205
[050]: https://www.cctalk.com/v/16501801841966
[051]: https://www.cctalk.com/v/16520639214118
[052]: https://www.cctalk.com/v/16525972863665
[053]: https://www.cctalk.com/v/16551875274459
[054]: https://www.cctalk.com/v/16551876033662
[055]: https://www.cctalk.com/v/16558003571816
[056]: https://www.cctalk.com/v/16562391483157
[057]: https://www.cctalk.net/v/16566997322462
[058]: https://www.cctalk.net/v/16574310111172
[059]: https://www.cctalk.net/v/16574325601701
[060]: https://www.cctalk.net/v/16586599461554
[061]: https://www.cctalk.net/v/16592486764588
[062]: https://www.cctalk.net/v/16598634312907
[063]: https://www.kernel.org/doc/html/latest/trace/fprobe.html
[064]: https://www.kernel.org/doc/html/latest/trace/user_events.html
[065]: https://archriscv.felixc.at/
[066]: https://drewdevault.com/2018/12/20/Porting-Alpine-Linux-to-RISC-V.html
[067]: https://gitee.com/openeuler/RISC-V
[068]: https://gitee.com/tinylab/riscv-lab
[069]: https://gitee.com/tinylab/riscv-linux/issues/I57Q94
[070]: https://gitee.com/tinylab/riscv-linux/issues/I58N1O
[071]: https://gitee.com/tinylab/riscv-linux/issues/I5CXOQ
[072]: https://gitee.com/tinylab/riscv-linux/issues/I5E4VB
[073]: https://gitee.com/tinylab/riscv-linux/issues/I5E5EP
[074]: https://gitee.com/tinylab/riscv-linux/issues/I5EE48
[075]: https://gitee.com/tinylab/riscv-linux/issues/I5EIOA
[076]: https://gitee.com/tinylab/riscv-linux/issues/I5ENEF
[077]: https://gitee.com/tinylab/riscv-linux/issues/I5ENMI
[078]: https://gitee.com/tinylab/riscv-linux/issues/I5FNUO
[079]: https://gitee.com/tinylab/riscv-linux/issues/I5GYUU
[080]: https://gitee.com/tinylab/riscv-linux/issues/I5L9H0
[081]: https://gitee.com/tinylab/riscv-linux/issues/I5LIEI
[082]: https://gitee.com/tinylab/riscv-linux/issues/I5MU96
[083]: https://github.com/linuxdeepin/deepin-riscv
[084]: https://lwn.net/Articles/164802
[085]: https://lwn.net/Articles/749443/
[086]: https://riscv.org/wp-content/uploads/2018/05/14.25-15.00-RISCVMemoryModelTutorial.pdf
[087]: https://tinylab.org/jump-label-part5/
[088]: https://tinylab.org/riscv-irq-analysis/
[089]: https://tinylab.org/riscv-irq-analysis-part2-interrupt-handling-plic/
[090]: https://tinylab.org/riscv-irq-analysis-part3-interrupt-handling-cpu/
[091]: https://tinylab.org/riscv-memblock/
[092]: https://tinylab.org/riscv-sparsemem/
[093]: https://tinylab.org/riscv-syscall-part1-usage/
[094]: https://tinylab.org/riscv-syscall-part2-procedure/
[095]: https://tinylab.org/riscv-syscall-part3-vdso-overview/
[096]: https://tinylab.org/riscv-syscall-part4-vdso-implementation/
[097]: https://wiki.debian.org/RISC-V
[098]: https://wiki.ubuntu.com/RISC-V
[099]: https://www.bilibili.com/video/BV13D4y117MZ
[100]: https://www.bilibili.com/video/BV13e4y1r7n9?spm_id_from=333.999.0.0
[101]: https://www.bilibili.com/video/BV18Y4y1F7be
