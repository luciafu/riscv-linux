# RISC-V Linux 内核及周边技术动态

## 20221204：第 23 期

### 内核动态

#### RISC-V 架构支持

* [v2: riscv: support for hardware breakpoints/watchpoints](http://lore.kernel.org/linux-riscv/20221203215535.208948-1-geomatsi@gmail.com/)

   RISC-V Debug specification includes Sdtrig ISA extension. This extension
   describes Trigger Module. Triggers can cause a breakpoint exception,
   entry into Debug Mode, or a trace action without having to execute a
   special instruction. For native debugging triggers can be used to
   implement hardware breakpoints and watchpoints.

* [v15: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20221203064629.1601299-1-apatel@ventanamicro.com/)

   These patches were originally part of the "Linux RISC-V ACLINT Support"
   series but this now a separate series so that it can be merged independently
   of the "Linux RISC-V ACLINT Support" series.

* [v1: mm: Stop alaising VM_FAULT_HINDEX_MASK in arch code](http://lore.kernel.org/linux-riscv/20221203030356.3917-1-palmer@rivosinc.com/)

   The RISC-V patch is on top of the linked patch above, which isn't in any
   proper tree yet.  Everything else should apply to 6.1-rc1, but I'm in no
   particular rush to get that cleanup in so rush on my end for 6.2.  That
   said I figured it would be easier to send along this now, just in case
   someone who knows the MM code better thinks it can manifest as a bug.

* [v3: Generic IPI sending tracepoint](http://lore.kernel.org/linux-riscv/20221202155817.2102944-1-vschneid@redhat.com/)

   Detecting IPI *reception* is relatively easy, e.g. using
   trace_irq_handler_{entry,exit} or even just function-trace
   flush_smp_call_function_queue() for SMP calls.  

   Figuring out their *origin*, is trickier as there is no generic tracepoint tied
   to e.g. smp_call_function():
   o AFAIA x86 has no tracepoint tied to sending IPIs, only receiving them
     (cf. trace_call_function{_single}_entry()).

* [v1: Add watchdog driver for StarFive RISC-V SoCs](http://lore.kernel.org/linux-riscv/20221202093943.149674-1-xingyu.wu@starfivetech.com/)

   This patch serises are to add watchdog driver for the StarFive RISC-V SoCs
   such as JH7110. The first patch adds docunmentation to describe device
   tree bindings. The subsequent patch adds watchdog driver and support
   JH7110 SoC. The last patch adds device node about watchdog to JH7110 dts.

* [v1: Implement ioremap_prot support (v2)](http://lore.kernel.org/linux-riscv/20221202083356.169176-1-jiangjianwen@uniontech.com/)

    Feature ioremap_prot only needs an implementation of pte_pgprot on riscv.
    That macro is similar to the same one on platform loongarch, mips and sh.

* [v3: riscv : select FTRACE_MCOUNT_USE_PATCHABLE_FUNCTION_ENTRY](http://lore.kernel.org/linux-riscv/20221201151336.8044-1-suagrfillet@gmail.com/)

   In RISC-V, -fpatchable-function-entry option is used to support
   dynamic ftrace in this commit afc76b8b8011 ("riscv: Using
   PATCHABLE_FUNCTION_ENTRY instead of MCOUNT"). So recordmcount
   don't have to be called to create the __mcount_loc section before the vmlinux linking.

* [v1: Documentation: riscv: note that counter access is part of the uABI](http://lore.kernel.org/linux-riscv/20221201135110.3855965-1-conor.dooley@microchip.com/)

   Commit 5a5294fbe020 ("RISC-V: Re-enable counter access from userspace")
   fixed userspace access to CYCLE, TIME & INSTRET counters and left a nice
   comment in-place about why they must not be restricted. Since we now
   have a uABI doc in RISC-V land, add a section documenting it.

* [v14: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20221201130135.1115380-1-apatel@ventanamicro.com/)

   These patches were originally part of the "Linux RISC-V ACLINT Support"
   series but this now a separate series so that it can be merged independently of the "Linux RISC-V ACLINT Support" series.
   
   These patches can also be found in riscv_ipi_imp_v14 branch at:
   https://github.com/avpatel/linux.git

* [v5: Improve CLOCK_EVT_FEAT_C3STOP feature setting](http://lore.kernel.org/linux-riscv/20221201123954.1111603-1-apatel@ventanamicro.com/)

   This series improves the RISC-V timer driver to set CLOCK_EVT_FEAT_C3STOP
   feature based on RISC-V platform capabilities.

   These patches can also be found in riscv_timer_dt_imp_v5 branch at:
   https://github.com/avpatel/linux.git

* [v1: riscv: Apply a static assert to riscv_isa_ext_id](http://lore.kernel.org/linux-riscv/20221201113750.18021-1-ajones@ventanamicro.com/)

   Add a static assert to ensure a RISCV_ISA_EXT_* enum is never
   created with a value >= RISCV_ISA_EXT_MAX. We can do this by
   putting RISCV_ISA_EXT_ID_MAX to more work. Before it was
   redundant with RISCV_ISA_EXT_MAX and hence only used to
   document the limit. Now it grows with the enum and is used to check the limit.

* [v1: Add Ethernet driver for StarFive JH7110 SoC](http://lore.kernel.org/linux-riscv/20221201090242.2381-1-yanhong.wang@starfivetech.com/)

   This series adds ethernet support for the StarFive JH7110 RISC-V SoC.
   The series includes PHY and MAC drivers. The PHY model is
   YT8531 (from Motorcomm Inc), and the MAC version is dwmac-5.20 (from Synopsys DesignWare). 
   
* [v1: Putting some basic order on isa extension lists](http://lore.kernel.org/linux-riscv/20221130234125.2722364-1-conor@kernel.org/)

   With that caveat out of the way - all I did here was try to make things consistent so that it'd be easier to point patch submitters at a "do this order please".

   I also added a sprinkling of comments around which things should be sorted in which way.
   
* [v2: KVM: Rework kvm_init() and hardware enabling](http://lore.kernel.org/linux-riscv/20221130230934.1014142-1-seanjc@google.com/)

   The main theme of this series is to kill off kvm_arch_init(),
   kvm_arch_hardware_(un)setup(), and kvm_arch_check_processor_compat(), which
   all originated in x86 code from way back when, and needlessly complicate both common KVM code and architecture code.
   
* [v3: Zbb string optimizations and call support in alternatives](http://lore.kernel.org/linux-riscv/20221130225614.1594256-1-heiko@sntech.de/)

   The Zbb extension can be used to make string functions run a lot faster.

   In my half-scientific test-case of running the functions in question
   on a 95 character string in a loop of 10000 iterations, the Zbb
   variants shave off around 2/3 of the original runtime.

* [v2: pwm: Allow .get_state to fail](http://lore.kernel.org/linux-riscv/20221130152148.2769768-1-u.kleine-koenig@pengutronix.de/)

   I forgot about this series and was remembered when I talked to Conor
   Dooley about how .get_state() should behave in an error case.

* [v1: crypto: starfive: Add driver for cryptographic engine](http://lore.kernel.org/linux-riscv/20221130055214.2416888-1-jiajie.ho@starfivetech.com/)

   This patch series adds kernel driver support for Starfive crypto engine.
   The engine supports hardware acceleration for HMAC/hash functions,
   AES block cipher operations and RSA. The first patch adds basic driver
   for device probe and DMA init. The subsequent patches adds supported
   crypto primitives to the driver which include hash functions, AES and RSA. 
   
* [V9: [-next]: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20221130034059.826599-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support. Some optimization for entry.S with new .macro and
   merge ret_from_kernel_thread into ret_from_fork.

* [v1: gpio: mpfs: Make the irqchip immutable](http://lore.kernel.org/linux-riscv/4ee1a396acc34871dbae73a5b032915f745795ec.1669738949.git.geert+renesas@glider.be/)

   Commit 6c846d026d49 ("gpio: Don't fiddle with irqchips marked as
   immutable") added a warning to indicate if the gpiolib is altering the internals of irqchips. 
   
* [v1: Putting some basic order on isa extension stuff](http://lore.kernel.org/linux-riscv/20221129144742.2935581-1-conor.dooley@microchip.com/)

   With those caveats out of the way - all I did here was try to make
   things consistent so that it'd be easier to point patch submitters at a
   "do this order please".

* [v3: RISC-V: Ensure Zicbom has a valid block size](http://lore.kernel.org/linux-riscv/20221129143447.49714-1-ajones@ventanamicro.com/)

   When a DT puts zicbom in the isa string, but does not provide a block size, ALT_CMO_OP() will attempt to do cache operations on address zero since the start address will be ANDed with zero. We can't simply BUG() in riscv_init_cbom_blocksize() when we fail to find a block size because the failure will happen before logging works, leaving users to scratch their heads as to why the boot hung. 

* [v13: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20221129142449.886518-1-apatel@ventanamicro.com/)

   These patches were originally part of the "Linux RISC-V ACLINT Support"
   series but this now a separate series so that it can be merged independently of the "Linux RISC-V ACLINT Support" series.
  
#### 进程调度

* [v1: Revert "drm/sched: Use parent fence instead of finished"](http://lore.kernel.org/lkml/20221202172330.39219-1-Arvind.Yadav@amd.com/)

   This reverts commit e4dc45b1848bc6bcac31eb1b4ccdd7f6718b3c86.

    This is causing instability on Linus' desktop, and Observed System hung  when running MesaGL benchmark or VK CTS runs.

* [v4: sched/core: Minor optimize ttwu_runnable()](http://lore.kernel.org/lkml/20221202080644.76999-1-zhouchengming@bytedance.com/)

   ttwu_runnable() is used as a fast wakeup path when the wakee task is running on CPU or runnable on RQ, in both cases we can just set its state to TASK_RUNNING to prevent a sleep.

   If the wakee task is on_cpu running, we don't need to update_rq_clock() or check_preempt_curr().

* [v3: sched/fair: Choose the CPU where short task is running during wake up](http://lore.kernel.org/lkml/cover.1669862147.git.yu.c.chen@intel.com/)

   The main purpose of this change is to avoid too many cross CPU
   wake up when it is unnecessary. The frequent cross CPU wake up
   brings significant damage to some workloads, especially on high core count systems.

* [[PATCHSET RFC] sched: Implement BPF extensible scheduler class](http://lore.kernel.org/lkml/20221130082313.3241517-1-tj@kernel.org/)

   This patch set proposes a new scheduler class called &#8216;ext_sched_class&#8217;, or
   sched_ext, which allows scheduling policies to be implemented as BPF programs.

   More details will be provided on the overall architecture of sched_ext
   throughout the various patches in this set, as well as in the &#8220;How&#8221; section below.
   
* [v2: sched: Introduce IPC classes for load balance](http://lore.kernel.org/lkml/20221128132100.30253-1-ricardo.neri-calderon@linux.intel.com/)

   This is the v2 of the patchset. Since it did not receive strong objections
   on the design, I took the liberty of promoting the series from RFC to PATCH :)

   The problem statement and design do not change in this version. Thus, I did
   not repeat the cover letter. 
   
* [v2: Documentation: sched: Add a new sched-util-clamp.rst](http://lore.kernel.org/lkml/20221127142657.1649347-1-qyousef@layalina.io/)

   The new util clamp feature needs a document explaining what it is and
   how to use it. The new document hopefully covers everything one needs to know about uclamp.

#### 内存管理

* [v1: mm/vmalloc.c: allow vread() to read out vm_map_ram areas](http://lore.kernel.org/linux-mm/20221204013046.154960-1-bhe@redhat.com/)

   Stephen reported vread() will skip vm_map_ram areas when reading out
   /proc/kcore with drgn utility. Please see below link to get more about it:
     /proc/kcore reads 0's for vmap_block
     https://lore.kernel.org/all/87ilk6gos2.fsf@oracle.com/T/#u

* [v4: Shadow stacks for userspace](http://lore.kernel.org/linux-mm/20221203003606.6838-1-rick.p.edgecombe@intel.com/)

   This series implements Shadow Stacks for userspace using x86's Control-flow 
   Enforcement Technology (CET). CET consists of two related security features: 
   Shadow Stacks and Indirect Branch Tracking. This series implements just the 
   Shadow Stack part of this feature, and just for userspace.

* [v3: mm: Add nodes= arg to memory.reclaim](http://lore.kernel.org/linux-mm/20221202223533.1785418-1-almasrymina@google.com/)

   The nodes= arg instructs the kernel to only scan the given nodes for
   proactive reclaim. For example use cases, consider a 2 tier memory system:

   nodes 0,1 -> top tier
   nodes 2,3 -> second tier

   $ echo "1m nodes=0" > memory.reclaim

   This instructs the kernel to attempt to reclaim 1m memory from node 0.
   Since node 0 is a top tier node, demotion will be attempted first. This
   is useful to direct proactive reclaim to specific nodes that are under pressure.

* [v4: mm/khugepaged: add tracepoint to collapse_file()](http://lore.kernel.org/linux-mm/20221202201807.182829-1-gautammenghani201@gmail.com/)

   Currently, is_shmem is not being captured. Capturing is_shmem is useful 
   as it can indicate if tmpfs is being used as a backing store instead of 
   persistent storage. Add the tracepoint in collapse_file() named 
   "mm_khugepaged_collapse_file" for capturing is_shmem.

* [v3: proc: improve root readdir latency with many threads](http://lore.kernel.org/linux-mm/20221202171620.509140-1-bfoster@redhat.com/)

   Here's v3 of the /proc readdir optimization patches. See v1 for the full
   introductary cover letter.

   Most of the feedback received to this point has been around switching
   the pid code over to use the xarray api instead of the idr. Matt Wilcox
   posted most of the code to do that.
   
* [v1: mm: do not BUG_ON missing brk mapping, because userspace can unmap it](http://lore.kernel.org/linux-mm/20221202162724.2009-1-Jason@zx2c4.com/)

   The following program will trigger the BUG_ON that this patch removes,
  
   Instead, just error out if the original mapping has been removed.

   Fixes: 2e7ce7d354f2 ("mm/mmap: change do_brk_flags() to expand existing VMA and add do_brk_munmap()")

* [v1: mm/userfaultfd: enable writenotify while userfaultfd-wp is enabled for a VMA](http://lore.kernel.org/linux-mm/20221202122748.113774-1-david@redhat.com/)

   Currently, we don't enable writenotify when enabling userfaultfd-wp on
   a shared writable mapping (for now we only support SHMEM). The consequence
   is that vma->vm_page_prot will still include write permissions, to be set
   as default for all PTEs that get remapped (e.g., mprotect(), NUMA hinting,
   page migration, ...).

* [v3: iov_iter: Add extraction helpers](http://lore.kernel.org/linux-mm/166997419665.9475.15014699817597102032.stgit@warthog.procyon.org.uk/)

   Here are four patches to provide support for extracting pages from an
   iov_iter, where such a thing makes sense, if you could take a look?

   [!] NOTE that I've switched the functions to be exported GPL-only at Christoph's request[1].  

* [v1: crypto: Driver conversions for DMA alignment](http://lore.kernel.org/linux-mm/Y4nDL50nToBbi4DS@gondor.apana.org.au/)

   These are the rest of the driver conversions in order for arm64
   to safely lower the kmalloc alignment below that required for DMA.
   
* [v10: KVM: mm: fd-based approach for supporting KVM](http://lore.kernel.org/linux-mm/20221202061347.1070246-1-chao.p.peng@linux.intel.com/)

   This patch series implements KVM guest private memory for confidential
   computing scenarios like Intel TDX[1]. If a TDX host accesses
   TDX-protected guest memory, machine check can happen which can further
   crash the running host system, this is terrible for multi-tenant
   configurations. 
   
* [v1: mm/mmap: Properly unaccount memory on mas_preallocate() failure](http://lore.kernel.org/linux-mm/20221202045339.2999017-1-apopple@nvidia.com/)

   security_vm_enough_memory_mm() accounts memory via a call to
   vm_acct_memory(). Therefore any subsequent failures should unaccount
   for this memory prior to returning the error.

* [v9: mm: add zblock - new allocator for use via zpool API](http://lore.kernel.org/linux-mm/20221202040110.14291-1-a.badmaev@clicknet.pro/)

    The range from 0 to PAGE_SIZE is divided into the number of intervals
   corresponding to the number of lists and each list only operates objects
   of size from its interval. Thus the block lists are isolated from each
   other, which makes it possible to simultaneously perform actions with
   several objects from different lists.
     
* [v1: mm: disable top-tier fallback to reclaim on proactive reclaim](http://lore.kernel.org/linux-mm/20221201233317.1394958-1-almasrymina@google.com/)

   Reclaiming directly from top tier nodes breaks the aging pipeline of
   memory tiers.  If we have a RAM -> CXL -> storage hierarchy, we
   should demote from RAM to CXL and from CXL to storage. If we reclaim
   a page from RAM, it means we 'demote' it directly from RAM to storage,
   bypassing potentially a huge amount of pages colder than it in CXL.

* [v1: Improving userfaultfd scalability for live migration](http://lore.kernel.org/linux-mm/CADrL8HVDB3u2EOhXHCrAgJNLwHkj2Lka1B_kkNb0dNwiWiAN_Q@mail.gmail.com/)

   One of the main challenges of using userfaultfd is its performance. I
   have some ideas for how userfaultfd could be made scalable for
   post-copy live migration. I'm not sending a series for now; I want to
   make sure the general approach here is something upstream would be interested in.

* [v8: Memory poison recovery in khugepaged collapsing](http://lore.kernel.org/linux-mm/20221201005931.3877608-1-jiaqiyan@google.com/)

   Memory DIMMs are subject to multi-bit flips, i.e. memory errors.
   As memory size and density increase, the chances of and number of
   memory errors increase. The increasing size and density of server
   RAM in the data center and cloud have shown increased uncorrectable
   memory errors. There are already mechanisms in the kernel to recover
   from uncorrectable memory errors. This series of patches provides
   the recovery mechanism for the particular kernel agent khugepaged
   when it collapses memory pages.

* [v1: Split page pools from struct page](http://lore.kernel.org/linux-mm/20221130220803.3657490-1-willy@infradead.org/)

   The MM subsystem is trying to reduce struct page to a single pointer.
   The first step towards that is splitting struct page by its individual
   users, as has already been done with folio and slab.  This attempt chooses
   'netmem' as a name, but I am not even slightly committed to that name,
   and will happily use another.

* [v2: implement DAMOS filtering for anon pages and](http://lore.kernel.org/linux-mm/20221130200937.118005-1-sj@kernel.org/)

   DAMOS let users do system operations in a data access pattern oriented
   way.  The data access pattern, which is extracted by DAMON, is somewhat
   accurate more than what user space could know in many cases.  However,
   in some situation, users could know something more than the kernel about
   the pattern or some special requirements for some types of memory or processes.

* [[PATCH v2 mm] kasan: fail non-kasan KUnit tests on KASAN reports](http://lore.kernel.org/linux-mm/7be29a8ea967cee6b7e48d3d5a242d1d0bd96851.1669820505.git.andreyknvl@google.com/)

   After the recent changes done to KUnit-enabled KASAN tests, non-KASAN KUnit
   tests stopped being failed when KASAN report is detected.

   Recover that property by failing the currently running non-KASAN KUnit test
   when KASAN detects and prints a report for a bad memory access.

* [v1: mm/huge_memory: add TRANSPARENT_HUGEPAGE_NEVER for THP](http://lore.kernel.org/linux-mm/202211301651462590168@zte.com.cn/)

   Sometimes we may need the /sys/kernel/mm/transparent_hugepage/enabled to
   default as [never] at the first time.

* [v1: mm: memcontrol: protect the memory in cgroup from being oom killed](http://lore.kernel.org/linux-mm/20221130070158.44221-1-chengkaitao@didiglobal.com/)

   We created a new interface <memory.oom.protect> for memory, If there is
   the OOM killer under parent memory cgroup, and the memory usage of a
   child cgroup is within its effective oom.protect boundary, the cgroup's
   tasks won't be OOM killed unless there is no unprotected tasks in other
   children cgroups. It draws on the logic of <memory.min/low> in the
   inheritance relationship.

* [v5: [mm-unstable]: convert core hugetlb functions to folios](http://lore.kernel.org/linux-mm/20221129225039.82257-1-sidhartha.kumar@oracle.com/)

   Now that many hugetlb helper functions that deal with hugetlb specific
   flags[1] and hugetlb cgroups[2] are converted to folios, higher level
   allocation, prep, and freeing functions within hugetlb can also be
   converted to operate in folios.

   Patch 1 of this series implements the wrapper functions around setting
   the compound destructor and compound order for a folio. Besides the user
   added in patch 1, patch 2 and patch 9 also use these helper functions.

* [v1: mm/hugetlb: Make huge_pte_offset() thread-safe for pmd unshare](http://lore.kernel.org/linux-mm/20221129193526.3588187-1-peterx@redhat.com/)

   This can be seen as a follow-up series to Mike's recent hugetlb vma lock
   series for pmd unsharing, but majorly covering safe use of huge_pte_offset.

   Comparing to previous rfcv2, the major change is I dropped the new pgtable
   lock but only use vma lock for locking. 

#### 文件系统

* [v2: hfsplus: Add module parameter to enable force writes](http://lore.kernel.org/linux-fsdevel/79C58B25-8ECF-432B-AE90-2194921DB797@live.com/)

   This patch enables users to permanently enable writes of HFS+ locked
   and/or journaled volumes using a module parameter.

   Why module parameter?
   Reason being, its not convenient to manually mount the volume with force
   everytime. There are use cases which are fine with force enabling writes on journaled volumes. I've seen many on various online forums and I am one of them as well.

* [v1: io_uring: don't hold uring_lock when calling io_run_task_work*](http://lore.kernel.org/linux-fsdevel/20221203002305.1203792-1-skhawaja@google.com/)

   commit 8bad28d8a305b0e5ae444c8c3051e8744f5a4296 upstream.

   This is caused by calling io_run_task_work_sig() to do work under
   uring_lock while the caller io_sqe_files_unregister() already held uring_lock.
 
* [v2: Turn iomap_page_ops into iomap_folio_ops](http://lore.kernel.org/linux-fsdevel/20221201180957.1268079-1-agruenba@redhat.com/)

   we're seeing a race between journaled data writes and the shrinker on
   gfs2.  What's happening is that gfs2_iomap_page_done() is called after
   the page has been unlocked, so try_to_free_buffers() can come in and
   free the buffers while gfs2_iomap_page_done() is trying to add them to the transaction.
   
* [v1: selftests: Add missing <sys/syscall.h> to mount_setattr test](http://lore.kernel.org/linux-fsdevel/20221201150218.2374366-1-daan.j.demeyer@gmail.com/)

   Including <sys/syscall.h> is required to define __NR_mount_setattr
   and __NR_open_tree which the mount_setattr test relies on.

* [v1: v1: iomap: zeroing needs to be pagecache aware](http://lore.kernel.org/linux-fsdevel/20221201005214.3836105-1-david@fromorbit.com/)

   Unwritten extents can have page cache data over the range being
   zeroed so we can't just skip them entirely. Fix this by checking for
   an existing dirty folio over the unwritten range we are zeroing
   and only performing zeroing if the folio is already dirty.

* [v3: coredump: Use vmsplice_to_pipe() for pipes in dump_emit_page()](http://lore.kernel.org/linux-fsdevel/20221130053734.2811-1-yepeilin.cs@gmail.com/)

   Currently, there is a copy for each page when dumping VMAs to pipe
   handlers using dump_emit_page(). 
   
* [v1: Shut down frozen filesystems on last unmount](http://lore.kernel.org/linux-fsdevel/20221129230736.3462830-1-agruenba@redhat.com/)

   currently, when a frozen filesystem is unmouted, it turns into a zombie
   rather than being shut down; it can only be shut down after remounting
   and thawing it.  That's silly for local filesystems, but it's worse for
   filesystems like gfs2 which freeze the filesystem on all nodes when
   fsfreeze is called on any of the nodes: there, the nodes that didn't
   initiate the freeze cannot shut down the filesystem at all.

* [v1: xfs: regression test for writeback corruption bug](http://lore.kernel.org/linux-fsdevel/Y4aAOn7CUTr9tUBN@magnolia/)

   This is a regression test for a data corruption bug that existed in XFS'
   copy on write code between 4.9 and 4.19.  The root cause is a
   concurrency bug wherein we would drop ILOCK_SHARED after querying the
   CoW fork in xfs_map_cow and retake it before querying the data fork in
   xfs_map_blocks.  See the test description for a lot more details.

* [v5: fsverity: stop using PG_error to track error status](http://lore.kernel.org/linux-fsdevel/20221129070401.156114-1-ebiggers@kernel.org/)

   As a step towards freeing the PG_error flag for other uses, change ext4
   and f2fs to stop using PG_error to track verity errors.  Instead, if a
   verity error occurs, just mark the whole bio as failed.  The coarser
   granularity isn't really a problem since it isn't any worse than what
   the block layer provides, and errors from a multi-page readahead aren't
   reported to applications unless a single-page read fails too.

* [v3: epoll: use refcount to reduce ep_mutex contention](http://lore.kernel.org/linux-fsdevel/1aedd7e87097bc4352ba658ac948c585a655785a.1669657846.git.pabeni@redhat.com/)

   The application is multi-threaded, creates a new epoll entry for
   each incoming connection, and does not delete it before the
   connection shutdown - that is, before the connection's fd close().

   Many different threads compete frequently for the epmutex lock,
   affecting the overall performance.

* [v1: fs/remap_range: avoid spurious writeback on zero length request](http://lore.kernel.org/linux-fsdevel/20221128160813.3950889-1-bfoster@redhat.com/)

   generic_remap_checks() can reduce the effective request length (i.e.,
   after the reflink extend to EOF case is handled) down to zero. If this
   occurs, __generic_remap_file_range_prep() proceeds through dio
   serialization, file mapping flush calls, and may invoke file_modified()
   before returning back to the filesystem caller, all of which immediately
   check for len == 0 and return.

* [v1: Composefs: an opportunistically sharing verified image filesystem](http://lore.kernel.org/linux-fsdevel/cover.1669631086.git.alexl@redhat.com/)

   Giuseppe Scrivano and I have recently been working on a new project we
   call composefs. This is the first time we propose this publically and 
   we would like some feedback on it.

   At its core, composefs is a way to construct and use read only images
   that are used similarly to how you would use e.g. loop-back mounted
   squashfs images. On top of this composefs has two new fundamental features. 
   
* [[PATCH 2/2 v2] fs/namespace.c: coding-style update](http://lore.kernel.org/linux-fsdevel/20221127125441.10686-1-rdunlap@infradead.org/)

   Fix coding style to use documented multi-line comment style
   and EXPORT_SYMBOL()s to immediately follow their function's closing brace line.

   Also fix a little punctuation and a few typos.

#### 网络设备

* [v1: net-next: ethtool: add PLCA RS support](http://lore.kernel.org/netdev/cover.1670121214.git.piergiorgio.beruto@gmail.com/)

   This patchset is related to the proposed "add PLCA RS support and onsemi
   NCN26000" patchset on the kernel. It adds userland support for
   getting/setting the configuration of the Physical Layer Collision
   Avoidance (PLCA) Reconciliation Sublayer (RS) defined in the IEEE 802.3 specifications, amended by IEEE802.3cg-2019.

* [v1: net-next: add PLCA RS support and onsemi NCN26000](http://lore.kernel.org/netdev/cover.1670117772.git.piergiorgio.beruto@gmail.com/)

   This patchset adds support for getting/setting the Physical Layer
   Collision Avoidace (PLCA) Reconciliation Sublayer (RS) configuration and
   status on Ethernet PHYs that supports it.

* [[PATCH net] tipc: call tipc_lxc_xmit without holding node_read_lock](http://lore.kernel.org/netdev/5bdd1f8fee9db695cfff4528a48c9b9d0523fb00.1670110641.git.lucien.xin@gmail.com/)

   When sending packets between nodes in netns, it calls tipc_lxc_xmit() for
   peer node to receive the packets where tipc_sk_mcast_rcv()/tipc_sk_rcv()
   might be called, and it's pretty much like in tipc_rcv().

   Currently the local 'node rw lock' is held during calling tipc_lxc_xmit() to protect the peer_net not being freed by another thread. However, when receiving these packets, tipc_node_add_conn() might be called where the peer 'node rw lock' is acquired. 
   
* [v1: net-next: tsnep: XDP support](http://lore.kernel.org/netdev/20221203215416.13465-1-gerhard@engleder-embedded.com/)

   Implement XDP support for tsnep driver. I tried to follow existing
   drivers like igb/igc as far as possible. Some prework was already done
   in previous patch series, so in this series only actual XDP stuff is included.

* [v1: net: xsk: Don't include <linux/rculist.h>](http://lore.kernel.org/netdev/88d6a1d88764cca328610854f890a9ca1f4b029e.1670086246.git.christophe.jaillet@wanadoo.fr/)

   There is no need to include <linux/rculist.h> here.

   Prefer the less invasive <linux/types.h> which is needed for 'hlist_head'.

* [v1: packet: Don't include <linux/rculist.h>](http://lore.kernel.org/netdev/adc33d6c7dd01e29c848b9519b6a601219ba6780.1670086158.git.christophe.jaillet@wanadoo.fr/)

   There is no need to include <linux/rculist.h> here.

   Prefer the less invasive <linux/types.h> which is needed for 'hlist_head'.

* [[PATCH net-next] net: ethernet: mtk_wed: add reset to rx_ring_setup callback](http://lore.kernel.org/netdev/26fa16f2f212bff5edfdbe8a4f41dba7a132b0be.1670072570.git.lorenzo@kernel.org/)

   Introduce reset parameter to mtk_wed_rx_ring_setup signature.
   This is a preliminary patch to add Wireless Ethernet Dispatcher reset support.

* [v3: [net-next]: net: lan966x: Enable PTP on bridge interfaces](http://lore.kernel.org/netdev/20221203104348.1749811-1-horatiu.vultur@microchip.com/)

   Before it was not allowed to run ptp on ports that are part of a bridge
   because in case of transparent clock the HW will still forward the frames
   so there would be duplicate frames.
   Now that there is VCAP support, it is possible to add entries in the VCAP
   to trap frames to the CPU and the CPU will forward these frames.

* [v1: bpf-next,v6: xfrm: interface: Add unstable helpers for XFRM metadata](http://lore.kernel.org/netdev/20221203084659.1837829-1-eyal.birger@gmail.com/)

   This patch series adds xfrm metadata helpers using the unstable kfunc
   call interface for the TC-BPF hooks.

   This allows steering traffic towards different IPsec connections based
   on logic implemented in bpf programs.

* [v2: net: vmw_vsock: vmci: Check memcpy_from_msg()](http://lore.kernel.org/netdev/20221203083312.923029-1-artem.chernyshev@red-soft.ru/)

   vmci_transport_dgram_enqueue() does not check the return value
   of memcpy_from_msg(). Return with an error if the memcpy fails.

   Found by Linux Verification Center (linuxtesting.org) with SVACE.

* [[PATCH linux-next] net: record times of netdev_budget exhausted](http://lore.kernel.org/netdev/202212031612057505056@zte.com.cn/)

   A long time ago time_squeeze was used to only record netdev_budget
   exhausted[1]. Then we added netdev_budget_usecs to enable softirq
   tuning[2]. And when polling elapsed netdev_budget_usecs, it's also
   record by time_squeeze.
   For tuning netdev_budget and netdev_budget_usecs respectively, we'd
   better distinguish netdev_budget exhausted from netdev_budget_usecs
   elapsed, so add a new recorder to record netdev_budget exhausted.

   [1] commit 1da177e4c3f4("Linux-2.6.12-rc2")
   [2] commit 7acf8a1e8a28("Replace 2 jiffies with sysctl netdev_budget_usecs to enable softirq tuning")

* [v1: bpf-next,v5: xfrm: interface: Add unstable helpers for XFRM metadata](http://lore.kernel.org/netdev/20221203073035.1798108-1-eyal.birger@gmail.com/)

   This patch series adds xfrm metadata helpers using the unstable kfunc
   call interface for the TC-BPF hooks.

   This allows steering traffic towards different IPsec connections based
   on logic implemented in bpf programs.

* [v1: NFC: nci: Bounds check struct nfc_target arrays](http://lore.kernel.org/netdev/20221202214410.never.693-kees@kernel.org/)

   While running under CONFIG_FORTIFY_SOURCE=y, syzkaller reported:

     memcpy: detected field-spanning write (size 129) of single field "target->sensf_res" at net/nfc/nci/ntf.c:260 (size 18)

   This appears to be a legitimate lack of bounds checking in
   nci_add_new_protocol(). Add the missing checks.

* [v4: dt-binding preparation for ocelot switches](http://lore.kernel.org/netdev/20221202204559.162619-1-colin.foster@in-advantage.com/)

   Ocelot switches have the abilitiy to be used internally via
   memory-mapped IO or externally via SPI or PCIe. This brings up issues
   for documentation, where the same chip might be accessed internally in a
   switchdev manner, or externally in a DSA configuration. This patch set
   is perparation to bring DSA functionality to the VSC7512, utilizing as
   much as possible with an almost identical VSC7514 chip.

* [v1: phy: mdio: Reorganize defines](http://lore.kernel.org/netdev/20221202204237.1084376-1-sean.anderson@seco.com/)

   Reorder all registers to be grouped by MMD. Groups fields in
   similarly-named registers in the same way. This is especially useful for
   registers which may have some bits in common, but interpret other bits in different ways. The comments have been tweaked to more closely follow 802.3's naming.

* [v1: net-next: ptp: Introduce .getfine callback to ptp_clock_info](http://lore.kernel.org/netdev/20221202201528.26634-1-rrameshbabu@nvidia.com/)

   The current state of the ptp driver provides the ability to query the frequency
   of the ptp clock device by caching the frequency values used in previous
   adjustments done through the ptp driver. This works great when the ptp driver is
   the only means for changing the clock frequency. However, some devices support
   ways to adjust the frequency outside the ptp driver stack. When this occurs, the ptp cached value is inaccurate. 
   
* [v1: xfrm-next: mlx5 IPsec packet offload support (Part I)](http://lore.kernel.org/netdev/cover.1670011885.git.leonro@nvidia.com/)

   This is second part with implementation of packet offload.

* [v2: [net-next]: dsa: lan9303: Move to PHYLINK](http://lore.kernel.org/netdev/20221202191749.27437-1-jerry.ray@microchip.com/)

   This patch series moves the lan9303 driver to use the phylink api away from phylib.

   At this point, I do not see anything this driver needs from the other phylink APIs.

* [v10: [xfrm-next]: Extend XFRM core to allow packet offload configuration](http://lore.kernel.org/netdev/cover.1670005543.git.leonro@nvidia.com/)

   The following series extends XFRM core code to handle a new type of IPsec
   offload - packet offload.

   In this mode, the HW is going to be responsible for the whole data path, so both policy and state should be offloaded.

* [v3: [net-next]: phy: aquantia: Determine rate adaptation support from registers](http://lore.kernel.org/netdev/20221202181719.1068869-1-sean.anderson@seco.com/)

   This attempts to address the problems first reported in [1]. Tim has an
   Aquantia phy where the firmware is set up to use "5G XFI" (underclocked
   10GBASE-R) when rate adapting lower speeds. This results in us
   advertising that we support lower speeds and then failing to bring the link up. 
   
* [[PATCH bpf-next v2] bpf: Upgrade bpf_{g,s}etsockopt return values](http://lore.kernel.org/netdev/DU0P192MB1547FE6F35CC1A3EEA1AFDECD6179@DU0P192MB1547.EURP192.PROD.OUTLOOK.COM/)

   Returning -EINVAL almost all the time when error occurs is not very
   helpful for the bpf prog to figure out what is wrong. This patch
   upgrades some return values so that they will be much more helpful.

* [v5: virtio/vsock: replace virtio_vsock_pkt with sk_buff](http://lore.kernel.org/netdev/20221202173520.10428-1-bobby.eshleman@bytedance.com/)

   This commit changes virtio/vsock to use sk_buff instead of
   virtio_vsock_pkt. Beyond better conforming to other net code, using
   sk_buff allows vsock to use sk_buff-dependent features in the future
   (such as sockmap) and improves throughput.

* [v1: can: can327: Flush tx_work on ldisc .close()](http://lore.kernel.org/netdev/20221202160148.282564-1-max@enpas.org/)

   Additionally, remove it from .ndo_stop().

   This ensures that the worker is not called after being freed, and that
   the UART TX queue remains active to send final commands when the netdev is stopped.

* [[PATCH net-next] net: phy: mxl-gpy: rename MMD_VEND1 macros to match datasheet](http://lore.kernel.org/netdev/20221202144900.3298204-1-michael@walle.cc/)

   Rename the temperature sensors macros to match the names in the
   datasheet.

   For the curious: I probably copied the prefix from the already existing
   VPSPEC2_ macros in the driver, the datasheet also mentions some VPSPEC2_
   names, but I suspect they are typos.

* [[PATCH net] nfp: correct desc type when header dma len is 4096](http://lore.kernel.org/netdev/20221202134646.311108-1-simon.horman@corigine.com/)

   When there's only one buffer to dma and its length is 4096, then
   only one data descriptor is needed to carry it according to current
   descriptor definition. So the descriptor type should be `simple`
   instead of `gather`, the latter requires more than one descriptor,
   otherwise it'll be dropped by application firmware.

* [v1: wifi: ath11k_pci: add a soft dependency on qrtr-mhi](http://lore.kernel.org/netdev/20221202130600.883174-1-hch@lst.de/)

   Add a MODULE_SOFTDEP statement to bring the module in (and as a hint
   for kernel packaging) for those cases where it isn't autoloaded already for some reason.

* [[PATCH bpf] bpf: Proper R0 zero-extension for BPF_CALL instructions](http://lore.kernel.org/netdev/20221202103620.1915679-1-bjorn@kernel.org/)

   From: Bj&#246;rn T&#246;pel <bjorn@rivosinc.com>

   A BPF call instruction can be, correctly, marked with zext_dst set to true. 
   
* [v1: bpf-next,v4: xfrm: interface: Add unstable helpers for XFRM metadata](http://lore.kernel.org/netdev/20221202095920.1659332-1-eyal.birger@gmail.com/)

   This patch series adds xfrm metadata helpers using the unstable kfunc
   call interface for the TC-BPF hooks.

   This allows steering traffic towards different IPsec connections based
   on logic implemented in bpf programs.

   The helpers are integrated into the xfrm_interface module. For this
   purpose the main functionality of this module is moved to xfrm_interface_core.c.

* [[PATCH net-next] nfp: add support for multicast filter](http://lore.kernel.org/netdev/20221202094214.284673-1-simon.horman@corigine.com/)

   Rewrite nfp_net_set_rx_mode() to implement interface to delivery mc address and operations to firmware by using general mailbox for filtering multicast packets.

   The operations include add mc address and delete mc address. And the limitation of mc addresses number is 1024 for each net device.

* [v3: [iproute2-next]: Add pcp-prio and new apptrust subcommand](http://lore.kernel.org/netdev/20221202092235.224022-1-daniel.machon@microchip.com/)

   This patch series makes use of the newly introduced [1] DCB_APP_SEL_PCP
   selector, for PCP/DEI prioritization, and DCB_ATTR_IEEE_APP_TRUST
   attribute for configuring per-selector trust and trust-order.

* [v1: net: ipa: use sysfs_emit() to instead of scnprintf()](http://lore.kernel.org/netdev/202212021642142044742@zte.com.cn/)

   Follow the advice of the Documentation/filesystems/sysfs.rst and show()
   should only use sysfs_emit() or sysfs_emit_at() when formatting the
   value to be returned to user space.

* [[PATCH net-next] net: ngbe: Add mdio bus driver.](http://lore.kernel.org/netdev/20221202083558.57618-1-mengyuanlou@net-swift.com/)

   Add mdio bus register for ngbe.
   Added phy changed event detection.

* [[PATCH v3 net] net: microchip: sparx5: correctly free skb in xmit](http://lore.kernel.org/netdev/20221202083544.2905207-1-casper.casan@gmail.com/)

   consume_skb on transmitted, kfree_skb on dropped, do not free on
   TX_BUSY.

   Previously the xmit function could return -EBUSY without freeing, which
   supposedly is interpreted as a drop. And was using kfree on successfully
   transmitted packets.

* [v1: netfilter: nfnetlink: check 'skb->dev' pointer in nfulnl_log_packet()](http://lore.kernel.org/netdev/20221202083304.9005-1-liqiong@nfschina.com/)

   The 'skb->dev' may be NULL, it should be better to check it.

* [V2: [net-next]: devlink: Add port function attribute to enable/disable Roce and migratable](http://lore.kernel.org/netdev/20221202082622.57765-1-shayd@nvidia.com/)

   This series is a complete rewrite of the series "devlink: Add port
   function attribute to enable/disable roce"
   link:
   https://lore.kernel.org/netdev/20221102163954.279266-1-danielj@nvidia.com/

   Currently mlx5 PCI VF and SF are enabled by default for RoCE
   functionality. And mlx5 PCI VF is disable by dafault for migratable functionality.

* [v5: RTW88: Add support for USB variants](http://lore.kernel.org/netdev/20221202081224.2779981-1-s.hauer@pengutronix.de/)

   This has only small changes to the last version. I dropped the endless
   loop check again and added the "Edimax EW-7611ULB V2" to the rtw8723du id table.

* [[PATCH net] ip_gre: do not report erspan version on GRE interface](http://lore.kernel.org/netdev/20221202075337.2890001-1-liuhangbin@gmail.com/)

   Although the type I ERSPAN is based on the barebones IP + GRE
   encapsulation and no extra ERSPAN header. Report erspan version on GRE
   interface looks unreasonable. Fix this by separating the erspan and gre fill info.

* [[PATCH net-next v2] net: phy: Add driver for Motorcomm yt8531 gigabit ethernet phy](http://lore.kernel.org/netdev/20221202073648.3182-1-Frank.Sae@motor-comm.com/)

   Add a driver for the motorcomm yt8531 gigabit ethernet phy. We have verified
   the patch on AM335x platform which has one YT8531 interface
   card and passed all test cases.
   The tested cases indluding: YT8531 UTP function with support of 10M/100M/1000M
   and wol(based on magic packet).

* [v1: netfilter: initialize 'ret' variable](http://lore.kernel.org/netdev/20221202070331.10865-1-liqiong@nfschina.com/)

   The 'ret' should need to be initialized to 0, in case
   return a uninitialized value.

* [v6: wwan: core: Support slicing in port TX flow of WWAN subsystem](http://lore.kernel.org/netdev/20221202064035.57197-1-haozhe.chang@mediatek.com/)

   wwan_port_fops_write inputs the SKB parameter to the TX callback of
   the WWAN device driver. However, the WWAN device (e.g., t7xx) may
   have an MTU less than the size of SKB, causing the TX buffer to be
   sliced and copied once more in the WWAN device driver.

   This patch implements the slicing in the WWAN subsystem and gives
   the WWAN devices driver the option to slice(by frag_len) or not. By
   doing so, the additional memory copy is reduced.

* [[PATCH net-next] tcp: use 2-arg optimal variant of kfree_rcu()](http://lore.kernel.org/netdev/20221202052847.2623997-1-edumazet@google.com/)

   kfree_rcu(1-arg) should be avoided as much as possible,
   since this is only possible from sleepable contexts, and incurr extra rcu barriers.

#### 安全增强

* [v1: exit: Allow oops_limit to be disabled](http://lore.kernel.org/linux-hardening/20221202210617.never.105-kees@kernel.org/)

   In preparation for keeping oops_limit logic in sync with warn_limit,
   have oops_limit == 0 disable checking the Oops counter.

* [v2: NFSD: Avoid clashing function prototypes](http://lore.kernel.org/linux-hardening/20221202204854.gonna.644-kees@kernel.org/)

   When built with Control Flow Integrity, function prototypes between
   caller and function declaration must match. These mismatches are visible
   at compile time with the new -Wcast-function-type-strict in Clang[1].

* [v3: mm/memfd: Add write seals when apply SEAL_EXEC to executable memfd](http://lore.kernel.org/linux-hardening/20221202013404.163143-7-jeffxu@google.com/)

   When apply F_SEAL_EXEC to an executable memfd, add write seals also to
   prevent modification of memfd.

* [v3: mm/memfd: security hook for memfd_create](http://lore.kernel.org/linux-hardening/20221202013404.163143-6-jeffxu@google.com/)

   The new security_memfd_create allows lsm to check flags of
   memfd_create.

   The security by default system (such as chromeos) can use this to implement system wide lsm to allow only non-executable memfd  being created.

* [v3: selftests/memfd: add tests for MFD_NOEXEC_SEAL MFD_EXEC](http://lore.kernel.org/linux-hardening/20221202013404.163143-5-jeffxu@google.com/)

   Tests to verify MFD_NOEXEC, MFD_EXEC and vm.memfd_noexec sysctl.

   Co-developed-by: Daniel Verkamp <dverkamp@chromium.org>

* [v3: mm/memfd: add MFD_NOEXEC_SEAL and MFD_EXEC](http://lore.kernel.org/linux-hardening/20221202013404.163143-3-jeffxu@google.com/)

   The new MFD_NOEXEC_SEAL and MFD_EXEC flags allows application to
   set executable bit at creation time (memfd_create).

   When MFD_NOEXEC_SEAL is set, memfd is created without executable bit
   (mode:0666), and sealed with F_SEAL_EXEC, so it can't be chmod to
   be executable (mode: 0777) after creation.

   when MFD_EXEC flag is set, memfd is created with executable bit
   (mode:0777), this is the same as the old behavior of memfd_create.

* [v3: mm/memfd: add F_SEAL_EXEC](http://lore.kernel.org/linux-hardening/20221202013404.163143-2-jeffxu@google.com/)

   The new F_SEAL_EXEC flag will prevent modification of the exec bits:
   written as traditional octal mask, 0111, or as named flags, S_IXUSR |
   S_IXGRP | S_IXOTH. Any chmod(2) or similar call that attempts to modify
   any of these bits after the seal is applied will fail with errno EPERM.

* [v3: mm/memfd: MFD_NOEXEC_SEAL and MFD_EXEC](http://lore.kernel.org/linux-hardening/20221202013404.163143-1-jeffxu@google.com/)

   Since Linux introduced the memfd feature, memfd have always had their execute bit set, and the memfd_create() syscall doesn't allow setting it differently.

   However, in a secure by default system, such as ChromeOS, (where all executables should come from the rootfs, which is protected by Verified boot), this executable nature of memfd opens a door for NoExec bypass and enables &#8220;confused deputy attack&#8221;. &#160;E.g, in VRP bug [1]: cros_vm process created a memfd to share the content with an external process, however the memfd is overwritten and used for executing arbitrary code and root escalation. [2] lists more VRP in this kind.

* [v1: wifi: ieee80211: Do not open-code qos address offsets](http://lore.kernel.org/linux-hardening/20221130212641.never.627-kees@kernel.org/)

   When building with -Wstringop-overflow, GCC's KASAN implementation does
   not correctly perform bounds checking within some complex structures
   when faced with literal offsets, and can get very confused. For example,
   this warning is seen due to literal offsets into sturct ieee80211_hdr
   that may or may not be large enough:

* [v1: Add DEXCR support](http://lore.kernel.org/linux-hardening/20221128024458.46121-1-bgray@linux.ibm.com/)

   This series is based on initial work by Chris Riedl that was not sent
   to the list.

   Adds a kernel interface for userspace to interact with the DEXCR.
   The DEXCR is a SPR that allows control over various execution
   'aspects', such as indirect branch prediction and enabling the
   hashst/hashchk instructions. Further details are in ISA 3.1B Book 3 chapter 12.

#### 异步 IO

* [v1: for-next: poll & rsrc quiesce improvements](http://lore.kernel.org/io-uring/cover.1669821213.git.asml.silence@gmail.com/)

   A bunch of random patches cleaning up poll and doing some
   preparation for future work.

#### Rust For Linux

* [v2: Rust core additions](http://lore.kernel.org/rust-for-linux/20221202161502.385525-1-ojeda@kernel.org/)

   This is v2 of the first batch of changes to upstream the rest of
   the Rust support. v1 is at:

       https://lore.kernel.org/rust-for-linux/20221110164152.26136-1-ojeda@kernel.org/

   I collected the tags and applied the agreed changes. The full diff
   follows (with respect to v1), since they are minor enough.

#### BPF

* [[PATCH bpf-next] bpf: Do not mark certain LSM hook arguments as trusted](http://lore.kernel.org/bpf/20221203204954.2043348-1-yhs@fb.com/)

   Martin mentioned that the verifier cannot assume arguments from
   LSM hook sk_alloc_security being trusted since after the hook
   is called, the sk ref_count is set to 1. This will overwrite
   the ref_count changed by the bpf program and may cause ref_count underflow later on.

* [v2: [bpf-next]: bpf: Handle MEM_RCU type properly](http://lore.kernel.org/bpf/20221203184557.476871-1-yhs@fb.com/)

   Patch set [1] added rcu support for bpf programs. In [1], a rcu
   pointer is considered to be trusted and not null. This is actually
   not true in some cases. The rcu pointer could be null, and for non-null
   rcu pointer, it may have reference count of 0. This small patch set
   fixed this problem. Patch 1 is the kernel fix. Patch 2 adjusted selftests properly. 
   
* [[PATCH bpf-next v2] libbpf: parse usdt args without offset on x86 (e.g. 8@(%rsp))](http://lore.kernel.org/bpf/20221203123746.2160-1-timo.hunziker@eclipso.ch/)

   Parse USDT arguments like "8@(%rsp)" on x86. These are emmited by
   SystemTap. The argument syntax is similar to the existing "memory
   dereference case" but the offset left out as it's zero (i.e. read
   the value from the address in the register). We treat it the same
   as the the "memory dereference case", but set the offset to 0.

* [v2: BPF Iterator Document](http://lore.kernel.org/bpf/20221202221710.320810-1-ssreevani@meta.com/)

   Removed SVG image file.

* [v1: bpf-next: Document some recent core kfunc additions](http://lore.kernel.org/bpf/20221202220736.521227-1-void@manifault.com/)

   A series of recent patch sets introduced kfuncs that allowed struct
   task_struct and struct cgroup objects to be used as kptrs. 
   These are "core" kfuncs, in that they may be used by a wide variety of
   possible BPF tracepoint or struct_ops programs, and are defined in
   kernel/bpf/helpers.c. Even though as kfuncs they have no ABI stability guarantees, they should still be properly documented. This patch set adds that documentation.

* [[PATCH bpf-next v2] bpf: Upgrade bpf_{g,s}etsockopt return values](http://lore.kernel.org/bpf/DU0P192MB1547FE6F35CC1A3EEA1AFDECD6179@DU0P192MB1547.EURP192.PROD.OUTLOOK.COM/)

   Returning -EINVAL almost all the time when error occurs is not very
   helpful for the bpf prog to figure out what is wrong. This patch
   upgrades some return values so that they will be much more helpful.

* [[PATCH bpf-next] libbpf: parse usdt args without offset on x86 (e.g. 8@(%rsp))](http://lore.kernel.org/bpf/20221202153816.1180450-1-timo.hunziker@gmx.ch/)

   Parse USDT arguments like "8@(%rsp)" on x86. These are emmited by
   systemtap. The syntax is a mixture between the "memory dereference
   case" and the "register read case" as the offset is zero but the
   register is wrapped in parentheses. We treat them the same as the
   the "register read case".

* [[PATCH bpf] bpf: Proper R0 zero-extension for BPF_CALL instructions](http://lore.kernel.org/bpf/20221202103620.1915679-1-bjorn@kernel.org/)

   From: Bj&#246;rn T&#246;pel <bjorn@rivosinc.com>

   A BPF call instruction can be, correctly, marked with zext_dst set to true. 
   
* [[PATCH bpf-next v2] libbpf:Improved usability of the Makefile in libbpf](http://lore.kernel.org/bpf/20221202081738.128513-1-liuxin350@huawei.com/)

   Current libbpf Makefile does not contain the help command, which
   is inconvenient to use. Similar to the Makefile help command of the
   perf, a help command is provided to list the commands supported by
   libbpf make and the functions of the commands.

* [v1: bpf-next: Refactor verifier prune and jump point handling](http://lore.kernel.org/bpf/20221202051030.3100390-1-andrii@kernel.org/)

   Disentangle prune and jump points in BPF verifier code. They are conceptually
   independent but currently coupled together. This small patch set refactors
   related code and make it possible to have some instruction marked as pruning
   or jump point independently.

* [v1: Improvements to incremental builds](http://lore.kernel.org/bpf/20221202045743.2639466-1-irogers@google.com/)

   Switching to using install_headers caused incremental builds to always
   rebuild most targets. This was caused by the headers always being
   reinstalled and then getting new timestamps causing dependencies to be rebuilt.
   
* [v1: panic: Taint kernel if fault injection has been used](http://lore.kernel.org/bpf/166995635931.455067.17768077948832448089.stgit@devnote3/)

   Since the function error injection framework in the fault injection
   subsystem can change the function code flow forcibly, it may cause
   unexpected behavior (and that is the purpose of this feature) even
   if it is applied to the ALLOW_ERROR_INJECTION functions.
   So this feature must be used only for debugging or testing purpose.

* [v1: bpf-next,v3: xfrm: interface: Add unstable helpers for XFRM metadata](http://lore.kernel.org/bpf/20221201211425.1528197-1-eyal.birger@gmail.com/)

   This patch series adds xfrm metadata helpers using the unstable kfunc
   call interface for the TC-BPF hooks.

   This allows steering traffic towards different IPsec connections based
   on logic implemented in bpf programs.

   The helpers are integrated into the xfrm_interface module. For this
   purpose the main functionality of this module is moved to xfrm_interface_core.c.

* [v1: selftests/bpf: add GCC compatible builtins to bpf_legacy.h](http://lore.kernel.org/bpf/20221201190939.3230513-1-james.hilliard1@gmail.com/)

   The bpf_legacy.h header uses llvm specific load functions, add
   GCC compatible variants as well to fix tests using these functions under GCC.

* [v1: net: check for dev pointer being NULL in dev_hard_header() to avoid GPF](http://lore.kernel.org/bpf/1669817512-4560-1-git-send-email-george.kennedy@oracle.com/)

   The dev pointer can be NULL in dev_hard_header(). Add check for dev being
   NULL in dev_hard_header() to avoid GPF.

* [[PATCH bpf-next] selftests: xsk: changes for setting up NICs to run xsk self-tests](http://lore.kernel.org/bpf/20221130094142.545051-1-tirthendu.sarkar@intel.com/)

   ETH devies need to be set up for running xsk self-tests, like enable
   loopback, set promiscuous mode, MTU etc. This patch adds those settings
   before running xsk self-tests and reverts them back once done.

* [[PATCHSET RFC] sched: Implement BPF extensible scheduler class](http://lore.kernel.org/bpf/20221130082313.3241517-1-tj@kernel.org/)

   This patch set proposes a new scheduler class called &#8216;ext_sched_class&#8217;, or
   sched_ext, which allows scheduling policies to be implemented as BPF programs.

* [[PATCH intel-net] ice: xsk: do not use xdp_return_frame() on tx_buf->raw_buf](http://lore.kernel.org/bpf/20221129171125.4092238-1-maciej.fijalkowski@intel.com/)

   Previously ice XDP xmit routine was changed in a way that it avoids
   xdp_buff->xdp_frame conversion as it is simply not needed for handling
   XDP_TX action and what is more it saves us CPU cycles. This routine is re-used on ZC driver to handle XDP_TX action.

   Although for XDP_TX on Rx ZC xdp_buff that comes from xsk_buff_pool is
   converted to xdp_frame, xdp_frame itself is not stored inside
   ice_tx_buf, we only store raw data pointer. Casting this pointer to
   xdp_frame and calling against it xdp_return_frame in
   ice_clean_xdp_tx_buf() results in undefined behavior.

* [[RFC PATCH bpf-next] bpf: Allow get bpf object with CAP_BPF](http://lore.kernel.org/bpf/20221129161612.45765-1-laoar.shao@gmail.com/)

   In the containerized envriomentation, if a container is not
   privileged but with CAP_BPF, it is not easy to debug bpf created in this
   container, let alone using bpftool. Because these bpf objects are
   invisible if they are not pinned in bpffs. Currently we have to
   interact with the process which creates these bpf objects to get the information. 
   
* [[PATCH bpf] bpf, docs: Correct the example of BPF_XOR](http://lore.kernel.org/bpf/20221129134558.2757043-1-zhengyejian1@huawei.com/)

   Refer to description of BPF_XOR, dst_reg should be used but not src_reg
   in the examples.

* [v1: ipsec-next,v2: xfrm: interface: Add unstable helpers for XFRM metadata](http://lore.kernel.org/bpf/20221129132018.985887-1-eyal.birger@gmail.com/)

   This patch series adds xfrm metadata helpers using the unstable kfunc
   call interface for the TC-BPF hooks.

   This allows steering traffic towards different IPsec connections based
   on logic implemented in bpf programs.

* [v1: Improved usability of the Makefile in libbpf](http://lore.kernel.org/bpf/20221129074235.116969-1-liuxin350@huawei.com/)

   Current libbpf Makefile does not contain the help command, which
   is inconvenient to use. A help command is provided to list the
   commands supported by libbpf make and the functions of the commands.

* [[PATCH bpf-next] bpf: Handle MEM_RCU type properly](http://lore.kernel.org/bpf/20221129023713.2216451-1-yhs@fb.com/)

   Commit 9bb00b2895cb ("bpf: Add kfunc bpf_rcu_read_lock/unlock()")
   introduced MEM_RCU and bpf_rcu_read_lock/unlock() support. In that
   commit, a rcu pointer is tagged with both MEM_RCU and PTR_TRUSTED
   so that it can be passed into kfuncs or helpers as an argument.

* [v5: [bpf-next]: execmem_alloc for BPF programs](http://lore.kernel.org/bpf/20221128190245.2337461-1-song@kernel.org/)

  
   This patchset replaces bpf_prog_pack with a better API and makes it
   available for other dynamic kernel text, such as modules, ftrace, kprobe.

   Memory allocated by execmem_alloc() is RO+X, so this doesnot violate W^X.
   The caller has to update the content with text_poke like mechanism.
   Specifically, execmem_fill() is provided to update memory allocated by execmem_alloc().
   
* [[RFC bpf-next 0/2] bpf: verify scalar ids mapping in regsafe() using check_ids()](http://lore.kernel.org/bpf/20221128163442.280187-1-eddyz87@gmail.com/)

   This happens because function regsafe() used (indirectly) form
   is_state_visited() does not compare id mapping for scalar registers.

   In current form the patch does not have a big impact on the tests verification time. 
   
* [v1: ipsec-next: xfrm: interface: Add unstable helpers for XFRM metadata](http://lore.kernel.org/bpf/20221128160501.769892-1-eyal.birger@gmail.com/)

   This patch series adds xfrm metadata helpers using the unstable kfunc
   call interface for the TC-BPF hooks.

   This allows steering traffic towards different IPsec connections based
   on logic implemented in bpf programs.

* [v2: lsm: Improve LSM hooks documentation](http://lore.kernel.org/bpf/20221128144240.210110-1-roberto.sassu@huaweicloud.com/)

   The recent discussion about return values from LSM hooks, available here:

   https://lore.kernel.org/bpf/20221115175652.3836811-1-roberto.sassu@huaweicloud.com/

   motivated revisiting the documentation in include/linux/lsm_hooks.h, to
   ensure that it is complete and accurate. It was also a good occasion to fix
   discovered formatting issues.

* [v1: bpf: Check timer_off for map_in_map only when map value has timer](http://lore.kernel.org/bpf/20221126105351.2578782-1-hengqi.chen@gmail.com/)

   The timer_off value could be -EINVAL or -ENOENT when map value of inner map is struct and contains no bpf_timer.

   Since timer_off is different, the map_in_map outer in the above case
   can NOT be updated using map_fd in the first case. This patch tries to fix such restriction.

* [v3: [bpf-next]: bpf: Support kernel function call in 32-bit ARM](http://lore.kernel.org/bpf/20221126094530.226629-1-yangjihong1@huawei.com/)

   1. Patch1 is dependent patch to fix zext extension error in 32-bit ARM.
   2. Patch2 supports bpf fkunc in 32-bit ARM for EABI.
   3. Patch3 is used to add test cases to cover some parameter scenarios
      states by AAPCS.
   4. Patch4 fix a comment error.

   The following is the test_progs result in the 32-bit ARM environment:

### 周边技术动态

#### Qemu

* [v1: target/riscv: Set pc_succ_insn for !rvc illegal insn](http://lore.kernel.org/qemu-devel/20221203175744.151365-1-richard.henderson@linaro.org/)

   Failure to set pc_succ_insn may result in a TB covering zero bytes,
   which triggers an assert within the code generator.

   Resolves: https://gitlab.com/qemu-project/qemu/-/issues/1224

* [v3: riscv: Allow user to set the satp mode](http://lore.kernel.org/qemu-devel/20221201093623.1394747-1-alexghiti@rivosinc.com/)

   RISC-V specifies multiple sizes for addressable memory and Linux probes for
   the machine's support at startup via the satp CSR register (done in
   csr.c:validate_vm).

   As per the specification, sv64 must support sv57, which in turn must
   support sv48...etc. So we can restrict machine support by simply setting the
   "highest" supported mode and the bare mode is always supported.

#### Buildroot

* [[git commit] package/libmdbx: bump version to 0.11.13 "Swashplate"](http://lore.kernel.org/buildroot/20221201212240.A21BB84894@busybox.osuosl.org/)

   This is stable bugfix release of libmdbx, in Family Glory and
   in memory of Boris Yuriev (the inventor of Helicopter and Swashplate in 1911) on his 133rd birthday.

   It is reasonable to backport this patch to all applicable releases/branches of Buildroot.

#### U-Boot

* [v5: IPv6 support](http://lore.kernel.org/u-boot/20221202091818.2382748-1-v.v.mitrofanov@yadro.com/)

   This patch set adds basic IPv6 support to U-boot.
   It is based on Chris's Packham patches
   (https://lists.denx.de/pipermail/u-boot/2017-January/279366.html)
   Chris's patches were taken as base. There were efforts to launch it on
   HiFive SiFive Unmatched board but the board didn't work well. The code was
   refactored, fixed some bugs as CRC for little-endian, some parts were implemented in
   our own way, something was taken from Linux. Finally we did manual tests and the board worked well.

## 20221127：第 22 期

### 内核动态

#### RISC-V 架构支持

* [v1: genirq: oneshot-safe threaded EOIs](http://lore.kernel.org/linux-riscv/20221126234134.32660-1-samuel@sholland.org/)

   This is the optimization that I promised back in
   https://lore.kernel.org/lkml/20220701202440.59059-1-samuel@sholland.org/
   and got back around to cleaning up. To quote that cover letter:

   "A further optimization is to take advantage of the fact that multiple
   IRQs can be claimed at once. 
   
* [v1: irqchip/sifive-plic: Support wake IRQs](http://lore.kernel.org/linux-riscv/20221126194805.19431-1-samuel@sholland.org/)

   The PLIC does not define any special method for marking interrupts as
   wakeup-capable, so it should have the IRQCHIP_SKIP_SET_WAKE flag set.

* [v12: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20221126173453.306088-1-apatel@ventanamicro.com/)

   These patches were originally part of the "Linux RISC-V ACLINT Support"
   series but this now a separate series so that it can be merged independently
   of the "Linux RISC-V ACLINT Support" series.
   
* [v2: riscv: Allwinner D1/D1s platform support](http://lore.kernel.org/linux-riscv/20221125234656.47306-1-samuel@sholland.org/)

   This series adds the Kconfig/defconfig plumbing and devicetrees for a
   range of Allwinner D1 and D1s-based boards. Many features are already
   enabled, including USB, Ethernet, and WiFi.

* [v1: riscv: increase boot command line size to 1K](http://lore.kernel.org/linux-riscv/20221125133713.314796-1-andrea.righi@canonical.com/)

   Kernel parameters string is limited to 512 characters on riscv (using
   the default from include/uapi/asm-generic/setup.h).

* [v3: Improve CLOCK_EVT_FEAT_C3STOP feature setting](http://lore.kernel.org/linux-riscv/20221125112105.427045-1-apatel@ventanamicro.com/)

   This series improves the RISC-V timer driver to set CLOCK_EVT_FEAT_C3STOP
   feature based on RISC-V platform capabilities.

   These patches can also be found in riscv_timer_dt_imp_v3 branch at:
   https://github.com/avpatel/linux.git

* [v4: AX45MP: Add support to non-coherent DMA](http://lore.kernel.org/linux-riscv/20221124172207.153718-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   Andes AX45MP core has a Programmable Physical Memory Attributes (PMA)
   block that allows dynamic adjustment of memory attributes in the runtime.
   It contains a configurable amount of PMA entries implemented as CSR
   registers to control the attributes of memory locations in interest. PMA
   regions are passed from the l2 node which are configured as
   non-cacheable + bufferable with the SBI call.         

* [v2: riscv: VMAP_STACK overflow detection thread-safe](http://lore.kernel.org/linux-riscv/20221124094845.1907443-1-debug@rivosinc.com/)

   commit 31da94c25aea ("riscv: add VMAP_STACK overflow detection") added
   support for CONFIG_VMAP_STACK. If overflow is detected, CPU switches to
   `shadow_stack` temporarily before switching finally to per-cpu `overflow_stack`.

* [v1: clk: microchip: enable the MPFS clk driver by default if SOC_MICROCHIP_POLARFIRE](http://lore.kernel.org/linux-riscv/20221123161921.81195-1-conor@kernel.org/)

   With the intent of removing driver selects from Kconfig.socs in
   arch/riscv, essential drivers that were being selected there could
   instead by enabled by defaulting them to the value of the SoC's Kconfig symbol.

* [v1: riscv: boot: add zstd support](http://lore.kernel.org/linux-riscv/20221123150257.3108-1-jszhang@kernel.org/)

   Support build the zstd compressed Image.zst. Similar as other
   compressed formats, the Image.zst is not self-decompressing and
   the bootloader still needs to handle decompression before launching the kernel image.

* [v1: riscv/ftrace: add WITH_DIRECT_CALLS support](http://lore.kernel.org/linux-riscv/20221123142025.1504030-1-suagrfillet@gmail.com/)

   This series adds DYNAMIC_FTRACE_WITH_DIRECT_CALLS support for RISC-V.
   SAMPLE_FTRACE_DIRECT and SAMPLE_FTRACE_DIRECT_MULTI are also included
   here as the samples for testing DIRECT_CALLS related interface.

* [v2: Revert "clocksource/drivers/riscv: Events are stopped during CPU suspend"](http://lore.kernel.org/linux-riscv/20221122121620.3522431-1-conor.dooley@microchip.com/)

   This does not cover whether any given events actually reach the hart or
   not, just what the hart will do if it receives an event. On PolarFire
   SoC, and potentially other SiFive based implementations, events from the
   RISC-V timer do reach a hart during suspend. This is not the case for
   the implementation on the Allwinner D1 - there timer events are not
   received during suspend.

* [v1: pinctrl: call of_node_put() when breaking out of for_each_available_child_of_node()](http://lore.kernel.org/linux-riscv/20221122075853.2496680-1-zhangpeng362@huawei.com/)

   Since for_each_available_child_of_node() will increase the refcount of
   node, we need to call of_node_put() manually when breaking out of the iteration.

* [v1: fpga: add PolarFire SoC IAP support](http://lore.kernel.org/linux-riscv/20221121225748.124900-1-conor@kernel.org/)

   Add support for "IAP" reprogramming of the FPGA fabric on PolarFire SoC.
   RFC yadda yadda, but not asking people to look at the code per se -
   really what I am sending this RFC to achieve is a bit of feedback on
   what the re-programming "flow" looks like.

* [v1: cpuidle: riscv-sbi: Stop using non-retentive suspend](http://lore.kernel.org/linux-riscv/20221121205647.23343-1-palmer@rivosinc.com/)

   This should allow us to revert 232ccac1bd9b ("clocksource/drivers/riscv:
   Events are stopped during CPU suspend"), which fixes suspend on the D1
   but breaks timers everywhere.
  
* [v1: Introduce CONFIG_SLUB_TINY and deprecate SLOB](http://lore.kernel.org/linux-riscv/20221121171202.22080-1-vbabka@suse.cz/)

   this continues the discussion from [1]. Reasons to remove SLOB are
   outlined there and no-one has objected so far. The last patch of this
   series therefore deprecates CONFIG_SLOB and updates all the defconfigs
   using CONFIG_SLOB=y in the tree.

* [v2: arch_topology: Build cacheinfo from primary CPU](http://lore.kernel.org/linux-riscv/20221121171217.3581004-1-pierre.gondois@arm.com/)

   To prevent this bug, allocate the cacheinfo from the primary CPU when
   preemption and interrupts are enabled and before booting secondary
   CPUs. The cache levels/leaves are computed from DT/ACPI PPTT information
   only, without relying on the arm64 CLIDR_EL1 register.
   If no cache information is found in the DT/ACPI PPTT, then fallback
   to the current state, triggering [4] on PREEMPT_RT kernels.

* [v1: riscv: Sync efi page table's kernel mappings before switching](http://lore.kernel.org/linux-riscv/20221121133303.1782246-1-alexghiti@rivosinc.com/)

   The EFI page table is initially created as a copy of the kernel page table.
   With VMAP_STACK enabled, kernel stacks are allocated in the vmalloc area:
   if the stack is allocated in a new PGD (one that was not present at the
   moment of the efi page table creation or not synced in a previous vmalloc
   fault), the kernel will take a trap when switching to the efi page table
   when the vmalloc kernel stack is accessed, resulting in a kernel panic.

* [v1: Some DT binding quirks for T-Head C9xx](http://lore.kernel.org/linux-riscv/20221121041757.418645-1-uwu@icenowy.me/)

   These patchset is just full of DT binding patches related to T-Head
   C906/C910. These cores now have an open-source fixed-configuration
   edition, which enables everyone to explore with them.

* [[PATCH -next] RISC-V: KVM: optimize kvm_arch_hardware_enable()](http://lore.kernel.org/linux-riscv/20221121003915.2817102-1-chenlifu@huawei.com/)

   The values of CSR_HEDELEG and CSR_HIDELEG registers are constants,
   so change them from variables to macros.

* [v4: riscv/ftrace: make function graph use ftrace directly](http://lore.kernel.org/linux-riscv/20221120084230.910152-1-suagrfillet@gmail.com/)

   In RISC-V architecture, when we enable the ftrace_graph tracer on some
   functions, the function tracings on other functions will suffer extra
   graph tracing work. In essence, graph_ops isn't limited by its func_hash
   due to the global ftrace_graph_[regs]_call label. That should be corrected.

* [v1: riscv: add Bouffalolab bl808 support](http://lore.kernel.org/linux-riscv/20221120082114.3030-1-jszhang@kernel.org/)

   This series adds Bouffalolab uart driver and basic devicetrees for
   Bouffalolab bl808 SoC and Sipeed M1S dock board.

* [v1: riscv: defconfig: add SERIAL_8250_DW](http://lore.kernel.org/linux-riscv/20221119121953.3897171-1-clabbe@baylibre.com/)

   jh7100-starfive-visionfive-v1 DTB was recently added, but all my try to
   boot it in kernelCI failed.
   This is due to a missing serial driver in defconfig.
   So let's add CONFIG_SERIAL_8250_DW which is needed.

* [v7: riscv, mm: detect svnapot cpu support at runtime](http://lore.kernel.org/linux-riscv/20221119112242.3593646-1-panqinglin2020@iscas.ac.cn/)

   Svnapot is a RISC-V extension for marking contiguous 4K pages as a non-4K
   page. This patch set is for using Svnapot in hugetlb fs and huge vmap.

   This patchset adds a Kconfig item for using Svnapot in
   "Platform type"->"SVNAPOT extension support".
   
#### 进程调度

* [v2: x86/sched: Avoid unnecessary migrations within SMT domains](http://lore.kernel.org/lkml/20221122203532.15013-1-ricardo.neri-calderon@linux.intel.com/)

   This v2 of this patchset. V1 can be found here [1]. In this version I took
   the suggestion of Peter to teach arch_asym_cpu_priority() the CPU state.
   Also, I reworded the cover letter to explain better the intent.

* [[GIT PULL] sched/urgent for 6.1-rc6](http://lore.kernel.org/lkml/Y3oU5cB9LOcBLfSS@zn.tnic/)

   The following changes since commit 094226ad94f471a9f19e8f8e7140a09c2625abaa:
   
     sched: Fix race in task_call_func() (2022-11-14 09:58:32 +0100)

   - Fix a small race on the task's exit path where there's a
   misunderstanding whether the task holds rq->lock or not

   - Prevent processes from getting killed when using deprecated or unknown
   rseq ABI flags in order to be able to fuzz the rseq() syscall with syzkaller

#### 内存管理


* [[PATCH mm] kasan: fail non-kasan KUnit tests on KASAN reports](http://lore.kernel.org/linux-mm/655fd7e303b852809d3a8167d28091429f969c73.1669486407.git.andreyknvl@google.com/)

   After the recent changes done to KUnit-enabled KASAN tests, non-KASAN KUnit
   tests stopped being failed when KASAN report is detected.

   Recover that property by failing the currently running non-KASAN KUnit test
   when KASAN detects and prints a report for a bad memory access.

* [[v2 PATCH 0/9] crypto: Add helpers for allocating with DMA alignment](http://lore.kernel.org/linux-mm/Y4BGC2BPesy3qsEm@gondor.apana.org.au/)

   This patch series adds helpers to allow drivers to explicitly
   request ARCH_DMA_MINALIGN when allocating memory through the Crypto API.

* [v1: s390/mm: Use pmd_pgtable_page() helper in __gmap_segment_gaddr()](http://lore.kernel.org/linux-mm/20221125034502.1559986-1-anshuman.khandual@arm.com/)

   This applies on v6.1-rc6 but after the following patch. Build tested for
   s390 plaform (defconfig).

* [v1: implement DAMOS filtering for anon pages and](http://lore.kernel.org/linux-mm/20221124212114.136863-1-sj@kernel.org/)

   DAMOS let users do system operations in a data access pattern oriented
   way.  The data access pattern, which is extracted by DAMON, is somewhat
   accurate more than what user space could know in many cases.  However,
   in some situation, users could know something more than the kernel about
   the pattern or some special requirements for some types of memory or
   processes. 

* [v1: mm/thp: Rename pmd_to_page() as pmd_pgtable_page()](http://lore.kernel.org/linux-mm/20221124131641.1523772-1-anshuman.khandual@arm.com/)

   Current pmd_to_page(), which derives the page table page containing the pmd
   address has a very misleading name. The problem being, it sounds similar to
   pmd_page() which derives page embedded in a given pmd entry either for next
   level page or a mapped huge page. Rename it as pmd_pgtable_page() instead.

* [[PATCH linux-next ] mm: vmscan: use sysfs_emit() to instead of scnprintf()](http://lore.kernel.org/linux-mm/202211241929015476424@zte.com.cn/)

   Replace the open-code with sysfs_emit() to simplify the code.

* [v1: mm/vmalloc: Add check for KMEM_CACHE](http://lore.kernel.org/linux-mm/20221124040226.17953-1-jiasheng@iscas.ac.cn/)

   As KMEM_CACHE may return NULL pointer, it should
   be better to check the return value in order to
   avoid NULL pointer dereference in kmem_cache_zalloc.

* [v2: iov_iter: Add extraction helpers](http://lore.kernel.org/linux-mm/166920902005.1461876.2786264600108839814.stgit@warthog.procyon.org.uk/)

   Here are four patches to provide support for extracting pages from an
   iov_iter, where such a thing makes sense, if you could take a look?

* [v2: mm: introduce arch_has_hw_nonleaf_pmd_young()](http://lore.kernel.org/linux-mm/20221123064510.16225-1-jgross@suse.com/)

   When running as a Xen PV guests commit eed9a328aa1a ("mm: x86: add
   CONFIG_ARCH_HAS_NONLEAF_PMD_YOUNG") can cause a protection violation
   in pmdp_test_and_clear_young():

   This happens because the Xen hypervisor can't emulate direct writes to
   page table entries other than PTEs.


* [v1: documentation/mm: Update pmd_present() in arch_pgtable_helpers.rst](http://lore.kernel.org/linux-mm/20221123051319.1312582-1-anshuman.khandual@arm.com/)

   Although pmd_present() might seem to indicate a valid and mapped pmd entry,
   in reality it returns true when pmd_page() points to a valid page in memory
   , regardless whether the pmd entry is mapped or not. 
   
* [v1: mm: Add memory.demote for proactive demotion only](http://lore.kernel.org/linux-mm/20221122203850.2765015-2-almasrymina@google.com/)

   The kernel will only attempt to demote pages with this interface. It
   will not attempt any other kind of reclaim (swap, writeback or
   reclaiming clean file pages).

* [V1: mm: Disable demotion from proactive reclaim](http://lore.kernel.org/linux-mm/20221122203850.2765015-1-almasrymina@google.com/)

   Since commit 3f1509c57b1b ("Revert "mm/vmscan: never demote for memcg
   reclaim""), the proactive reclaim interface memory.reclaim does both
   reclaim and demotion. This is likely fine for us for latency critical
   jobs where we would want to disable proactive reclaim entirely, and is
   also fine for latency tolerant jobs where we would like to both
   proactively reclaim and demote.

* [v1: Follow-up to Leave IRQs enabled for per-cpu page allocations](http://lore.kernel.org/linux-mm/20221122131229.5263-1-mgorman@techsingularity.net/)

   The following two patches are a fixup patch and a simplification as
   suggested by Vlastimil Babka.

* [v2: mm: set the vma flags dirty before testing if it is mergeable](http://lore.kernel.org/linux-mm/20221122115007.2787017-1-usama.anjum@collabora.com/)

   The VM_SOFTDIRTY should be set in the vma flags to be tested if new
   allocation should be merged in previous vma or not. With this patch,
   the new allocations are merged in the previous VMAs.

* [v2: mm,thp,rmap: rework the use of subpages_mapcount](http://lore.kernel.org/linux-mm/a5849eca-22f1-3517-bf29-95d982242742@google.com/)

   Andrew, please replace the 1/3, 1/3 fix, 2/3, 3/3 in mm-unstable
   by these v2 three: which incorporate the uninitialized warning fix,
   and adjustments according to Kirill's review comments, plus his
   Acks - I couldn't quite manage them just by -fixes.

* [v1: zswap: do not allocate from atomic pool](http://lore.kernel.org/linux-mm/20221122013338.3696079-1-senozhatsky@chromium.org/)

   zswap_frontswap_load() should be called from preemptible
   context (we even call mutex_lock() there) and it does not
   look like we need to do GFP_ATOMIC allocaion for temp
   buffer there. Use GFP_KERNEL instead.

* [v1: Introduce merge identical pages mechanism](http://lore.kernel.org/linux-mm/20221121190020.66548-1-avromanov@sberdevices.ru/)

   This RFC series adds feature which allows merge identical
   compressed pages into a single one. The main idea is that
   zram only stores object references, which store the compressed
   content of the pages. Thus, the contents of the zsmalloc objects
   don't change in any way.

* [v8: mm: add zblock - new allocator for use via zpool API](http://lore.kernel.org/linux-mm/20221121145435.41002-1-a.badmaev@clicknet.pro/)

     The range from 0 to PAGE_SIZE is divided into the number of intervals
   corresponding to the number of lists and each list only operates objects
   of size from its interval. Thus the block lists are isolated from each
   other, which makes it possible to simultaneously perform actions with
   several objects from different lists.
 
* [v2: v1: shmem: user and group quota support for tmpfs](http://lore.kernel.org/linux-mm/20221121142854.91109-1-lczerner@redhat.com/)

   people have been asking for quota support in tmpfs many times in the past
   mostly to avoid one malicious user, or misbehaving user/program to consume
   all of the system memory. This has been partially solved with the size
   mount option, but some problems still prevail.

* [[PATCH -next] mm/hugetlb: stop using 0 as NULL pointer](http://lore.kernel.org/linux-mm/20221121102037.75307-1-yang.lee@linux.alibaba.com/)

   mm/hugetlb.c:1531:37: warning: Using plain integer as NULL pointer

   Link: https://bugzilla.openanolis.cn/show_bug.cgi?id=3224

* [v7: TDX host kernel support](http://lore.kernel.org/linux-mm/cover.1668988357.git.kai.huang@intel.com/)

   Intel Trusted Domain Extensions (TDX) protects guest VMs from malicious
   host and certain physical attacks. 
   
#### 文件系统

* [v2: epoll: use refcount to reduce ep_mutex contention](http://lore.kernel.org/linux-fsdevel/f35e58ed5af8131f0f402c3dc6c3033fa96d1843.1669312208.git.pabeni@redhat.com/)

   The application is multi-threaded, creates a new epoll entry for
   each incoming connection, and does not delete it before the
   connection shutdown - that is, before the connection's fd close().

* [v1: writeback: Add asserts for adding freed inode to lists](http://lore.kernel.org/linux-fsdevel/20221124141806.6194-1-jack@suse.cz/)

   In the past we had several use-after-free issues with inodes getting
   added to writeback lists after evict() removed them. These are painful
   to debug so add some asserts to catch the problem earlier.

* [v5: fscache,cachefiles: add prepare_ondemand_read() interface](http://lore.kernel.org/linux-fsdevel/20221124034212.81892-1-jefflexu@linux.alibaba.com/)

   In short, here each erofs filesystem is composed of multiple blobs (or
   devices).  Each blob corresponds to one fscache cookie to strictly
   follow on-disk format and implement the image downloading in a
   deterministic manner, which means it has a unique checksum and is signed by vendors.

* [v1: fuse: lock inode unconditionally in fuse_fallocate()](http://lore.kernel.org/linux-fsdevel/20221123104336.1030702-1-mszeredi@redhat.com/)

   file_modified() must be called with inode lock held.  fuse_fallocate()
   didn't lock the inode in case of just FALLOC_KEEP_SIZE flags value, which
   resulted in a kernel Warning in notify_change().

* [v5: Implement copy offload support](http://lore.kernel.org/linux-fsdevel/20221123055827.26996-1-nj.shetty@samsung.com/)

   The patch series covers the points discussed in November 2021 virtual
   call [LSF/MM/BFP TOPIC] Storage: Copy Offload [0].
   We have covered the initial agreed requirements in this patchset and
   further additional features suggested by community.
   Patchset borrows Mikulas's token based approach for 2 bdev
   implementation.

* [v2: FUSE BPF: A Stacked Filesystem Extension for FUSE](http://lore.kernel.org/linux-fsdevel/20221122021536.1629178-1-drosen@google.com/)

   These patches extend FUSE to be able to act as a stacked filesystem. This
   allows pure passthrough, where the fuse file system simply reflects the lower
   filesystem, and also allows optional pre and post filtering in BPF and/or the
   userspace daemon as needed. This can dramatically reduce or even eliminate
   transitions to and from userspace.

* [[PATCH linux-next] vboxfs: use strscpy() is more robust and safer](http://lore.kernel.org/linux-fsdevel/202211220858139474929@zte.com.cn/)

   The implementation of strscpy() is more robust and safer.
   That's now the recommended way to copy NUL terminated strings.

* [v2: afs: Stop implementing ->writepage()](http://lore.kernel.org/linux-fsdevel/166902976709.269117.16991162776475572981.stgit@warthog.procyon.org.uk/)

   We're trying to get rid of the ->writepage() hook[1].  Stop afs from using
   it by unlocking the page and calling afs_writepages_region() rather than
   folio_write_one().

   A flag is passed to afs_writepages_region() to indicate that it should only
   write a single region so that we don't flush the entire file in
   ->write_begin(), but do add other dirty data to the region being written to
   try and reduce the number of RPC ops.

* [v1: blk: optimization for classic polling](http://lore.kernel.org/linux-fsdevel/3578876466-3733-1-git-send-email-nj.shetty@samsung.com/)

   This removes the dependency on interrupts to wake up task. Set task
   state as TASK_RUNNING, if need_resched() returns true,
   while polling for IO completion.
   Earlier, polling task used to sleep, relying on interrupt to wake it up.
   This made some IO take very long when interrupt-coalescing is enabled in NVMe.

#### 网络设备

* [[PATCH net-next] r8169: enable GRO software interrupt coalescing per default](http://lore.kernel.org/netdev/9d94f2d8-d297-7550-2932-793a34e5efb9@gmail.com/)

   There are reports about r8169 not reaching full line speed on certain
   systems (e.g. SBC's) with a 2.5Gbps link.
   There was a time when hardware interrupt coalescing was enabled per
   default, but this was changed due to ASPM-related issues on few systems.

* [v1: pull request (net-next): ipsec-next 2022-11-26](http://lore.kernel.org/netdev/20221126110303.1859238-1-steffen.klassert@secunet.com/)

   1) Remove redundant variable in esp6.
     
   2) Update x->lastused for every packet. It was used only for outgoing mobile IPv6 packets, but showed to be usefull to check if the a SA is still in use in general. From Antony Antony.
   
   3) Remove unused variable in xfrm_byidx_resize.
   
   4) Finalize extack support for xfrm.
  
* [v1: Add check for nla_nest_start()](http://lore.kernel.org/netdev/20221126100634.106887-1-yuancan@huawei.com/)

   This series contains two patches about checking the return value of
   nla_nest_start().

* [v3: [bpf-next]: bpf: Support kernel function call in 32-bit ARM](http://lore.kernel.org/netdev/20221126094530.226629-1-yangjihong1@huawei.com/)

   1. Patch1 is dependent patch to fix zext extension error in 32-bit ARM.
   2. Patch2 supports bpf fkunc in 32-bit ARM for EABI.
   3. Patch3 is used to add test cases to cover some parameter scenarios
      states by AAPCS.
   4. Patch4 fix a comment error.

* [v6: [net-next]: optimize the parallelism of SMC-R connections](http://lore.kernel.org/netdev/1669453422-38152-1-git-send-email-alibuda@linux.alibaba.com/)

   This patch set attempts to optimize the parallelism of SMC-R connections,
   mainly to reduce unnecessary blocking on locks, and to fix exceptions that
   occur after thoses optimization.

* [v1: net-next: mptcp: MSG_FASTOPEN and TFO listener side support](http://lore.kernel.org/netdev/20221125222958.958636-1-matthieu.baerts@tessares.net/)

   Before this series, only the initiator of a connection was able to combine both
   TCP FastOpen and MPTCP when using TCP_FASTOPEN_CONNECT socket option.

* [v1: RFC net-next: net/sched: retpoline wrappers for tc](http://lore.kernel.org/netdev/20221125175207.473866-1-pctammela@mojatatu.com/)

   In tc all qdics, classifiers and actions can be compiled as modules.
   This results today in indirect calls in all transitions in the tc hierarchy.
   Due to CONFIG_RETPOLINE, CPUs with mitigations=on might pay an extra cost on
   indirect calls.
   
* [v2: can: j1939: do not wait 250 ms if the same addr was already claimed](http://lore.kernel.org/netdev/20221125170418.34575-1-devid.filoni@egluetechnologies.com/)

   The ISO 11783-5 standard, in "4.5.2 - Address claim requirements", states:
     d) No CF shall begin, or resume, transmission on the network until 250
        ms after it has successfully claimed an address except when
        responding to a request for address-claimed.

* [v2: vsock: update tools and error handling](http://lore.kernel.org/netdev/9d96f6c6-1d4f-8197-b3bc-8957124c8933@sberdevices.ru/)

   Patchset consists of two parts:

   1) Kernel patches
   Three patches from Bobby Eshleman. I took single patch from Bobby:
   https://lore.kernel.org/lkml/d81818b868216c774613dd03641fcfe63cc55a45
   .1660362668.git.bobby.eshleman@bytedance.com/ and split it to three
   patches according different parts of vsock subsystem.

* [[PATCH net v4] igb: Allocate MSI-X vector when testing](http://lore.kernel.org/netdev/20221125133031.46845-1-akihiko.odaki@daynix.com/)

   To fix this, route IRQs correctly to the first MSI-X vector by setting
   IVAR_MISC. Also, set bit 0 of EIMS so that the vector will not be
   masked. 
   
* [v1: net-next: net: pcs: altera-tse: simplify and clean-up the driver](http://lore.kernel.org/netdev/20221125131801.64234-1-maxime.chevallier@bootlin.com/)

   This small series does a bit of code cleanup in the altera TSE pcs
   driver, removong unused register definitions, handling 1000BaseX speed
   configuration correctly according to the datasheet, and making use of
   proper poll_timeout helpers.

* [v1: tc: allow gact pipe action offload](http://lore.kernel.org/netdev/20221125124932.2877006-1-vladbu@nvidia.com/)

   Flow action infrastructure and mlx5 only.

* [v1: iproute2-next: Implement new netlink attributes for devlink-rate in iproute2](http://lore.kernel.org/netdev/20221125123421.36297-1-michal.wilczynski@intel.com/)

   Patch implementing new netlink attributes for devlink-rate got merged to
   net-next.
   https://lore.kernel.org/netdev/20221115104825.172668-1-michal.wilczynski@intel.com/

* [v1: net: broadcom: Add PTP_1588_CLOCK_OPTIONAL dependency for BCMGENET under ARCH_BCM2835](http://lore.kernel.org/netdev/20221125115003.30308-1-yuehaibing@huawei.com/)

   commit 8d820bc9d12b ("net: broadcom: Fix BCMGENET Kconfig") fixes the build
   that contain 99addbe31f55 ("net: broadcom: Select BROADCOM_PHY for BCMGENET")
   and enable BCMGENET=y but PTP_1588_CLOCK_OPTIONAL=m, which otherwise
   leads to a link failure. However this may trigger a runtime failure.

* [[PATCH net-next v2] nfp: ethtool: support reporting link modes](http://lore.kernel.org/netdev/20221125113030.141642-1-simon.horman@corigine.com/)

   A new command `SPCODE_READ_MEDIA` is added to read info from
   management firmware. Also, the mapping table `nfp_eth_media_table`
   associates the link modes between NFP and kernel. Both of them
   help to support this ability.

* [[PATCH net-next] Revert "net: stmmac: use sysfs_streq() instead of strncmp()"](http://lore.kernel.org/netdev/20221125105304.3012153-1-vladimir.oltean@nxp.com/)

   This reverts commit f72cd76b05ea1ce9258484e8127932d0ea928f22.
   This patch is so broken, it hurts. Apparently no one reviewed it and it
   passed the build testing (because the code was compiled out), but it was
   obviously never compile-tested.

* [v1: net-next: Add support for lan966x IS2 VCAP](http://lore.kernel.org/netdev/20221125095010.124458-1-horatiu.vultur@microchip.com/)

   This provides initial support for lan966x for 'tc' traffic control
   userspace tool and its flower filter. For this is required to use
   the VCAP library.

* [[patch iproute2-main REPOST] devlink: load ifname map on demand from ifname_map_rev_lookup() as well](http://lore.kernel.org/netdev/20221125091251.1782079-1-jiri@resnulli.us/)

   Commit 5cddbb274eab ("devlink: load port-ifname map on demand") changed
   the ifname map to be loaded on demand from ifname_map_lookup(). However,
   it didn't put this on-demand loading into ifname_map_rev_lookup() which
   causes ifname_map_rev_lookup() to return -ENOENT all the time.

* [[PATCH wireless] mt76: mt7915: add missing of_node_put()](http://lore.kernel.org/netdev/1669367167-12983-1-git-send-email-wangyufen@huawei.com/)

   Add missing of_node_put() after of_reserved_mem_lookup()

* [v2: dt-bindings: net: realtek-bluetooth: Add RTL8723DS](http://lore.kernel.org/netdev/20221125040956.18648-1-samuel@sholland.org/)

   RTL8723DS is another variant of the RTL8723 WiFi + Bluetooth chip. It is
   already supported by the hci_uart/btrtl driver. Document the compatible.

* [v1: RESEND: can: esd_usb: Some preparation for supporting esd CAN-USB/3](http://lore.kernel.org/netdev/20221124203806.3034897-1-frank.jungclaus@esd.eu/)

   Being sucked into another higher prioritized project in the mid of
   July, my work on preparations for adding support of the newly available
   esd CAN-USB/3 to esd_usb.c came to a halt. Let's start again ...

* [[RFC hid v1 00/10] HID-BPF: add support for in-tree BPF programs](http://lore.kernel.org/netdev/20221124151603.807536-1-benjamin.tissoires@redhat.com/)

   While presenting HID-BPF, I always mentioned that device fixes should be
   integrated in the kernel. And I am trying to do that in this series.

* [v1: ipsec-next: xfrm: add extack support to some more message types](http://lore.kernel.org/netdev/cover.1668507420.git.sd@queasysnail.net/)

   This is the last part of my extack work for xfrm, adding extack
   messages to the last remaining operations: NEWSPDINFO, ALLOCSPI,
   MIGRATE, NEWAE, DELSA, EXPIRE.

* [v1: net-next: gro: simplify tcp_gro_receive](http://lore.kernel.org/netdev/20221124115143.GA73639@debian/)

   This patch series attempts to make tcp_gro_receive more readable, and easier to
   understand. The first patch also removes a few unnecessary runtime checks.

* [v2: net: usb: cdc_ether: add u-blox 0x1343 composition](http://lore.kernel.org/netdev/20221124112811.3548-1-davide.tronchin.94@gmail.com/)

   Add CDC-ECM support for LARA-L6.

   LARA-L6 module can be configured (by AT interface) in three different
   USB modes:
   * Default mode (Vendor ID: 0x1546 Product ID: 0x1341) with 4 serial
   interfaces
   * RmNet mode (Vendor ID: 0x1546 Product ID: 0x1342) with 4 serial
   interfaces and 1 RmNet virtual network interface
   * CDC-ECM mode (Vendor ID: 0x1546 Product ID: 0x1343) with 4 serial
  
* [[GIT PULL] Networking for 6.1-rc7](http://lore.kernel.org/netdev/20221124112557.17960-1-pabeni@redhat.com/)

   Mostly driver fixes with a relevant exception: the DCCP/TCP bind
   fix, which covers a quite uncommon case.

* [[PATCH wireless] wilc1000: add missing unregister_netdev() in wilc_netdev_ifc_init()](http://lore.kernel.org/netdev/1669289902-23639-1-git-send-email-wangyufen@huawei.com/)

   The root case here is alloc_ordered_workqueue() fails, but
   cfg80211_unregister_netdevice() or unregister_netdev() not be called in
   error handling path. To fix add unregister_netdev goto lable to add the
   unregister operation in error handling path.

#### 安全增强

* [v3: x86_64: Improvements at compressed kernel stage](http://lore.kernel.org/linux-hardening/cover.1668958803.git.baskov@ispras.ru/)

   This patchset is aimed
   * to improve UEFI compatibility of compressed kernel code for x86_64
   * to setup proper memory access attributes for code and rodata sections
   * to implement W^X protection policy throughout the whole execution 
     of compressed kernel for EFISTUB code path. 

* [v1: [next] drm/amdgpu: Replace remaining 1-element array with flex-array](http://lore.kernel.org/linux-hardening/Y3soBt1jmXHUKhW9@mail.google.com/)

   One-element arrays are deprecated, and we are replacing them with
   flexible array members instead. So, replace one-element array with
   flexible-array member in struct GOP_VBIOS_CONTENT and refactor the
   rest of the code accordingly.

* [v2: Add a bunch of msm8953 dts files](http://lore.kernel.org/linux-hardening/20221119203758.888207-1-luca@z3ntu.xyz/)

   The following patches add a bunch of msm8953-based devices that have
   been created in the msm8953-mainline[0] repository, which includes
   Snapdragon 450 (SDM450), Snapdragon 625 (msm8953) and Snapdragon 632 (SDM632) devices.
 
#### 异步 IO

* [v2: io_uring: clear TIF_NOTIFY_SIGNAL if set and task_work not available](http://lore.kernel.org/io-uring/93a72543-80b9-7412-61f9-9c28be9ec18e@kernel.dk/)

   If we have io-wq or SQPOLL setting the task_work notify signal but the
   task itself doesn't have task_work to process, we don't clear the
   flag and hence will enter a repeated check loop if we're waiting on
   events or file/buf references to go away.

* [v2: [liburing]: Ensure we mark non-exported functions and variables as static](http://lore.kernel.org/io-uring/20221124162633.3856761-1-ammar.faizi@intel.com/)

   This series is a -Wmissing-prototypes enforcement. -Wmissing-prototypes
   is a clang C compiler flag that warns us if we have functions or
   variables that are not used outisde the translation unit, but not marked
   as static.

* [v1: liburing: tests for deferred multishot completions](http://lore.kernel.org/io-uring/20221124103042.4129289-1-dylany@meta.com/)

   This adds a couple of tests that expose some trickier corner cases for
   multishot APIS, specifically in light of the latest series to batch &
   defer multishot completion.

* [v3: [for-next]: io_uring: batch multishot completions](http://lore.kernel.org/io-uring/20221124093559.3780686-1-dylany@meta.com/)

   Multishot completions currently all go through io_post_aux_cqe which will
   do a lock/unlock pair of the completion spinlock, and also possibly signal
   an eventfd if registered. This can slow down applications that use these features.

* [v1: [liburing]: Ensure we mark internal functions and variables as static](http://lore.kernel.org/io-uring/20221124075846.3784701-1-ammar.faizi@intel.com/)

   This series is a -Wmissing-prototypes enforcement. -Wmissing-prototypes
   is a clang C compiler flag that warns us if we have functions or
   variables that are not used outisde the translation unit, but not marked
   as static. This enforcement is good because it hints the compiler to do
   escape analysis and optimization better.

* [v2: RFC on how to include LSM hooks for io_uring commands](http://lore.kernel.org/io-uring/20221122103144.960752-1-j.granados@samsung.com/)

   The motivation for this patch is to continue the discussion around how to
   include LSM callback hooks in the io_uring infrastructure. This is the
   second version of the RFC and is meant to elicit discussion. I'll leave
   general questions and the descriptions of the different approaches.
  
* [v5: liburing: add api for napi busy poll](http://lore.kernel.org/io-uring/20221121191459.998388-1-shr@devkernel.io/)

   The patch series also contains the documentation for the two new functions
   and two example programs. The client program is called napi-busy-poll-client
   and the server program napi-busy-poll-server. The client measures the  roundtrip times of requests.

* [v5: io_uring: add napi busy polling support](http://lore.kernel.org/io-uring/20221121191437.996297-1-shr@devkernel.io/)

   This adds the napi busy polling support in io_uring.c. It adds a new
   napi_list to the io_ring_ctx structure. This list contains the list of
   napi_id's that are currently enabled for busy polling. This list is
   used to determine which napi id's enabled busy polling.

* [v3: poll_refs armoring](http://lore.kernel.org/io-uring/cover.1668963050.git.asml.silence@gmail.com/)

   Make poll_refs more robust and protected from overflows. The mechanism
   description is in 2/2. 1/2 helps to make the second patch a little bit
   cleaner by tunnelling all edge cases from arming to a tw.

#### Rust For Linux

* [v2: scripts: add rust in scripts/Makefile.package](http://lore.kernel.org/rust-for-linux/20221123220044.GA6513@DESKTOP-NK4TH6S.localdomain/)

   Add rust argument at TAR_CONTENT in
   scripts/Makefile.package script with alphabetical order.

#### BPF

* [v1: bpf: Check timer_off for map_in_map only when map value has timer](http://lore.kernel.org/bpf/20221126105351.2578782-1-hengqi.chen@gmail.com/)

   The timer_off value could be -EINVAL or -ENOENT when map value of
   inner map is struct and contains no bpf_timer.

* [[PATCH v2 bpf-next] bpf: Tighten ptr_to_btf_id checks.](http://lore.kernel.org/bpf/20221125220617.26846-1-alexei.starovoitov@gmail.com/)

   The networking programs typically don't require CAP_PERFMON, but through kfuncs
   like bpf_cast_to_kern_ctx() they can access memory through PTR_TO_BTF_ID. In
   such case enforce CAP_PERFMON.
   Also make sure that only GPL programs can access kernel data structures.

* [[PATCH bpf-next] bpf: Tighten ptr_to_btf_id checks.](http://lore.kernel.org/bpf/20221125183546.1964-1-alexei.starovoitov@gmail.com/)

   The networking programs typically don't require CAP_PERFMON, but through kfuncs
   like bpf_cast_to_kern_ctx() they can access memory through PTR_TO_BTF_ID. In
   such case enforce CAP_PERFMON. Also make sure that those programs are GPL if
   they access kernel data structures. All kfuncs require GPL anyway.

* [v5: evm: Correct inode_init_security hooks behaviors](http://lore.kernel.org/bpf/Y4Dl2yjVRkJvBflq@archlinux/)

   Fixes a NULL pointer dereference occurring in the
   `evm_protected_xattr_common` function of the EVM LSM. The bug is
   triggered if a `inode_init_security` hook returns 0 without initializing
   the given `struct xattr` fields (which is the case of BPF) and if no
   other LSM overrides thoses fields after. This also leads to memory leaks.

* [v3: [bpf-next]: bpf: Add LDX/STX/ST sanitize in jited BPF progs](http://lore.kernel.org/bpf/20221125122912.54709-1-sunhao.th@gmail.com/)

   The verifier sometimes makes mistakes[1][2] that may be exploited to
   achieve arbitrary read/write. Currently, syzbot is continuously testing
   bpf, and can find memory issues in bpf syscalls, but it can hardly find
   mischecking/bugs in the verifier. We need runtime checks like KASAN in
   BPF programs for this. 

* [[PATCH bpf-next] bpf: Don't mark arguments to fentry/fexit programs as trusted.](http://lore.kernel.org/bpf/20221124215314.55890-1-alexei.starovoitov@gmail.com/)

   The PTR_TRUSTED flag should only be applied to pointers where the verifier can
   guarantee that such pointers are valid.
   The fentry/fexit/fmod_ret programs are not in this category.
 
* [[RFC hid v1 00/10] HID-BPF: add support for in-tree BPF programs](http://lore.kernel.org/bpf/20221124151603.807536-1-benjamin.tissoires@redhat.com/)

   While presenting HID-BPF, I always mentioned that device fixes should be
   integrated in the kernel. And I am trying to do that in this series.

* [v1: powerpc/bpf: Only update ldimm64 during extra pass when it is an address](http://lore.kernel.org/bpf/3f6d302a2068d9e357efda2d92c8da99a0f2d0b2.1669278892.git.christophe.leroy@csgroup.eu/)

   ldimm64 is not only used for loading function addresses, and
   the NOPs added for padding are impacting performance, so avoid
   them when not necessary.

* [v10: [bpf-next]: bpf: Add bpf_rcu_read_lock() support](http://lore.kernel.org/bpf/20221124053201.2372298-1-yhs@fb.com/)

   Currently, without rcu attribute info in BTF, the verifier treats
   rcu tagged pointer as a normal pointer. This might be a problem
   for sleepable program where rcu_read_lock()/unlock() is not available.

* [v5: [net-next]: net: lan966x: Extend xdp support](http://lore.kernel.org/bpf/20221123203139.3828548-1-horatiu.vultur@microchip.com/)

   Extend the current support of XDP in lan966x with the action XDP_TX and
   XDP_REDIRECT.
   The first patches just prepare the things such that it would be easier
   to add XDP_TX and XDP_REDIRECT actions. Like adding XDP_PACKET_HEADROOM,
   introduce helper functions, use the correct dma_dir for the page pool
 
* [[PATCH bpf-next] selftests/bpf: Mount debugfs in setns_by_fd](http://lore.kernel.org/bpf/20221123200829.2226254-1-sdf@google.com/)

   Jiri reports broken test_progs after recent commit 68f8e3d4b916
   ("selftests/bpf: Make sure zero-len skbs aren't redirectable").
   Apparently we don't remount debugfs when we switch back networking namespace.
   Let's explicitly mount /sys/kernel/debug.

* [[RFC bpf-next 0/5] bpf: making BTF self-describing](http://lore.kernel.org/bpf/1669225312-28949-1-git-send-email-alan.maguire@oracle.com/)

   One problem with the BPF Type Format (BTF) is that it is hard
   to extend.  BTF consists of a set of kinds, each representing
   an aspect of type or variable information such as an integer,
   struct, array and so on.  
   
* [v1: bpf-next: bpf: Add LDX/STX/ST sanitize in jited BPF progs](http://lore.kernel.org/bpf/20221123141546.238297-1-sunhao.th@gmail.com/)

   The verifier sometimes makes mistakes[1][2] that may be exploited to
   achieve arbitrary read/write. Currently, syzbot is continuously testing
   bpf, and can find memory issues in bpf syscalls, but it can hardly find
   mischecking/bugs in the verifier. We need runtime checks like KASAN in
   BPF programs for this. 
   
* [[PATCH bpf-next v3] bpf, docs: Document BPF_MAP_TYPE_BLOOM_FILTER](http://lore.kernel.org/bpf/20221123141151.54556-1-donald.hunter@gmail.com/)

   Add documentation for BPF_MAP_TYPE_BLOOM_FILTER including
   kernel BPF helper usage, userspace usage and examples.

* [[PATCH bpf-next] bpf: Don't use idx variable when registering kfunc dtors](http://lore.kernel.org/bpf/20221123135253.637525-1-void@manifault.com/)

   In commit fda01efc6160 ("bpf: Enable cgroups to be used as kptrs"), I
   added an 'int idx' variable to kfunc_init() which was meant to
   dynamically set the index of the btf id entries of the
   'generic_dtor_ids' array. 

* [v1: bpf-next: Add kfunc for doing pid -> task lookup](http://lore.kernel.org/bpf/20221122145300.251210-1-void@manifault.com/)

   A series of prior patches added support for storing struct task_struct *
   objects as kptrs. This patch set proposes extending this with another
   kfunc called bpf_task_from_pid() which performs a lookup of a task from
   its pid, from the root pid namespace idr.

* [[PATCH bpf-next v2] bpf, docs: Document BPF_MAP_TYPE_BLOOM_FILTER](http://lore.kernel.org/bpf/20221122140824.89305-1-donald.hunter@gmail.com/)

   Add documentation for BPF_MAP_TYPE_BLOOM_FILTER including
   kernel BPF helper usage, userspace usage and examples.

* [v1: virtio_net: support multi buffer xdp](http://lore.kernel.org/bpf/20221122074348.88601-1-hengqi@linux.alibaba.com/)

   Currently, virtio net only supports xdp for single-buffer packets
   or linearized multi-buffer packets. This patchset supports xdp for
   multi-buffer packets, then GRO_HW related features can be
   negotiated, and do not affect the processing of single-buffer xdp.

* [v1: bpf-next: Support storing struct cgroup * objects as kptrs](http://lore.kernel.org/bpf/20221122055458.173143-1-void@manifault.com/)

   In [0], we added support for storing struct task_struct * objects as
   kptrs. This patch set extends this effort to also include storing struct cgroup * object as kptrs.

* [[PATCH bpf-next v4] docs/bpf: Update btf selftests program and add link](http://lore.kernel.org/bpf/tencent_1FA6904156E8E599CAE4ABDBE80F22830106@qq.com/)

   commit c64779e24e88("selftests/bpf: Merge most of test_btf into
   test_progs") rename selftests/bpf btf test from 'test_btf.c' to 'prog_tests/btf.c'.

* [[PATCH bpf-next] bpf: Restrict attachment of bpf program to some tracepoints](http://lore.kernel.org/bpf/20221121213123.1373229-1-jolsa@kernel.org/)

   We hit following issues [1] [2] when we attach bpf program that calls
   bpf_trace_printk helper to the contention_begin tracepoint.

   As described in [3] with multiple bpf programs that call bpf_trace_printk
   helper attached to the contention_begin might result in exhaustion of
   printk buffer or cause a deadlock [2].

* [v2: [bpf-next]: xdp: hints via kfuncs](http://lore.kernel.org/bpf/20221121182552.2152891-1-sdf@google.com/)

   Please see the first patch in the series for the overall design and use-cases.

* [[PATCH bpf-next v6] docs/bpf: Add table of BPF program types to libbpf docs](http://lore.kernel.org/bpf/20221121121734.98329-1-donald.hunter@gmail.com/)

   Extend the libbpf documentation with a table of program types,
   attach points and ELF section names.

   This latest version of the patchset uses an inline table that is
   formatted by hand. The section name extras are documented in footnotes
   with cross references from within the table.

* [v3: [bpf-next]: bpf: Pin the start cgroup for cgroup iterator](http://lore.kernel.org/bpf/20221121073440.1828292-1-houtao@huaweicloud.com/)

   The patchset tries to fix the potential use-after-free problem in cgroup
   iterator. The problem is similar with the UAF problem fixed in map
   iterator and the fix is also similar: pinning the iterated resource in
   .init_seq_private() and unpinning it in .fini_seq_private(). An
   alternative fix is pinning iterator link when opening iterator fd, but
   it will make iterator link still being visible after the close of
   iterator link fd and the behavior is different with other link types, so just fixing the bug alone by pinning the start cgroup when creating cgroup iterator. 

* [[PATCH bpf-next] bpf: Disallow bpf_obj_new_impl call when bpf_mem_alloc_init fails](http://lore.kernel.org/bpf/20221120212610.2361700-1-memxor@gmail.com/)

   In the unlikely event that bpf_global_ma is not correctly initialized,
   instead of checking the boolean everytime bpf_obj_new_impl is called,
   simply check it while loading the program and return an error if bpf_global_ma_set is false.

### 周边技术动态

#### Qemu

* [v2: riscv: Add RISCVCPUConfig.satp_mode to set sv48, sv57, etc.](http://lore.kernel.org/qemu-devel/20221125105954.149267-1-alexghiti@rivosinc.com/)

   RISC-V specifies multiple sizes for addressable memory and Linux probes for
   the machine's support at startup via the satp CSR register (done in
   csr.c:validate_vm).

* [v2: target/riscv: Add some comments for sstatus CSR in riscv_cpu_dump_state()](http://lore.kernel.org/qemu-devel/20221125050354.3166023-1-bmeng@tinylab.org/)

   sstatus register dump is currently missing in riscv_cpu_dump_state().
   As sstatus is a copy of mstatus, which is described in the priv spec,
   it seems redundant to print the same information twice.

* [v1: target/riscv: support cache-related PMU events in virtual mode](http://lore.kernel.org/qemu-devel/20221123090635.6574-1-jim.shu@sifive.com/)

   let tlb_fill() function also increments PMU counter when it is from
   two-stage translation, so QEMU could also monitor these PMU events when CPU runs in VS/VU mode (like running guest OS).

* [v1: target/riscv: Dump sstatus CSR in riscv_cpu_dump_state()](http://lore.kernel.org/qemu-devel/20221122154628.3138131-1-bmeng@tinylab.org/)

   sstatus register dump is currently missing in riscv_cpu_dump_state().

#### Buildroot

* [Xtensa qemu nommu defconfig not working](http://lore.kernel.org/buildroot/20221125191155.111c4ddc@windsurf/)

   There were some changes in elf2flt:

   9dd179d43ffe9be98035f921a0db2bf051b632fb package/elf2flt: fix fatal error regression on m68k, xtensa, riscv64
   b07210b272c6e8810b038190cf5c1fd1f7b48397 package/gcc: remove BR2_GCC_ENABLE_LTO
   9db5eb258cf492567bac33a33cb606f14045639d package/elf2flt: Remove Config.in.host

* [[branch/next] configs/andes_ae350_45: bump OpenSBI, U-Boot and Linux](http://lore.kernel.org/buildroot/20221124215705.667CC8462D@busybox.osuosl.org/)

   Linux kernel is hosted on AndesTech Github which includes ethernet,
   SD card, DMAC, RTC, WDT drivers support. OpenSBI is based on v1.1 with andes platfrom fdt driver.

* [[branch/next] package/z3: new package](http://lore.kernel.org/buildroot/20221120135623.522B183FD2@busybox.osuosl.org/)

   commit: https://git.buildroot.net/buildroot/commit/?id=2ad68ff8df71a485085b0b83ff494081e4d926b7
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/next
   Z3, also known as the Z3 Theorem Prover, is a cross-platform satisfiability modulo theories (SMT) solver.

## 20221120：第 21 期

### 内核动态

#### RISC-V 架构支持

* [v4: riscv/ftrace: make function graph use ftrace directly](http://lore.kernel.org/linux-riscv/20221120084230.910152-1-suagrfillet@gmail.com/)

   In RISC-V architecture, when we enable the ftrace_graph tracer on some
   functions, the function tracings on other functions will suffer extra
   graph tracing work. In essence, graph_ops isn't limited by its func_hash
   due to the global ftrace_graph_[regs]_call label. That should be corrected.

 

* [v1: riscv: add Bouffalolab bl808 support](http://lore.kernel.org/linux-riscv/20221120082114.3030-1-jszhang@kernel.org/)

   This series adds Bouffalolab uart driver and basic devicetrees for
   Bouffalolab bl808 SoC and Sipeed M1S dock board.

   It's too late for v6.2-rc1, but I hope I can catch up the v6.3-rc1
   window.

* [v1: riscv: defconfig: add SERIAL_8250_DW](http://lore.kernel.org/linux-riscv/20221119121953.3897171-1-clabbe@baylibre.com/)

   jh7100-starfive-visionfive-v1 DTB was recently added, but all my try to
   boot it in kernelCI failed.
   This is due to a missing serial driver in defconfig.
   So let's add CONFIG_SERIAL_8250_DW which is needed.

* [v7: riscv, mm: detect svnapot cpu support at runtime](http://lore.kernel.org/linux-riscv/20221119112242.3593646-1-panqinglin2020@iscas.ac.cn/)

   Svnapot is a RISC-V extension for marking contiguous 4K pages as a non-4K
   page. This patch set is for using Svnapot in hugetlb fs and huge vmap.

   This patchset adds a Kconfig item for using Svnapot in
   "Platform type"->"SVNAPOT extension support". Its default value is on,
   and people can set it off if they don't allow kernel to detect Svnapot
   hardware support and leverage it.

   
* [v2: Documentation: riscv: Document the sv57 VM layout](http://lore.kernel.org/linux-riscv/20221118171556.1612190-1-bjorn@kernel.org/)

   From: Bj&#246;rn T&#246;pel <bjorn@rivosinc.com>

   RISC-V has been supporting the "sv57" address translation mode for a
   while, but is has not been added to the VM layout documentation. Let
   us fix that.

* [v1: riscv: dts: renesas: rzfive-smarc-som: Enable WDT](http://lore.kernel.org/linux-riscv/20221118135715.14410-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   Enable WDT node on RZ/Five SMARC SoM.

   Note, WDT block is enabled in RZ/G2UL SMARC SoM DTSI [0] hence deleting
   the disabled node from RZ/Five SMARC SoM DTSI enables it here too as we
   include [0] in RZ/Five SMARC SoM DTSI.

   [0] arch/arm64/boot/dts/renesas/rzg2ul-smarc-som.dtsi

* [v1: JH7110 Power Domain Support](http://lore.kernel.org/linux-riscv/20221118133216.17037-1-walker.chen@starfivetech.com/)

   This patchset adds power domain controller driver for the StarFive JH7110 SoC.
   The series has been tested on the VisionFive 2 board.

* [v2: Basic device tree support for StarFive JH7110 RISC-V SoC](http://lore.kernel.org/linux-riscv/20221118011714.70877-1-hal.feng@starfivetech.com/)

   The original patch series "Basic StarFive JH7110 RISC-V SoC support" [1]
   is split into 3 patch series. They respectively add basic clock&reset,
   pinctrl and device tree support for StarFive JH7110 SoC. These patch
   series are independent, but the Visionfive2 board can boot up successfully
   only if all these patches series applied. This one adds basic device
   tree support. 

* [v1: irqchip/sifive-plic: default to enabled](http://lore.kernel.org/linux-riscv/20221117185942.3896559-1-conor@kernel.org/)

   The SiFive PLIC driver is used by all current implementations, including
   those that do not have a SiFive PLIC. Default the driver to enabled,
   with the intention of later removing the current "every SOC selects
   this" situation in Kconfig.socs at the moment.

   The speculative "potential others" in the description no longer makes
   any sense, as the driver is always used. Update the Kconfig symbol's
   description to reflect the driver's ubiquitous state.

  

* [v1: PCI: microchip: Partition address translations](http://lore.kernel.org/linux-riscv/20221116135504.258687-1-daire.mcnamara@microchip.com/)

   Microchip PolarFire SoC is a 64-bit device and has DDR starting at
   Coreplex via an FPGA fabric. The AXI connections between the Coreplex and
   the fabric are 64-bit and the AXI connections between the fabric and the
   rootport are 32-bit.  For the CPU CorePlex to act as an AXI-Master to the
   PCIe devices and for the PCIe devices to act as bus masters to DDR at these
   base addresses, the fabric can be customised to add/remove offsets for bits
   customer's design.

  
* [v1: riscv: Implement ioremap_prot support](http://lore.kernel.org/linux-riscv/20221116025709.27368-1-jiangjianwen@uniontech.com/)

   Feature ioremap_prot only needs an implementation of pte_pgprot on riscv.
   That macro is similar on platform loongarch, mips and sh.
   We just need to replace _PFN_MASK with _PAGE_PFN_MASK, and select
   HAVE_IOREMAP_PROT in arch/riscv/Kconfig.

* [v5: scripts/gdb: add lx_current support for riscv](http://lore.kernel.org/linux-riscv/20221115221051.1871569-1-debug@rivosinc.com/)

   csr_sscratch CSR holds current task_struct address when hart is in
   user space. Trap handler on entry spills csr_sscratch into "tp" (x2)
   register and zeroes out csr_sscratch CSR. Trap handler on exit reloads
   "tp" with expected user mode value and place current task_struct address
   again in csr_sscratch CSR.

   
* [v2: RISC-V: Dynamic ftrace support for RV32I](http://lore.kernel.org/linux-riscv/20221115200832.706370-1-jamie@jamieiles.com/)

   This series enables dynamic ftrace support for RV32I bringing it to 
   parity with RV64I.  Most of the work is already there, this is largely 
   just assembly fixes to handle register sizes, correct handling of the 
   psABI calling convention and Kconfig change.

   Validated with all ftrace boot time self test with qemu for RV32I and 
   RV64I in addition to real tracing on an RV32I FPGA design.

* [v1: RZ/Five: Enable ADC/CANFD/I2C/OPP/Thermal Zones/TSU](http://lore.kernel.org/linux-riscv/20221115105135.1180490-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   Hi All,

   This patch series aims to enable support for below blocks
   on RZ/Five SoC/SMARC EVK:
   - ADC
   - CANFD
   - I2C
   - OPP
   - Thermal Zones
   - TSU

   Note, patches apply on top of [0].

   [0] https://git.kernel.org/pub/scm/linux/kernel/git/geert/renesas-devel.git/log/?h=renesas-riscv-dt-for-v6.2

   Cheers,
   Prabhakar

* [v2: riscv: mm: Proper page permissions after initmem free](http://lore.kernel.org/linux-riscv/20221115090641.258476-1-bjorn@kernel.org/)

   From: Bj&#246;rn T&#246;pel <bjorn@rivosinc.com>

   64-bit RISC-V kernels have the kernel image mapped separately to alias
   the linear map. The linear map and the kernel image map are documented
   as "direct mapping" and "kernel" respectively in [1].

   At image load time, the linear map corresponding to the kernel image
   is set to PAGE_READ permission, and the kernel image map is set to
   PAGE_READ|PAGE_EXEC.

   When the initmem is freed, the pages in the linear map should be
   restored to PAGE_READ|PAGE_WRITE, whereas the corresponding pages in
   the kernel image map should be restored to PAGE_READ, by removing the
   PAGE_EXEC permission.

* [v2: net:macb: driver support acpi mode](http://lore.kernel.org/linux-riscv/20221114114126.1881-1-xiaowu.ding@jaguarmicro.com/)

   Cadence gem driver suuport acpi mode. Current the macb driver
   just support device tree mode.So we add new acpi mode code with
   this driver.

* [v11: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20221114093904.1669461-1-apatel@ventanamicro.com/)

   This series aims to improve IPI support in Linux RISC-V in following ways:
    1) Treat IPIs as normal per-CPU interrupts instead of having custom RISC-V
       specific hooks. This also makes Linux RISC-V IPI support aligned with
       other architectures.
    2) Remote TLB flushes and icache flushes should prefer local IPIs instead
       of SBI calls whenever we have specialized hardware (such as RISC-V AIA
       IMSIC and RISC-V SWI) which allows S-mode software to directly inject
       IPIs without any assistance from M-mode runtime firmware.
  
* [v6: Add PMEM support for RISC-V](http://lore.kernel.org/linux-riscv/20221114090536.1662624-1-apatel@ventanamicro.com/)

   The Linux NVDIMM PEM drivers require arch support to map and access the
   persistent memory device. This series adds RISC-V PMEM support using
   recently added Svpbmt and Zicbom support.

   First two patches are fixes and remaining two patches add the required
   PMEM support for Linux RISC-V.

  

* [v1: riscv: Update k210 defconfigs](http://lore.kernel.org/linux-riscv/20221114013151.714578-1-damien.lemoal@opensource.wdc.com/)

   In preparation for SLOB deprecation, update the nommu_k210_* defconfigs
   to switch to using SLUB. To save memory, CONFIG_SLUB_CPU_PARTIAL is not
   selected.

* [v1: RISC-V: KVM: Simplify kvm_arch_prepare_memory_region()](http://lore.kernel.org/linux-riscv/c5e918630ba37273d7b0f4e4dbb6f90d4c2f321d.1668347565.git.christophe.jaillet@wanadoo.fr/)

   In kvm_arch_prepare_memory_region(), if no error occurs, a spin_lock()/
   spin_unlock() call can be avoided.

   Switch to kvm_riscv_gstage_iounmap() that is the same as the current code,
   but with a better semantic.
   It also embeds the locking logic. So it is avoided if ret == 0.

#### 进程调度

* [[GIT PULL] sched/urgent for 6.1-rc6](http://lore.kernel.org/lkml/Y3oU5cB9LOcBLfSS@zn.tnic/)

  

   The following changes since commit 094226ad94f471a9f19e8f8e7140a09c2625abaa:

     Linux 6.1-rc5 (2022-11-13 13:12:55 -0800)

   are available in the Git repository at:

     git://git.kernel.org/pub/scm/linux/kernel/git/tip/tip.git tags/sched_urgent_for_v6.1_rc6

   for you to fetch changes up to 91dabf33ae5df271da63e87ad7833e5fdb4a44b9:

     sched: Fix race in task_call_func() (2022-11-14 09:58:32 +0100)

   - Fix a small race on the task's exit path where there's a
   misunderstanding whether the task holds rq->lock or not

   - Prevent processes from getting killed when using deprecated or unknown
   rseq ABI flags in order to be able to fuzz the rseq() syscall with
   syzkaller

   
* [[PATCH-tip] sched: Don't call kfree() in do_set_cpus_allowed()](http://lore.kernel.org/lkml/20221118193302.522399-1-longman@redhat.com/)

   Commit 851a723e45d1 ("sched: Always clear user_cpus_ptr in
   do_set_cpus_allowed()") may call kfree() if user_cpus_ptr was previously
   set. Unfortunately, some of the callers of do_set_cpus_allowed()
   may not be in a context where kfree() can be safely called. So the
   following splats may be printed:

      WARNING: possible circular locking dependency detected
      BUG: sleeping function called from invalid context

* [[RESEND PATCH v3] sched/topology: Add __init for init_defrootdomain](http://lore.kernel.org/lkml/20221118034208.267330-1-huangbing775@126.com/)

   init_defrootdomain is only used in initialization

* [v5: sched/fair: Introduce priority load balance for CFS](http://lore.kernel.org/lkml/20221117131246.202545-1-zhangsong34@huawei.com/)

   For co-location with idle and non-idle tasks, when CFS do load balance,
   it is reasonable to prefer migrating non-idle tasks and migrating idle
   tasks lastly to improve QoS of non-idle(Latency Sensitive) tasks.

   Consider a simple scenario. Assume that CPU0 has two non-idle tasks 
   whose weight is 2*1024=2048, also CPU0 has 3000 idle tasks whose 
   weight is 3000*3 = 9000. Now CPU1 is idle and IDLE load balance is 
   triggered. CPU1 needs to pull a certain number of tasks from CPU0.

  

* [v3: sched: async unthrottling for cfs bandwidth](http://lore.kernel.org/lkml/20221117005418.3499691-1-joshdon@google.com/)

   CFS bandwidth currently distributes new runtime and unthrottles cfs_rq's
   inline in an hrtimer callback. Runtime distribution is a per-cpu
   operation, and unthrottling is a per-cgroup operation, since a tg walk
   is required. On machines with a large number of cpus and large cgroup
   hierarchies, this cpus*cgroups work can be too much to do in a single
   hrtimer callback: since IRQ are disabled, hard lockups may easily occur.
   Specifically, we've found this scalability issue on configurations with
   256 cpus, O(1000) cgroups in the hierarchy being throttled, and high
   memory bandwidth usage.


* [v1: sched/core: use correct cpu_capacity in wake_affine_weight()](http://lore.kernel.org/lkml/20221116091027.78906-1-wanghonglei@didiglobal.com/)

   It seems make more sense to do the load weight calculation with
   respective cpu_capacity.

* [v1: sched: use nr_cpus_node() here](http://lore.kernel.org/lkml/Y3Rd8B7oRjds4yCB@localhost.localdomain/)

   use nr_cpus_node() here

* [v1: sched/rt: Use cpu_active_mask to prevent rto_push_irq_work's dead loop](http://lore.kernel.org/lkml/20221114120453.3233-1-xuewen.yan@unisoc.com/)

   We are performing the stress test related to cpu hotplug,
   when there are only two cpus left in the system(for example: cpu0 and cpu1),
   if cpu1 is offline at this time, the following infinite loop may occur:

   When cpu0 contains more than one rt task, it will push the other rt tasks
   to cpux for execution. If cpu1 is going through the hotplug process at this time,
   it woule start a stop_machine to be responsible for migrating the tasks on cpu1
   to other online cpus. 

#### 内存管理

* [v7: mm: add zblock - new allocator for use via zpool API](http://lore.kernel.org/linux-mm/20221119082159.63636-1-a.badmaev@clicknet.pro/)

       Zblock stores integer number of compressed objects per zblock block.
   These blocks consist of several physical pages (1/2/4/8) and are arranged
   in linked lists.
       The range from 0 to PAGE_SIZE is divided into the number of intervals
   corresponding to the number of lists and each list only operates objects
   of size from its interval. Thus the block lists are isolated from each
   other, which makes it possible to simultaneously perform actions with
   several objects from different lists.
       
* [v1: maple_tree: allow TEST_MAPLE_TREE only when DEBUG_KERNEL is set](http://lore.kernel.org/linux-mm/20221119055117.14094-1-rdunlap@infradead.org/)

   Prevent a kconfig warning that is caused by TEST_MAPLE_TREE by adding a
   "depends on" clause for TEST_MAPLE_TREE since 'select' does not follow
   any kconfig dependencies.

   WARNING: unmet direct dependencies detected for DEBUG_MAPLE_TREE
     Depends on [n]: DEBUG_KERNEL [=n]
     Selected by [y]:
     - TEST_MAPLE_TREE [=y] && RUNTIME_TESTING_MENU [=y]

   Fixes: 120b116208a0 ("maple_tree: reorganize testing to restore module testing")

* [v6: Implement writeback for zsmalloc](http://lore.kernel.org/linux-mm/20221119001536.2086599-1-nphamcs@gmail.com/)

   Changelog:
   v6:
     * Move the move-to-front logic into zs_map_object (patch 4)
       (suggested by Minchan Kim).
     * Small clean up for free_zspage at free_handles() call site
       (patch 6) (suggested by Minchan Kim).
   v5:
     * Add a new patch that eliminates unused code in zpool and simplify
       the logic for storing evict handler in zbud/z3fold (patch 2)
     * Remove redudant fields in zs_pool (previously required by zpool)
       (patch 3)
     * Wrap under_reclaim and deferred handle freeing logic in CONFIG_ZPOOL
       (patch 6) (suggested by Minchan Kim)
     * Move a small piece of refactoring from patch 6 to patch 4.
  

   This series of patches implements writeback for zsmalloc. When the zswap
   pool becomes full, zsmalloc will attempt to evict all the compressed
   objects in the least-recently used zspages.
   
* [v1: Documentation: admin-guide: correct "it's" to possessive "its"](http://lore.kernel.org/linux-mm/20221118232317.3244-1-rdunlap@infradead.org/)

   Correct 2 uses of "it's" to the possessive "its" as needed.

* [v3: Leave IRQs enabled for per-cpu page allocations](http://lore.kernel.org/linux-mm/20221118101714.19590-1-mgorman@techsingularity.net/)

   Changelog since V2
   o Fix list corruption issue					(hughd)
   o Tighen up locking protocol					(hughd)
   o Micro-optimisations						(hughd)

   Changelog since V1
   o Use trylock in free_unref_page_list due to IO completion from
     softirq context						(yuzhao)

   This was a standalone patch but now split in two. The main changes since
   v2 are fixing the issues exposed by Hugh's shmfs stress test.

   Patch 1 is a long-standing issue that is technically a bug but happened
   to work because of how it was used.

* [v1: mm,thp,rmap: rework the use of subpages_mapcount](http://lore.kernel.org/linux-mm/c4b8485b-1f26-1a5f-bdf-c6c22611f610@google.com/)

   Linus was underwhelmed by the earlier compound mapcounts series:
   this series builds on top of it (as in next-20221117) to follow
   up on his suggestions - except rmap.c still using lock_page_memcg(),
   since I hesitate to steal the pleasure of deletion from Johannes.

* [v2: mm: Make ksize() a reporting-only function](http://lore.kernel.org/linux-mm/20221118035656.gonna.698-kees@kernel.org/)

   With all "silently resizing" callers of ksize() refactored, remove the
   logic in ksize() that would allow it to be used to effectively change
   the size of an allocation (bypassing __alloc_size hints, etc). Users
   wanting this feature need to either use kmalloc_size_roundup() before an
   allocation, or use krealloc() directly.

   For kfree_sensitive(), move the unpoisoning logic inline. Replace the
   some of the partially open-coded ksize() in __do_krealloc with ksize()
   now that it doesn't perform unpoisoning.

* [v2: slab: Provide full coverage for __alloc_size attribute](http://lore.kernel.org/linux-mm/20221118034713.gonna.754-kees@kernel.org/)

   Hi,

   These patches work around a deficiency in GCC (>=11) and Clang (<16)
   where the __alloc_size attribute does not apply to inlines. :(
   https://gcc.gnu.org/bugzilla/show_bug.cgi?id=96503

   This manifests as reduced overflow detection coverage for many allocation
   sites under CONFIG_FORTIFY_SOURCE=y, where the allocation size was not
   actually being propagated to __builtin_dynamic_object_size(). The problem
   was in two halves: the trace wrapper (now fixed in -next), and const-0
   special-casing (covered here).

   Thanks,

   -Kees

* [v7: Memory poison recovery in khugepaged collapsing](http://lore.kernel.org/linux-mm/20221118013157.1333622-1-jiaqiyan@google.com/)

   Memory DIMMs are subject to multi-bit flips, i.e. memory errors.
   As memory size and density increase, the chances of and number of
   memory errors increase. The increasing size and density of server
   RAM in the data center and cloud have shown increased uncorrectable
   memory errors. There are already mechanisms in the kernel to recover
   from uncorrectable memory errors. This series of patches provides
   the recovery mechanism for the particular kernel agent khugepaged
   when it collapses memory pages.

* [v2: mm/hugetlb: Make huge_pte_offset() thread-safe for pmd unshare](http://lore.kernel.org/linux-mm/20221118011025.2178986-1-peterx@redhat.com/)

   Based on latest mm-unstable (96aa38b69507).

   This can be seen as a follow-up series to Mike's recent hugetlb vma lock
   series for pmd unsharing, so this series also depends on that one.
   Hopefully this series can make it a more complete resolution for pmd
   unsharing.

   PS: so far no one strongly ACKed this, let me keep the RFC tag.  But I
   think I'm already more confident than many of the RFCs I posted.

   PS2: there're a lot of changes comparing to rfcv1, so I'm just not adding
   the changelog.  The whole idea is still the same, though.


* [v3: exit: Put an upper limit on how often we can oops](http://lore.kernel.org/linux-mm/20221117233838.give.484-kees@kernel.org/)

   Hi,

   This builds on Jann's v1 patch[1]. Changes in v3:
   - fix #if/#ifdef confusion (Bill)
   - rename from "reason" or "origin" and add it to the warn output (Marco)

   v2: https://lore.kernel.org/lkml/20221109194404.gonna.558-kees@kernel.org/

   Thanks,

   -Kees

   [1] https://lore.kernel.org/lkml/20221107201317.324457-1-jannh@google.com

* [v3: [mm-unstable]: convert core hugetlb functions to folios](http://lore.kernel.org/linux-mm/20221117211501.17150-1-sidhartha.kumar@oracle.com/)

   Now that many hugetlb helper functions that deal with hugetlb specific
   flags[1] and hugetlb cgroups[2] are converted to folios, higher level
   allocation, prep, and freeing functions within hugetlb can also be
   converted to operate in folios.

   Patch 1 of this series implements the wrapper functions around setting
   the compound destructor and compound order for a folio. Besides the user
   added in patch 1, patch 2 and patch 9 also use these helper functions.

   Patches 2-10 convert the higher level hugetlb functions to folios.

  
* [v4: [bpf-next]: execmem_alloc for BPF programs](http://lore.kernel.org/linux-mm/20221117202322.944661-1-song@kernel.org/)

   This patchset tries to address the following issues:

   1. Direct map fragmentation

   On x86, STRICT_*_RWX requires the direct map of any RO+X memory to be also
   RO+X. These set_memory_* calls cause 1GB page table entries to be split
   into 2MB and 4kB ones. This fragmentation in direct map results in bigger
   and slower page table, and pressure for both instruction and data TLB.

   2. iTLB pressure from BPF program

   Dynamic kernel text such as modules and BPF programs (even with current bpf_prog_pack) use 4kB pages on x86, when the total size of modules and BPF program is big.

   3. TLB shootdown for short-living BPF programs

   Before bpf_prog_pack loading and unloading BPF programs requires global
   TLB shootdown. This patchset (and bpf_prog_pack) replaces it with a local
   TLB flush.

   4. Reduce memory usage by BPF programs (in some cases)

   Most BPF programs and various trampolines are small, and they often
   occupies a whole page. From a random server in our fleet, 50% of the
   loaded BPF programs are less than 500 byte in size, and 75% of them are
   less than 2kB in size.

   5. Introduce a unified API to allocate memory with special permissions.

   This will help get rid of set_vm_flush_reset_perms calls from users of
   vmalloc, module_alloc, etc.

* [v1: iov_iter: Add extraction helpers](http://lore.kernel.org/linux-mm/166869687556.3723671.10061142538708346995.stgit@warthog.procyon.org.uk/)

   Hi Al,

   Here are four patches to provide support for extracting pages from an
   iov_iter, where such a thing makes sense, if you could take a look?

   The first couple of patches provide iov_iter general stuff:

    (1) Move the FOLL_* flags to linux/mm_types.h so that linux/uio.h can make
        use of them.

    (2) Add a function to list-only, get or pin pages from an iterator as a
        future replacement for iov_iter_get_pages*().  Pointers to the pages
        are placed into an array (which will get allocated if not provided)
        and, depending on the iterator type and direction, the pages will have
        a ref or a pin get on them, or left untouched (on the assumption that
        the caller manages their lifetime).

   Then there are a couple of patches that add stuff to netfslib that I want
   to use there as well as in cifs:

    (3) Add a netfslib function to use (2) to extract pages from an ITER_IOBUF
        or ITER_UBUF iterator into an ITER_BVEC iterator.  This will get or
        pin the pages as appropriate.

    (4) Add a netfslib function to extract pages from an iterator that's of
        type ITER_UBUF/IOVEC/BVEC/KVEC/XARRAY and add them to a scatterlist.

* [v7: arm64: support batched/deferred tlb shootdown during page reclamation](http://lore.kernel.org/linux-mm/20221117082648.47526-1-yangyicong@huawei.com/)

   Though ARM64 has the hardware to do tlb shootdown, the hardware
   broadcasting is not free.
   A simplest micro benchmark shows even on snapdragon 888 with only
   8 cores, the overhead for ptep_clear_flush is huge even for paging
   out one page mapped by only one process:

   While pages are mapped by multiple processes or HW has more CPUs,
   the cost should become even higher due to the bad scalability of
   tlb shootdown.

* [[PATCH RESEND] mm: Kconfig: make config SECRETMEM visible with EXPERT](http://lore.kernel.org/linux-mm/20221116131922.25533-1-lukas.bulwahn@gmail.com/)

   Commit 6a108a14fa35 ("kconfig: rename CONFIG_EMBEDDED to CONFIG_EXPERT")
   introduces CONFIG_EXPERT to carry the previous intent of CONFIG_EMBEDDED
   and just gives that intent a much better name. That has been clearly a good
   and long overdue renaming, and it is clearly an improvement to the kernel
   build configuration that has shown to help managing the kernel build
   configuration in the last decade.

   However, rather than bravely and radically just deleting CONFIG_EMBEDDED,
  
* [[PATCH -next] migrate: stop using 0 as NULL pointer](http://lore.kernel.org/linux-mm/20221116012345.84870-1-yang.lee@linux.alibaba.com/)

   mm/migrate.c:1198:24: warning: Using plain integer as NULL pointer

   Link: https://bugzilla.openanolis.cn/show_bug.cgi?id=3080

* [v1: mm-unstable: convert core hugetlb functions to folios](http://lore.kernel.org/linux-mm/20221115212217.19539-1-sidhartha.kumar@oracle.com/)

   Patch 1 of this series implements the wrapper functions around setting
   the compound destructor and compound order for a folio. Besides the user
   added in patch 1, patch 2 and patch 9 also use these helper functions.

   Patches 2-10 convert the higher level hugetlb functions to folios.

* [v1: epoll: use refcount to reduce ep_mutex contention](http://lore.kernel.org/linux-fsdevel/d83b1962271d06c18d97f05266ea37241ee435e2.1668793437.git.pabeni@redhat.com/)
  
   To reduce the contention this patch introduces explicit reference counting
   for the eventpoll struct. Each registered event acquires a reference,
   and references are released at ep_remove() time. ep_free() doesn't touch
   anymore the event RB tree, it just unregisters the existing callbacks
   and drops a reference to the ep struct. The struct itself is freed when
   the reference count reaches 0. 
   
* [v2: namespace: Added pointer check in copy_mnt_ns()](http://lore.kernel.org/linux-fsdevel/20221118114137.128088-1-arefev@swemel.ru/)

   Return value of a function 'next_mnt' is dereferenced at
   namespace.c:3377 without checking for null,
   but it is usually checked for this function

   Found by Linux Verification Center (linuxtesting.org) with SVACE.

* [v1: afs: Stop implementing ->writepage()](http://lore.kernel.org/linux-fsdevel/166876785552.222254.4403222906022558715.stgit@warthog.procyon.org.uk/)

   We're trying to get rid of the ->writepage() hook[1].  Stop afs from using
   it by unlocking the page and calling filemap_fdatawrite_wbc() rather than
   folio_write_one().  We drop the folio lock so that writeback can include
   pages on both sides of the target page in the write without risking
   deadlock.

* [v3: convert tree to get_random_u32_{below,above,inclusive}()](http://lore.kernel.org/linux-fsdevel/20221117202906.2312482-1-Jason@zx2c4.com/)

   Hey everyone,

   [Changes v2->v3: rename get_random_u32_between() to
    get_random_u32_inclusive(), and implement with closed interval.]

   This series is the second tranche of tree-wide conversions to get random
   integer handling a bit tamer. It's another Coccinelle-based patchset.

   First we s/prandom_u32_max/get_random_u32_below/, since the former is
   just a deprecated alias for the latter. Then later, we can remove
   prandom_u32_max all together. I'm quite happy about finally being able
   to do that. It means that prandom.h is now only for deterministic and
   repeatable randomness, not non-deterministic/cryptographic randomness.
   That line is no longer blurred.

   In order to clean up a bunch of inefficient patterns, we use two simple
   helper functions built on top of get_random_u32_below:
   get_random_u32_above and get_random_u32_inclusive. The next two patches
   convert some gnarly open-coded number juggling to use these helpers.

   I've used Coccinelle for these three treewide patches, so hopefully
   review is rather uneventful. I didn't accept all of the changes that
   Coccinelle proposed, though, as these tend to be somewhat
   context-specific. I erred on the side of just going with the most
   obvious cases, at least this time through. And then we can address more
   complicated cases through actual maintainer trees.

   Since get_random_u32_below() and others sits in my random.git tree,
   these patches too will flow through that same tree.

   Regards,
   Jason

* [v1: exfat: move exfat_entry_set_cache from heap to stack](http://lore.kernel.org/linux-fsdevel/PUZPR04MB63163BEABDCE76AFD33442FF81069@PUZPR04MB6316.apcprd04.prod.outlook.com/)

   This patchset reduces the size of exfat_entry_set_cache and moves
   it from heap to stack.


* [v1: Add a new generic system call which has better performance, to get /proc data, than existing mechanisms](http://lore.kernel.org/linux-fsdevel/1668623844-9114-1-git-send-email-anjali.k.kulkarni@oracle.com/)

   Currently, reading from /proc is an expensive operation, for a performance sensitive application &#8211; that is because the /proc files are in ascii and you need to convert from binary to ascii & vice versa. Add to that, if the application needs to read a huge number of files from /proc in a short amount of time, the time taken is very high and performance is affected for the application. We want to address this issue by creating a generic system call, which can be used to read any of the data in /proc, without going through /proc, but fetching the data from the kernel directly. Additionally, a vectored list of /proc items can be fetched in one system call to further save multiple system calls.
    
* [v3: fs: do not update freeing inode i_io_list](http://lore.kernel.org/linux-fsdevel/20221115202001.324188-1-feldsherov@google.com/)

   After commit cbfecb927f42 ("fs: record I_DIRTY_TIME even if inode
   already has I_DIRTY_INODE") writeback_single_inode can push inode with
   I_DIRTY_TIME set to b_dirty_time list. In case of freeing inode with
   I_DIRTY_TIME set this can happen after deletion of inode from i_io_list
   at evict. Stack trace is following.

   evict
   fat_evict_inode
   fat_truncate_blocks
   fat_flush_inodes
   writeback_inode
   sync_inode_metadata(inode, sync=0)
   writeback_single_inode(inode, wbc) <- wbc->sync_mode == WB_SYNC_NONE

   This will lead to use after free in flusher thread.

   Similar issue can be triggered if writeback_single_inode in the
   stack trace update inode->i_io_list. Add explicit check to avoid it.

   Fixes: cbfecb927f42 ("fs: record I_DIRTY_TIME even if inode already has I_DIRTY_INODE")
   Reported-by: syzbot+6ba92bd00d5093f7e371@syzkaller.appspotmail.com

   
* [v1: file: Added pointer check](http://lore.kernel.org/linux-fsdevel/20221115130351.77351-1-arefev@swemel.ru/)

   Return value of a function 'affs_bread' is dereferenced at file.c:970
   without checking for null, but it is usually checked for this function.

* [v3: mm: anonymous shared memory naming](http://lore.kernel.org/linux-fsdevel/20221115020602.804224-1-pasha.tatashin@soleen.com/)

   Since: commit 9a10064f5625 ("mm: add a field to store names for private
   anonymous memory"), name for private anonymous memory, but not shared
   anonymous, can be set. However, naming shared anonymous memory just as
   useful for tracking purposes.

  
* [v2: fs: do not update freeing inode io_list](http://lore.kernel.org/linux-fsdevel/20221114212155.221829-1-feldsherov@google.com/)

   After commit cbfecb927f42 ("fs: record I_DIRTY_TIME even if inode
   already has I_DIRTY_INODE") writeiback_single_inode can push inode with
   I_DIRTY_TIME set to b_dirty_time list. In case of freeing inode with
   I_DIRTY_TIME set this can happened after deletion of inode io_list at
   evict. Stack trace is following.

* [v2: convert tree to get_random_u32_{below,above,between}()](http://lore.kernel.org/linux-fsdevel/20221114164558.1180362-1-Jason@zx2c4.com/)

   This series is the second tranche of tree-wide conversions to get random
   integer handling a bit tamer. It's another Coccinelle-based patchset.

   First we s/prandom_u32_max/get_random_u32_below/, since the former is
   just a deprecated alias for the latter. Then later, we can remove
   prandom_u32_max all together. I'm quite happy about finally being able
   to do that. It means that prandom.h is now only for deterministic and
   repeatable randomness, not non-deterministic/cryptographic randomness.
   That line is no longer blurred.

* [v4: [printk]: reduce console_lock scope](http://lore.kernel.org/linux-fsdevel/20221114162932.141883-1-john.ogness@linutronix.de/)

   This is v4 of a series to prepare for threaded/atomic
   printing. v3 is here [0]. This series focuses on reducing the
   scope of the BKL console_lock. It achieves this by switching to
   SRCU and a dedicated mutex for console list iteration and
   modification, respectively. The console_lock will no longer
   offer this protection.

* [v1: filelock: new helper: vfs_file_has_locks](http://lore.kernel.org/linux-fsdevel/20221114140747.134928-1-jlayton@kernel.org/)

   Ceph has a need to know whether a particular file has any locks set on
   it. It's currently tracking that by a num_locks field in its
   filp->private_data, but that's problematic as it tries to decrement this
   field when releasing locks and that can race with the file being torn
   down.

   Add a new vfs_file_has_locks helper that will scan the flock and posix
   lists, and return true if any of the locks have a fl_file that matches
   the given one. Ceph can then call this instead of doing its own
   tracking.

* [v1: erofs/zmap.c: Bail out when no further region remains](http://lore.kernel.org/linux-fsdevel/20221114120349.472418-1-code@siddh.me/)

   This was reported as a crash by syzbot under an issue about
   warning encountered in iomap_iter_done(), but unrelated to
   erofs. Hence, not adding issue hash in Reported-by line.

   C reproducer: https://syzkaller.appspot.com/text?tag=ReproC&x=1037a6b2880000
   Kernel config: https://syzkaller.appspot.com/text?tag=KernelConfig&x=e2021a61197ebe02
   Dashboard link: https://syzkaller.appspot.com/bug?extid=a8e049cd3abd342936b6

 

* [v3: zonefs: add sanity check for aggregated conventional zones](http://lore.kernel.org/linux-fsdevel/9467a1aafb97779fad2288739f70082e5d65d00c.1668426869.git.johannes.thumshirn@wdc.com/)

   When initializing a file inode, check if the zone's size if bigger than
   the number of device zone sectors. This can only be the case if we mount
   the filesystem with the -oaggr_cnv mount option.

   Emit an error in case this case happens and fail the mount.

  

* [v1: fs: ext4: initialize fsdata in pagecache_write()](http://lore.kernel.org/linux-fsdevel/20221114082935.3007497-1-glider@google.com/)

   When aops->write_begin() does not initialize fsdata, KMSAN reports
   an error passing the latter to aops->write_end().

   Fix this by unconditionally initializing fsdata.

   Also speculatively fix similar issues in affs, f2fs, hfs, hfsplus,
   as suggested by Eric Biggers.

   Fixes: c93d8f885809 ("ext4: add basic fs-verity support")
   Reported-by: syzbot+9767be679ef5016b6082@syzkaller.appspotmail.com

* [[PATCH 2/2 v2] ceph: use a xarray to record all the opened files for each inode](http://lore.kernel.org/linux-fsdevel/20221114051901.15371-3-xiubli@redhat.com/)

   When releasing the file locks the fl->fl_file memory could be
   already released by another thread in filp_close(), so we couldn't
   depend on fl->fl_file to get the inode. Just use a xarray to record
   the opened files for each inode.

* [[PATCH 1/2 v2] ceph: add ceph_lock_info support for file_lock](http://lore.kernel.org/linux-fsdevel/20221114051901.15371-2-xiubli@redhat.com/)

   When ceph releasing the file_lock it will try to get the inode pointer
   from the fl->fl_file, which the memory could already be released by
   another thread in filp_close(). Because in VFS layer the fl->fl_file
   doesn't increase the file's reference counter.

* [v1: blk: optimization for classic polling](http://lore.kernel.org/linux-fsdevel/3578876466-3733-1-git-send-email-nj.shetty@samsung.com/)

   This removes the dependency on interrupts to wake up task. Set task
   state as TASK_RUNNING, if need_resched() returns true,
   while polling for IO completion.
   Earlier, polling task used to sleep, relying on interrupt to wake it up.

#### 网络设备

* [[PATCH net] intel/igbvf: free irq on the error path in igbvf_request_msix()](http://lore.kernel.org/netdev/20221120061757.264242-1-cuigaosheng1@huawei.com/)

   In igbvf_request_msix(), irqs have not been freed on the err path,
   we need to free it. Fix it.

   Fixes: d4e0fe01a38a ("igbvf: add new driver to support 82576 virtual functions")

* [[RESEND PATCH] Bluetooth: hci_conn: Use kzalloc for kmalloc+memset](http://lore.kernel.org/netdev/Y3lCNjPGoVJT9Y35@qemulion/)

   Use of kzalloc preferred over combination of kmalloc & memset. Issue
   identified using coccicheck.

* [[PATCH net-next] selftests: net: Add cross-compilation support for BPF programs](http://lore.kernel.org/netdev/20221119171841.2014936-1-bjorn@kernel.org/)

   From: Bj&#246;rn T&#246;pel <bjorn@rivosinc.com>

   The selftests/net does not have proper cross-compilation support, and
   does not properly state libbpf as a dependency. Mimic/copy the BPF
   build from selftests/bpf, which has the nice side-effect that libbpf
   is built as well.

* [[PATCH net] l2tp: Don't sleep and disable BH under writer-side sk_callback_lock](http://lore.kernel.org/netdev/20221119130317.39158-1-jakub@cloudflare.com/)

   When holding a reader-writer spin lock we cannot sleep. Calling
   setup_udp_tunnel_sock() with write lock held violates this rule, because we
   end up calling percpu_down_read(), which might sleep, 
   Trim the writer-side critical section for sk_callback_lock down to the
   minimum, so that it covers only operations on sk_user_data.

   Also, when grabbing the sk_callback_lock, we always need to disable BH, as
   Eric points out. 
   
* [v1: i2c: Complete conversion to i2c_probe_new](http://lore.kernel.org/netdev/20221118224540.619276-1-uwe@kleine-koenig.org/)

   since commit b8a1a4cd5a98 ("i2c: Provide a temporary .probe_new()
   call-back type") from 2016 there is a "temporary" alternative probe
   callback for i2c drivers.

   This series completes all drivers to this new callback (unless I missed
   something). It's based on current next/master.
   
* [v1: net:[pull request] Intel Wired LAN Driver Updates 2022-11-18 (iavf)](http://lore.kernel.org/netdev/20221118222439.1565245-1-anthony.l.nguyen@intel.com/)

   This series contains updates to iavf driver only.

   Ivan Vecera resolves issues related to reset by adding back call to
   netif_tx_stop_all_queues() and adding calls to dev_close() to ensure
   device is properly closed during reset.

   Stefan Assmann removes waiting for setting of MAC address as this breaks
   ARP.

* [v2: [wpan-next]: IEEE 802.15.4 PAN discovery handling](http://lore.kernel.org/netdev/20221118221041.1402445-1-miquel.raynal@bootlin.com/)

   In all the past, current and future submissions, David and Romuald from
   Qorvo are credited in various ways (main author, co-author,
   suggested-by) depending of the amount of rework that was involved on
   each patch, reflecting as much as possible the open-source guidelines we
   follow in the kernel.

* [v1: net: nfc: st-nci: Restructure validating logic in EVT_TRANSACTION](http://lore.kernel.org/netdev/20221118220423.4038455-1-mfaltesek@google.com/)

   These are the same 3 patches that were applied in st21nfca here:
   https://lore.kernel.org/netdev/20220607025729.1673212-1-mfaltesek@google.com
   with a couple minor differences.

   st-nci has nearly identical code to that of st21nfca for EVT_TRANSACTION, except that there
   are two extra validation checks that are not present in the st-nci code. The 3/3 patch as coded
   for st21nfca pulls those checks in, bringing both drivers into parity.

* [[PATCH net-next] net: bcmgenet: Clear RGMII_LINK upon link down](http://lore.kernel.org/netdev/20221118213754.1383364-1-f.fainelli@gmail.com/)

   Clear the RGMII_LINK bit upon detecting link down to be consistent with
   setting the bit upon link up. We also move the clearing of the
   out-of-band disable to the runtime initialization rather than for each
   link up/down transition.

* [[PATCH net] net: sched: allow act_ct to be built without NF_NAT](http://lore.kernel.org/netdev/b6386f28d1ba34721795fb776a91cbdabb203447.1668807183.git.lucien.xin@gmail.com/)

   In commit f11fe1dae1c4 ("net/sched: Make NET_ACT_CT depends on NF_NAT"),
   it fixed the build failure when NF_NAT is m and NET_ACT_CT is y by
   adding depends on NF_NAT for NET_ACT_CT. However, it would also cause
   NET_ACT_CT cannot be built without NF_NAT, which is not expected. This
   patch fixes it by changing to use "(!NF_NAT || NF_NAT)" as the depend.

* [v1: carl9170: Replace zero-length array of trailing structs with flex-array](http://lore.kernel.org/netdev/20221118211146.never.395-kees@kernel.org/)

   Zero-length arrays are deprecated[1] and are being replaced with
   flexible array members in support of the ongoing efforts to tighten the
   FORTIFY_SOURCE routines on memcpy(), correctly instrument array indexing
   with UBSAN_BOUNDS, and to globally enable -fstrict-flex-arrays=3.

* [v1: net-next: mptcp: More specific netlink command errors](http://lore.kernel.org/netdev/20221118184608.187932-1-mathew.j.martineau@linux.intel.com/)

   This series makes the error reporting for the MPTCP_PM_CMD_ADD_ADDR netlink
   command more specific, since there are multiple reasons the command could fail.

   Note that patch 2 adds a GENL_SET_ERR_MSG_FMT() macro to genetlink.h,
   which is outside the MPTCP subsystem.

   Patch 1 refactors in-kernel listening socket and endpoint creation to
   simplify the second patch.

   Patch 2 updates the error values returned by the in-kernel path manager
   when it fails to create a local endpoint.


* [v1: dt-bindings: first batch of dt-schema conversions for Amlogic Meson bindings](http://lore.kernel.org/netdev/20221117-b4-amlogic-bindings-convert-v1-0-3f025599b968@linaro.org/)

   Batch conversion of the following bindings:
   - meson_sm.txt
   - amlogic-efuse.txt
   - amlogic-meson-mx-efuse.txt
   - meson-wdt.txt
   - meson-ir.txt
   - rtc-meson.txt
   - amlogic,meson6-timer.txt
   - meson-gxl-usb2-phy.txt
   - amlogic,meson-gx.txt
   - amlogic,meson-pcie.txt
   - mdio-mux-meson-g12a.txt

   The amlogic,meson-gx-pwrc.txt is removed since deprecated and unused 
   for a few releases now.

   Martin Blumenstingl was also added as bindings maintainer for Meson6/8/8b
   related bindings.

* [[PATCH 6.1-rc6] l2tp: call udp_tunnel_encap_enable() and sock_release() without sk_callback_lock](http://lore.kernel.org/netdev/c64284f4-2c2a-ecb9-a08e-9e49d49c720b@I-love.SAKURA.ne.jp/)

   Since we don't want to drop sk->sk_callback_lock inside
   setup_udp_tunnel_sock() right before calling udp_tunnel_encap_enable(),
   introduce a variant which does not call udp_tunnel_encap_enable(). And
   call udp_tunnel_encap_enable() after dropping sk->sk_callback_lock.

   Also, drop sk->sk_callback_lock before calling sock_release() in order to
   avoid circular locking dependency problem.

* [[PATCH net v3] nfp: flower: Added pointer check and continue](http://lore.kernel.org/netdev/20221118080317.119749-1-arefev@swemel.ru/)

   Return value of a function 'kmalloc_array' is dereferenced at 
   lag_conf.c:347 without checking for null, 
   but it is usually checked for this function.

   Found by Linux Verification Center (linuxtesting.org) with SVACE.
   Fixes: bb9a8d031140 ("nfp: flower: monitor and offload LAG groups")

* [[PATCH net] net: stmmac: Set MAC's flow control register to reflect current settings](http://lore.kernel.org/netdev/20221118072051.31313-1-wei.sheng.goh@intel.com/)

   Currently, pause frame register GMAC_RX_FLOW_CTRL_RFE is not updated
   correctly when 'ethtool -A <IFACE> autoneg off rx off tx off' command
   is issued. This fix ensures the flow control change is reflected directly
   in the GMAC_RX_FLOW_CTRL_RFE register.

#### 安全增强

* [v2: Add a bunch of msm8953 dts files](http://lore.kernel.org/linux-hardening/20221119203758.888207-1-luca@z3ntu.xyz/)

   The following patches add a bunch of msm8953-based devices that have
   been created in the msm8953-mainline[0] repository, which includes
   Snapdragon 450 (SDM450), Snapdragon 625 (msm8953) and Snapdragon 632
   (SDM632) devices.
   The dts files are trimmed down to what is currently supported upstream,
   as a way to also minimizing diff for further patches.

* [v1: [next] scsi: qla2xxx: Use struct_size() in code related to struct ct_sns_gpnft_rsp](http://lore.kernel.org/linux-hardening/9bd4775fe9c88b33c3194f841a2ec2f559d58032.1668814746.git.gustavoars@kernel.org/)

   Prefer struct_size() over open-coded versions of idiom:

   sizeof(struct-with-flex-array) + sizeof(typeof-flex-array-elements) * count

   where count is the max number of items the flexible array is supposed to
   contain.

   Link: https://github.com/KSPP/linux/issues/160

* [v1: IB/hfi1: Replace 1-element array with singleton](http://lore.kernel.org/linux-hardening/20221118215847.never.416-kees@kernel.org/)

   Zero-length arrays are deprecated[1] and are being replaced with
   flexible array members in support of the ongoing efforts to tighten the
   FORTIFY_SOURCE routines on memcpy(), correctly instrument array indexing
   with UBSAN_BOUNDS, and to globally enable -fstrict-flex-arrays=3.

   Replace zero-length array with flexible-array member "lvs" in struct
   opa_port_data_counters_msg and struct opa_port_error_counters64_msg.



* [v1: drm/nouveau/fb/ga102: Replace zero-length array of trailing structs with flex-array](http://lore.kernel.org/linux-hardening/20221118211207.never.039-kees@kernel.org/)

   Zero-length arrays are deprecated[1] and are being replaced with
   flexible array members in support of the ongoing efforts to tighten the
   FORTIFY_SOURCE routines on memcpy(), correctly instrument array indexing
   with UBSAN_BOUNDS, and to globally enable -fstrict-flex-arrays=3.

* [v1: ACPICA: Replace fake flexible arrays with flexible array members](http://lore.kernel.org/linux-hardening/20221118181538.never.225-kees@kernel.org/)

   Functionally identical to ACPICA upstream pull request 813:
   https://github.com/acpica/acpica/pull/813

   One-element arrays (and multi-element arrays being treated as
   dynamically sized) are deprecated[1] and are being replaced with
   flexible array members in support of the ongoing efforts to tighten the
   FORTIFY_SOURCE routines on memcpy(), correctly instrument array indexing
   with UBSAN_BOUNDS, and to globally enable -fstrict-flex-arrays=3.
 
* [v1: [next] wifi: brcmfmac: Use struct_size() in code ralated to struct brcmf_dload_data_le](http://lore.kernel.org/linux-hardening/41845ad3660ed4375f0c03fd36a67b2e12fafed5.1668548907.git.gustavoars@kernel.org/)

   Prefer struct_size() over open-coded versions of idiom:

   sizeof(struct-with-flex-array) + sizeof(typeof-flex-array-elements) * count

   where count is the max number of items the flexible array is supposed to contain.

* [v1: [next] wifi: brcmfmac: replace one-element array with flexible-array member in struct brcmf_dload_data_le](http://lore.kernel.org/linux-hardening/905f5b68cf93c812360d081caae5b15221db09b6.1668548907.git.gustavoars@kernel.org/)

   One-element arrays are deprecated, and we are replacing them with flexible
   array members instead. So, replace one-element array with flexible-array
   member in struct brcmf_dload_data_le.

   Important to mention is that doing a build before/after this patch results
   in no binary output differences.

   This helps with the ongoing efforts to tighten the FORTIFY_SOURCE routines
   on memcpy() and help us make progress towards globally enabling
   -fstrict-flex-arrays=3 [1].

   Link: https://github.com/KSPP/linux/issues/230
   Link: https://github.com/KSPP/linux/issues/79
   Link: https://gcc.gnu.org/pipermail/gcc-patches/2022-October/602902.html [1]

* [v1: [next] drm/amdgpu: Replace one-elements array with flex-array members](http://lore.kernel.org/linux-hardening/Y3CgReK3e519a7bs@mail.google.com/)

   One-element arrays are deprecated, and we are replacing them with
   flexible array members instead. So, replace one-element array with
   flexible-array member in structs ATOM_I2C_VOLTAGE_OBJECT_V3,
   ATOM_ASIC_INTERNAL_SS_INFO_V2, ATOM_ASIC_INTERNAL_SS_INFO_V3,
   and refactor the rest of the code accordingly.

#### 异步 IO

* [[PATCH for-6.1 v2] io_uring: make poll refs more robust](http://lore.kernel.org/io-uring/394b02d5ebf9d3f8ec0428bb512e6a8cd4a69d0f.1668802389.git.asml.silence@gmail.com/)

   poll_refs carry two functions, the first is ownership over the request.
   The second is notifying the io_poll_check_events() that there was an
   event but wake up couldn't grab the ownership, so io_poll_check_events()
   should retry.
 
* [[PATCH for-6.1] io_uring: disallow self-propelled ring polling](http://lore.kernel.org/io-uring/3124038c0e7474d427538c2d915335ec28c92d21.1668785722.git.asml.silence@gmail.com/)

   When we post a CQE we wake all ring pollers as it normally should be.
   However, if a CQE was generated by a multishot poll request targeting
   its own ring, it'll wake that request up, which will make it to post
   a new CQE, which will wake the request and so on until it exhausts all
   CQ entries.

* [v1: io_uring: kill tw-related outdated comments](http://lore.kernel.org/io-uring/deb4db0984b07e026d08b7bd1886cfc120d67f17.1668710788.git.asml.silence@gmail.com/)

   task_work fallback is executed from a workqueue, so current and
   req->task are not necessarily the same. It's still safe to poke into it
   as the request holds a task_struct reference.

* [v1: RFC on how to include LSM hooks for io_uring commands](http://lore.kernel.org/io-uring/20221116125051.3338926-1-j.granados@samsung.com/)

   The motivation for this patch is to continue the discussion around how to
   include LSM callback hooks in the io_uring infrastructure. To begin I take
   the nvme io_uring passthrough and try to include it in the already existing
   LSM infrastructure that is there for ioctl. This is far from a general
   io_uring approach, but its a start :)
-1-shr@devkernel.io/)

#### BPF

* [v4: [bpf-next]: clean-up bpftool from legacy support](http://lore.kernel.org/bpf/20221120112515.38165-1-sahid.ferdjaoui@industrialdiscipline.com/)

   As part of commit 93b8952d223a ("libbpf: deprecate legacy BPF map
   definitions") and commit bd054102a8c7 ("libbpf: enforce strict libbpf
   1.0 behaviors") The --legacy option is not relevant anymore. #1 is
   removing it. #4 is cleaning the code from using libbpf_get_error().

* [v9: [bpf-next]: Support storing struct task_struct objects as kptrs](http://lore.kernel.org/bpf/20221120051004.3605026-1-void@manifault.com/)

   Now that BPF supports adding new kernel functions with kfuncs, and
   storing kernel objects in maps with kptrs, we can add a set of kfuncs
   which allow struct task_struct objects to be stored in maps as
   referenced kptrs.

* [v1: perf lock contention: Do not use BPF task local storage](http://lore.kernel.org/bpf/20221118190109.1512674-1-namhyung@kernel.org/)

   It caused some troubles when a lock inside kmalloc is contended
   because task local storage would allocate memory using kmalloc.
   It'd create a recusion and even crash in my system.

   There could be a couple of workarounds but I think the simplest
   one is to use a pre-allocated hash map.  We could fix the task
   local storage to use the safe BPF allocator, but it takes time
   so let's change this until it happens actually.

* [v1: [bpf-next]: Follow ups for bpf-list set](http://lore.kernel.org/bpf/20221118185938.2139616-1-memxor@gmail.com/)

   Make a few changes
    - Remove bpf_global_ma_set check at runtime, disallow calling bpf_obj_new during verification
    - Disable spin lock failure test when JIT does not support kfunc (s390x)

   base-commit: db6bf999544c8c8dcae093e91eba4570647874b1

* [[PATCH bpf-next v3] bpf/verifier: Use kmalloc_size_roundup() to match ksize() usage](http://lore.kernel.org/bpf/20221118183409.give.387-kees@kernel.org/)

   Most allocation sites in the kernel want an explicitly sized allocation
   (and not "more"), and that dynamic runtime analysis tools (e.g. KASAN,
   UBSAN_BOUNDS, FORTIFY_SOURCE, etc) are looking for precise bounds checking
   (i.e. not something that is rounded up). A tiny handful of allocations
   were doing an implicit alloc/realloc loop that actually depended on
   ksize(), and didn't actually always call realloc. This has created a
   long series of bugs and problems over many years related to the runtime
   bounds checking, so these callers are finally being adjusted to _not_
   depend on the ksize() side-effect,
  
* [[PATCH bpf-next v5] docs/bpf: Add table of BPF program types to libbpf docs](http://lore.kernel.org/bpf/20221118152859.69645-1-donald.hunter@gmail.com/)

   Extend the libbpf documentation with a table of program types,
   attach points and ELF section names.

* [[PATCH bpf-next] samples: bpf: Use "grep -E" instead of "egrep"](http://lore.kernel.org/bpf/1668765001-12477-1-git-send-email-yangtiezhu@loongson.cn/)

   The latest version of grep claims the egrep is now obsolete so the build
   now contains warnings that look like:
   	egrep: warning: egrep is obsolescent; using grep -E
   fix this up by moving the related file to use "grep -E" instead.

     sed -i "s/egrep/grep -E/g" `grep egrep -rwl samples/bpf`

* [v10: [bpf-next]: Allocated objects, BPF linked lists](http://lore.kernel.org/bpf/20221118015614.2013203-1-memxor@gmail.com/)

   This series introduces user defined BPF objects of a type in program
   BTF. This allows BPF programs to allocate their own objects, build their
   own object hierarchies, and use the basic building blocks provided by
   BPF runtime to build their own data structures flexibly.

* [[PATCH bpf-next] libbpf: ignore hashmap__find() result explicitly in btf_dump](http://lore.kernel.org/bpf/20221117192824.4093553-1-andrii@kernel.org/)

   Coverity is reporting that btf_dump_name_dups() doesn't check return
   result of hashmap__find() call. This is intentional, so make it explicit
   with (void) cast.

* [v6: [bpf-next]: bpf: Add bpf_rcu_read_lock() support](http://lore.kernel.org/bpf/20221117042818.1086954-1-yhs@fb.com/)

   Currently, without rcu attribute info in BTF, the verifier treats
   rcu tagged pointer as a normal pointer. This might be a problem
   for sleepable program where rcu_read_lock()/unlock() is not available.
   For example, for a sleepable fentry program, if rcu protected memory
   access is interleaved with a sleepable helper/kfunc, it is possible
   the memory access after the sleepable helper/kfunc might be invalid

* [[PATCH bpf-next] selftests/bpf: allow unpriv bpf for selftests by default](http://lore.kernel.org/bpf/20221116015456.2461135-1-eddyz87@gmail.com/)

   Enable unprivileged bpf for selftests kernel by default.
   This forces CI to run test_verifier tests in both privileged
   and unprivileged modes.

* [v3: [bpf-next]: propagate nullness information for reg to reg comparisons](http://lore.kernel.org/bpf/20221115224859.2452988-1-eddyz87@gmail.com/)

   This patchset adds ability to propagates nullness information for
   branches of register to register equality compare instructions. The
   following rules are used:
    - suppose register A maybe null
    - suppose register B is not null
    - for JNE A, B, ... - A is not null in the false branch
    - for JEQ A, B, ... - A is not null in the true branch

* [v2: [net-next]: net: lan966x: Extend xdp support](http://lore.kernel.org/bpf/20221115214456.1456856-1-horatiu.vultur@microchip.com/)

   Extend the current support of XDP in lan966x with the action XDP_TX and
   XDP_REDIRECT.
   The first patch introduced a headroom for all the received frames. This
   is needed when the XDP_TX and XDP_REDIRECT is added.
   The next 2 patches, just introduced some helper functions that can be
   reused when is needed to transmit back the frame.
  
* [v1: security: Ensure LSMs return expected values](http://lore.kernel.org/bpf/20221115175652.3836811-1-roberto.sassu@huaweicloud.com/)

   LSMs should follow the conventions stated in include/linux/lsm_hooks.h for
   return values, as these conventions are followed by callers of the LSM
   infrastructure for error handling.

* [[PATCH bpf-next v2] bpftool: Check argc first before "file" in do_batch()](http://lore.kernel.org/bpf/1668517207-11822-1-git-send-email-yangtiezhu@loongson.cn/)

   If the parameters for batch are more than 2, check argc first can
   return immediately, no need to use is_prefix() to check "file" with
   a little overhead and then check argc, it is better to check "file"
   only when the parameters for batch are 2.

### 周边技术动态

#### Qemu

* [v1: riscv: Add RISCVCPUConfig.satp_mode to set sv48, sv57, etc.](http://lore.kernel.org/qemu-devel/20221117072841.240839-1-alexghiti@rivosinc.com/)

   RISC-V specifies multiple sizes for addressable memory and Linux probes for
   the machine's support at startup via the satp CSR register (done in
   csr.c:validate_vm).

   As per the specification, sv64 must support sv57, which in turn must
   support sv48...etc. So we can restrict machine support by simply setting the
   "highest" supported mode in the satp_mode property. And the bare mode is
   always supported.

#### Buildroot

* [[branch/next] package/z3: new package](http://lore.kernel.org/buildroot/20221120135623.522B183FD2@busybox.osuosl.org/)

   commit: https://git.buildroot.net/buildroot/commit/?id=2ad68ff8df71a485085b0b83ff494081e4d926b7
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/next

   Z3, also known as the Z3 Theorem Prover, is a cross-platform
   satisfiability modulo theories (SMT) solver.

* [v1: arch/riscv: RISC-V 32-bit NO MMU support](http://lore.kernel.org/buildroot/20221117140349.27692-1-ustcymgu@gmail.com/)

   The following patch adds support for 32-bit RISC-V No-MMU build.
   Currently for No-MMU RISC-V, only 64-bit is supported. There're no
   substancial changes in code but macros and compiling options. Uclibc,
   elf2flt, and the kernel need to be patched. 
   This is based on damien-lemoal/buildroot which first added 64-bit No-MMU
   support, and my earlier porting attempt regymm/buildroot. 
  
* [Buildroot 2022.08.2 released](http://lore.kernel.org/buildroot/87v8neczcn.fsf@dell.be.48ers.dk/)

   git://git.buildroot.org/buildroot

   Buildroot 2022.08.2 is a bugfix release, fixing a number of important /
   security related issues discovered since the 2022.08.1 release.

   For more details, see the CHANGES file:

   https://git.buildroot.net/buildroot/plain/CHANGES?id=2022.08.2

   Users of the affected packages are strongly encouraged to upgrade.

   Many thanks to all the people contributing to this release:

   git shortlog -sn 2022.08.1..

* [v1: package/qemu: refactor target emulator selection](http://lore.kernel.org/buildroot/20221113224719.185759-1-unixmania@gmail.com/)

   The current mechanism to select emulation targets works this way:

   - BR2_PACKAGE_QEMU_SYSTEM selects the "system" (softmmu) targets. It
     selects FDT and creates a dependency on the "dtc" package but this is
     not always necessary. Only 14 system targets, out of 31, actually
     require FDT.

   - BR2_PACKAGE_QEMU_LINUX_USER selects the "linux-user" targets. It does
     not select FDT, which is not required by linux-user emulators.

* [[branch/next] package/libmdbx: bump version to 0.11.13 "Swashplate"](http://lore.kernel.org/buildroot/20221113215530.C5C8C83853@busybox.osuosl.org/)

   [-- Attachment #1: Type: text/plain, Size: 2828 bytes --]

   commit: https://git.buildroot.net/buildroot/commit/?id=cd25808d7d3b292f61043d612c2e34aab5a422fd
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/next

   This is stable bugfix release of libmdbx, in Family Glory and
   in memory of Boris Yuriev (the inventor of Helicopter and
   Swashplate in 1911) on his 133rd birthday.

* [[branch/2022.08.x] toolchain/toolchain-buildroot: introduce BR2_TOOLCHAIN_BUILDROOT_NONE](http://lore.kernel.org/buildroot/20221113143351.2A6E683616@busybox.osuosl.org/)

   In the internal toolchain backend, we have a choice..endchoice block
   to allow the user to select the C library, between glibc, uClibc and
   musl.

   However, there are situations were no C library at all is
   supported. In this case, the choice does not appear, and does not
   allow to see the Config.in comments that are within the
   choice..endchoice block and that may explain why no C library is
   available.

#### U-Boot

* [v1: riscv: use imply instead of select for SPL_SEPARATE_BSS](http://lore.kernel.org/u-boot/20221116070839.28550-1-zong.li@sifive.com/)

   Use imply instead of select, then it can still be disabled by
   board-specific defconfig, or be set to n manually.

* [[PULL] u-boot-riscv/master](http://lore.kernel.org/u-boot/Y3SAOObPbrYTX6g8@ubuntu01/)

   The following changes since commit c4ee4fe92e9be120be6d12718273dec6b63cc7d9:

   CI shows no issue: https://source.denx.de/u-boot/custodians/u-boot-riscv/-/pipelines/14105

   - Fix and improve microchip's clock driver to allow sync'ing DTS with linux 
   - Improve the help message in "SBI_V02" Kconfig
   - Improve DTS property "isa-string" parsing rule


## 20221113：第 20 期

### 内核动态

* [v1: riscv: mm: Proper page permissions after initmem free](http://lore.kernel.org/linux-riscv/20221112113543.3165646-1-bjorn@kernel.org/)

   64-bit RISC-V kernels have the kernel image mapped separately, and in
   addition to the linear map. When the kernel is loaded, the linear map
   of kernel image is set to PAGE_READ permission, and the kernel map is
   set to PAGE_READ and PAGE_EXEC.

* [v1: RISC-V: Fix unannoted hardirqs-on in return to userspace slow-path](http://lore.kernel.org/linux-riscv/20221111223108.1976562-1-abrestic@rivosinc.com/)

   The return to userspace path in entry.S may enable interrupts without the
   corresponding lockdep annotation, producing a splat[0] when DEBUG_LOCKDEP
   is enabled. Simply calling __trace_hardirqs_on() here gets a bit messy
   due to the use of RA to point back to ret_from_exception, so just move
   the whole slow-path loop into C. It's more readable and it lets us use
   local_irq_{enable,disable}(), avoiding the need for manual annotations
   altogether.

* [[GIT PULL] RISC-V Fixes for 6.1-rc5](http://lore.kernel.org/linux-riscv/mhng-a9c9d14d-1da7-4c8d-b117-b207616f77a4@palmer-ri-x1c9/)

   RISC-V Fixes for 6.1-rc5
   * A fix to add the missing PWM LEDs into the  SiFive HiFive Unleashed device tree.
   * A fix to fully clear a task's registers on creation, as they end up in userspace and thus leak kernel memory.
   * A pair of VDSO-related build fixes that manifest on recent LLVM-based toolchains.
   * A fix to our early init to ensure the DT is adequately processed before reserved memory nodes are processed.

* [V3: riscv: asid: Fixup stale TLB entry cause application crash](http://lore.kernel.org/linux-riscv/20221111075902.798571-1-guoren@kernel.org/)

   After use_asid_allocator is enabled, the userspace application will
   crash by stale TLB entries. Because only using cpumask_clear_cpu without
   local_flush_tlb_all couldn't guarantee CPU's TLB entries were fresh.
   Then set_mm_asid would cause the user space application to get a stale
   value by stale TLB entry, but set_mm_noasid is okay.

* [v1: Linux RISC-V AIA Support](http://lore.kernel.org/linux-riscv/20221111044207.1478350-1-apatel@ventanamicro.com/)

   The RISC-V AIA specification is now frozen as-per the RISC-V international
   process. The latest frozen specifcation can be found at:
   https://github.com/riscv/riscv-aia/releases/download/1.0-RC1/riscv-interrupts-1.0-RC1.pdf  

* [v1: Zbb string optimizations and call support in alternatives](http://lore.kernel.org/linux-riscv/20221110164924.529386-1-heiko@sntech.de/)

   The Zbb extension can be used to make string functions run a lot faster.
   
   In my half-scientific test-case of running the functions in question
   on a 95 character string in a loop of 10000 iterations, the Zbb
   variants shave off around 2/3 of the original runtime.

* [v1: Route RISC-V SoC drivers/firmware/DT via the soc tree](http://lore.kernel.org/linux-riscv/20221109212219.1598355-1-conor@kernel.org/)

   Spoke to Palmer today at the linux-riscv pw sync-up (see [0] for anyone
   that may see this and is interested), and he said go for it w/
   MAINTAINERS changes. Routing patches this way was proposed by Palmer @
   [1] where Arnd & I had a wee bit of back and forth about the maintainers
   entries required.

* [v1: RISC-V: vdso: Do not add missing symbols to version section in linker script](http://lore.kernel.org/linux-riscv/20221108171324.3377226-1-nathan@kernel.org/)

   Recently, ld.lld moved from '--undefined-version' to
   '--no-undefined-version' as the default, which breaks the compat vDSO
   build:

     ld.lld: error: version script assignment of 'LINUX_4.15' to symbol '__vdso_gettimeofday' failed: symbol not defined
     ld.lld: error: version script assignment of 'LINUX_4.15' to symbol '__vdso_clock_gettime' failed: symbol not defined
     ld.lld: error: version script assignment of 'LINUX_4.15' to symbol '__vdso_clock_getres' failed: symbol not defined

* [v1: RISC-V: KVM: Exit run-loop immediately if xfer_to_guest fails](http://lore.kernel.org/linux-riscv/20221108115543.1425199-1-apatel@ventanamicro.com/)

   If xfer_to_guest_mode_handle_work() fails in the run-loop then exit
   the run-loop immediately instead of doing it after some more work.

* [v1: arch_topology: Build cacheinfo from primary CPU](http://lore.kernel.org/linux-riscv/20221108110424.166896-1-pierre.gondois@arm.com/)

   To prevent this bug, allocate the cacheinfo from the primary CPU when
   preemption and interrupts are enabled and before booting secondary
   CPUs. The cache levels/leaves are computed from DT/ACPI PPTT information
   only, without relying on the arm64 CLIDR_EL1 register.
   If no cache information is found in the DT/ACPI PPTT, then fallback
   to the current state, triggering on PREEMPT_RT kernels.
  
* [v1: riscv: asid: Fixup stale TLB entry cause application crash](http://lore.kernel.org/linux-riscv/20221108090219.3285030-1-guoren@kernel.org/)

   When use_asid_allocator is enabled, the userspace application would
   crash by stale tlb entry. Because only using cpumask_clear_cpu without
   local_flush_tlb_all couldn't guarantee CPU's tlb contains fresh mapping
   entry. Then set_mm_asid would cause user space application get a stale
   value by stale tlb entry, but set_mm_noasid is okay.
  
* [v4: Add OPTPROBES feature on RISCV](http://lore.kernel.org/linux-riscv/20221106100316.2803176-1-chenguokai17@mails.ucas.ac.cn/)

   Add jump optimization support for RISC-V.

   Replaces ebreak instructions used by normal kprobes with an
   auipc+jalr instruction pair, at the aim of suppressing the probe-hit
   overhead.
  
### 周边技术动态

#### Qemu

* [v2: hw/riscv: pfsoc: add missing FICs as unimplemented](http://lore.kernel.org/qemu-devel/20221112133414.262448-3-conor@kernel.org/)

   The Fabric Interconnect Controllers provide interfaces between the FPGA
   fabric and the core complex. There are 5 FICs on PolarFire SoC, numbered
   0 through 4. FIC2 is an AXI4 slave interface from the FPGA fabric and
   does not show up on the MSS memory map. FIC4 is dedicated to the User
   Crypto Processor and does not show up on the MSS memory map either.

* [v1: hw/riscv: virt: Remove the redundant ipi-id property](http://lore.kernel.org/qemu-devel/20221111201337.3320349-1-atishp@rivosinc.com/)

   The imsic DT binding has changed and no longer require an ipi-id.
   The latest IMSIC driver dynamically allocates ipi id if slow-ipi
   is not defined.

* [v5: target/riscv: initialise MemTxAttrs for CPU access](http://lore.kernel.org/qemu-devel/20221111182535.64844-13-alex.bennee@linaro.org/)

   get_physical_address works in the CPU context. Use the new
   MEMTXATTRS_CPU constructor to ensure the correct CPU id is filled in
   should it ever be needed by any devices later.

* [v2: target/riscv: Typo fix in sstc() predicate](http://lore.kernel.org/qemu-devel/20221108125703.1463577-2-apatel@ventanamicro.com/)

   We should use "&&" instead of "&" when checking hcounteren.TM and
   henvcfg.STCE bits.

   Fixes: 3ec0fe18a31f ("target/riscv: Add vstimecmp suppor")

* [V2: hw/riscv: virt: Remove size restriction for pflash](http://lore.kernel.org/qemu-devel/20221107130217.2243815-1-sunilvl@ventanamicro.com/)

   The pflash implementation currently assumes fixed size of the
   backend storage. Due to this, the backend storage file needs to be
   exactly of size 32M. Otherwise, there will be an error like below.

#### Buildroot

* [v1: package/libmdbx: bump version to 0.11.13 "Swashplate"](http://lore.kernel.org/buildroot/20221110111109.99724-1-leo@yuriev.ru/)

   This is stable bugfix release of libmdbx, in Family Glory and
   in memory of Boris Yuriev (the inventor of Helicopter and
   Swashplate in 1911) on his 133rd birthday.

* [v2: andes_ae350_45_defconfig: bump opensbi, u-boot and linux](http://lore.kernel.org/buildroot/20221106042114.8030-1-peterlin@andestech.com/)

   Linux kernel is hosted on AndesTech Github which includes ethernet,
   SD card, DMAC, RTC, WDT drivers support. OpenSBI is based on v1.1
   with andes platfrom fdt driver.

* [[branch/2022.02.x] package/go: add support for riscv64 architecture](http://lore.kernel.org/buildroot/20221105214006.C062187E79@busybox.osuosl.org/)

   Enable the supported "riscv64" GOARCH.
   Add a patch to fix a build failure due to GOARCH leaking into the calls to the
   go-bootstrap compiler. Unsets the GOARCH before calling go-bootstrap.

#### U-Boot

* [v1: efi_loader: replace a u16_strdup with alloc + memcpy](http://lore.kernel.org/u-boot/20221111180431.1307444-1-ilias.apalodimas@linaro.org/)

   Heinrich reports that on RISC-V unaligned access is emulated by OpenSBI
   which is very slow.  Performance wise it's better if we skip the calls
   to u16_strdup() -- which in turn calls u16_strsize() and just allocate/copy the
   memory directly.  The access to dp.length may still be unaligned, but that's
   way less than what u16_strsize() would do

* [v5: buildman: Drop mention of old architectures](http://lore.kernel.org/u-boot/20221110021455.1004335-10-sjg@chromium.org/)

   Support for some architectures has been removed since buildman was first
   written. Also all toolchains are now available at kernel.org so we don't
   need the links, except for arc where the kernel.org toolchain fails to
   build all boards.

* [v1: riscv: clarify meaning of CONFIG_SBI_V02](http://lore.kernel.org/u-boot/20221108145312.40371-1-heinrich.schuchardt@canonical.com/)

   Describe that CONFIG_SBI_V02=y does not mean SBI specification v0.2
   but v0.2 or later.

* [v4: u-boot-initial-env: rework make target](http://lore.kernel.org/u-boot/20221108085222.1378781-2-max.oss.09@gmail.com/)

   With LTO enabled the U-Boot initial environment is no longer stored
   in an easy accessible section in env/common.o. I.e. the section name
   changes from build to build, its content maybe compressed and it is
   annotated with additional data.

* [v4: rockchip: Convert all boards to use binman](http://lore.kernel.org/u-boot/20221106224011.606743-6-sjg@chromium.org/)

   Instead of the bash script, use binman to generate the FIT for arm64.

   For 32-bit boards, use binman for all images, dropping the intermediate files.
  
* [v2: riscv: Fix detecting FPU support in standard extension](http://lore.kernel.org/u-boot/20221105060214.2526-1-peterlin@andestech.com/)

   We should check the string until it hits underscore, in case it
   searches for the letters in the custom extension. For example,
   "rv64imac_xandes" will be treated as D extension support since
   there is a "d" in "andes", resulting illegal instruction caused
   by initializing FCSR.

## 20221106：第 19 期

### 内核动态

* [v1: RISC-V: add infrastructure to allow different str* implementations](http://lore.kernel.org/linux-riscv/20221104225153.2710873-8-heiko@sntech.de/)

   Depending on supported extensions on specific RISC-V cores,
   optimized str* functions might make sense.

   This adds basic infrastructure to allow patching the function calls
   via alternatives later on.

* [v1: HACK: disable strchr call in overlay-fs](http://lore.kernel.org/linux-riscv/20221104225153.2710873-10-heiko@sntech.de/)

   Although the standard strchr function does _not_
   use strlen at all, the compiled result with the str*-alternatives
   somehow wants to link against a then-nonexistent strlen symbol.

* [v1: efi/riscv: libstub: mark when compiling libstub](http://lore.kernel.org/linux-riscv/20221104225153.2710873-3-heiko@sntech.de/)

   We want to runtime-optimize some core functions (str*, mem*)
   but not have this leak into libstub. Instead libstub
   for the short while it's running should just use the generic
   implementation.

* [v1: RISC-V: add rd reg parsing to parse_asm header](http://lore.kernel.org/linux-riscv/20221104225153.2710873-6-heiko@sntech.de/)

   Add a macro to allow parsing of the rd register from an instruction.

* [v1: RISC-V: fix auipc-jalr addresses in patched alternatives](http://lore.kernel.org/linux-riscv/20221104225153.2710873-7-heiko@sntech.de/)

   Alternatives live in a different section, so addresses used by call
   functions will point to wrong locations after the patch got applied.

* [v1: string: allow override for strstarts()](http://lore.kernel.org/linux-riscv/20221104225153.2710873-2-heiko@sntech.de/)

   In some special cases it may be necessary to also override the
   inline strstarts() function, so move the relevant #ifdef in place.

* [v4: Fix /proc/cpuinfo cpumask warning](http://lore.kernel.org/linux-riscv/20221103142504.278543-1-ajones@ventanamicro.com/)

   This series addresses the issue for x86. riscv has already merged an
   equivalent patch (v3 of this series). Also, from a quick grep of cpuinfo
   seq operations, I think at least openrisc, powerpc, and s390 could get an
   equivalent patch. While the test is simple (see next paragraph) I'm not
   equipped to test on each architecture.

* [V8: [-next]: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20221103075047.1634923-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support. Some optimization for entry.S with new .macro and
   merge ret_from_kernel_thread into ret_from_fork..

* [[GIT PULL] Please pull powerpc/linux.git powerpc-6.1-4 tag](http://lore.kernel.org/linux-riscv/87o7tossaa.fsf@mpe.ellerman.id.au/)

   powerpc fixes for 6.1 #4
    - Fix an endian thinko in the asm-generic compat_arg_u64() which led to syscall arguments
      being swapped for some compat syscalls.

    - Fix syscall wrapper handling of syscalls with 64-bit arguments on 32-bit kernels, which
      led to syscall arguments being misplaced.

    - A build fix for amdgpu on Book3E with AltiVec disabled.

* [v1: KVM: Rework kvm_init() and hardware enabling](http://lore.kernel.org/linux-riscv/20221102231911.3107438-1-seanjc@google.com/)

   Non-x86 folks, please test on hardware when possible.  I made a _lot_ of
   mistakes when moving code around.  Thankfully, x86 was the trickiest code
   to deal with, and I'm fairly confident that I found all the bugs I
   introduced via testing.  But the number of mistakes I made and found on
   x86 makes me more than a bit worried that I screwed something up in other
   arch code.

* [v2: sched, smp: Trace smp callback causing an IPI](http://lore.kernel.org/linux-riscv/20221102183336.3120536-7-vschneid@redhat.com/)

   While CSD_TYPE_SYNC/ASYNC and CSD_TYPE_IRQ_WORK share a similar backing
   struct layout (meaning their callback func can be accessed without caring
   about the actual CSD type), CSD_TYPE_TTWU doesn't even have a function
   attached to its struct. 

* [v2: trace: Add trace_ipi_send_cpumask()](http://lore.kernel.org/linux-riscv/20221102183336.3120536-1-vschneid@redhat.com/)

   trace_ipi_raise() is unsuitable for generically tracing IPI sources due to
   its "reason" argument being an uninformative string (on arm64 all you get
   is "Function call interrupts" for SMP calls).

* [v2: DO-NOT-MERGE: tracing: Add __cpumask to denote a trace event field that is a cpumask_t](http://lore.kernel.org/linux-riscv/20221102182949.3119584-2-vschneid@redhat.com/)

   The trace events have a __bitmask field that can be used for anything
   that requires bitmasks. Although currently it is only used for CPU
   masks, it could be used in the future for any type of bitmasks.

* [v2: Generic IPI sending tracepoint](http://lore.kernel.org/linux-riscv/20221102182949.3119584-1-vschneid@redhat.com/)

   Detecting IPI *reception* is relatively easy, e.g. using
   trace_irq_handler_{entry,exit} or even just function-trace
   flush_smp_call_function_queue() for SMP calls.  

   Figuring out their *origin*, is trickier as there is no generic tracepoint tied
   to e.g. smp_call_function():

   o AFAIA x86 has no tracepoint tied to sending IPIs, only receiving them
     (cf. trace_call_function{_single}_entry()).
  
* [v2: riscv: vdso: fix section overlapping under some conditions](http://lore.kernel.org/linux-riscv/20221102170254.1925-1-jszhang@kernel.org/)

   lkp reported a build error, I tried the config and can reproduce build error as below

* [v10: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20221101143400.690000-1-apatel@ventanamicro.com/)

   These patches were originally part of the "Linux RISC-V ACLINT Support"
   series but this now a separate series so that it can be merged independently
   of the "Linux RISC-V ACLINT Support" series.
   
* [v1: RISC-V: KVM: use vma_lookup() instead of find_vma_intersection()](http://lore.kernel.org/linux-riscv/20221101053811.5884-1-liubo03@inspur.com/)

   vma_lookup() finds the vma of a specific address with a cleaner interface
   and is more readable.
  
* [v1: riscv: support for hardware breakpoints/watchpoints](http://lore.kernel.org/linux-riscv/20221031213225.912258-1-geomatsi@gmail.com/)

   RISC-V Debug specification includes Sdtrig ISA extension. This extension
   describes Trigger Module. Triggers can cause a breakpoint exception,
   entry into Debug Mode, or a trace action without having to execute a
   special instruction. For native debugging triggers can be used to
   implement hardware breakpoints and watchpoints.

* [v3: PCI: Remove unnecessary <linux/of_irq.h> includes](http://lore.kernel.org/linux-riscv/20221031153954.1163623-1-helgaas@kernel.org/)

   Many host controller drivers #include <linux/of_irq.h> even though they
   don't need it.  Remove the unnecessary #includes.

* [v3: riscv: fix race when vmap stack overflow](http://lore.kernel.org/linux-riscv/20221030124517.2370-1-jszhang@kernel.org/)

   Currently, when detecting vmap stack overflow, riscv firstly switches
   to the so called shadow stack, then use this shadow stack to call the
   get_overflow_stack() to get the overflow stack. 
   
* [v3: Add OPTPROBES feature on RISCV](http://lore.kernel.org/linux-riscv/20221030090141.2550837-1-chenguokai17@mails.ucas.ac.cn/)

   Add jump optimization support for RISC-V.

   Replaces ebreak instructions used by normal kprobes with an
   auipc+jalr instruction pair, at the aim of suppressing the probe-hit overhead.

### 周边技术动态

#### Qemu

* [v2: hw/riscv: sifive_e: Support the watchdog timer of HiFive 1 rev b.](http://lore.kernel.org/qemu-devel/20221101024656.517608-3-tommy.wu@sifive.com/)

   Create the AON device when we realize the sifive_e machine.
   This patch only implemented the functionality of the watchdog timer,
   not all the functionality of the AON device.

#### Buildroot

* [[branch/2022.02.x] package/go: add support for riscv64 architecture](http://lore.kernel.org/buildroot/20221105214006.C062187E79@busybox.osuosl.org/)

   Enable the supported "riscv64" GOARCH.

   Add a patch to fix a build failure due to GOARCH leaking into the calls to the
   go-bootstrap compiler. Unsets the GOARCH before calling go-bootstrap.

* [v3: configs/lichee_rv_dock: new defconfig](http://lore.kernel.org/buildroot/20221104091727.690022-4-angelo@amarulasolutions.com/)

   Lichee RV Dock is a RISC-V Linux development kits with high integration,
   small size and affordable price designed for opensource developer.

* [v3: Lichee RV and Lichee RV dock support](http://lore.kernel.org/buildroot/20221104091727.690022-1-angelo@amarulasolutions.com/)

   Changelog:
   v2:
   * Renamed the configs from lichee_rv_* to sipeed_lichee_rv_*
   * Moved the boards directories intoboard/sipeed
   * Added bluetooth support for lichee_rv_dock
   * Updated the opensbi, uboot, linux version to the same as nezha

* [v1: andes_ae350_45_defconfig: bump opensbi, u-boot and linux](http://lore.kernel.org/buildroot/20221104080407.25870-1-peterlin@andestech.com/)

   Linux kernel is hosted on AndesTech Github which includes ethernet,
   SD card, DMAC, RTC, WDT drivers support. OpenSBI is based on v1.1
   with andes platfrom fdt driver, hence adding patches for U-boot to
   update the plmt and plicsw compatible strings and modify the IPI scheme.

* [[git commit] package/tinycompress: new package](http://lore.kernel.org/buildroot/20221030192451.8F07D875D0@busybox.osuosl.org/)

   commit: https://git.buildroot.net/buildroot/commit/?id=2f02d159bb9eceabc1d0a3a4ffee97e726aad21d
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/master

   tinycompress is a library for compress audio offload in alsa.
   It also contains the "cplay" and "crecord" programs.
   tinycompress is part of the ALSA project.

* [v1: package/tinycompress: new package](http://lore.kernel.org/buildroot/20221029115321.718694-1-ju.o@free.fr/)

   tinycompress is a library for compress audio offload in alsa.
   It also contains the "cplay" and "crecord" programs.
   tinycompress is part of the ALSA project.

#### U-Boot

* [v2: riscv: Fix detecting FPU support in standard extension](http://lore.kernel.org/u-boot/20221105060214.2526-1-peterlin@andestech.com/)

   We should check the string until it hits underscore, in case it
   searches for the letters in the custom extension. For example,
   "rv64imac_xandes" will be treated as D extension support since
   there is a "d" in "andes", resulting illegal instruction caused by initializing FCSR.

* [v3: Makefile: rework u-boot-initial-env target](http://lore.kernel.org/u-boot/20221104124458.890133-2-max.oss.09@gmail.com/)

   With LTO enabled the U-Boot initial environment is no longer stored
   in an easy accessible section in env/common.o. I.e. the section name
   changes from build to build, its content maybe compressed and it is
   annotated with additional data.

* [[PULL] u-boot-riscv/master](http://lore.kernel.org/u-boot/Y2NoAaeeB2LEYUwR@ubuntu01/) 

   The following changes since commit c8d9ff634fc429db5acf2f5386ea937f0fef1ae7:

     Merge branch '2022-10-31-FWU-add-FWU-multi-bank-update-feature-support' (2022-11-01 09:32:21 -0400)

* [v1: Makefile: Rename u-boot-spl.kwb to u-boot-with-spl.kwb](http://lore.kernel.org/u-boot/20221102175128.10654-1-pali@kernel.org/)

   File name with pattern u-boot-spl* is used on all places except in kwb
   image for binary with SPL-only code. Combined binary with both SPL and
   proper U-Boot in other places has file name pattern u-boot-with-spl*.
 
* [v5: cmd: Add new parser command](http://lore.kernel.org/u-boot/20221101192029.10231-12-francis.laniel@amarulasolutions.com/)

   This command can be used to print the current parser with 'parser print'.
   It can also be used to set the current parser with 'parser set'.
   For the moment, only one value is valid for set: old.

* [v1: spl: sunxi: Replace ARCH_SUNXI with BOARD_SUNXI](http://lore.kernel.org/u-boot/20221101050835.2421-23-samuel@sholland.org/)

   This provides a unified configuration across all sunxi boards, regardless of CPU architecture.

* [v1: sunxi: Move SYS_LOAD_ADDR to the board Kconfig](http://lore.kernel.org/u-boot/20221101050835.2421-17-samuel@sholland.org/)

   This will provide a default value for RISC-V when that is added, and it
   makes sense to put this option next to the other DRAM layout options.

## 20221030：第 18 期

### 内核动态

* [v3: Add OPTPROBES feature on RISCV](http://lore.kernel.org/linux-riscv/20221030090141.2550837-1-chenguokai17@mails.ucas.ac.cn/)

   Add jump optimization support for RISC-V.

   Replaces ebreak instructions used by normal kprobes with an
   auipc+jalr instruction pair, at the aim of suppressing the probe-hit overhead.
   
* [v1: riscv: process: fix kernel info leakage](http://lore.kernel.org/linux-riscv/20221029113450.4027-1-jszhang@kernel.org/)

   thread_struct's s[12] may contain random kernel memory content, which
   may be finally leaked to userspace. This is a security hole. Fix it
   by clearing the s[12] array in thread_struct when fork.

* [v1: RISC-V: Do not issue remote fences until smp is available](http://lore.kernel.org/linux-riscv/20221028231929.347918-1-atishp@rivosinc.com/)

   It is useless to issue remote fences if there is a single core
   available. It becomes a bottleneck for sbi based rfences where
   we will be making those ECALLs for no reason. 

* [[GIT PULL] RISC-V Fixes for 6.1-rc3](http://lore.kernel.org/linux-riscv/mhng-d47a53b0-2125-467d-a9ff-c17fd41d646b@palmer-ri-x1c9a/)

   RISC-V Fixes for 6.1-rc3
   * A fix for a build warning in the jump_label code.
   * One of the git://github -> https://github cleanups, for the SiFive
     drivers.
   * A fix for the kasan initialization code, this still likely warrants
     some cleanups but that's a bigger problem and at least this fixes the
     crashes in the short term.
   * A pair of fixes for extension support detection on mixed LLVM/GNU
     toolchains.
   * A fix for a runtime warning in the /proc/cpuinfo code.

* [v5: Add support for Renesas RZ/Five SoC](http://lore.kernel.org/linux-riscv/20221028165921.94487-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   The RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP Single)
   1.0 GHz, 16-bit DDR3L/DDR4 interface. And it also has many interfaces such
   as Gbit-Ether, CAN, and USB 2.0, making it ideal for applications such as
   entry-class social infrastructure gateway control and industrial gateway control.

* [v1: Fixup perf tests for 32-bit systems with 64-bit time_t](http://lore.kernel.org/linux-riscv/20221028095632.1081262-1-alistair.francis@opensource.wdc.com/)

   This series aims to fix perf for 32-bit systems with a 64-bit time_t
   (like RV32).

   This series also adds a public futex waitv syscall that userspace
   can use to avoid this complexity.
 
* [v5: arm64: support batched/deferred tlb shootdown during page reclamation](http://lore.kernel.org/linux-riscv/20221028081255.19157-1-yangyicong@huawei.com/)

   Though ARM64 has the hardware to do tlb shootdown, the hardware
   broadcasting is not free.
   A simplest micro benchmark shows even on snapdragon 888 with only
   8 cores, the overhead for ptep_clear_flush is huge even for paging
   out one page mapped by only one process:
   While pages are mapped by multiple processes or HW has more CPUs,
   the cost should become even higher due to the bad scalability of
   tlb shootdown.

* [v1: RISC-V: Dynamic ftrace support for RV32I](http://lore.kernel.org/linux-riscv/20221027172435.2687118-1-jamie@jamieiles.com/)

   This series enables dynamic ftrace support for RV32I bringing it to 
   parity with RV64I.  Most of the work is already there, this is largely 
   just assembly fixes to handle register sizes, correct handling of the 
   psABI calling convention and Kconfig change.

* [Patch "riscv: topology: fix default topology reporting" has been added to the 5.4-stable tree](http://lore.kernel.org/linux-riscv/16668668016253@kroah.com/)

   The filename of the patch is:
        riscv-topology-fix-default-topology-reporting.patch
   and it can be found in the queue-5.4 subdirectory.

* [v1: riscv: Rewrite percpu operations and support cmpxchg-local feature](http://lore.kernel.org/linux-riscv/20221026104015.565468-1-haiwenyao@uniontech.com/)

   The series try to use riscv amo instructions to optimise some percpu
   operations and select HAVE_CMPXCHG_LOCAL to support cmpxchg-local feature.

* [v2: RZ/G2UL separate out SoC specific parts](http://lore.kernel.org/linux-riscv/20221025220629.79321-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   This patch series aims to split up the RZ/G2UL SoC DTSI into common parts
   so that this can be shared with the RZ/Five SoC.

* [v1: riscv: dts: microchip: fix memory node unit address for icicle](http://lore.kernel.org/linux-riscv/20221025195643.1215890-1-conor@kernel.org/)

   Evidently I forgot to update the unit address for the 38-bit cached
   memory node when I changed the address in the reg property..
   Update it to match.

* [v2: PCI: Remove unnecessary <linux/of_irq.h> includes](http://lore.kernel.org/linux-riscv/20221025185147.665365-1-helgaas@kernel.org/)

   Many host controller drivers #include <linux/of_irq.h> even though they
   don't need it.  Remove the unnecessary #includes.

* [v2: RISC-V: Ensure Zicbom has a valid block size](http://lore.kernel.org/linux-riscv/20221024091309.406906-1-ajones@ventanamicro.com/)

   When a DT puts zicbom in the isa string, but does not provide a block
   size, ALT_CMO_OP() will attempt to do cache operations on address
   zero since the start address will be ANDed with zero. 
   
* [v1: cleanup stackprotector canary generation](http://lore.kernel.org/linux-riscv/20221023203208.118919-1-Jason@zx2c4.com/)

   Stack canary generation currently lives partially in random.h, where it
   doesn't belong, and is in general a bit overcomplicated. This small
   patchset fixes up both issues. I'll take these in my tree, unless
   somebody else prefers to do so.

### 周边技术动态

#### Qemu

* [v1: target/riscv: Typo fix in sstc() predicate](http://lore.kernel.org/qemu-devel/20221027164743.194265-2-apatel@ventanamicro.com/)

   We should use "&&" instead of "&" when checking hcounteren.TM and
   henvcfg.STCE bits.

* [v1: target/riscv: Apply KVM policy to ISA extensions](http://lore.kernel.org/qemu-devel/20221027054649.69228-1-mchitale@ventanamicro.com/)

   Currently the single and multi letter ISA extensions exposed to the guest
   vcpu don't confirm to the KVM policies. This patchset updates the kvm headers
   and applies policies set in KVM to the extensions exposed to the guest.

* [v1: hw/riscv/opentitan: bump opentitan](http://lore.kernel.org/qemu-devel/20221025043335.339815-2-wilfred.mallawa@opensource.wdc.com/)

   This patch updates the OpenTitan model to match
   the specified register layout as per [1]. Which is also the latest
   commit of OpenTitan supported by TockOS.

* [v1: tcg/riscv: Fix base regsiter for qemu_ld/st](http://lore.kernel.org/qemu-devel/20221023124148.6283-1-zhiwei_liu@linux.alibaba.com/)

   When guest base is zero, we should use addr_regl as base regiser instead of
   the initial register TCG_REG_TMP0. Besides, we can remove the unnecessary use of base register.

#### Buildroot

* [[git commit] package/glibc: headers >= 5.4 needed on RISC-V 32-bit](http://lore.kernel.org/buildroot/20221030114429.1C2D48749F@busybox.osuosl.org/)

   commit: https://git.buildroot.net/buildroot/commit/?id=2b3f0153bb5accd1f543a43088a8cf792cda95fc
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/master

   Since glibc 2.33 (upstream commit
   to build glibc for RISC-V 32-bit. 

* [v1: package/tinycompress: new package](http://lore.kernel.org/buildroot/20221029115321.718694-1-ju.o@free.fr/)

   tinycompress is a library for compress audio offload in alsa.
   It also contains the "cplay" and "crecord" programs.
   tinycompress is part of the ALSA project.

* [v1: package/glibc: headers >= 5.4 needed on RISC-V 32-bit](http://lore.kernel.org/buildroot/20221026183427.3933387-2-thomas.petazzoni@bootlin.com/)

   Since glibc 2.33 (upstream commit
   to build glibc for RISC-V 32-bit. 

   In order to take into account this dependency, we add the appropriate
   logic in package/glibc/Config.in and
   toolchain/toolchain-buildroot/Config.in.
 
* [v1: toolchain/toolchain-buildroot: introduce BR2_TOOLCHAIN_BUILDROOT_NONE](http://lore.kernel.org/buildroot/20221026183427.3933387-1-thomas.petazzoni@bootlin.com/)

   In the internal toolchain backend, we have a choice..endchoice block
   to allow the user to select the C library, between glibc, uClibc and musl.

   However, there are situations were no C library at all is
   supported. In this case, the choice does not appear, and does not
   allow to see the Config.in comments that are within the
   choice..endchoice block and that may explain why no C library is available.

#### U-Boot

* [v1: doc: update sbi command example](http://lore.kernel.org/u-boot/20221028203105.36388-1-heinrich.schuchardt@canonical.com/)

   The output of the sbi command has been changed since the last release of
   the man-page. Update the example.

* [v3: riscv: Update Microchip MPFS Icicle Kit support](http://lore.kernel.org/u-boot/20221027060202.2495112-5-padmarao.begari@microchip.com/)

   This patch updates Microchip MPFS Icicle Kit support. For now,
   add Microchip QSPI driver and a small 4MB reservation is
   made at the end of 32-bit DDR to provide some memory for the HSS to use.

* [v1: riscv: dts: fix the mpfs's reference clock frequency](http://lore.kernel.org/u-boot/20221025075848.110754-7-conor.dooley@microchip.com/)

   The initial devicetree for PolarFire SoC incorrectly created a fixed
   frequency clock in the devicetree to represent the msspll, but the
   msspll is not a fixed frequency clock. 
  
* [v1: clk: microchip: mpfs: incremental fixes](http://lore.kernel.org/u-boot/20221025075848.110754-1-conor.dooley@microchip.com/)

  The mss pll is not a fixed frequency clock and the Linux devicetree has the actual off-chip oscillator & the clock driver there uses that. This had gone un-noticed in the original dt upstreaming to Linux and I noticed it while upstreaming the clock driver there.

## 20221023：第 17 期

### 内核动态

* [v1: RISC-V: enable sparsemem by default for defconfig](http://lore.kernel.org/linux-riscv/20221021160028.4042304-1-conor@kernel.org/)

   on an arch level, RISC-V defaults to FLATMEM. On PolarFire SoC, the
   memory layout is almost always sparse, with a maximum of 1 GiB at
   for example, has 2 GiB of DDR - so there's a big hole in the memory map
   between the two gigs. Prior to v6.1-rc1, boot times from defconfig
   builds were pretty bad on Icicle but enabling sparsemem would fix those
   issues.
   
* [v1: RISC-V: Ensure Zicbom has a valid block size](http://lore.kernel.org/linux-riscv/20221021105905.206385-1-ajones@ventanamicro.com/)

   When a DT puts zicbom in the isa string, but does not provide a block
   size, ALT_CMO_OP() will attempt to do cache operations on address
   zero since the start address will be ANDed with zero. We can't simply
   BUG() in riscv_init_cbom_blocksize() when we fail to find a block
   size because the failure will happen before logging works, leaving
   users to scratch their heads as to why the boot hung. 

* [[GIT PULL] KVM/riscv fixes for 6.1, take #1](http://lore.kernel.org/linux-riscv/CAAhSdy30JYf3SjDaAm6LHTU-yD36Nb8=FYaPpECm68O8XFdBDg@mail.gmail.com/)

   We have two fixes for 6.1:
   1) Fix for compile error seen when RISCV_ISA_ZICBOM is disabled. This fix touches code outside KVM RISC-V but I am including this here since it was affecting KVM compilation.
   2) Fix for checking pending timer interrupt when RISC-V Sstc extension is available.

* [v2: riscv: fix race when vmap stack overflow](http://lore.kernel.org/linux-riscv/20221020143329.3276-1-jszhang@kernel.org/)

   Currently, when detecting vmap stack overflow, riscv firstly switches
   to the so called shadow stack, then use this shadow stack to call the
   get_overflow_stack() to get the overflow stack. However, there's
   a race here if two or more harts use the same shadow stack at the same time.

* [V5: riscv: kexec: Fxiup crash_save percpu and machine_kexec_mask_interrupts](http://lore.kernel.org/linux-riscv/20221020141603.2856206-1-guoren@kernel.org/)

   Current riscv kexec can't crash_save percpu states and disable
   interrupts properly. The patch series fix them, make kexec work correct.

* [v1: riscv: dts: icicle: Add GPIO controlled LEDs](http://lore.kernel.org/linux-riscv/20221020083854.1127643-1-emil.renner.berthing@canonical.com/)

   This adds the 4 GPIO controlled LEDs to the Microchip PolarFire-SoC
   Icicle Kit device tree. The schematic doesn't specify any special
   function for the LEDs, so they're added here without any default
   triggers and named led1, led2, led3 and led4 just like in the schematic.

* [v5: Add PMEM support for RISC-V](http://lore.kernel.org/linux-riscv/20221020075846.305576-1-apatel@ventanamicro.com/)

   The Linux NVDIMM PEM drivers require arch support to map and access the
   persistent memory device. This series adds RISC-V PMEM support using
   recently added Svpbmt and Zicbom support.

   First two patches are fixes and remaining two patches add the required
   PMEM support for Linux RISC-V.

* [[Crash-utility]V4: Support RISCV64 arch and common commands](http://lore.kernel.org/linux-riscv/20221020015014.46085-1-xianting.tian@linux.alibaba.com/)

   To make the crash tool work normally for RISCV64 arch, we need a Linux kernel
   patch, which exports the kernel virtual memory layout, va_bits, phys_ram_base
   to vmcoreinfo, it can simplify the development of crash tool.

* [v3: soc: renesas: Add L2 cache management for RZ/Five SoC](http://lore.kernel.org/linux-riscv/20221019220242.4746-3-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   On the AX45MP core, cache coherency is a specification option so it may
   not be supported. In this case DMA will fail. As a workaround, firstly we
   allocate a global dma coherent pool from which DMA allocations are taken
   and marked as non-cacheable + bufferable using the PMA region as specified
   in the device tree. 

* [v3: dt-bindings: cache: r9a07g043f-l2-cache: Add DT binding documentation for L2 cache controller](http://lore.kernel.org/linux-riscv/20221019220242.4746-2-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   Add DT binding documentation for L2 cache controller found on RZ/Five SoC.

   The Renesas RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP
   Single) from Andes. The AX45MP core has an L2 cache controller, this patch
   describes the L2 cache block.

* [v3: AX45MP: Add support to non-coherent DMA](http://lore.kernel.org/linux-riscv/20221019220242.4746-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   On the Andes AX45MP core, cache coherency is a specification option so it
   may not be supported. In this case DMA will fail.

* [v1: RISC-V: KVM: Fix kvm_riscv_vcpu_timer_pending() for Sstc](http://lore.kernel.org/linux-riscv/20221019114535.131469-1-apatel@ventanamicro.com/)

   The kvm_riscv_vcpu_timer_pending() checks per-VCPU next_cycles
   and per-VCPU software injected VS timer interrupt. This function
   returns incorrect value when Sstc is available because the per-VCPU
   next_cycles are only updated by kvm_riscv_vcpu_timer_save() called
   from kvm_arch_vcpu_put(). As a result, when Sstc is available the
   VCPU does not block properly upon WFI traps.


* [V4: Support VMCOREINFO export for RISCV64](http://lore.kernel.org/linux-riscv/20221019103623.7008-1-xianting.tian@linux.alibaba.com/)

   As disscussed in below patch set, the patch of 'describe VMCOREINFO export in Documentation'
   need to update according to Bagas's comments. 
   
* [v1: riscv: remove special treatment for the link order of head.o](http://lore.kernel.org/linux-riscv/20221018141200.1040-1-jszhang@kernel.org/)

   arch/riscv/kernel/head.o does not need any special treatment - the only
   requirement is the ".head.text" section must be placed before the
   normal ".text" section.

* [v4: Enable initial support for StarFive VisionFive V1 SBC](http://lore.kernel.org/linux-riscv/20221017210542.979051-1-cristian.ciocaltea@collabora.com/)

   The StarFive VisionFive V1 SBC [1] is similar with the already supported
   BeagleV Starlight Beta board, both being based on the StarFive JH7100 SoC.

* [[RFC RESEND PATCH 0/2] RZ/G2UL separate out SoC specific parts](http://lore.kernel.org/linux-riscv/20221017091201.199457-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   This patch series aims to split up the RZ/G2UL SoC DTSI into common parts
   so that this can be shared with the RZ/Five SoC.

* [V7: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20221015114702.3489989-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support.

### 周边技术动态

#### Qemu

* [v1: tcg/riscv: Fix range matched by TCG_CT_CONST_M12](http://lore.kernel.org/qemu-devel/20221022095821.2441874-1-richard.henderson@linaro.org/)

   We were matching a signed 13-bit range, not a 12-bit range.
   Expand the commentary within the function and be explicit
   about all of the ranges.

* [v1: tcg/riscv: Remove a wrong optimization for addsub2](http://lore.kernel.org/qemu-devel/20221020104154.4276-4-zhiwei_liu@linux.alibaba.com/)

   It's not clear what it is doing here. And it's wrong because bl and
   al are both register, so we can't add them by an ADDI instruction.

* [v1: tcg/riscv: Fix tcg_out_opc_imm when imm exceeds](http://lore.kernel.org/qemu-devel/20221020104154.4276-3-zhiwei_liu@linux.alibaba.com/)

   TYPE-I immediate can only represent a signed 12-bit value. If immediate
   exceed, mov it to an register.
  
* [v11: target/riscv: smstateen knobs](http://lore.kernel.org/qemu-devel/20221016124726.102129-6-mchitale@ventanamicro.com/)

   Add knobs to allow users to enable smstateen and also export it via the
   ISA extension string.

#### Buildroot

* [[branch/2022.08.x] configs/sipeed_*_sdcard: Add dependency on host-openssl](http://lore.kernel.org/buildroot/20221019080332.15C6986EE1@busybox.osuosl.org/)

   commit: https://git.buildroot.net/buildroot/commit/?id=b44e2cf19a1f8f19e228019f320921fd06834f01
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/2022.08.x

   Fix this issue by adding BR2_TARGET_UBOOT_NEEDS_OPENSSL=y to the config
   files so that host-openssl gets built as a dependency of U-Boot.

* [[autobuild.buildroot.net] Daily results for 2022-10-17](http://lore.kernel.org/buildroot/20221018054308.D5A2F82F37@smtp1.osuosl.org/)

   Autobuild statistics for 2022-10-17

* [v2: riscv: dts: Update memory configuration](http://lore.kernel.org/u-boot/20221021065922.2327875-2-padmarao.begari@microchip.com/)

   In the v2022.10 Icicle reference design, the seg registers are going to be
   changed, resulting in a required change to the memory map.
   A small 4MB reservation is made at the end of 32-bit DDR to provide some
   memory for the HSS to use, so that it can cache its payload between
   reboots of a specific context.

* [v3: vbe: Implement the full firmware flow](http://lore.kernel.org/u-boot/20221021002320.1169603-1-sjg@chromium.org/)

   This series provides an implementation of VBE from TPL through to U-Boot
   proper, using VBE to load the relevant firmware stages. It buils a single
   image.
  
* [v2: riscv: ae350: Check firmware_fdt_addr header](http://lore.kernel.org/u-boot/20221020055617.7253-1-rick@andestech.com/)

   Check firmware_fdt_addr header to see if it is a valid fdt blob.

* [v1: riscv: qemu: spl: Fix booting Linux kernel with OpenSBI 1.0+](http://lore.kernel.org/u-boot/20221016164206.2223067-1-bmeng.cn@gmail.com/)

   Since OpenSBI commit bf3ef53bb7f5 ("firmware: Enable FW_PIC by default"),
   OpenSBI runs directly at the load address without any code movement.
   This causes the SPL version of QEMU 'virt' U-Boot does not boot Linux
   kernel anymore. In that case, OpenSBI is loaded and runs at 0x81000000,
   and it creates a 512KiB PMP window from that address. 

## 20221016：第 16 期

### 内核动态

* [V7: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20221015114702.3489989-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support.
  

* [[GIT PULL] RISC-V Patches for the 6.1 Merge Window, Part 2](http://lore.kernel.org/linux-riscv/mhng-569ea3ca-5e4e-4339-b3fa-84723ba9ca77@palmer-ri-x1c9a/)

 
   RISC-V Patches for the 6.1 Merge Window, Part 2

   * A handful of DT updates for the PolarFire SOC.
   * A fix to correct the handling of write-only mappings.
   * m{vetndor,arcd,imp}id is now in /proc/cpuinfo
   * The SiFive L2 cache controller support has been refactored to also
     support L3 caches.

  

* [v3: Fix /proc/cpuinfo cpumask warning](http://lore.kernel.org/linux-riscv/20221014155845.1986223-1-ajones@ventanamicro.com/)

   Commit 78e5a3399421 ("cpumask: fix checking valid cpu range") has
   started issuing warnings[*] when cpu indices equal to nr_cpu_ids - 1
   are passed to cpumask_next* functions. seq_read_iter() and cpuinfo's
   start and next seq operations implement a pattern like

     
* [V2: Support VMCOREINFO export for RISCV64](http://lore.kernel.org/linux-riscv/20221014134139.5151-1-xianting.tian@linux.alibaba.com/)

   As disscussed in below patch set, the patch of 'describe VMCOREINFO export in Documentation'
   need to update according to Bagas's comments. 
   https://lore.kernel.org/linux-riscv/22AAF52E-8CC8-4D11-99CB-88DE4D113444@kernel.org/

   As others patches in above patch set already applied, so this patch set only contains below two
   patches.

   
* [v1: RISC-V Hardware Probing User Interface](http://lore.kernel.org/linux-riscv/20221013163551.6775-1-palmer@rivosinc.com/)

   These are very much up for discussion, as it's a pretty big new user
   interface and it's quite a bit different from how we've historically
   done things: this isn't just providing an ISA string to userspace, this
   has its own format for providing information to userspace.

* [v2: RISC-V: KVM: Fix compilation without RISCV_ISA_ZICBOM](http://lore.kernel.org/linux-riscv/20221013134217.1850349-1-ajones@ventanamicro.com/)

   riscv_cbom_block_size and riscv_init_cbom_blocksize() should always
   be available and riscv_init_cbom_blocksize() should always be
   invoked, even when compiling without RISCV_ISA_ZICBOM enabled. This
   is because disabling RISCV_ISA_ZICBOM means "don't use zicbom
   instructions in the kernel" not "pretend there isn't zicbom, even when there is". 

* [[GIT PULL 0/2] microchip maintainers updates](http://lore.kernel.org/linux-riscv/20221010221704.2161221-1-conor@kernel.org/)

   Two maintainers changes that I posted on the lists a while back.
   Neither applied at the time b/c the maintainers entry was being updated
   in the soc fixes branch and in some for-next branches at the same time
   and they conflicted.

* [[rft, PATCH v2 00/36] pinctrl: Clean up and add missed headers](http://lore.kernel.org/linux-riscv/20221010201453.77401-1-andriy.shevchenko@linux.intel.com/)

   Currently the header inclusion inside the pinctrl headers seems more arbitrary
   than logical. This series is basically out of two parts:
   - add missed headers to the pin control drivers / users
   - clean up the headers of pin control subsystem

* [v1: RISC-V: KVM: Fix compilation without RISCV_ISA_ZICBOM](http://lore.kernel.org/linux-riscv/20221010094029.1579672-1-ajones@ventanamicro.com/)

   Fix undefined reference of riscv_cbom_block_size when compiling KVM
   without RISCV_ISA_ZICBOM. Note, RISCV_ISA_ZICBOM is a sufficient
   guard as it selects RISCV_DMA_NONCOHERENT, which is needed to compile
   dma-noncoherent.c (which is the file where riscv_cbom_block_size and
   its initializer live).

   Fixes: afd5dde9a186 ("RISC-V: KVM: Provide UAPI for Zicbom block size")

### 周边技术动态

#### Qemu

* [[PULL 00/10] riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20221014075830.914722-1-alistair.francis@opensource.wdc.com/)

   Third RISC-V PR for QEMU 7.2
   * Update qtest comment
   * Fix coverity issue with Ibex SPI
   * Move load_image_to_fw_cfg() to common location
   * Enable booting S-mode firmware from pflash on virt machine
   * Add disas support for vector instructions
   * Priority level fixes for PLIC
   * Fixup TLB size calculation when using PMP

* [v1: target/riscv: Add itrigger_enabled field to CPURISCVState](http://lore.kernel.org/qemu-devel/20221013062946.7530-5-zhiwei_liu@linux.alibaba.com/)

   Avoid calling riscv_itrigger_enabled() when calculate the tbflags.
   As the itrigger enable status can only be changed when write
   tdata1, migration load or itrigger fire, update env->itrigger_enabled
   at these places.

* [v1: target/riscv: Fix PMP propagation for tlb](http://lore.kernel.org/qemu-devel/20221012060016.30856-1-zhiwei_liu@linux.alibaba.com/)

   Only the pmp index that be checked by pmp_hart_has_privs can be used
   by pmp_get_tlb_size to avoid an error pmp index.

   Before modification, we may use an error pmp index. For example,
   we check address 0x4fc, and the size 0x4 in pmp_hart_has_privs. 
   
* [v1: hw/riscv: Update comment for qtest check in riscv_find_firmware()](http://lore.kernel.org/qemu-devel/BN7PR08MB435525C92550BAC5467BE672BF219@BN7PR08MB4355.namprd08.prod.outlook.com/)

   Since commit 4211fc553234 ("roms/opensbi: Remove ELF images"), the
   comment for qtest check in riscv_find_firmware() is out of date.
   Update it to reflect the latest status.

#### Buildroot


* [[autobuild.buildroot.net] Daily results for 2022-10-15](http://lore.kernel.org/buildroot/20221016053828.119BB41601@smtp4.osuosl.org/)

   Autobuild statistics for 2022-10-15
  
* [[git commit] configs/sipeed_*_sdcard: Add dependency on host-openssl](http://lore.kernel.org/buildroot/20221015154300.63F5986DE8@busybox.osuosl.org/)

   Gitlab CI reported build failures for the sipeed RISC-V nommu boards
   with the u-boot/sdcard enabled default configuration. 

* [v2: Lichee RV and Lichee RV dock support](http://lore.kernel.org/buildroot/20221014065900.3311604-1-angelo@amarulasolutions.com/)

   In order to support bluetooth on the dock, I had to fully enable the
   CONFIG_EXTRA_FIRMWARE mechanism: indeed the bluetooth driver requires a
   firmware which is not available at the moment of loading if not embedded
   in the kernel image.

#### U-Boot

* [v1: riscv: andes_plic.c: use modified IPI scheme](http://lore.kernel.org/u-boot/20221014070018.30280-1-peterlin@andestech.com/)

   The IPI scheme in OpenSBI has been updated to support 8-core AE350
   platform, the plicsw configuration needs to be modified accordingly.

* [v2: vbe: Implement the full firmware flow](http://lore.kernel.org/u-boot/20221013122927.636867-1-sjg@chromium.org/)

   This series provides an implementation of VBE from TPL through to U-Boot
   proper, using VBE to load the relevant firmware stages. It buils a single
   image.bin file containing all the phases

* [[GIT PULL] xilinx patches for v2023.01-rc1 (round 3)](http://lore.kernel.org/u-boot/12ac2391-3a8a-869a-b8f2-0c2c7c9df0f6@monstr.eu/)

   please pull the following patches to your tree. Based on discussion with Simon I 
   have also include fpga uclass series which was reviewed by him.
   There is also one gcc12 patch we discussed over IRC which was fixed by Heinrich.

* [v1: riscv: support building double-float modules](http://lore.kernel.org/u-boot/20221008091757.53072-1-heinrich.schuchardt@canonical.com/)

   The riscv32 toolchain for GCC-12 provided by kernel.org contains libgcc.a
   compiled for double-float. To link to it we have to adjust how we build
   U-Boot.

* [v1: riscv32 compiler flags](http://lore.kernel.org/u-boot/62e55f39-dab6-fbf9-b918-30ffaf7336c5@canonical.com/)

   in origin master we have moved to compiling riscv32 using the GCC the 
   32bit toolchain provided by kernel.org. This works fine with GCC-11.

## 20221009：第 15 期

### 内核动态

* [v2: riscv: jump_label: mark arguments as const to satisfy asm constraints](http://lore.kernel.org/linux-riscv/20221008145437.491-1-jszhang@kernel.org/)

   Samuel reported that the static branch usage in cpu_relax() breaks
   building with CONFIG_CC_OPTIMIZE_FOR_SIZE:

* [v2: riscv: Support HAVE_ARCH_HUGE_VMAP and HAVE_ARCH_HUGE_VMALLOC](http://lore.kernel.org/linux-riscv/20221008140506.1066805-1-liushixin2@huawei.com/)

   Since riscv64 has already support SATP_MODE_57 by default, it is time to
   support more hugepage-related features. These two patches will enable
   HAVE_ARCH_HUGE_VMAP and HAVE_ARCH_HUGE_VMALLOC.

* [[GIT PULL] RISC-V Patches for the 6.1 Merge Window, Part 1](http://lore.kernel.org/linux-riscv/mhng-6190fb79-f9f6-41bd-90f3-d4d6009d41a5@palmer-ri-x1c9/)

   RISC-V Patches for the 6.1 Merge Window, Part 1
   * Improvements to the CPU topology subsystem, which fix some issues
     where RISC-V would report bad topology information.
   * The default NR_CPUS has increased to XLEN, and the maximum
     configurable value is 512.
   * The CD-ROM filesystems have been enabled in the defconfig.
   * Support for THP_SWAP has been added for rv64 systems.

* [v1: treewide: Rename and trace arch-definitions of smp_send_reschedule()](http://lore.kernel.org/linux-riscv/20221007154533.1878285-5-vschneid@redhat.com/)

   To be able to trace invocations of smp_send_reschedule(), rename the
   arch-specific definitions of it to arch_smp_send_reschedule() and wrap it
   into an smp_send_reschedule() that contains a tracepoint.

* [v1: irq_work: Trace calls to arch_irq_work_raise()](http://lore.kernel.org/linux-riscv/20221007154533.1878285-4-vschneid@redhat.com/)

   Adding a tracepoint to send_call_function_single_ipi() covers
   irq_work_queue_on() when the CPU isn't the local one - add a tracepoint to
   irq_work_raise() to cover the other cases.
  
* [v1: Generic IPI sending tracepoint](http://lore.kernel.org/linux-riscv/20221007154145.1877054-1-vschneid@redhat.com/)

   Detecting IPI *reception* is relatively easy, e.g. using
   trace_irq_handler_{entry,exit} or even just function-trace
   flush_smp_call_function_queue() for SMP calls.  

* [v1: (attempt to) Fix RISC-V toolchain extension support detection](http://lore.kernel.org/linux-riscv/20221006173520.1785507-1-conor@kernel.org/)

   Our current checks for extension support only cover the compiler, but it
   appears to me that we need to check both the compiler & linker support
   in case of "pot-luck" configurations that mix different versions of
   LD,AS,CC etc.

* [v6: KVM: selftests: Implement ucall "pool" (for SEV)](http://lore.kernel.org/linux-riscv/20221006003409.649993-1-seanjc@google.com/)

   Rework the ucall infrastructure to use a pool of ucall structs to pass
   memory instead of using the guest's stack.  For confidential VMs with
   encrypted memory, e.g. SEV, the guest's stack "needs" to be private memory
   and so can't be used to communicate with the host.

* [v1: RISC-V: stop selecting device drivers in Kconfig.socs](http://lore.kernel.org/linux-riscv/20221005171348.167476-1-conor@kernel.org/)

   As my RFC [0] series doing the symbol name changes has not yet reached
   consensus, I've split out the removal of device driver selects into a
   new series. I kept the plic as a direct select - although given how Maz
   is treating the SiFive plic driver as the RISC-V plic driver, maybe that
   should just be selected by default at an arch level...

* [v1: riscv: bring up batched unmap tlb flush](http://lore.kernel.org/linux-riscv/20221005144324.6157-1-tjytimi@163.com/)

   For riscv, this feature can decrease the times of
   flush_tlb_pages(), and decrease the times of boardcasting for SMP.
   Add @start and @end in struct arch_tlbflush_unmap_barch in riscv
   compared to x86, which can record the minimum and maximum addresses
   in this flush batch to reduce the flush range.If there are a lot of 
   pages to reclaim, or pages are shared with many tasks like server 
   application,this feature is more beneficial.

* [v6: riscv, mm: detect svnapot cpu support at runtime](http://lore.kernel.org/linux-riscv/20221005112926.3043280-1-panqinglin2020@iscas.ac.cn/)

   Svnapot is a RISC-V extension for marking contiguous 4K pages as a non-4K
   page. This patch set is for using Svnapot in Linux Kernel's boot process
   and hugetlb fs.

   This patchset adds a Kconfig item for using Svnapot in
   "Platform type"->"SVNAPOT extension support". Its default value is off,
   and people can set it on if they allow kernel to detect Svnapot hardware
   support and leverage it.
   
* [v4: riscv_pmu_sbi: add support for PMU variant on T-Head C9xx cores](http://lore.kernel.org/linux-riscv/20221004203724.1459763-1-heiko@sntech.de/)

   The PMU on T-Head C9xx cores is quite similar to the SSCOFPMF extension
   but not completely identical, so this series

* [v2: AX45MP: Add support to non-coherent DMA](http://lore.kernel.org/linux-riscv/20221003223222.448551-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   On the Andes AX45MP core, cache coherency is a specification option so it
   may not be supported. In this case DMA will fail. To get around with this
   issue this patch series does the below.

* [v2: soc: renesas: Add L2 cache management for RZ/Five SoC](http://lore.kernel.org/linux-riscv/20221003223222.448551-3-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   On the AX45MP core, cache coherency is a specification option so it may
   not be supported. In this case DMA will fail. As a workaround, firstly we
   allocate a global dma coherent pool from which DMA allocations are taken
   and marked as non-cacheable + bufferable using the PMA region as specified
   in the device tree. 
   
* [v2: dt-bindings: soc: renesas: r9a07g043f-l2-cache: Add DT binding documentation for L2 cache controller](http://lore.kernel.org/linux-riscv/20221003223222.448551-2-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   Add DT binding documentation for L2 cache controller found on RZ/Five SoC.
   The Renesas RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP
   Single) from Andes. The AX45MP core has an L2 cache controller, this patch
   describes the L2 cache block.

* [V6: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20221002012451.2351127-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support.

### 周边技术动态

#### Qemu

* [v4: RISC-V: Add Zawrs ISA extension support](http://lore.kernel.org/qemu-devel/20221005144948.3421504-1-christoph.muellner@vrull.eu/)

   This patch adds support for the Zawrs ISA extension.
   Given the current (incomplete) implementation of reservation sets
   there seems to be no way to provide a full emulation of the WRS
   instruction (wake on reservation set invalidation or timeout or
   interrupt). Therefore, we just exit the TB and return to the main loop.

* [V5: hw/riscv: virt: Enable booting S-mode firmware from pflash](http://lore.kernel.org/qemu-devel/20221004092351.18209-4-sunilvl@ventanamicro.com/)

   To boot S-mode firmware payload like EDK2 from persistent
   flash storage, qemu needs to pass the flash address as the
   next_addr in fw_dynamic_info to the opensbi.

   When both -kernel and -pflash options are provided in command line,
   the kernel (and initrd if -initrd) will be copied to fw_cfg table.
   The S-mode FW will load the kernel/initrd from fw_cfg table.

* [v10: target/riscv: smstateen knobs](http://lore.kernel.org/qemu-devel/20221003114718.30659-6-mchitale@ventanamicro.com/)

   Add knobs to allow users to enable smstateen and also export it via the
   ISA extension string.

* [v10: RISC-V Smstateen support](http://lore.kernel.org/qemu-devel/20221003114718.30659-1-mchitale@ventanamicro.com/)

   This series adds support for the Smstateen specification which provides a
   mechanism to plug the potential covert channels which are opened by extensions
   that add to processor state that may not get context-switched. Currently access
   to *envcfg registers and floating point(fcsr) is controlled via smstateen.

#### Buildroot

* [v1: configs/visionfive_defconfig: new defconfig](http://lore.kernel.org/buildroot/20221008184016.2327196-1-peter@korsgaard.com/)

   Add a defconfig for the Starfive VisionFive board, a board built around the
   Starfive JH7100 RISC-V 64bit SoC (same as Beaglev).

   This board comes with functional lowlevel and U-Boot bootloaders in SPI
   flash.  The defconfig reuses these and only builds a (6.0 based) kernel and
   rootfs.

   The factory shipped U-Boot is hard coded to look at MMC partition 3 and
   misses some variables, so we provide a uEnv.txt to fix that up, based on
   what is done in provided Fedora image.

#### U-Boot

* [v1: riscv: support building double-float modules](http://lore.kernel.org/u-boot/20221008091757.53072-1-heinrich.schuchardt@canonical.com/)

   The riscv32 toolchain for GCC-12 provided by kernel.org contains libgcc.a
   compiled for double-float. To link to it we have to adjust how we build
   U-Boot.

   As U-Boot actually does not use floating point at all this should not
   make a significant difference for the produced binaries.

* [v1: riscv32 compiler flags](http://lore.kernel.org/u-boot/62e55f39-dab6-fbf9-b918-30ffaf7336c5@canonical.com/)

   in origin master we have moved to compiling riscv32 using the GCC the 
   32bit toolchain provided by kernel.org. This works fine with GCC-11.

* [v2: cmd/sbi: user friendly short texts](http://lore.kernel.org/u-boot/20221004080954.17884-4-heinrich.schuchardt@canonical.com/)

   In the sbi command use the same short texts for the legacy extensions
   as the SBI specification 1.0.0.

* [v4: buildman: differentiate between riscv32, riscv64](http://lore.kernel.org/u-boot/20221003160754.6991-3-heinrich.schuchardt@canonical.com/)

   riscv32 needs a different toolchain than riscv64

* [v4: riscv: Fix build against binutils 2.38](http://lore.kernel.org/u-boot/20221003160754.6991-4-heinrich.schuchardt@canonical.com/)

   The following description is copied from the equivalent patch for the
   Linux Kernel proposed by Aurelien Jarno:

   From version 2.38, binutils default to ISA spec version 20191213. This
   means that the csr read/write (csrr*/csrw*) instructions and fence.i
   instruction has separated from the `I` extension, become two standalone
   extensions: Zicsr and Zifencei. As the kernel uses those instruction,
   this causes the following build failure.

* [v4: docker: install riscv32 toolchain](http://lore.kernel.org/u-boot/20221003160754.6991-2-heinrich.schuchardt@canonical.com/)

   For building riscv32 targets we should use the riscv32 toolchain.
   Add it to the Docker image.

   Drop the riscv toolchain-alias as we do not need it in future.

* [v2: test: test bit shift operations on RISC-V](http://lore.kernel.org/u-boot/20221001195709.35244-4-heinrich.schuchardt@canonical.com/)

   Add unit tests for library functions __ashldi3() and __lshrdi3).

* [v2: riscv: implement __ashldi3, __lshrdi3](http://lore.kernel.org/u-boot/20221001195709.35244-2-heinrich.schuchardt@canonical.com/)

   On 32bit RISC-V calls to __ashrdi3 and __lshrdi3 are generated.
   These functions are normally provided by glibc but U-Boot is freestanding
   and needs its own implementation.

## 20221002：第 14 期

### 内核动态

* [[GIT PULL] KVM/riscv changes for 6.1](http://lore.kernel.org/linux-riscv/CAAhSdy134Ve1mbeK+TNRx-pWpQ=nVzNLptDcUyPaDU4v18Qyaw@mail.gmail.com/)

   KVM/riscv changes for 6.1
   - Improved instruction encoding infrastructure for
     instructions not yet supported by binutils
   - Svinval support for both KVM Host and KVM Guest
   - Zihintpause support for KVM Guest
   - Zicbom support for KVM Guest
   - Record number of signal exits as a VCPU stat
   - Use generic guest entry infrastructure

* [V6: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20221002012451.2351127-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support.

* [v1: arm64: dts: renesas: r9a07g043: Introduce SOC_PERIPHERAL_IRQ() macro to specify interrupt property](http://lore.kernel.org/linux-riscv/20220929172356.301342-2-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   Introduce SOC_PERIPHERAL_IRQ() macro to specify interrupt property so
   that we can share the common parts of the SoC DTSI with the RZ/Five
   (RISC-V) SoC and the RZ/G2UL (ARM64) SoC.

   This patch adds a new file r9a07g043u.dtsi to separate out RZ/G2UL
   (ARM64) SoC specific parts. No functional changes (same DTB).

* [v1: RZ/G2UL separate out SoC specific parts](http://lore.kernel.org/linux-riscv/20220929172356.301342-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   This patch series aims to split up the RZ/G2UL SoC DTSI into common parts
   so that this can be shared with the RZ/Five SoC.
   Implementation is based on the discussion [0] where I have used option#2.
   
   The Renesas RZ/G2UL (ARM64) and RZ/Five (RISC-V) have almost the same
   identical blocks to avoid duplication a base SoC dtsi (r9a07g043.dtsi) is
   created which will be used by the RZ/G2UL (r9a07g043u.dtsi) and RZ/Five
   (r9a07g043F.dtsi)

   Sending this as an RFC to get some feedback.

* [v1: Basic StarFive JH7110 RISC-V SoC support](http://lore.kernel.org/linux-riscv/20220929143225.17907-1-hal.feng@linux.starfivetech.com/)

   This series adds basic support for the StarFive JH7110 RISC-V SoC to
   boot up and get a serial console. This series includes basic clock, 
   reset, pinctrl and uart drivers, which are necessary for booting.
   It's should be noted that the reset and clock driver codes of JH7110
   are partly common with those of JH7100, so the common codes are
   factored out and can be reused by drivers of JH7110 and other more
   SoCs from StarFive.

* [v2: riscv: entry: further clean up and VMAP_STACK fix](http://lore.kernel.org/linux-riscv/20220928162007.3791-1-jszhang@kernel.org/)

   I planed to do similar generic entry transaction as Guo Ren did[1], and
   I had some commits in local. Since Guo has sent out the series, I
   dropped my version and just provide those in my local repo but missing
   in Guo's series. However, this doesn't mean this series depends on
   Guo's series, in fact except the first one, the remaining three patches
   are independent on generic entry.

* [v6: New PolarFire SoC devkit devicetrees & 22.09 reference design updates](http://lore.kernel.org/linux-riscv/20220927111922.3602838-1-conor.dooley@microchip.com/)

   Resending with an extra patch making some more memory map changes that
   are to be introduced in the v2022.10 reference design. Since the
   v2022.10 and v2022.09 reference designs both indepedently break
   backwards compat, v2022.09 is not compatible with <= v2022.05 and
   v2022.10 is not compatible with v2022.09, I am doing the jump directly
   to v2022.10 rather than putting an intermediate step at v2022.09.

   I did not drop Krzysztof's Ack on patch 1 as s/09/10 has no impact on
   the correctness of the binding.

* [V4: kprobe: Optimize the performance of patching ss](http://lore.kernel.org/linux-riscv/20220927022435.129965-1-liaochang1@huawei.com/)

   Single-step slot would not be used until kprobe is enabled, that means
   no race condition occurs on it under SMP, hence it is safe to pacth ss
   slot without stopping machine.

* [v3: riscv: kvm: use generic entry for TIF_NOTIFY_RESUME and misc](http://lore.kernel.org/linux-riscv/20220925162400.1606-1-jszhang@kernel.org/)

   This series is a preparation series to add PREEMPT_RT support to riscv:
   patch1 adds the missing number of signal exits in vCPU stat
   patch2 switches to the generic guest entry infrastructure
   patch3 select HAVE_POSIX_CPU_TIMERS_TASK_WORK which is a requirement for RT

### 周边技术动态

#### Qemu

* [Question about RISC-V brom register a1 set value](http://lore.kernel.org/qemu-devel/CACv+nXAT-tfuabs04y5+DE-R=Hn81nmM3bKsgOkVkWTOYpTHFA@mail.gmail.com/)

   As I know, brom will pass 3 parameters to the next stage bootloader, ex:
   openSBI.
   a0 will pass hartid, a2 will pass fw_dynamic_info start address.
   although a1 doesn't use directly in openSBI.
   a1 read value is determined in compile time rather than read from the
   original a1 that passes from brom.
   In qemu/hw/riscv/boot.c
   both 32bit and 64bit machines read 4byte that offset 32byte from the brom
   start address.
   for 64 bits machine: a1 read low 32bit data member magic of fw_dynamic_info,
   the value will same as FW_DYNAMIC_INFO_MAGIC_VALUE because risc-v is little
   endian.

* [v1: target/riscv: add support for Zcmt extension](http://lore.kernel.org/qemu-devel/20220930012345.5248-6-liweiwei@iscas.ac.cn/)

   Add encode, trans* functions and helper functions support for Zcmt
   instrutions
   Add support for jvt csr

* [v3: disas/riscv.c: rvv: Add disas support for vector instructions](http://lore.kernel.org/qemu-devel/20220928051842.16207-1-liuyang22@iscas.ac.cn/)

   Tested with https://github.com/ksco/rvv-decoder-tests

   Expected checkpatch errors for consistency and brevity reasons:

   ERROR: line over 90 characters
   ERROR: trailing statements should be on next line
   ERROR: braces {} are necessary for all arms of this statement

* [v1: riscv/opentitan: connect lifecycle controller](http://lore.kernel.org/qemu-devel/20220928050827.319293-3-wilfred.mallawa@opensource.wdc.com/)

   Connects the ibex lifecycle controller with opentitan,
   with this change, we can now get past the lifecycle checks
   in the boot rom.

* [[PULL v2 00/22] riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20220927063104.2846825-1-alistair.francis@opensource.wdc.com/)

   Second RISC-V PR for QEMU 7.2
   * Fixup typos and register addresses for Ibex SPI
   * Cleanup the RISC-V virt machine documentation
   * Remove the sideleg and sedeleg CSR macros
   * Fix the CSR check for cycle{h}, instret{h}, time{h}, hpmcounter3-31{h}
   * Remove fixed numbering from GDB xml feature files
   * Allow setting the resetvec for the OpenTitan machine
   * Check the correct exception cause in vector GDB stub
   * Fix inheritance of SiFiveEState
   * Improvements to the RISC-V debugger spec
   * Simplify some vector code

* [v8: hw/riscv: set machine->fdt in spike_board_init()](http://lore.kernel.org/qemu-devel/20220926173855.1159396-16-danielhb413@gmail.com/)

   This will enable support for the 'dumpdtb' QMP/HMP command for the spike
   machine.
   
   Reviewed-by: Philippe Mathieu-Daud&#233; <f4bug@amsat.org>

#### Buildroot


* [[branch/2022.08.x] utils/genrandconfig: disable libopenssl without atomics](http://lore.kernel.org/buildroot/20220929135526.E5B76864D1@busybox.osuosl.org/)

   The nothreads case can theoretically happen in many different
   situations, but in practice nobody disables threads. So the only
   interesting case is the FLAT case. Since ARM and RISC-V 64 both have
   atomics intrinsics, that leaves just m68k NOMMU as FLAT. So this is
   truly a corner case.

   The proper solution would be to patch GCC to also provide libatomic in
   those cases.
   - For nothreads, atomics are in fact not needed, so libatomic can simply
     be implemented as stubs.
   - For FLAT, it's probably just a matter of having a match to uclinux in
     libatomic/configure.tgt.
   
* [[git commit] package/uhd: bump to version 4.3.0.0](http://lore.kernel.org/buildroot/20220926121730.DDE79863CC@busybox.osuosl.org/)

   - boost regex is not needed since
     https://github.com/EttusResearch/uhd/commit/f773cf9fb96e25d064f43cffdc893ac905d91f15
   - Drop all patches (already in version) except first one which has been
     reverted by upstream:
     https://github.com/EttusResearch/uhd/commit/1a00949b19eaecb84af0f27c370400dc71a9fd84
   - Add LGPL-3.0+ for fpga/usrp3:
     https://github.com/EttusResearch/uhd/commit/bafa9d95453387814ef25e6b6256ba8db2df612f
   - N230 is not supported since
     https://github.com/EttusResearch/uhd/commit/d94140a4129d6b2153b15860eeb2406672ebb414
   - RFNoC is not supported since
     https://github.com/EttusResearch/uhd/commit/7d69dcdcc318ccdf87038b732acbf2bf7c087b60

#### U-Boot

* [v3: riscv: Fix build against binutils 2.38](http://lore.kernel.org/u-boot/20221002022124.270094-4-heinrich.schuchardt@canonical.com/)

   The following description is copied from the equivalent patch for the
   Linux Kernel proposed by Aurelien Jarno:

   From version 2.38, binutils default to ISA spec version 20191213. This
   means that the csr read/write (csrr*/csrw*) instructions and fence.i
   instruction has separated from the `I` extension, become two standalone
   extensions: Zicsr and Zifencei. As the kernel uses those instruction,
  
* [v3: buildman: differentiate between riscv32, riscv64](http://lore.kernel.org/u-boot/20221002022124.270094-3-heinrich.schuchardt@canonical.com/)

   riscv32 needs a different toolchain than riscv64

* [v2: riscv: implement __ashldi3, __lshrdi3](http://lore.kernel.org/u-boot/20221001195709.35244-2-heinrich.schuchardt@canonical.com/)

   On 32bit RISC-V calls to __ashrdi3 and __lshrdi3 are generated.
   These functions are normally provided by glibc but U-Boot is freestanding
   and needs its own implementation.


* [v1: cmd/sbi: format RustSBI version number](http://lore.kernel.org/u-boot/20221001073940.10018-2-heinrich.schuchardt@canonical.com/)

   The SBI command can print out the version number of the SBI implementation.
   Choose the correct output format for RustSBI.

* [v1: common: spl: Add spl NVMe boot support](http://lore.kernel.org/u-boot/20220929095639.355675-4-mchitale@ventanamicro.com/)

   Add spl_nvme to read a fat partition from a bootable NVMe device.

* [v3: net: emaclite: enable for more architectures](http://lore.kernel.org/u-boot/20220927112103.155689-1-samuel.obuch@codasip.com/)

   Function ioremap_nocache seems to be defined only for MIPS and Microblaze
   architectures. Therefore, the function call in the emaclite driver causes
   this driver to be unusable with other architectures, for example RISC-V.

   Use ioremap function instead of ioremap_nocache, and include linux/io.h
   instead of asm/io.h, so that ioremap function is automatically created,
   if not defined by the architecture. We can switch to the ioremap function,
   as Microblaze's ioremap_nocache is just empty and in MIPS implementations
   of ioremap_nocache and ioremap are the same.

* [[PULL] u-boot-riscv/next](http://lore.kernel.org/u-boot/YzFXKg%2FTPTYMmkT3@ubuntu01/)

   The following changes since commit 435596d57f8beedf36b5dc858fe7ba9d6c03334b:

     Merge tag 'u-boot-imx-20220922' of https://gitlab.denx.de/u-boot/custodians/u-boot-imx (2022-09-22 10:29:29 -0400)

   are available in the Git repository at:

     https://source.denx.de/u-boot/custodians/u-boot-riscv.git next

   for you to fetch changes up to 3c1ec13317292933fd01d9c60aae3ff1d5bc171e:

     riscv: ae350: Disable AVAILABLE_HARTS (2022-09-26 14:29:44 +0800)

   CI result shows no issue: https://source.denx.de/u-boot/custodians/u-boot-riscv/-/pipelines/13595

* [v1: Rename CONFIG_SYS_TEXT_BASE to CONFIG_TEXT_BASE](http://lore.kernel.org/u-boot/20220925150248.2524421-2-sjg@chromium.org/)

   The current name is inconsistent with SPL which uses CONFIG_SPL_TEXT_BASE
   and this makes it imposible to use CONFIG_VAL().

   Rename it to resolve this problem.

* [v1: vbe: Implement the full firmware flow](http://lore.kernel.org/u-boot/20220925150248.2524421-1-sjg@chromium.org/)

   This series provides an implementation of VBE from TPL through to U-Boot
   proper, using VBE to load the relevant firmware stages. It builds a single
   image.bin file containing all the phases:


## 20220925：第 13 期

### 内核动态

* [v1: riscv: vdso: reorganise the file to remove two forward declarations.](http://lore.kernel.org/linux-riscv/20220924074523.3764-1-jszhang@kernel.org/)

   Move the vdso_join_timens() after the compat_vdso_info and vdso_info
   structure definitions so that we can remove the two forward
   declarations. No functional changes.

* [v3: riscv: vdso: fix NULL deference in vdso_join_timens() when vfork](http://lore.kernel.org/linux-riscv/20220924070737.3048-1-jszhang@kernel.org/)

   This is because the mm->context.vdso_info is NULL in vfork case. From
   another side, mm->context.vdso_info either points to vdso info
   for RV64 or vdso info for compat, there's no need to bloat riscv's
   mm_context_t, we can handle the difference when setup the additional
   page for vdso.

* [v1: riscv: stop directly selecting drivers for ARCH_CANAAN](http://lore.kernel.org/linux-riscv/20220923185605.1900083-28-conor@kernel.org/)

   The serial and clock drivers will be enabled by default if the symbol
   itself is enabled, so stop directly selecting the drivers in Kconfigs.socs.
   
* [v1: wireguard: selftests: swap SOC_VIRT for ARCH_VIRT on riscv](http://lore.kernel.org/linux-riscv/20220923185605.1900083-21-conor@kernel.org/)

   At LPC we decided to convert all of the SOC_ symbols in arch/riscv to
   ARCH_ for consistency between "incumbent" vendors and those with a
   legacy from other architectures. To that end, swap SOC_VIRT for ARCH_VIRT.

* [[GIT PULL] RISC-V Fixes for 6.0-rc7](http://lore.kernel.org/linux-riscv/mhng-d96d9461-4a76-47d6-8a6c-fe0890247b0b@palmer-ri-x1c9/)

   RISC-V Fixes for 6.0-rc7
   * A handful of build fixes for the T-Head errata, including some
     functional issues the compilers found.
   * A fix for a nasty sigreturn bug.

* [V3: kprobe: Optimize the performance of patching ss](http://lore.kernel.org/linux-riscv/20220923084658.99304-1-liaochang1@huawei.com/)

   Single-step slot would not be used until kprobe is enabled, that means
   no race condition occurs on it under SMP, hence it is safe to pacth ss
   slot without stopping machine.

* [v1: RISC-V: Make port I/O string accessors actually work](http://lore.kernel.org/linux-riscv/alpine.DEB.2.21.2209220223080.29493@angie.orcam.me.uk/)

   Fix port I/O string accessors such as `insb', `outsb', etc. which use 
   the physical PCI port I/O address rather than the corresponding memory 
   mapping to get at the requested location, which in turn breaks at least 
   accesses made by our parport driver to a PCIe parallel port such as:

   PCI parallel port detected: 1415:c118, I/O at 0x1000(0x1008), IRQ 20
   parport0: PC-style at 0x1000 (0x1008), irq 20, using FIFO [PCSPP,TRISTATE,COMPAT,EPP,ECP]


* [v1: Prctl to enable vector commands, previous vector patches rebased](http://lore.kernel.org/linux-riscv/20220921194629.1480202-1-stillson@rivosinc.com/)

   This patch adds a prctl to enable, disable, or query whether vectors are enabled or not.  This is to allow a process to "opt out" of the overhead incurred by using vectors. Because this is build on top of an existing set of patches to work with vectors, they have been rebased to Linux 6.0-rc1.  

* [v4: mm: arm64: bring up BATCHED_UNMAP_TLB_FLUSH](http://lore.kernel.org/linux-riscv/20220921084302.43631-1-yangyicong@huawei.com/)

   Though ARM64 has the hardware to do tlb shootdown, the hardware
   broadcasting is not free.
   A simplest micro benchmark shows even on snapdragon 888 with only
   8 cores, the overhead for ptep_clear_flush is huge even for paging
   out one page mapped by only one process:

   While pages are mapped by multiple processes or HW has more CPUs,
   the cost should become even higher due to the bad scalability of
   tlb shootdown.
   
* [V2: riscv: ftrace: Fixup ftrace detour code](http://lore.kernel.org/linux-riscv/20220921034910.3142465-1-guoren@kernel.org/)

   The previous ftrace detour implementation fc76b8b8011 ("riscv: Using
   PATCHABLE_FUNCTION_ENTRY instead of MCOUNT") contain three problems. The
   most horrible bug is preemption panic which found by Andy [1]. Let's
   disable preemption for ftrace first, and Andy could continue the
   ftrace preemption work.

* [v1: RISC-V: Avoid dereferening NULL regs in die()](http://lore.kernel.org/linux-riscv/20220920200037.6727-1-palmer@rivosinc.com/)

   I don't think we can actually die() without a regs pointer, but the
   compiler was warning about a NULL check after a dereference.  It seems
   prudent to just avoid the possibly-NULL dereference, given that when
   die()ing the system is already toast so who knows how we got there.

* [v6: riscv: dts: microchip: add the mpfs' fabric clock control](http://lore.kernel.org/linux-riscv/20220920093154.24765-1-conor.dooley@microchip.com/)

   The "fabric clocks" in current PolarFire SoC device trees are not
   really fixed clocks. Their frequency is set by the bitstream, so having
   them located in -fabric.dtsi is not a problem - they're just as "fixed" as the IP blocks etc used in the FPGA fabric.
   
* [v1: RISC-V: KVM: Make ISA ext mappings explicit](http://lore.kernel.org/linux-riscv/20220919133719.230337-1-ajones@ventanamicro.com/)

   While adding new extensions at the bottom of the array isn't hard to
   do, it's a pain to review in order to ensure we're not missing any.
   Also, resolving merge conflicts for multiple new ISA extensions can be
   error-prone. To make adding new mappings foolproof, explicitly assign
   the array elements. And, now that the order doesn't matter, we can
   alphabetize the extensions, so we do that too.

* [V5: riscv: Add GENERIC_ENTRY support and related features](http://lore.kernel.org/linux-riscv/20220918155246.1203293-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Additionally, add independent irq stacks (IRQ_STACKS)
   for percpu to prevent kernel stack overflows. Add generic_entry based
   STACKLEAK support.

### 周边技术动态

#### Qemu

* [[PULL 00/12] riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20220923040704.428285-1-alistair.francis@opensource.wdc.com/)

   Second RISC-V PR for QEMU 7.2
   * Fixup typos and register addresses for Ibex SPI
   * Cleanup the RISC-V virt machine documentation
   * Remove the sideleg and sedeleg CSR macros
   * Fix the CSR check for cycle{h}, instret{h}, time{h}, hpmcounter3-31{h}
   * Remove fixed numbering from GDB xml feature files
   * Allow setting the resetvec for the OpenTitan machine
   * Check the correct exception cause in vector GDB stub
   * Fix inheritance of SiFiveEState
   
* [v1: hw/riscv/sifive_e: Fix inheritance of SiFiveEState](http://lore.kernel.org/qemu-devel/20220922075232.33653-1-shentey@gmail.com/)

   SiFiveEState inherits from SysBusDevice while it's TypeInfo claims it to
   inherit from TYPE_MACHINE. This is an inconsistency which can cause undefined behavior such as memory corruption.

* [v9: target/riscv: smstateen check for fcsr](http://lore.kernel.org/qemu-devel/20220919062908.643945-4-mchitale@ventanamicro.com/)

   If smstateen is implemented and sstateen0.fcsr is clear then the floating point
   operations must return illegal instruction exception or virtual instruction
   trap, if relevant.

* [v9: RISC-V Smstateen support](http://lore.kernel.org/qemu-devel/20220919062908.643945-1-mchitale@ventanamicro.com/)

   This series adds support for the Smstateen specification which provides a
   mechanism to plug the potential covert channels which are opened by extensions
   that add to processor state that may not get context-switched. Currently access
   to *envcfg registers and floating point(fcsr) is controlled via smstateen.

#### Buildroot

* [[git commit] package/qemu: add support for RISC-V](http://lore.kernel.org/buildroot/20220923215507.08F3685F56@busybox.osuosl.org/)

   commit: https://git.buildroot.net/buildroot/commit/?id=10fc3fa81d08f8af5bddce0b9a44dc07f001becc
   branch: https://git.buildroot.net/buildroot/commit/?id=refs/heads/master

   QEMU is supported on both 32-bit and 64-bit RISC-V hosts, so let's
   enable support for it in buildroot.

* [v1: configs/stm32f746g-disco: new defconfig](http://lore.kernel.org/buildroot/YywR+1YTnzQtkCrC@waldemar-brodkorb.de/)

   The LCD and Ethernet are _not_ yet supported by Linux upstream.
   The RAM is very limited, so the init script is stolen from the
   RISCV noMMU systems to support booting to a shell without crashing.

* [v4: package/rdma-core: new package](http://lore.kernel.org/buildroot/20220919213616.378496-1-ju.o@free.fr/)

   This is the userspace components for the Linux Kernel's
   drivers/infiniband subsystem.

* [[git commit] utils/genrandconfig: disable libopenssl without atomics](http://lore.kernel.org/buildroot/20220918134912.03172851A2@busybox.osuosl.org/)

   The proper solution would be to patch GCC to also provide libatomic in
   those cases.
   - For nothreads, atomics are in fact not needed, so libatomic can simply
     be implemented as stubs.
   - For FLAT, it's probably just a matter of having a match to uclinux in
     libatomic/configure.tgt.
     
   Again, though, this happens only in such niche cases that it's not worth
   working on it.
   
#### U-Boot

* [v2: net: emaclite: fix broken build](http://lore.kernel.org/u-boot/20220923123121.49411-1-samuel.obuch@codasip.com/)

   Function ioremap_nocache seems to be defined only for mips and microblaze
   architectures. Therefore, the function call in the emaclite driver causes
   this driver to be unusable with other architectures, for example riscv.

* [v5: board: qemu-riscv: enable semihosting](http://lore.kernel.org/u-boot/20220923070320.617623-4-kconsul@ventanamicro.com/)

   To enable semihosting we also need to enable the following
   configs in defconfigs:
   CONFIG_SEMIHOSTING
   CONFIG_SPL_SEMIHOSTING
   CONFIG_SEMIHOSTING_SERIAL
   CONFIG_SERIAL_PROBE_ALL
   CONFIG_SPL_FS_EXT4
   CONFIG_SPL_FS_FAT

* [v5: arch/riscv: add semihosting support for RISC-V](http://lore.kernel.org/u-boot/20220923070320.617623-3-kconsul@ventanamicro.com/)

   We add RISC-V semihosting based serial console for JTAG based early
   debugging.

* [v5: lib: Add common semihosting library](http://lore.kernel.org/u-boot/20220923070320.617623-2-kconsul@ventanamicro.com/)

   We factor out the arch-independent parts of the ARM semihosting
   implementation as a common library so that it can be shared
   with RISC-V.

* [v5: Add riscv semihosting support in u-boot](http://lore.kernel.org/u-boot/20220923070320.617623-1-kconsul@ventanamicro.com/)

   Semihosting is a mechanism that enables code running on
   a target to communicate and use the Input/Output
   facilities on a host computer that is running a debugger.
   This patchset adds support for semihosting in u-boot for RISCV64 targets.

* [v1: riscv: ae350: Disable AVAILABLE_HARTS](http://lore.kernel.org/u-boot/20220921063455.7814-2-uboot@andestech.com/)

   Disable AVAILABLE_HARTS mechanism to make sure that all harts
   can boot to Kernel shell successfully.
   
* [v1: riscv: Introduce AVAILABLE_HARTS](http://lore.kernel.org/u-boot/20220921063455.7814-1-uboot@andestech.com/)

   In SMP all harts will register themself in available_hart
   during start up. Then main hart will send IPI to other harts
   according to this variables. But this mechanism may not
   guarantee that all other harts can jump to next stage.
   When main hart is sending IPI to other hart according to
   available_harts, but other harts maybe still not finish the
   registration. Then the SMP booting will miss some harts finally.
   So let it become an option and it will be enabled by default. 

## 20220918：第 12 期

### 内核动态

* [v2: riscv: support update_mmu_tlb() for riscv](http://lore.kernel.org/linux-riscv/20220918055940.24726-1-tjytimi@163.com/)

   Add macro definition to support updata_mmu_tlb() for riscv,
   this function is from commit:7df676974359 ("mm/memory.c:Update
   local TLB if PTE entry exists").

* [v4: riscv: Fix permissions for all mm's during mm init](http://lore.kernel.org/linux-riscv/20220917184709.115731-1-vladimir.isaev@syntacore.com/)

   It is possible to have more than one mm (init_mm) during memory
   permission fixes. In my case it was caused by request_module
   from drivers/net/phy/phy_device.
   This is because request_module attempted to modprobe module, so it created
   new mm with the copy of kernel's page table. And this copy won't be updated
   in case of 4M pages and RV32 (pgd is the leaf).

* [v1: make weak attributes in {k,u}probes](http://lore.kernel.org/linux-riscv/20220917015522.44583-1-zouyipeng@huawei.com/)

   We have some function implementation under some arch does nothing.
   We can mark it with weak attributes to improve.
   1. arch_init_kprobes in kprobes
   2. arch_uprobe_exception_notify in uprobes

* [[GIT PULL] RISC-V Fixes for 6.0-rc6](http://lore.kernel.org/linux-riscv/mhng-f5d21762-1321-4d35-927c-d47e0749abc0@palmer-ri-x1c9/)

   RISC-V Fixes for 6.0-rc6
   * A handful of build fixes for the T-Head errata, including some
     functional issues the compilers found.
   * A fix to avoid bad page permission initialization, which manifests on
     systems that may load modules early.
   * A fix for a nasty sigreturn bug.

* [v1: riscv: ftrace: Fixup ftrace detour code](http://lore.kernel.org/linux-riscv/20220916103817.9490-1-guoren@kernel.org/)

   The previous ftrace detour implementation fc76b8b8011 ("riscv: Using
   PATCHABLE_FUNCTION_ENTRY instead of MCOUNT") contain three problems. The
   most horrible bug is preemption panic which found by Andy [1]. I think we
   could disable preemption for ftrace first, and Andy could continue the
   ftrace preemption work.

* [v1: RISC-V: KVM: Allow Guest use Zihintpause extension](http://lore.kernel.org/linux-riscv/20220916054637.24133-1-mchitale@ventanamicro.com/)

   We should advertise Zihintpause ISA extension to KVM user-space whenever
   host supports it. This will allow KVM user-space (i.e. QEMU or KVMTOOL)
   to pass on this information to Guest via ISA string.

* [v2: riscv: ztso: disallow elf binaries needing TSO](http://lore.kernel.org/linux-riscv/20220916042331.1398823-1-vineetg@rivosinc.com/)

   As of now the software stack needs work to support ztso. Until that work
   is finished, disallow binaries needing TSO.

   This patch is needed to help ztso ratification and prolifiration of tso
   bits in tooling.

* [v1: riscv/ftrace: Set FTRACE_FORCE_LIST_FUNC if DYNAMIC_FTRACE_WITH_REGS is not set](http://lore.kernel.org/linux-riscv/20220916005135.91945-1-liaochang1@huawei.com/)

  The reason is: for static tracing, RISC-V mcount only support passing up
   ip and parent_ip, if it does not force list func, some C side effects
   occurs, where a function is called without passing a valid third
   parameter, then kernel will trap into page fault when jump from mcount
   to function_trace_call().


* [v4: Make mmap() with PROT_WRITE imply PROT_READ](http://lore.kernel.org/linux-riscv/20220915193702.2201018-1-abrestic@rivosinc.com/)

   Commit 2139619bcad7 ("riscv: mmap with PROT_WRITE but no PROT_READ is
   invalid") made mmap() reject mappings with only PROT_WRITE set in an
   attempt to fix an observed inconsistency in behavior when attempting
   to read from a PROT_WRITE-only mapping. The root cause of this behavior
   was actually that while RISC-V's protection_map maps VM_WRITE to
   readable PTE permissions (since write-only PTEs are considered reserved
   by the privileged spec), the page fault handler considered loads from
   VM_WRITE-only VMAs illegal accesses. 
  
* [v3: Add support for Renesas RZ/Five SoC](http://lore.kernel.org/linux-riscv/20220915181558.354737-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   The RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP Single)
   1.0 GHz, 16-bit DDR3L/DDR4 interface. And it also has many interfaces such
   as Gbit-Ether, CAN, and USB 2.0, making it ideal for applications such as
   entry-class social infrastructure gateway control and industrial gateway
   control.

* [v1: RISC-V: move riscv_cbom_block_size to the correct #ifdef block](http://lore.kernel.org/linux-riscv/20220914143648.74022-1-heiko@sntech.de/)

   riscv_cbom_block_size is used by all current non-coherent dma operations,
   not only the zicbom variant. So move it over the block also containing
   the riscv_noncoherent_supported() prototype.
   Fixes: 8f7e001e0325 ("RISC-V: Clean up the Zicbom block size probing")

* [v2: riscv: kprobes: implement optprobes](http://lore.kernel.org/linux-riscv/20220914031359.593629-1-chenguokai17@mails.ucas.ac.cn/)

   Add jump optimization support for RISC-V.

   Replaces ebreak instructions used by normal kprobes with an
   auipc+jalr instruction pair, at the aim of suppressing the probe-hit
   overhead.

   All known optprobe-capable RISC architectures have been using a single
   jump or branch instructions while this patch chooses not. RISC-V has a
   quite limited jump range (4KB or 2MB) for both its branch and jump
   instructions, which prevent optimizations from supporting probes that
   spread all over the kernel.

* [v2: riscv: align ftrace to 4 Byte boundary and increase ftrace prologue size](http://lore.kernel.org/linux-riscv/20220913094252.3555240-2-andy.chiu@sifive.com/)

   We are introducing a new ftrace mechanism in order to phase out
   stop_machine() and enable kernel preemption. The new mechanism requires
   ftrace patchable function entries to be 24 bytes and aligned to 4 Byte
   boundaries.

   Before applying this patch, the size of the kernel code, with 122465 of
   ftrace entries, was at 12.46 MB. Under the same configuration, the size
   has increased to 12.99 MB after applying this patch set.

* [v2: Enable ftrace with kernel preemption for RISC-V](http://lore.kernel.org/linux-riscv/20220913094252.3555240-1-andy.chiu@sifive.com/)

   This patch removes dependency of dynamic ftrace from calling
   stop_machine(), and makes it compatiable with kernel preemption.
   Originally, we ran into stack corruptions, or execution of partially
   updated instructions when starting or stopping ftrace on a fully
   preemptible kernel configuration. The reason is that kernel periodically
   calls rcu_momentary_dyntick_idle() on cores waiting for the code-patching
   core running in ftrace.
  
* [v5: Use composable cache instead of L2 cache](http://lore.kernel.org/linux-riscv/20220913061817.22564-1-zong.li@sifive.com/)

   Since composable cache may be L3 cache if private L2 cache exists, we
   should use its original name "composable cache" to prevent confusion.

   This patchset contains the modification which is related to ccache, such
   as DT binding and EDAC driver.

* [V2: riscv/kprobes: allocate detour buffer from module area](http://lore.kernel.org/linux-riscv/20220913022334.83997-1-liaochang1@huawei.com/)

   To address the limitation of PC-relative branch instruction
   on riscv architecture, detour buffer slot used for optprobes is
   allocated from a region, the distance of which from kernel should be
   less than 4GB.

   For the time being, Modules region always live before the kernel.
   But Vmalloc region reside far from kernel, the distance is half of the
   kernel address space (See Documentation/riscv/vm-layout.rst), hence it
   needs to override the alloc_optinsn_page() to make sure allocate detour
   buffer from jump-safe region.

### 周边技术动态

#### Qemu

* [v1: target/riscv: Check the correct exception cause in vector GDB stub](http://lore.kernel.org/qemu-devel/20220918083245.13028-1-frank.chang@sifive.com/)

   After RISCVException enum is introduced, riscv_csrrw_debug() returns
   RISCV_EXCP_NONE to indicate there's no error. RISC-V vector GDB stub
   should check the result against RISCV_EXCP_NONE instead of value 0.
   Otherwise, 'E14' packet would be incorrectly reported for vector CSRs
   when using "info reg vector" GDB command.

* [v2: RISC-V: Add support for Ztso](http://lore.kernel.org/qemu-devel/20220917072635.11616-1-palmer@rivosinc.com/)

   The Ztso extension was recently frozen, this adds it as a CPU property
   and adds various fences throughout the port in order to allow TSO
   targets to function on weaker hosts.  We need no fences for AMOs as
   they're already SC, the placess we need barriers are described.
 
* [[PULL 11/11] target/riscv: Honour -semihosting-config userspace=on and enable=on](http://lore.kernel.org/qemu-devel/20220914132308.118495-12-richard.henderson@linaro.org/)

   The riscv target incorrectly enabled semihosting always, whether the user asked for it or not.  Call semihosting_enabled() passing the correct value to the is_userspace argument, which fixes this and also handles the userspace=on argument.  Because we do this at translate time, we no longer need to check the privilege level in riscv_cpu_do_interrupt().

* [v1: hw/riscv: opentitan: Expose the resetvec as a SoC property](http://lore.kernel.org/qemu-devel/20220914101108.82571-4-alistair.francis@wdc.com/)

   On the OpenTitan hardware the resetvec is fixed at the start of ROM. In
   QEMU we don't run the ROM code and instead just jump to the next stage.
   This means we need to be a little more flexible about what the resetvec
   is.

   This patch allows us to set the resetvec from the command line with
   something like this:
       -global driver=riscv.lowrisc.ibex.soc,property=resetvec,value=0x20000400

   This way as the next stage changes we can update the resetvec.

#### Buildroot

* [[branch/2022.05.x] package/go: fix go on riscv64 in sv57 mode](http://lore.kernel.org/buildroot/20220914220453.219A583A8A@busybox.osuosl.org/)

   On machines supporting Riscv SV57 mode like Qemu, Go programs currently crash with the following type of error:
   runtime: lfstack.push invalid packing: node=0xffffff5908a940 cnt=0x1 packed=0xffff5908a9400001 -> node=0xffff5908a940
   The upstream PR fixes this error, but has not yet been merged.

#### U-Boot

* [v2: Add riscv semihosting support in u-boot](http://lore.kernel.org/u-boot/20220916081929.1975717-1-kconsul@ventanamicro.com/)

   Semihosting is a mechanism that enables code running on
   a target to communicate and use the Input/Output
   facilities on a host computer that is running a debugger.
   This patchset adds support for semihosting in u-boot
   for RISCV64 targets.

* [v2: board: qemu-riscv: enable semihosting](http://lore.kernel.org/u-boot/20220916081233.1970135-4-kconsul@ventanamicro.com/)

   To enable semihosting we also need to enable the following
   configs in defconfigs:
   CONFIG_SEMIHOSTING
   CONFIG_SPL_SEMIHOSTING
   CONFIG_SEMIHOSTING_SERIAL
   CONFIG_SERIAL_PROBE_ALL
   CONFIG_SPL_FS_EXT4
   CONFIG_SPL_FS_FAT

* [v2: arch/riscv: add semihosting support for RISC-V](http://lore.kernel.org/u-boot/20220916081233.1970135-3-kconsul@ventanamicro.com/)

   We add RISC-V semihosting based serial console for JTAG based early
   debugging.

* [v2: lib: Add common semihosting library](http://lore.kernel.org/u-boot/20220916081233.1970135-2-kconsul@ventanamicro.com/)

   We factor out the arch-independent parts of the ARM semihosting
   implementation as a common library so that it can be shared with RISC-V.

* [v3: timer: orion-timer: Add support for other Armada SoC's](http://lore.kernel.org/u-boot/20220915142043.1549061-3-sr@denx.de/)

   This patch adds support for other Marvell Armada SoC's, supporting the
   25MHz fixed clock operation, like the Armada XP etc.

* [V2: Kconfig: enlarge CONFIG_SYS_MALLOC_F_LEN](http://lore.kernel.org/u-boot/20220915013818.2212-1-peng.fan@oss.nxp.com/)

   "alloc space exhausted" happens in very early stage, which could be seen
   with DEBUG_UART options enabled and leeds to an non-functional board.

* [v17: eficonfig: menu-driven addition of UEFI boot option](http://lore.kernel.org/u-boot/20220912083359.24510-2-masahisa.kojima@linaro.org/)

   This commit add the "eficonfig" command.
   The "eficonfig" command implements the menu-driven UEFI boot option
   maintenance feature. This commit implements the addition of
   new boot option. User can select the block device volume having
   efi_simple_file_system_protocol and select the file corresponding
   to the Boot#### variable. User can also enter the description and
   optional_data of the BOOT#### variable in utf8.
   This commit adds "include/efi_config.h", it contains the common
   definition to be used from other menus such as UEFI Secure Boot
   key management.

## 20220912：第 11 期

### 内核动态

* [v4: Use composable cache instead of L2 cache](http://lore.kernel.org/linux-riscv/20220912065029.1793-1-zong.li@sifive.com/)

   Since composable cache may be L3 cache if private L2 cache exists, we
   should use its original name "composable cache" to prevent confusion.

   This patchset contains the modification which is related to ccache, such
   as DT binding and EDAC driver.

   The DT binding is based on top of Conor's patch, it has got ready for
   merging, and it looks that it would be taken into the next few 6.0-rc
   version. If there is any change, the next version of this series will be
   posted as well.
   
* [v3: riscv: KVM: Expose Zicbom to the guest](http://lore.kernel.org/linux-riscv/20220909144400.1114485-1-ajones@ventanamicro.com/)

   Add support for exposing the Zicbom extension to guests. This has been
   tested over a QEMU including the Zicbom support [1] ([1] was rebased and
   the Zicboz support from it was dropped.) QEMU was further modified to
   ensure the cache block size was provided in the DT. kvmtool was also
   modified [2] to test the new KVM API and provide the guest the cache
   block size in its DT (The kvmtool patches are based on Anup's Svpbmt/Sstc
   series [3]). These KVM patches are based on the riscv_init_cbom_blocksize()
   cleanup patch from Palmer and the move and expose riscv_cbom_block_size
   patch from Anup which was reposted [4]. This series is also available here
   [5].

   * [1] https://gitlab.com/jones-drew/qemu/-/commits/riscv/zicbom
   * [2] https://github.com/jones-drew/kvmtool/commits/riscv/zicbom
   * [3] https://github.com/avpatel/kvmtool/commits/riscv_svpbmt_sstc_v1
   * [4] https://lore.kernel.org/linux-riscv/20220906074509.928865-1-ajones@ventanamicro.com/T/#u
   * [5] https://github.com/jones-drew/linux/commits/riscv/kvm/zicbom-v3


* [[GIT PULL] RISC-V Fixes for 6.0-rc5](http://lore.kernel.org/linux-riscv/mhng-8b892856-b176-48da-8f9b-869810e0cf26@palmer-ri-x1c9/)

   * A pair of device tree fixes for the Polarfire SOC.
   * A fix to avoid overflowing the PMU counter array when firmware
     incorrectly reports the number of supported counters, which manifests
     on OpenSBI versions prior to 1.1.

* [v1: riscv: Fix TRACE_IRQFLAGS call points in entry.S](http://lore.kernel.org/linux-riscv/20220909094230.252031-1-zouyipeng@huawei.com/)

   TRACE_IRQFLAGS need trace all {Enable,Disable} hard interrupts points
   in entry.S.
   We miss a path(work_notifysig) that cause an WARN as shown below.

* [v2: riscv: vdso: fix NULL deference in vdso_join_timens() when vfork](http://lore.kernel.org/linux-riscv/20220908150421.323-1-jszhang@kernel.org/)

   This is because the mm->context.vdso_info is NULL in vfork case. From
   another side, mm->context.vdso_info either points to vdso info
   for RV64 or vdso info for compat, there's no need to bloat riscv's
   mm_context_t, we can handle the difference when setup the additional
   page for vdso.

   Fixes: 3092eb456375 ("riscv: compat: vdso: Add setup additional pages implementation")
  
* [v3: Enable initial support for StarFive VisionFive V1 SBC](http://lore.kernel.org/linux-riscv/20220908142914.359777-1-cristian.ciocaltea@collabora.com/)

   The StarFive VisionFive V1 SBC [1] is similar with the already supported
   BeagleV Starlight Beta board, both being based on the StarFive JH7100 SoC.

   In addition to documenting the necessary compatibles, this patch series 
   moves most of the content from jh7100-beaglev-starlight.dts to a new file
   jh7100-common.dtsi, to be shared between the two boards.

   No other changes are required in order to successfully boot the board.

* [v1: RISC-V: KVM: Change the SBI specification version to v1.0](http://lore.kernel.org/linux-riscv/20220908110404.186725-1-apatel@ventanamicro.com/)

   The SBI v1.0 specificaiton is functionally same as SBI v0.3
   specification except that SBI v1.0 specification went through
   the full RISC-V International ratification process.

* [V4: riscv: Add GENERIC_ENTRY, irq stack support](http://lore.kernel.org/linux-riscv/20220908022506.1275799-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from
   kernel/entry/*. Add independent irq stacks (IRQ_STACKS) for percpu to
   prevent kernel stack overflows. Add the HAVE_SOFTIRQ_ON_OWN_STACK
   feature for the IRQ_STACKS config. You can try it directly with [1].

   [1] https://github.com/guoren83/linux/tree/generic_entry_v4

* [v1: riscv: make t-head erratas depend on MMU](http://lore.kernel.org/linux-riscv/20220907154932.2858518-1-heiko@sntech.de/)

   Both basic extensions of SVPBMT and ZICBOM depend on CONFIG_MMU.
   Make the T-Head errata implementations of the similar functionality
   also depend on it to prevent build errors.
   Fixes: a35707c3d850 ("riscv: add memory-type errata for T-Head")
   Fixes: d20ec7529236 ("riscv: implement cache-management errata for T-Head SoCs")

* [v1: riscv : support update_mmu_tlb() for riscv](http://lore.kernel.org/linux-riscv/20220906121921.8355-1-tjytimi@163.com/)

   Add macro definition to support updata_mmu_tlb() for riscv,
   this function is from commit:7df676974359 ("mm/memory.c:Update 
   local TLB if PTE entry exists").

* [v3: riscv: lib: optimize memcmp with ld insn](http://lore.kernel.org/linux-riscv/20220906115359.173660-1-zouyipeng@huawei.com/)

   Currently memcmp was implemented in c code(lib/string.c), which compare
   memory per byte.
   This patch use ld insn compare memory per word to improve. From the test
   Results, this will take several times optimized.

* [v3: tty: TX helpers](http://lore.kernel.org/linux-riscv/20220906104805.23211-1-jslaby@suse.cz/)

   This series introduces DEFINE_UART_PORT_TX_HELPER +
   DEFINE_UART_PORT_TX_HELPER_LIMITED TX helpers. See PATCH 2/4 for the
   details. Comments welcome.

* [v1: riscv: vendors: andes: Add support to configure the PMA regions](http://lore.kernel.org/linux-riscv/20220906102154.32526-2-prabhakar.mahadev-lad.rj@bp.renesas.com/)

   The Andes AX45MP core has a Programmable Physical Memory Attributes (PMA)
   block that allows dynamic adjustment of memory attributes in the runtime.
   It contains a configurable amount of PMA entries implemented as CSR
   registers to control the attributes of memory locations in interest.

* [v2: Some style cleanups for recent extension additions](http://lore.kernel.org/linux-riscv/20220905111027.2463297-1-heiko@sntech.de/)

   As noted by some people, some parts of the recently added extensions
   (svpbmt, zicbom) + t-head errata could use some styling upgrades.
   So this series provides these.


### 周边技术动态

#### Qemu

* [v1: target/riscv/pmp: fix non-translated page size address checks w/ MPU](http://lore.kernel.org/qemu-devel/20220909152258.2568942-1-leon@is.currently.online/)

   This commit fixes PMP address access checks with non page-aligned PMP
   regions on harts with MPU enabled. Without this change, the presence
   of an MPU in the virtual CPU model would influence the PMP address
   check behavior when an access size was unknown (`size == 0`),
   regardless of whether virtual memory has actually been enabled by the
   guest.

* [v2: target/riscv: debug: Introduce tinfo CSR](http://lore.kernel.org/qemu-devel/20220909134215.1843865-6-bmeng.cn@gmail.com/)

     One bit for each possible type enumerated in tdata1.
     If the bit is set, then that type is supported by the currently
     selected trigger.

* [v2: target/riscv: Improve RISC-V Debug support](http://lore.kernel.org/qemu-devel/20220909134215.1843865-1-bmeng.cn@gmail.com/)

   This patchset refactors RISC-V Debug support to allow more types of
   triggers to be extended.
   The initial support of type 6 trigger, which is similar to type 2
   trigger with additional functionality, is also introduced in this
   patchset.
   This is a v2 respin of previous patch originally done by Frank Chang
   at SiFive. I've incorperated my review comments in v2 and rebased
   against QEMU mainline.

* [v2: target/riscv: debug: Determine the trigger type from tdata1.type](http://lore.kernel.org/qemu-devel/20220909134215.1843865-2-bmeng.cn@gmail.com/)

   Current RISC-V debug assumes that only type 2 trigger is supported.
   To allow more types of triggers to be supported in the future
   (e.g. type 6 trigger, which is similar to type 2 trigger with additional
    functionality), we should determine the trigger type from tdata1.type.
   RV_MAX_TRIGGERS is also introduced in replacement of TRIGGER_TYPE2_NUM.

* [[PULL] riscv-to-apply queue](http://lore.kernel.org/qemu-devel/20220907080353.111926-1-alistair.francis@wdc.com/)

   First RISC-V PR for QEMU 7.2
   * Update [m|h]tinst CSR in interrupt handling
   * Force disable extensions if priv spec version does not match
   * fix shifts shamt value for rv128c
   * move zmmul out of the experimental
   * virt: pass random seed to fdt
   * Add checks for supported extension combinations
   * Upgrade OpenSBI to v1.1
   * Fix typo and restore Pointer Masking functionality for RISC-V
   * Add mask agnostic behaviour (rvv_ma_all_1s) for vector extension
   * Add Zihintpause support
   * opentitan: bump opentitan version
   * microchip_pfsoc: fix kernel panics due to missing peripherals
   * Remove additional priv version check for mcountinhibit
   * virt machine device tree improvements
   * Add xicondops in ISA entry
   * Use official extension names for AIA CSRs

* [v1: RISC-V: Adding T-Head CondMov instructions](http://lore.kernel.org/qemu-devel/20220906122243.1243354-6-christoph.muellner@vrull.eu/)

   This patch adds support for the T-Head CondMov instructions.
   The patch uses the T-Head specific decoder and translation.
   
* [V4: hw/riscv: virt: Enable booting S-mode firmware from pflash](http://lore.kernel.org/qemu-devel/20220906090219.412517-1-sunilvl@ventanamicro.com/)

   These changes are tested with a WIP EDK2 port for virt machine. Below
   are the instructions to build and test this feature.

* [V4: hw/riscv: virt: Move create_fw_cfg() prior to loading kernel](http://lore.kernel.org/qemu-devel/20220906090219.412517-3-sunilvl@ventanamicro.com/)

   To enable both -kernel and -pflash options, the fw_cfg needs to be
   created prior to loading the kernel.

* [v1: docs/system: clean up code escape for riscv virt platform](http://lore.kernel.org/qemu-devel/20220905163939.1599368-1-alex.bennee@linaro.org/)

   The example code is rendered slightly mangled due to missing code
   block. Properly escape the code block and add shell prompt and qemu to
   fit in with the other examples on the page.

* [v6: hw/riscv: set machine->fdt in spike_board_init()](http://lore.kernel.org/qemu-devel/20220904233456.209027-13-danielhb413@gmail.com/)

   This will enable support for the 'dumpdtb' QMP/HMP command for the spike
   machine.

#### Buildroot

* [[git commit] nezha_defconfig: bump opensbi, u-boot and linux](http://lore.kernel.org/buildroot/20220911084829.3BD2C83620@busybox.osuosl.org/)

   The incompatibility between opensbi and u-boot is now fixed.
   The updated device tree in the kernel tree no longer specifies a memory
   node (and the board exists in 512M/1G/2G variants, so instead use the
   (otherwise identical) device tree provided by u-boot, where the memory
   node is fixed up based on the detected memory size.


#### U-Boot

* [v17: eficonfig: menu-driven addition of UEFI boot option](http://lore.kernel.org/u-boot/20220912083359.24510-2-masahisa.kojima@linaro.org/)

   This commit adds "include/efi_config.h", it contains the common
   definition to be used from other menus such as UEFI Secure Boot
   key management.

* [v1: board_f: Fix types for board_get_usable_ram_top()](http://lore.kernel.org/u-boot/20220909153246.8455-4-pali@kernel.org/)

   Commit 37dc958947ed ("global_data.h: Change ram_top type to phys_addr_t")
   changed type of ram_top member from ulong to phys_addr_t but did not
   changed types in board_get_usable_ram_top() function which returns value
   for ram_top.
   So change ulong to phys_addr_t type also in board_get_usable_ram_top()
   signature and implementations.

* [v4: IPv6 support](http://lore.kernel.org/u-boot/20220908115905.70928-1-v.v.mitrofanov@yadro.com/)

   This patch set adds basic IPv6 support to U-boot.
   It is based on Chris's Packham patches
   (https://lists.denx.de/pipermail/u-boot/2017-January/279366.html)
   Chris's patches were taken as base. There were efforts to launch it on
   HiFive SiFive Unmatched board but the board didn't work well.


* [v1: RISC-V: enable CONFIG_SYSRESET_SBI by default](http://lore.kernel.org/u-boot/20220905144049.791286-1-heinrich.schuchardt@canonical.com/)

   System reset via the SRST extension in the SBI should be the default.
   The driver checks if the extension is available when probing.
   So there is no risk in enabling it.

## 20220904：第 10 期

### 内核动态

* [V1: arch/riscv: kprobes: implement optprobes](https://lore.kernel.org/linux-riscv/999df577-48bd-6ffb-a868-cad8ba4615a1@microchip.com/T/#t)

   This patch adds jump optimization support for RISC-V.

   This patch replaces ebreak instructions used by normal kprobes with an
auipc+jalr instruction pair, at the aim of suppressing the probe-hit
overhead.

   All known optprobe-capable RISC architectures have been using a single
   jump or branch instructions while this patch chooses not. RISC-V has a
   quite limited jump range (4KB or 2MB) for both its branch and jump
   instructions, which prevent optimizations from supporting probes that
   spread all over the kernel.

   Auipc-jalr instruction pair is introduced with a much wider jump range
   (4GB), where auipc loads the upper 12 bits to a free register and jalr
   appends the lower 20 bits to form a 32 bit immediate. Note that returning
   from probe handler requires another free register. As kprobes can appear
   almost anywhere inside the kernel, the free register should be found in a
   generic way, not depending on calling convension or any other regulations.

* [V2: riscv: Add GENERIC_ENTRY, IRQ_STACKS support](http://lore.kernel.org/linux-riscv/20220904072637.8619-1-guoren@kernel.org/)

   The patches convert riscv to use the generic entry infrastructure from kernel/entry/*. Add independent irq stacks (IRQ_STACKS) for percpu to prevent kernel stack overflows. Add the HAVE_SOFTIRQ_ON_OWN_STACK feature for the IRQ_STACKS config.

* [V2: arch: Cleanup ptrace_disable](http://lore.kernel.org/linux-riscv/20220903162328.1952477-1-guoren@kernel.org/)

   This series cleanup ptrace_disable() in arch/*. Some architectures
   are duplicate clearing SYSCALL TRACE.

* [v9: RISC-V IPI Improvements](http://lore.kernel.org/linux-riscv/20220903161309.32848-1-apatel@ventanamicro.com/)

   This series aims to improve IPI support in Linux RISC-V in following ways:
    1) Treat IPIs as normal per-CPU interrupts instead of having custom RISC-V
       specific hooks. This also makes Linux RISC-V IPI support aligned with
       other architectures.
    2) Remote TLB flushes and icache flushes should prefer local IPIs instead
       of SBI calls whenever we have specialized hardware (such as RISC-V AIA
       IMSIC and RISC-V SWI) which allows S-mode software to directly inject
       IPIs without any assistance from M-mode runtime firmware.

   These patches were originally part of the "Linux RISC-V ACLINT Support"
   series but this now a separate series so that it can be merged independently
   of the "Linux RISC-V ACLINT Support" series.


* [v2: riscv: lib: optimize memcmp with ld insn](http://lore.kernel.org/linux-riscv/20220902110039.226016-1-zouyipeng@huawei.com/)

   Currently memcmp was implemented in c code(lib/string.c), which compare
   memory per byte.

   This patch use ld insn compare memory per word to improve. From the test
   Results, this will take several times optimized.

   Alloc 8,4,1KB buffer to compare, each loop 10k times:

   **// before**
   Size(B)|Min(ns) | AVG(ns) 
   -------|--------|----------
   8k     | 40800  | 46316
   4k     | 26500  | 32302
   1k     | 15600  | 17965

   **//after**
   Size(B)| Min(ns)| AVG(ns)
   -------|--------|-----------
   8k     | 16100  | 21281
   4k     | 14200  | 16446
   1k     | 12400  | 14316


* [v3: riscv: Fix permissions for all mm's during mm init](http://lore.kernel.org/linux-riscv/20220902101312.220350-1-vladimir.isaev@syntacore.com/)

   It is possible to have more than one mm (init_mm) during memory
   permission fixes. In my case it was caused by request_module
   from drivers/net/phy/phy_device.c and leads to following Oops
   during free_initmem() on RV32 platform.

* [v1: RISC-V: Add support for Ztso](http://lore.kernel.org/linux-riscv/20220902034352.8825-1-palmer@rivosinc.com/)

   The Ztso extension was recently frozen, this adds support for running
   binaries that depend on TSO on systems that support Ztso.

   This is very minimaly tested: I can run no-Ztso binaries on both
   yes-Ztso and no-Ztso QEMU instances, but I don't have a yes-Ztso
   userspace together in order to make sure that works.

* [v1: Enable initial support for StarFive VisionFive V1 SBC](http://lore.kernel.org/linux-riscv/20220901224253.2353071-1-cristian.ciocaltea@collabora.com/)

   The StarFive VisionFive V1 SBC [1] is similar with the already supported
   BeagleV Starlight Beta board, both being based on the StarFive JH7100 SoC.

   In addition to documenting the necessary compatibles, this patch series 
   moves most of the content from jh7100-beaglev-starlight.dts to a new file
   jh7100-common.dtsi, to be shared between the two boards.

   No other changes are required in order to successfully boot the board.

   [1] https://github.com/starfive-tech/VisionFive

* [v1: riscv: fix early reserved memory setup](http://lore.kernel.org/linux-riscv/20220901170627.2329578-1-mail@conchuod.ie/)

   Currently, RISC-V sets up reserved memory using the "early" copy of the
   device tree. As a result, when trying to get a reserved memory region
   in a remoteproc driver using of_reserved_mem_lookup(), the pointer to
   a reserved memory region's name is using an early, pre-virtual-memory
   address which causes a kernel panic.

* [v1: riscv: tracing: Improve hardirq tracing message](http://lore.kernel.org/linux-riscv/20220901104515.135162-1-zouyipeng@huawei.com/)

   Currently, hardirq tracing log in riscv showing the last {enabled,disabled}
   at __trace_hardirqs_{on,off} all the time.

   We can use trace_hardirqs_on_caller to display the caller we really want
   to see.

* [v2: riscv: add PREEMPT_RT support](http://lore.kernel.org/linux-riscv/20220831175920.2806-1-jszhang@kernel.org/)

   This series is to add PREEMPT_RT support to riscv:
   patch1 adds the missing number of signal exits in vCPU stat
   patch2 switches to the generic guest entry infrastructure
   patch3 select HAVE_POSIX_CPU_TIMERS_TASK_WORK which is a requirement for
   RT
   patch4 adds lazy preempt support
   patch5 allows to enable PREEMPT_RT

   I assume patch1, patch2 and patch3 can be reviewed and merged for
   riscv-next, patch4 and patch5 can be reviewed and maintained in rt tree,
   and finally merged once the remaining patches in rt tree are all
   mainlined.

* [v2: riscv: Introduce support for defining instructions](http://lore.kernel.org/linux-riscv/20220831172500.752195-1-ajones@ventanamicro.com/)

   When compiling with toolchains that haven't yet been taught about
   new instructions we need to encode them ourselves. This series
   creates a new file where support for instruction definitions can
   evolve. For starters the file is initiated with a macro for R-type
   encodings. The series then applies the R-type encoding macro to all
   instances of hard coded instruction definitions in KVM.

   Not only should using instruction encoding macros improve readability
   and maintainability of code, but we should also gain potential for
   more optimized code after compilation as the compiler will have control
   over the input and output registers used, which may provide more
   opportunities for inlining.

* [v1: riscv: mm: notify remote harts about mmu cache updates](http://lore.kernel.org/linux-riscv/20220829205219.283543-1-geomatsi@gmail.com/)

   Current implementation of update_mmu_cache function performs local TLB
   flush. It does not take into account ASID information. Besides, it does
   not take into account other harts currently running the same mm context
   or possible migration of the running context to other harts. Meanwhile
   TLB flush is not performed for every context switch if ASID support
   is enabled.

* [v3: riscv: enable THP_SWAP for RV64](http://lore.kernel.org/linux-riscv/20220829145742.3139-1-jszhang@kernel.org/)

   I have a Sipeed Lichee RV dock board which only has 512MB DDR, so
   memory optimizations such as swap on zram are helpful. As is seen
   in commit d0637c505f8a ("arm64: enable THP_SWAP for arm64") and
   commit bd4c82c22c367e ("mm, THP, swap: delay splitting THP after
   swapped out"), THP_SWAP can improve the swap throughput significantly.

   Enable THP_SWAP for RV64, testing the micro-benchmark which is
   introduced by commit d0637c505f8a ("arm64: enable THP_SWAP for arm64")
   shows below numbers on the Lichee RV dock board:

   swp out bandwidth w/o patch: 66908 bytes/ms (mean of 10 tests)
   swp out bandwidth w/ patch: 322638 bytes/ms (mean of 10 tests)

   Improved by 382%!

* [v1: Add PMEM support for RISC-V](http://lore.kernel.org/linux-riscv/20220829125226.511564-1-apatel@ventanamicro.com/)

   The Linux NVDIMM PEM drivers require arch support to map and access the
   persistent memory device. This series adds RISC-V PMEM support using
   recently added Svpbmt and Zicbom support.

   These patches can also be found in riscv_pmem_v1 branch at:
   https://github.com/avpatel/linux.git

* [v1: RISC-V: Add STACKLEAK erasing the kernel stack at the end of syscalls](http://lore.kernel.org/linux-riscv/20220828135407.3897717-1-xianting.tian@linux.alibaba.com/)

   This adds support for the STACKLEAK gcc plugin to RISC-V and disables
   the plugin in EFI stub code, which is out of scope for the protection.

   For the benefits of STACKLEAK feature, please check the commit
   afaef01c0015 ("x86/entry: Add STACKLEAK erasing the kernel stack at the end of syscalls")

   Performance impact (tested on qemu env with 1 riscv64 hart, 1GB mem)
       hackbench -s 512 -l 200 -g 15 -f 25 -P
       2.0% slowdown

### 周边技术动态

#### Qemu


* [v5: hw/riscv: set machine->fdt in spike_board_init()](http://lore.kernel.org/qemu-devel/20220903193420.115986-13-danielhb413@gmail.com/)

   This will enable support for the 'dumpdtb' QMP/HMP command for the spike
   machine.

* [v1: target/riscv: Implement PMU CSR predicate function for U-mode](http://lore.kernel.org/qemu-devel/20220902164649.4122331-1-aurelien@aurel32.net/)

   Recently the Linux kernel started to use a non default value, for
   the scounteren CSR, which is ignored by QEMU. Fix that by implementing
   the PMU CSR predicate function for U-mode.

* [v1: RISC-V: Add support for Ztso](http://lore.kernel.org/qemu-devel/20220902034412.8918-1-palmer@rivosinc.com/)

   Ztso, the RISC-V extension that provides the TSO memory model, was
   recently frozen.  This provides support for Ztso on targets that are
   themselves TSO.

* [[PULL 20/20] target/riscv: Make translator stop before the end of a page](http://lore.kernel.org/qemu-devel/20220901065210.117081-25-richard.henderson@linaro.org/)

   Right now the translator stops right *after* the end of a page, which
   breaks reporting of fault locations when the last instruction of a
   multi-insn translation block crosses a page boundary.

   Resolves: https://gitlab.com/qemu-project/qemu/-/issues/1155

* [[PULL 19/20] target/riscv: Add MAX_INSN_LEN and insn_len](http://lore.kernel.org/qemu-devel/20220901065210.117081-24-richard.henderson@linaro.org/)

   These will be useful in properly ending the TB.

* [v1: target/riscv: remove fixed numbering from GDB xml feature files](http://lore.kernel.org/qemu-devel/6069395f90e6fc24dac92197be815fedf42f5974.1661934573.git.aburgess@redhat.com/)

   The fixed register numbering in the various GDB feature files for
   RISC-V only exists because these files were originally copied from the
   GDB source tree.

* [v1: target/riscv: remove fflags, frm, and fcsr from riscv-*-fpu.xml](http://lore.kernel.org/qemu-devel/0fbf2a5b12e3210ff3867d5cf7022b3f3462c9c8.1661934573.git.aburgess@redhat.com/)

   While testing some changes to GDB's handling for the RISC-V registers
   fcsr, fflags, and frm, I spotted that QEMU includes these registers
   twice in the target description it sends to GDB, once in the fpu
   feature, and once in the csr feature.

   Right now things basically work OK, QEMU maps these registers onto two

* [v1: target/riscv: improvements to GDB target descriptions](http://lore.kernel.org/qemu-devel/cover.1661934573.git.aburgess@redhat.com/)

   I was running some GDB tests against QEMU, and noticed some oddities
   with the target description QEMU sends, the following two patches
   address these issues.


#### Buildroot


* [v1: package/qemu: add support for RISC-V](http://lore.kernel.org/buildroot/20220831133951.905279-1-alistair.francis@wdc.com/)

   QEMU is supported on both 32-bit and 64-bit RISC-V hosts, so let's
   enable support for it in buildroot.

* [[branch/2022.05.x] arch/Config.in.riscv: lp64f ABI is only supported if MMU is enabled](http://lore.kernel.org/buildroot/20220829203619.6DA7E87FE0@busybox.osuosl.org/)

   Even though that seems weird, the LP64F ABI is only supported when MMU
   support is enabled. Indeed, as per commit
   config with unsupported RISC-V float ABI"), uClibc does not support
   LP64F. But uClibc is the only C library that support RISC-V 64-bit
   noMMU.

   So the selection of LP64F and !MMU is impossible. Right now this
   selection causes a build failure as no C library is enabled.

   This commit fixes this by ensuring we cannot use LP64F when MMU
   support is not available.


#### U-Boot

* [v3: spl: introduce SPL_XIP to config](http://lore.kernel.org/u-boot/20220902084739.23813-1-nikita.shubin@maquefel.me/)

   U-Boot and SPL don't necessary share the same location, so we might end
   with U-Boot SPL in read-only memory (XIP) and U-Boot in read-write memory.

   In case of non XIP boot mode, we rely on such variables as "hart_lottery"
   and "available_harts_lock" which we use as atomics.

   The problem is that CONFIG_XIP also propagate to main U-Boot, not only SPL,
   so we need CONFIG_SPL_XIP to distinguish SPL XIP from other XIP modes.

   This adds an option special for SPL to behave it in XIP manner and we don't
   use hart_lottery and available_harts_lock, during start proccess.


* [v1: The riscv spec has breaking change from 2.2 to 20191213. Both ZICSR and ZIFENCEI are separated extension since 20191213. Gcc bump up the riscv spec to 20191213 since 12.x. So add below 4 build option for compatibility::](http://lore.kernel.org/u-boot/BYAPR04MB48242371E5FBE6AC36069334A4769@BYAPR04MB4824.namprd04.prod.outlook.com/)

   CONFIG_RISCV_ISA_F for single float point, default no.
   CONFIG_RISCV_ISA_D for double float point, default no.
   CONFIG_RISCV_ISA_ZICSR for control and status register, default no.
   CONFIG_RISCV_ISA_ZIFENCEI for instruction-fetch fence, default no.

   Example when build with gcc 12.x as below.

       make qemu-riscv64_defconfig && make \
           CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv \
           CONFIG_RISCV_ISA_F=y \
           CONFIG_RISCV_ISA_D=y \
           CONFIG_RISCV_ISA_ZICSR=y \
           CONFIG_RISCV_ISA_ZIFENCEI=y



## 20220828：第 9 期

### 内核动态

* V2: [riscv: enable THP_SWAP for RV64](http://lore.kernel.org/linux-riscv/20220827095815.698-1-jszhang@kernel.org/)

    I have a Sipeed Lichee RV dock board which only has 512MB DDR, so memory optimizations such as swap on zram are helpful. As is seen in commit d0637c505f8a ("arm64: enable THP_SWAP for arm64") and  commit bd4c82c22c367e ("mm, THP, swap: delay splitting THP after swapped out"), THP_SWAP can improve the swap throughput significantly.

* V5: [leds: Allwinner A100 LED controller support](http://lore.kernel.org/linux-riscv/20220826050942.20067-1-samuel@sholland.org/)

    This series adds bindings and a driver for the RGB LED controller found in some Allwinner SoCs, starting with A100. The hardware in the R329 and D1 SoCs appears to be identical.

* V5: [KVM: selftests: Implement ucall "pool" (for SEV)](http://lore.kernel.org/linux-riscv/20220825232522.3997340-1-seanjc@google.com/)

    Convert all implementations to the pool as all of the complexity is hidden in common code, and supporting multiple interfaces adds its own kind of complexity. 

   
* V4: [KVM: selftests: Implement ucall "pool" (for SEV)](http://lore.kernel.org/linux-riscv/20220824032115.3563686-1-seanjc@google.com/)

    This is "v4" of Peter's SEV series, minus the actual SEV tests.  My plan is to get this queued sooner than later so that Peter can build on top.

* V4: [Fix dt-validate issues on qemu dtbdumps due to dt-bindings](http://lore.kernel.org/linux-riscv/20220823183319.3314940-1-mail@conchuod.ie/)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU.

* V4: [riscv, mm: detect svnapot cpu support at runtime](http://lore.kernel.org/linux-riscv/20220822153413.4038052-1-panqinglin2020@iscas.ac.cn/)

    Svnapot is a RISC-V extension for marking contiguous 4K pages as a non-4K page. This patch set is for using Svnapot in Linux Kernel's boot process and hugetlb fs.

* V1: [-next: riscv: ptrace: implement PTRACE_{PEEK,POKE}USR](http://lore.kernel.org/linux-riscv/20220822030105.16053-1-zouyipeng@huawei.com/)

    The PTRACE_{PEEK,POKE}USR going to get/set tracee's user register. This patch sets add PTRACE_{PEEK,POKE}USR and compat implement to riscv.


* V1: [cpu/hotplug: Check the capability of kexec quick reboot](http://lore.kernel.org/linux-riscv/20220822021520.6996-5-kernelfans@gmail.com/)

    The kexec quick reboot needs each involved cpuhp_step to run in parallel. There are lots of teardown cpuhp_step, but not all of them belong to arm/arm64/riscv kexec reboot path. So introducing a member'support_kexec_parallel' in the struct cpuhp_step to signal whether the teardown supports parallel or not. 

* V1: [cpu/hotplug: Compile smp_shutdown_nonboot_cpus() conditioned on CONFIG_SHUTDOWN_NONBOOT_CPUS](http://lore.kernel.org/linux-riscv/20220822021520.6996-3-kernelfans@gmail.com/)

    Only arm/arm64/ia64/riscv share the smp_shutdown_nonboot_cpus(). So compiling this code conditioned on the macro CONFIG_SHUTDOWN_NONBOOT_CPUS. 

* V1: [riscv: make update_mmu_cache to support asid](http://lore.kernel.org/linux-riscv/20220821013926.8968-1-tjytimi@163.com/)

    The `update_mmu_cache` function in riscv flush tlb cache without asid information now, which will flush tlbs in other tasks' address space even if processor support asid. 

### 周边技术动态

#### Qemu

* V14: [target/riscv: Simplify counter predicate function](http://lore.kernel.org/qemu-devel/20220824221701.41932-3-atishp@rivosinc.com/)

    All the hpmcounters and the fixed counters (CY, IR, TM) can be represented as a unified counter. Thus, the predicate function doesn't need handle each case separately.

* V14: [hw/riscv: virt: Add PMU DT node to the device tree](http://lore.kernel.org/qemu-devel/20220824221701.41932-5-atishp@rivosinc.com/)

    Qemu virt machine can support few cache events and cycle/instret counters. It also supports counter overflow for these events.


## 20220821：第 8 期

### 内核动态

* V8: [RISC-V IPI Improvements](https://lore.kernel.org/linux-riscv/20220820065446.389788-1-apatel@ventanamicro.com/)

    These patches were originally part of the "Linux RISC-V ACLINT Support" series but this now a separate series so that it can be merged independently of the "Linux RISC-V ACLINT Support" series.

* V3: [Fix RISC-V/PCI dt-schema issues with dt-schema v2022.08](https://lore.kernel.org/linux-riscv/20220819231415.3860210-1-mail@conchuod.ie/)

    Got a few fixes for PCI dt-bindings that I noticed after upgrading my dt-schema to v2022.08. Since all the dts patches are for "my" boards, I'll take them once the bindings are approved.

* GIT PULL:[RISC-V Fixes for 6.0-rc2](https://lore.kernel.org/linux-riscv/mhng-74337228-62c4-40ed-b7af-0d988ff94993@palmer-mbp2014/)

    * A fix to make the ISA extension static keys writable after init.  This manifests at least as a crash when loading modules (including KVM).
    * A fixup for a build warning related to a poorly formed comment in our perf driver.

* V3: [riscv: kexec: Fixup crash_save percpu and machine_kexec_mask_interrupts](https://lore.kernel.org/linux-riscv/20220819025444.2121315-1-guoren@kernel.org/)

    Current riscv kexec can't crash_save percpu states and disable interrupts properly. The patch series fix them, make kexec work correct.

* V1: [riscv: dma-mapping: Use conventional cache operations for dma_sync*()](https://lore.kernel.org/linux-riscv/20220818165105.99746-1-s.miroshnichenko@yadro.com/) 

    As of for_device(FROM_DEVICE), according to the inspirational discussion about matching the DMA API and cache operations, the inval or a no-op should be here, and this would resonate with most other arches. But in a later discussion it was decided that the clean is indeed preferable and safer, though slower.

* V2: [Add support for Renesas RZ/Five SoC](https://lore.kernel.org/linux-riscv/20220815151451.23293-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

    The RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP Single) 1.0 GHz, 16-bit DDR3L/DDR4 interface. And it also has many interfaces such as Gbit-Ether, CAN, and USB 2.0, making it ideal for applications such as entry-class social infrastructure gateway control and industrial gateway control.

* V6: [RISC-V: Create unique identification for SoC PMU](https://lore.kernel.org/linux-riscv/20220815132251.25702-1-nikita.shubin@maquefel.me/)

    This series aims to provide matching vendor SoC with corresponded JSON bindings. Also added SBI firmware events pretty names, as any firmware that supports SBI PMU should also support firmare events.

* V1: [RISC-V: Add fast call path of crash_kexec()](https://lore.kernel.org/linux-riscv/20220814162922.2398723-6-sashal@kernel.org/) 

    Currently, almost all archs (x86, arm64, mips...) support fast call of crash_kexec() when "regs && kexec_should_crash()" is true. But RISC-V not, it can only enter crash system via panic().

* V1: [riscv: Allwinner D1 platform support](https://lore.kernel.org/linux-riscv/20220815050815.22340-1-samuel@sholland.org/)

    This series adds the Kconfig/defconfig plumbing and devicetrees for a range of Allwinner D1-based boards. Many features are already enabled, including USB, Ethernet, and WiFi.

### 周边技术动态

#### Qemu

* V3: [target/riscv: Use official extension names for AIA CSRs](https://lore.kernel.org/qemu-devel/20220820042958.377018-1-apatel@ventanamicro.com/)

    The arch review of AIA spec is completed and we now have official extension names for AIA: Smaia (M-mode AIA CSRs) and Ssaia (S-mode AIA CSRs).

* V13: [Improve PMU support](https://lore.kernel.org/qemu-devel/20220816232321.558250-1-atishp@rivosinc.com/)

    The latest version of the SBI specification includes a Performance Monitoring Unit(PMU) extension which allows the supervisor to start/stop/configure various PMU events.

* V1: [hw/riscv: Setting address of vector reset is improved](https://lore.kernel.org/qemu-devel/20220815174138.19766-1-coder@frtk.ru/)

    Previously address is set by default value 0x1000 which is hardcoded in target/riscv/cpu_bits.h If add to new RISC-V Machine in which ROM area is not based on 0x1000 address than there is problem of running simulation

#### U-Boot

* V1: [doc: qemu-riscv: describe booting with QEMU and KVM](https://lore.kernel.org/u-boot/20220814142248.2429845-1-heinrich.schuchardt@canonical.com/)

    The ELF U-Boot image produced by qemu-riscv64_smode_defconfig can be used to boot with QEMU and KVM.

## 20220814：第 7 期

### 内核动态

* GIT PULL: [RISC-V Patches for the 5.20 Merge Window, Part 2](https://lore.kernel.org/linux-riscv/mhng-563e7d68-e504-4e0a-b666-f7c2fbab62db@palmer-ri-x1c9/)

    There's still a handful of new features in here, but there are a lot of fixes/cleanups as well:
  * Support for the Zicbom for explicit cache-block management, along with
  the necessary bits to make the non-standard cache management ops on
  the Allwinner D1 function.
  * Support for the Zihintpause extension, which codifies a go-slow
  instruction used for cpu_relax().
  * Support for the Sstc extension for supervisor-mode timer/counter
  management.
  * Many device tree fixes and cleanups, including a large set for the
  Canaan device trees.
  * A handful of fixes and cleanups for the PMU driver.

* V3: [Support RISCV64 arch and common commands](https://lore.kernel.org/linux-riscv/20220813031753.3097720-1-xianting.tian@linux.alibaba.com/)

    This series of patches are for Crash-utility tool, it make crash tool support RISCV64 arch and the common commands(*, bt, p, rd, mod, log, set, struct, task, dis, help -r, help -m, and so on).

* V1: [riscv: enable CD-ROM file systems in defconfig](https://lore.kernel.org/linux-riscv/20220812200853.311474-1-heinrich.schuchardt@canonical.com/)

    CD-ROM images are still commonly used for installer images and other data exchange. These file systems should be supported on RISC-V by default like they are on x86_64.

* V2: [RISC-V: Clean up the Zicbom block size probing](https://lore.kernel.org/linux-riscv/20220812154010.18280-1-palmer@rivosinc.com/)

    This fixes two issues: I truncated the warning's hart ID when porting to the 64-bit hart ID code, and the original code's warning handling could fire on an uninitialized hart ID.

* V3: [Risc-V Svinval support](https://lore.kernel.org/linux-riscv/20220812042921.14508-1-mchitale@ventanamicro.com/)

    This patch adds support for the Svinval extension as defined in the Risc V Privileged specification.

* V1: [Fix RISC-V/PCI dt-schema issues with dt-schema v2022.08](https://lore.kernel.org/linux-riscv/20220811203306.179744-1-mail@conchuod.ie/)

    Got a few fixes for PCI dt-bindings that I noticed after upgrading my dt-schema to v2022.08. I am unsure if some of these patches are the right fixes, which I noted in the patches themselves, especially the address translation property.

* V6: [RISC-V fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220811074150.3020189-1-xianting.tian@linux.alibaba.com/)

    This patch set just put these patches together, and with three new patch 4, 5, 6. these six patches are the fixups for machine_kexec, kernel mode PC for vmcore and improvements for vmcoreinfo, memory layout dump and fixup schedule out issue in machine_crash_shutdown().

* V1: [wireguard: selftests: set CONFIG_NONPORTABLE on riscv32](https://lore.kernel.org/linux-riscv/20220809145757.83673-1-Jason@zx2c4.com/)

    When the CONFIG_PORTABLE/CONFIG_NONPORTABLE switches were added, various configs were updated, but the wireguard config was forgotten about. This leads to unbootable test kernels, causing CI fails.

* V9: [arch: Add qspinlock support and atomic cleanup](https://lore.kernel.org/linux-riscv/20220808071318.3335746-1-guoren@kernel.org/)

    RISC-V LR/SC pairs could provide a strong/weak forward guarantee that depends on micro-architecture. And RISC-V ISA spec has given out several limitations to let hardware support strict forward guarantee.

### 周边技术动态

#### Qemu

* V2: [riscv: Make semihosting configurable for all privilege modes](https://lore.kernel.org/qemu-devel/CA+tJHD4Fdv54_u9vffu9tNuor4Tu_Ld-eYZkLRmTxi=X2wknnw@mail.gmail.com/)

    Unlike ARM, RISC-V does not define a separate breakpoint type for semihosting. Instead, it is entirely ABI. However, RISC-V debug specification provides ebreak{m,s,u,vs,vu} configuration bits to allow ebreak behavior to be configured to trap into debug mode instead.

* V3: [QEMU: Fix RISC-V virt & spike machines' dtbs](https://lore.kernel.org/qemu-devel/20220810184612.157317-1-mail@conchuod.ie/)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU.
    
* V9: [Implement Sstc extension](https://lore.kernel.org/qemu-devel/20220810184548.3620153-1-atishp@rivosinc.com/)

    This series implements Sstc extension[1] which was ratified recently. The first patch is a prepartory patches while PATCH 2 adds stimecmp support while PATCH 3 adds vstimecmp support. This series is based on top of upstream commit (faee5441a038).

* V8: [RISC-V Smstateen support](https://lore.kernel.org/qemu-devel/20220809041643.124888-1-mchitale@ventanamicro.com/)

    This series adds support for the Smstateen specification which provides a mechanism to plug the potential covert channels which are opened by extensions that add to processor state that may not get context-switched.

#### Buildroot

* GIT COMMIT: [board/riscv/nommu: bump kernel version and drop no longer needed patch](https://lore.kernel.org/buildroot/20220811202816.E96F487348@busybox.osuosl.org/)

    Bump the kernel version for all riscv nommu configs from 5.18 to 5.19. That way, we can remove the one and only riscv nommu patch, since this patch is included in kernel 5.19.

## 20220807：第 6 期

### 内核动态

* GIT PULL: [RISC-V Patches for the 5.20 Merge Window, Part 1](https://lore.kernel.org/linux-riscv/mhng-1cbba637-6dd2-456a-859b-9d3f8be6bab7@palmer-mbp2014/)

    There are also a handful of cleanups and improvements
    * Enabling the FPU is now a static_key.
    * Improvements to the Svpbmt support.
    * CPU topology bindings for a handful of systems.
    * Support for systems with 64-bit hart IDs.
    * Many settings have been enabled in the defconfig, including both support for the StarFive systems and many of the Docker requirements.

* V1: [QEMU: Fix RISC-V virt & spike machines' dtbs](https://lore.kernel.org/linux-riscv/20220805155405.1504081-1-mail@conchuod.ie)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU

* V5: [RISC-V fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220802121818.2201268-1-xianting.tian@linux.alibaba.com/)

    This patch set just put these patches together, and with three new patch 4, 5, 6. these six patches are the fixups for machine_kexec, kernel mode PC for vmcore and improvements for vmcoreinfo, memory layout dump and fixup schedule out issue in machine_crash_shutdown().

* V2: [Support RISCV64 arch and common commands](https://lore.kernel.org/linux-riscv/20220801043040.2003264-1-xianting.tian@linux.alibaba.com/)

    This series of patches are for Crash-utility tool, it make crash tool support RISCV64 arch and the common commands(*, bt, p, rd, mod, log, set, struct, task, dis, help -r, help -m, and so on).

* V1: [DT schema warnings on Risc-V virt machine](https://lore.kernel.org/linux-riscv/20220803170552.GA2250266-robh@kernel.org/)

    There could also be other warnings from non-default configurations.

### 周边技术动态

#### Qemu

* V1: [QEMU: Fix RISC-V virt & spike machines' dtbs](https://lore.kernel.org/qemu-devel/20220805155405.1504081-1-mail@conchuod.ie/)

    The device trees produced automatically for the virt and spike machines fail dt-validate on several grounds. Some of these need to be fixed in the linux kernel's dt-bindings, but others are caused by bugs in QEMU.
    
* V8: [Implement Sstc extension](https://lore.kernel.org/qemu-devel/20220804014240.2514957-1-atishp@rivosinc.com/)

    This series implements Sstc extension[1] which was ratified recently. The first patch is a prepartory patches while PATCH 2 adds stimecmp support while PATCH 3 adds vstimecmp support. 

* V7: [RISC-V Smstateen support](https://lore.kernel.org/qemu-devel/20220801171843.72986-1-mchitale@ventanamicro.com/)

    This series adds support for the Smstateen specification which provides a mechanism plug potential covert channels which are opened by extensions that add to processor state that may not get context-switched.

#### Buildroot

* V1: [package/llvm: Support for RISC-V on the LLVM package](https://lore.kernel.org/buildroot/20220801215433.1927D86C6D@busybox.osuosl.org/)

    The initial support for the LLVM package did not include RISC-V, and needed to be added. Some special casing is needed for the LLVM_TARGETS_TO_BUILD variable, which expects a RISCV value regardless of whether riscv32 or riscv64 is chosen.

## 202200731：第 5 期

### 内核动态

* GIT PULL: [A Single RISC-V Fix for 5.19](https://lore.kernel.org/linux-riscv/mhng-b20f34d5-a1cb-4254-8cc5-d3c7752b3908@palmer-mbp2014/)

    A build fix for "make vdso_install" that avoids an issue trying to install the compat VDSO. 

* GIT PULL: [KVM/riscv changes for 5.20](https://lore.kernel.org/linux-riscv/CAAhSdy2iH-WpitweQ_nCYm6p0S5S_fmQ3x37FdAe7tEmu_np0A@mail.gmail.com/)

    We have following KVM RISC-V changes for 5.20:

    - Track ISA extensions used by Guest using bitmap
    - Added system instruction emulation framework
    - Added CSR emulation framework
    - Added gfp_custom flag in struct kvm_mmu_memory_cache
    - Added G-stage ioremap() and iounmap() functions
    - Added support for Svpbmt inside Guest

* V1: [riscv: enable software resend of irqs](https://lore.kernel.org/linux-riscv/20220729111116.259146-1-conor.dooley@microchip.com/)

    The PLIC specification does not describe the interrupt pendings bits as read-write, only that they "can be read". To allow for retriggering of 
    interrupts enable HARDIRQS_SW_RESEND for RISC-V.

* V1: [doc: RISC-V: Document that misaligned accesses are supported](https://lore.kernel.org/linux-riscv/20220728210715.17214-1-palmer@rivosinc.com/)

    The RISC-V ISA manual used to mandate that misaligned accesses were supported in user mode, but that requirement was removed in 2018 via riscv-isa- 
    manual commit 61cadb9 Since the Linux uABI was already frozen at that point it's just been demoted to part of the uABI, but that was never written 
    down.

* V2: [Improve CLOCK_EVT_FEAT_C3STOP feature setting](https://lore.kernel.org/linux-riscv/20220727114302.302201-1-apatel@ventanamicro.com/) 

    This series improves the RISC-V timer driver to set CLOCK_EVT_FEAT_C3STOP feature based on RISC-V platform capabilities.

* V2: [RISC-V: Add mvendorid, marchid, and mimpid to /proc/cpuinfo output](https://lore.kernel.org/linux-riscv/20220727043829.151794-1-apatel@ventanamicro.com/)

    Identifying the underlying RISC-V implementation can be important for some of the user space applications. For example, the perf tool uses arch specific CPU implementation id (i.e. CPUID) to select a JSON file describing custom perf events on a CPU. Currently, there is no way to identify RISC-V implementation so we add mvendorid, marchid, and mimpid to /proc/cpuinfo output.

* V1: [checkstack: add riscv support for scripts/checkstack.pl](https://lore.kernel.org/linux-riscv/20220713194112.15557-1-wafgo01@gmail.com/)

    scripts/checkstack.pl lacks support for the riscv architecture. Add support to detect "addi sp,sp,-FRAME_SIZE" stack frame generation instruction

* V1: [Add support for Renesas RZ/Five SoC](https://lore.kernel.org/linux-riscv/20220726180623.1668-1-prabhakar.mahadev-lad.rj@bp.renesas.com/)

    The RZ/Five microprocessor includes a RISC-V CPU Core (AX45MP Single) 1.0 GHz, 16-bit DDR3L/DDR4 interface. And it also has many interfaces 
    such as Gbit-Ether, CAN, and USB 2.0, making it ideal for applications such as entry-class social infrastructure gateway control and industrial 
    gateway control.

* V4: [RISC-V fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220726093729.1231867-1-xianting.tian@linux.alibaba.com/)

    This patch series just put these patches together, and with two new patch 4, 5. these five patches are the fixups for machine_kexec, 
    kernel mode PC for vmcore and improvements for vmcoreinfo and memory layout dump.

* V8: [arch: Add qspinlock support with combo style](https://lore.kernel.org/linux-riscv/20220724122517.1019187-1-guoren@kernel.org/)

    Enable qspinlock and meet the requirements mentioned in a8ad07e5240c9 RISC-V LR/SC pairs could provide a strong/weak forward guarantee 
    that depends on micro-architecture.

### 周边技术动态

#### Qemu

* V11: [Improve PMU support](https://lore.kernel.org/qemu-devel/20220727064913.1041427-1-atishp@rivosinc.com/)

    The latest version of the SBI specification includes a Performance Monitoring Unit(PMU) extension which allows the supervisor to 
    start/stop/configure various PMU events.This series implements remaining PMU infrastructure to support PMU in virt machine.

* V5: [target/riscv: Add Zihintpause support](https://lore.kernel.org/qemu-devel/20220725034728.2620750-1-daolu@rivosinc.com/)

    This patch adds RISC-V Zihintpause support. The extension is set to be enabled by default and opcode has been added to insn32.decode. 
    Added trans_pause to exit the TB and return to main loop.
    
#### Buildroot

* V2: [arch/Config.in.riscv: lp64f ABI is only supported if MMU is enabled](https://lore.kernel.org/buildroot/20220726163951.2111731-1-thomas.petazzoni@bootlin.com/)

    Even though that seems weird, the LP64F ABI is only supported when MMU support is enabled. In deed, as per commit 
    9a51381cedc16e6d70cb85e1144f6e0fa89af69a ("package/uclibc: prevent config with unsupported RISC-V float ABI"), 
    uClibc does not support LP64F. But uClibc is the only C library that support RISC-V 64-bit noMMU.

## 20220723：第 4 期

### 内核动态

* v4: [Fix RISC-V's arch-topology reporting](https://lore.kernel.org/linux-riscv/96972ad8-d146-3bc2-0e49-ffe88580bbee@microchip.com/T/#t)

    arm64's topology code basically applies to RISC-V too, so it has been made generic along with the removal of MPIDR related code, which appears to be redudant code since '3102bc0e6ac7 ("arm64: topology: Stop using MPIDR for topology information")' replaced the code that actually interacted with MPIDR with default values.
    
* v1: [Support RISCV64 arch and common commands](https://lore.kernel.org/linux-riscv/20220717042929.370022-1-xianting.tian@linux.alibaba.com/)

    This series of patches make crash tool support RISCV64 arch and the common commands(*, bt, p, rd, mod, log, set, struct, task, dis and so on). To make the crash tool work normally for RISCV64 arch, we need a Linux kernel patch(under reviewing), which exports the kernel virtual memory layout, va_bits, phys_ram_base to vmcoreinfo, it can simplify the development of crash tool.
    
* v1: [Improve CLOCK_EVT_FEAT_C3STOP feature setting](https://lore.kernel.org/linux-riscv/20220719054729.2224766-1-apatel@ventanamicro.com/)

    This series improves the RISC-V timer driver to set CLOCK_EVT_FEAT_C3STOP feature based on RISC-V platform capabilities.
    
* v1: [Fixups to work with crash tool](https://lore.kernel.org/linux-riscv/20220717101323.370245-1-xianting.tian@linux.alibaba.com/)

    his patch series just put these patches together, and with a new patch 5. these five patches are the fixups for kexec, vmcore and improvements for vmcoreinfo and memory layout dump. With these 5 patches(patch 3 is must), crash tool can work well to analyze a vmcore.
    
* v5: [Canaan devicetree fixes](https://lore.kernel.org/linux-riscv/20220705215213.1802496-1-mail@conchuod.ie/T/#t)

    This series should rid us of dtbs_check errors for the RISC-V Canaan k210 based boards. To make keeping it that way a little easier, I changed the Canaan devicetree Makefile so that it would build all of the devicetrees in the directory if SOC_CANAAN.
    
* v7: [RISC-V IPI Improvements](https://lore.kernel.org/linux-riscv/20220720152348.2889109-1-apatel@ventanamicro.com/#r)

    These patches were originally part of the "Linux RISC-V ACLINT Support" series but this now a separate series so that it can be merged independently of the "Linux RISC-V ACLINT Support" series.
    
* V3: [Support for 64bit hartid on RV64 platforms](https://lore.kernel.org/linux-riscv/mhng-4c49edf1-6367-4dd0-bec7-c6719745ecb5@palmer-mbp2014/T/#t)

    The hartid can be a 64bit value on RV64 platforms. This series updates the code so that 64bit hartid can be supported on RV64 platforms.
    
* v4: [riscv:uprobe fix SR_SPIE set/clear handling](https://lore.kernel.org/linux-riscv/20220721065820.245755-1-zouyipeng@huawei.com/T/#u)    
    
    In riscv the process of uprobe going to clear spie before exec the origin insn,and set spie after that.But When access the page which origin insn has been placed a page fault may happen and irq was disabled in arch_uprobe_pre_xol function,It cause a WARN as follows.    
    
* v1: [Add HiFive Unmatched LEDs](https://lore.kernel.org/linux-riscv/20220717110249.GF14285@duo.ucw.cz/T/#t)
    
    This series adds support for the two LEDs on the HiFive Unmatched RISC-V board.    
    
* v7: [Add Sstc extension support](https://lore.kernel.org/linux-riscv/20220722165047.519994-1-atishp@rivosinc.com/)
    
     This series implements Sstc extension support which was ratified recently. Before the Sstc extension, an SBI call is necessary to generate timer interrupts as only M-mode have access to the timecompare registers. Thus, there is significant latency to generate timer interrupts at kernel. 

### 周边技术动态

#### Qemu

* v6: [target/riscv: Add stimecmp support](https://lore.kernel.org/qemu-devel/20220722010046.343744-3-atishp@rivosinc.com/)

    stimecmp allows the supervisor mode to update stimecmp CSR directly to program the next timer interrupt. This CSR is part of the Sstc extension which was ratified recently.
    
* v1: [Fix pointer masking functionality for RISC-V](https://lore.kernel.org/qemu-devel/20220717101543.478533-1-space.monkey.delivers@gmail.com/)    
    
    This patch fixes a typo which leads to broken pointer masking functionality for RISC-V.   
    
#### Buildroot

* v4: [Fix RV64 NOMMU and add Canaan K210 SoC support](https://lore.kernel.org/buildroot/20220720024531.435748-1-damien.lemoal@opensource.wdc.com/)

    This series adds support for building 64-bits RISC-V NOMMU kernels (both bootable kernels and u-boot sdcard boot envronements) for NOMMU RISC-V 64-bits boards. The board supported include QEMU and many boards using the dual-core RISC-V 64-bits Cannan Kendryte K210 SoC.

## 20220716：第 3 期

### 内核动态

* v5: [use static key to optimize pgtable_l4_enabled](https://lore.kernel.org/linux-riscv/20220715134847.2190-1-jszhang@kernel.org/T/#t)
    
    The pgtable_l4|[l5]_enabled check sits at hot code path, performance is impacted a lot. Since pgtable_l4|[l5]_enabled isn't 
    changed after boot, so static key can be used to solve the performance issue[1].
 
* v4: [Fix RISC-V's arch-topology reporting](https://lore.kernel.org/linux-riscv/20220715175155.3567243-1-mail@conchuod.ie/T/#t)
    
    The goal here is the fix the incorrectly reported arch topology on RISC-V which seems to have been broken since it was added.
    
* next: [arch_topology: Fix cache attributes detection in the CPU hotplug pat](https://lore.kernel.org/linux-riscv/YtBX8WX+oyPww%2Fm+@arm.com/T/#t)
    
    Move the call to detect_cache_attributes() inside update_siblings_masks() to ensure the cacheinfo is updated before the LLC 
    sibling masks are updated.
    
* v3: [clear riscv dtbs_check errors](https://lore.kernel.org/linux-riscv/mhng-91bfbf9d-d8cc-4642-9688-20ef7ab21512@palmer-ri-x1c9/T/#t)

    Couple conversions from txt to yaml here with the intent of fixing the the dtbs_check warnings for riscv when building with "defconfig". 
    Atul Khare already sent patches for the gpio-line-names & cache-sets (which went awol) and will clear the remaining two errors.

* v3: [Improve vmcoreinfo and memory layout dump](https://lore.kernel.org/linux-riscv/20220714113300.367854-2-xianting.tian@linux.alibaba.com/T/#u)

    This patch series are the improvements for vmcoreinfo and memory layout dump.
    
* v2: [riscv: dts: starfive: correct number of external interrupts](https://lore.kernel.org/linux-riscv/27617ba6-addf-6f29-e1a8-2cb813dc303a@microchip.com/T/#t)

    The PLIC integrated on the Vic_U7_Core integrated on the StarFive JH7100 SoC actually supports 133 external interrupts. 
    127 of these are exposed to the outside world; the remainder are used by other devices that are part of the core-complex 
    such as the L2 cache controller.

* v5: [Microchip soft ip corePWM driver](https://lore.kernel.org/linux-riscv/20220708143923.1129928-1-conor.dooley@microchip.com/T/#t)

    The duty cycle calculation has been fixed - the problem was exactly what I suspected in my replies to your review. I had 
    to block the use of a 0xFF period_steps register value (which I think should be covered by the updated comment and limitation #2).
    
* v1: [Proof of concept for rv32 svpbmt support](https://lore.kernel.org/linux-riscv/20220705100523.1204595-1-guoren@kernel.org/T/#t)

    RISC-V 32bit also requires svpbmt in cost-down chip embedded scenarios, and their RAM is limited (No more than 1GB). It is worth 
    mentioning that rv32-Linux currently only supports 1GB of DRAM, and there is no plan for high-memory.

* v2: [riscv: always honor the CONFIG_CMDLINE_FORCE when parsing dtb](https://lore.kernel.org/linux-riscv/PSBPR04MB399135DFC54928AB958D0638B1829@PSBPR04MB3991.apcprd04.prod.outlook.com/T/#u)

    his especially fixes the case where a device tree without the chosen node is supplied to the kernel. In such cases, 
    early_init_dt_scan would return true.

* v2: [riscv: mm: add Svnapot support](https://lore.kernel.org/linux-riscv/20220716085648.4156408-1-panqinglin2020@iscas.ac.cn/T/#t)

    Svnapot is a RISC-V extension for marking contiguous 4K pages as a non-4K page. This patch set is for using Svnapot in 
    Linux Kernel's boot process and hugetlb fs.

### 周边技术动态

#### Qemu

* v1:[RISC-V: Allow both Zmmul and M](https://lore.kernel.org/qemu-devel/20220714180033.22385-1-palmer@rivosinc.com/)

    We got to talking about how Zmmul and M interact with each other https://github.com/riscv/riscv-isa-manual/issues/869,
    and it turns out that QEMU's behavior is slightly wrong: having Zmmul and M is a legal combination, it just means that 
    the multiplication instructions are supported even when M is disabled at runtime via misa.

#### Buildroot

* v3:[Fix RV64 NOMMU and add Canaan K210 SoC support](https://lore.kernel.org/buildroot/bd4eeaff-f4ce-ecd3-8e3b-03d5a924b53a@opensource.wdc.com/)

     This series adds support for building 64-bits RISC-V NOMMU kernels (both bootable kernels and u-boot sdcard boot envronements) 
     for NOMMU RISC-V 64-bits boards.

* v1:[arch/riscv: Added support for RISC-V vector extension on the architecture menu.](https://lore.kernel.org/buildroot/CAF2ART_=XUWL8bgtdV7vUgz5SQJXVyBAAS0ixcYLVbaaA++_4w@mail.gmail.com/)

    This new setting will allow to test new toolchains already available that support the vector extension (more patches coming soon).

## 20220707：第 2 期

### 内核动态

* v5: [RISC-V: Create unique identification for SoC PMU](https://lore.kernel.org/linux-riscv/165710198557.2545727.12369986485829448520.b4-ty@kernel.org/T/#t)

    This series aims to provide matching vendor SoC with corresponded JSON bindings.Where MIMPID can vary as all impl supported the same number of 
    events, this might not be true for all future SoC however.Also added SBI firmware events pretty names, as any firmware that supports SBI PMU 
    should also support firmare events. 
    
* v7: [riscv: implement Zicbom-based CMO instructions + the t-head variant](https://lore.kernel.org/linux-riscv/20220706231536.2041855-1-heiko@sntech.de/T/#t)

    This series is based on the alternatives changes done in my svpbmt series and thus also depends on Atish's isa-extension parsing series.
    It implements using the cache-management instructions from the  Zicbom-extension to handle cache flush, etc actions on platforms needing them.

* v3: [clear riscv dtbs_check errors](https://lore.kernel.org/linux-riscv/20220606201343.514391-1-mail@conchuod.ie/T/#t)

    Couple conversions from txt to yaml here with the intent of fixing the dtbs_check warnings for riscv when building with "defconfig". 
    Atul Khare already sent patches for the gpio-line-names & cache-sets (which went awol) and will clear the remaining two errors.
    
* v4: [riscv: Optimize atomic implementation](https://lore.kernel.org/linux-riscv/YsYivaDEksXPQPaH@boqun-archlinux/T/#t)

    Here are some optimizations for riscv atomic implementation, the first three patches are normal cleanup and custom implementation without 
    relating to atomic semantics.
    
* v4: [use static key to optimize pgtable_l4_enabled](https://lore.kernel.org/linux-riscv/mhng-17913c13-57bd-42f9-9136-b4eb9632253c@palmer-mbp2014/T/#t)

    The pgtable_l4|[l5]_enabled check sits at hot code path, performance is impacted a lot. Since pgtable_l4|[l5]_enabled isn't changed after boot, 
    so static key can be used to solve the performance issue[1].
    
* v5: [arch_topology: Updates to add socket support and fix cluster ids](https://lore.kernel.org/linux-riscv/20220627165047.336669-1-sudeep.holla@arm.com/T/#t)

    This series intends to fix some discrepancies we have in the CPU topology parsing from the device tree /cpu-map node. 
    Also this diverges from the behaviour on a ACPI enabled platform. The expectation is that both DT and ACPI enabled systems 
    must present consistent view of the CPU topology.

* v5: [RISC-V: three fixup and cleanups](https://lore.kernel.org/linux-riscv/20220701120548.228261-1-xianting.tian@linux.alibaba.com/T/#t)

    Use __smp_processor_id() to avoid check the preemption context when CONFIG_DEBUG_PREEMPT enabled, as we will enter crash kernel and no return.
    
* v3: [Add PLIC support for Renesas RZ/Five SoC / Fix T-HEAD PLIC edge flow](https://lore.kernel.org/linux-riscv/92a45bf04cfe140c7605559fa3d8f4eb@kernel.org/T/#t)

    This patch series adds PLIC support for Renesas RZ/Five SoC. Since the T-HEAD C900 PLIC has the same behavior, it also applies the fix for 
    that variant. This series is an update of v2 of the RZ/Five series[0], and replaces the separate T-HEAD series[1].

* v2: [Improve instruction and CSR emulation in KVM RISC-V](https://lore.kernel.org/linux-riscv/CAAhSdy3gmnqB6La125i2hdVh6eNiwqG6saqz4RTTYF=2Gqo6cA@mail.gmail.com/T/#t)

    Currently, the instruction emulation for MMIO traps and Virtual instruction traps co-exist with general VCPU exit handling. 
    The instruction and CSR emulation will grow with upcoming SBI PMU, AIA, and Nested virtualization in KVM RISC-V. 

* v3: [irqchip: RISC-V PLIC cleanup and optimization](https://lore.kernel.org/linux-riscv/20220701202440.59059-1-samuel@sholland.org/T/#t)

    This series depends on my other series[2] making the IRQ affinity mask behavior more consistent between uniprocessor and SMP configurations.
    (The Allwinner D1 is a uniprocessor SoC containing a PLIC.)

* v2: [dt-bindings: sifive: fix dt-schema errors](https://lore.kernel.org/linux-riscv/fb861221-2e9d-7d4a-dd52-b16b3b581fd6@microchip.com/T/#t)

    The patch series fixes dt-schema validation errors that can be reproduced using the following: make ARCH=riscv defconfig; 
    make ARCH=riscv dt_binding_check dtbs_check

* v2: [riscv: Fix missing PAGE_PFN_MASK](https://lore.kernel.org/linux-riscv/d5d1a6ef-1153-272b-af9b-9f369bbdd4e5@ghiti.fr/T/#t)

    here are a bunch of functions that use the PFN from a page table entry that end up with the svpbmt upper-bits because they are 
    missing the newly introduced PAGE_PFN_MASK which leads to wrong addresses conversions and then crash: fix this by adding this mask.
    
### 周边技术动态

#### Qemu

* v7: [KVM: Add KVM_EXIT_MEMORY_FAULT exit](https://lore.kernel.org/qemu-devel/20220706082016.2603916-11-chao.p.peng@linux.intel.com/)

    This new KVM exit allows userspace to handle memory-related errors. It indicates an error happens in KVM at guest memory range (gpa, gpa+size). 
    The flags includes additional information for userspace to handle the error. 
    
* v4: [target/riscv: Add Zihintpause support ](https://lore.kernel.org/qemu-devel/20220705174933.2898412-1-daolu@rivosinc.com/)   

    This patch adds RISC-V Zihintpause support. The extension is set to be enabled by default and opcode has been added to insn32.decode.    
    
* v2: [target/riscv: Support mcycle/minstret write operation](https://lore.kernel.org/qemu-devel/20220703001234.439716-13-alistair.francis@opensource.wdc.com/)

    mcycle/minstret are actually WARL registers and can be written with any given value. With SBI PMU extension, it will be used to store a 
    initial value provided from supervisor OS.     

* v2: [target/riscv: Set env->bins in gen_exception_illegal ](https://lore.kernel.org/qemu-devel/20220703001234.439716-3-alistair.francis@opensource.wdc.com/)

    While we set env->bins when unwinding for ILLEGAL_INST, from e.g. csrrw, we weren't setting it for immediately illegal instructions.    
    
* v2: [target/riscv: Fixup MSECCFG minimum priv check](https://lore.kernel.org/qemu-devel/20220703001234.439716-14-alistair.francis@opensource.wdc.com/)

    There is nothing in the RISC-V spec that mandates version 1.12 is required for ePMP and there is currently hardware that implements ePMP 
    (a draft version though) with the 1.11 priv spec.   

#### Buildroot

* v1: [arch/riscv: Added support for RISC-V vector extension on the architecture menu.](https://lore.kernel.org/buildroot/20220704085552.3499243-2-abel@x-silicon.com/)

    This new setting will allow to test new toolchains already available that support the vector extension (more patches coming soon).

* v1: [package/llvm: Support for RISC-V on the LLVM package](https://lore.kernel.org/buildroot/20220704085552.3499243-1-abel@x-silicon.com/)

    There is a new configuration parameter added(BR2_PACKAGE_LLVM_TARGETS_TO_BUILD) for dealing with the fact that the LLVM target and 
    the architecture have different naming for RISC-V.
    
#### U-Boot

* v1: [riscv: Remove additional ifdef from code guarded by CONFIG_IS_ENABLED](https://lore.kernel.org/u-boot/f8e3ff9124195cbd957874de9a65ef79760ef5e7.1657183634.git.michal.simek@amd.com/)

    CONFIG_OF_LIBFDT is used twice for guarding the same code. It is enough to do it once 
    that's why remove additional ifdefs from arm and risc-v code.

## 20220630：第 1 期

### 内核动态

* v5: [riscv: implement Zicbom-based CMO instructions + the t-head variant](https://lore.kernel.org/linux-riscv/20220629215944.397952-1-heiko@sntech.de/T/#t)

    It implements using the cache-management instructions from the  Zicbom-extension to handle cache flush, etc actions on platforms needing them. 

* next: [riscv: lib: uaccess: fix CSR_STATUS SR_SUM bit](https://lore.kernel.org/linux-riscv/11a0698c-5726-15e8-2448-3529d2d0b098@huawei.com/T/#t)

    Since commit 5d8544e2d007 ("RISC-V: Generic library routines and assembly")and commit ebcbd75e3962 ("riscv: Fix the bug in memory access fixup code"),if __clear_user and __copy_user return from an fixup branch, CSR_STATUS SR_SUM bit will be set, it is a vulnerability, so that S-mode memory accesses to pages that are accessible by U-mode will success. Disable S-mode access to U-mode memory should clear SR_SUM bit.

* v7: [riscv: Add qspinlock support with combo style](https://lore.kernel.org/linux-riscv/20220628081707.1997728-1-guoren@kernel.org/T/#t)

    RISC-V LR/SC pairs could provide a strong/weak forward guarantee that depends on micro-architecture. And RISC-V ISA spec has given out several limitations to let hardware support strict forward guarantee (RISC-V User ISA - 8.3 Eventual Success of Store-ConditionalInstructions):We restricted the length of LR/SC loops to fit within 64 contiguous instruction bytes in the base ISA to avoid undue restrictions on instruction cache and TLB size and associativity.

* v4: [RISC-V: Create unique identification for SoC PMU](https://lore.kernel.org/linux-riscv/20220624160117.3206-1-nikita.shubin@maquefel.me/T/#t)

    This series aims to provide matching vendor SoC with corresponded JSON bindings.The ID string is proposed to be in form of MVENDORID-MARCHID-MIMPID

* v2: [Add PLIC support for Renesas RZ/Five SoC](https://lore.kernel.org/linux-riscv/20220626004326.8548-1-prabhakar.mahadev-lad.rj@bp.renesas.com/T/#t)

    This patch series adds PLIC support for Renesas RZ/Five SoC.Sending this as an RFC based on the discussion [0].This patches have been tested with I2C and DMAC interface as these blocks have EDGE interrupts.

* v1: [riscv: atomic: Clean up unnecessary acquire and release definitions](https://lore.kernel.org/linux-riscv/20220625093945.423974-1-guoren@kernel.org/T/#u)

    Clean up unnecessary xchg_acquire, xchg_release, and cmpxchg_release custom definitions, because the generic implementation is the same as the riscv custom implementation.

* v5: [rtc: microchip: Add driver for PolarFire SoC](https://lore.kernel.org/linux-riscv/165609877582.32831.3964876505949828769.b4-ty@bootlin.com/T/#t)

    This is technically a v5 of [0], although a fair bit of time has passed since then. In the meantime I upstreamed the dt-binding, which was in the v1, and this patch depends on the fixes to the dt-binding and device tree etc which landed in v5.18-rc5.

* v2: [RISC-V: fix access beyond allocated array](https://lore.kernel.org/linux-riscv/20220624135902.520748-1-geomatsi@gmail.com/T/#t)

    These patches suggest some fixes and cleanups for the handling of pmu counters. The first patch fixes access beyond the allocated pmu_ctr_list array. The second patch fixes the counters mask sent to SBI firmware: it excludes counters that were not fully specified by SBI firmware on init.

* v6: [riscv: Support qspinlock with generic headers](https://lore.kernel.org/linux-riscv/7adc9e19-7ffc-4b11-3e18-6e3a5225638f@redhat.com/T/#t)

    RISC-V LR/SC pairs could provide a strong/weak forward guarantee that depends on micro-architecture. And RISC-V ISA spec has given out several limitations to let hardware support strict forward guarantee (RISC-V User ISA - 8.3 Eventual Success of Store-Conditional Instructions)

* v1: [RISC-V: KVM: Improve ISA extension by using a bitmap](https://lore.kernel.org/linux-riscv/20220620234254.2610040-1-atishp@rivosinc.com/T/#u)

    Using a bitmap allows the ISA extension to support any number of extensions. The CONFIG one reg interface implementation is modified to support the bitmap as well.

### 周边技术动态

#### Qemu

* v9: [QEMU RISC-V nested virtualization fixes](https://lore.kernel.org/qemu-devel/CAAhSdy2iTPwqzUAhV8s97k1d4sK-bne1z-T6pg__p3xfsUrdHg@mail.gmail.com/)

    This series does fixes and improvements to have nested virtualization on QEMU RISC-V. The RISC-V nested virtualization was tested on QEMU RISC-V using Xvisor RISC-V which has required hypervisor support to run another hypervisor as Guest/VM.
    
* v9：[target/riscv: Force disable extensions if priv spec version does not match ](https://lore.kernel.org/qemu-devel/20220630061150.905174-3-apatel@ventanamicro.com/)

    We should disable extensions in riscv_cpu_realize() if minimum required priv spec version is not satisfied. This also ensures that machines with priv spec v1.11 (or lower) cannot enable H, V, and various multi-letter extensions.
    
* v10：[target/riscv: Add sscofpmf extension support](https://lore.kernel.org/qemu-devel/20220620231603.2547260-9-atishp@rivosinc.com/)

    The Sscofpmf ('Ss' for Privileged arch and Supervisor-level extensions, and 'cofpmf' for Count OverFlow and Privilege Mode Filtering) extension allows the perf to handle overflow interrupts and filtering support. 

#### Bulidroot

* v2: [board: Add Sipeed MAIX-Go board support](https://lore.kernel.org/buildroot/20220530033836.474926-11-damien.lemoal@opensource.wdc.com/)

    Add two buildroot configuration files to build a minimal Linux environment for the Sipeed MAIX Bit board.

* v1: [configs/qemu_riscv64_nommu_virt_defconfig: new defconfig](https://lore.kernel.org/buildroot/20220607190455.B6C6E869E1@busybox.osuosl.org/)

    Add RISC-V 64-bit nommu defconfig for QEMU virt machine with MMU disabled. Unlike qemu_riscv64_virt, qemu_riscv64_nommu_virt does not use OpenSBI, since the kernel is running in machine mode (M-mode).

#### U-Boot

* v3: [valgrind: Disable on Risc-V](https://lore.kernel.org/u-boot/20220527140300.682989-2-seanga2@gmail.com/)

    There are no defined instruction sequences in include/valgrind.h for Risc-V, so CONFIG_VALGRIND will do nothing on this arch.

#### Yocto

* v1: [looking to build meta-riscv based HDMI image for nezha board](https://lore.kernel.org/yocto/8d419e1-2a74-36ee-effe-c3b622ee6195@crashcourse.ca/)

    After finally unpacking my risc-v nezha SBC, i'm trying to download/build an image that will boot to a desktop with as little effort as possible. 
