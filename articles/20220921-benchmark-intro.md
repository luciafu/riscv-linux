> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1-rc2 - [spaces]<br/>
> Author:   LucasXu <lucas.xuyq.work@outlook.com><br/>
> CoAuthor: Reset <KenterTan12138@outlook.com><br/>
> Revisor:  Taotieren<br/>
> Date:     2022/09/21<br/>
> Project:  [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Sponsor:  PLCT Lab, ISCAS

# Linux 下常用 Benchmark 工具一览

## 概述

该文章是 RISC-V Linux 硬件开发系列的第一篇文章，主要介绍 Linux 下常用的 Benchmark 工具，以及如何使用这些工具。

## Sysbench

### 简介

Sysbench 是一个用于测试 Linux 系统性能的工具，它可以测试 CPU、内存、文件系统、数据库等方面的性能。Sysbench 本身是一个命令行工具，但是它也可以通过 Lua 脚本来扩展，从而可以测试更多的性能指标。

但是根据 2019.12.19 的 [GitHub issue][001]，2021 年 [Google Groups][002] 以及目前 `LuaJIT` 对 RISC-V 的支持情况来看，Sysbench 在 RISC-V 上的性能测试还存在一些问题，故在接下来对哪吒开发板进行性能测试时，我们暂时不使用 Sysbench。

#### 项目地址

[Sysbench][003]

### 使用方法

```bash
$ sysbench [options]... [testname] [command]
```

* `testname` 是一个内置测试的可选名称（例如 fileio，内存，CPU 等），或一个捆绑的 Lua 脚本的名称（例如 oltp_read_only），或一个自定义 Lua 脚本的路径。如果在命令行上没有指定测试名称（因此，也没有命令，因为在这种情况下，它将被解析为测试名称），或者测试名称是一个破折号（"-"），那么 sysbench 期望在其标准输入上执行一个 Lua 脚本。
* `command` 是一个可选的参数，它将被 `sysbench` 传递给内置测试或用 testname 指定的脚本。command 定义了测试必须执行的操作。可用的命令列表取决于一个特定的测试。一些测试也实现了他们自己的自定义命令。
* 下面是对典型的测试命令及其目的的描述。
  * `prepare` - 在测试开始之前预处理。它可以用来创建测试所需的数据结构，例如数据库表，或者创建测试文件
  * `run` - 执行测试。这个命令可以被执行多次，以便获得更准确的结果
  * `cleanup` - 在测试结束之后执行一次。它可以用来删除测试所需的数据结构，例如数据库表，或者删除测试文件
  * `help` - 显示测试的帮助信息
* `options` 是一个可选的参数，它定义了测试的行为。可用的选项列表取决于一个特定的测试。一些测试也实现了他们自己的自定义选项。

## UnixBench

### 简介

UnixBench 是最原始的 Unix 系统基准测试套件，多年来被许多人更新和修订。

UnixBench 的目的是为类 Unix 系统的性能提供一个基本指标；因此，使用多个测试来测试系统性能的各个方面。然后将这些测试结果与基线系统的分数进行比较，产生一个指数值，这通常比原始分数更容易处理。然后将整套指数值结合起来，形成一个系统的总指数。

一些非常简单的图形测试被包括在内，以衡量系统的 2D 和 3D 图形性能。

多 CPU 系统也可以进行测试。如果你的系统有多个 CPU，默认行为是运行所选的测试两次--一次是每个测试程序的一个副本，另一次是 N 个副本，其中 N 是 CPU 的数量。这样，你可以比较单个 CPU 的性能，以及多个 CPU 的性能。

### 项目地址

[UnixBench][004]

### 包括的测试

`UnixBench` 由一些针对特定领域的单独测试组成。以下是每个测试的概要。
* `Dhrystone` 由 Reinhold Weicker 在 1984 年开发。这个基准测试用于测量和比较计算机的性能。该测试的重点是字符串处理，因为没有浮点操作。它在很大程度上受到硬件和软件设计、编译器和链接器选项、代码优化、高速缓冲存储器、等待状态和整数数据类型的影响。
* `Whetstone` 由 John Whetstone 开发。这个测试测量浮点操作的速度和效率。该测试包含几个模块，旨在代表科学应用中通常进行的混合操作。使用了各种各样的 C 函数，包括 sin、cos、sqrt、exp 和 log，以及整数和浮点数学运算、数组访问、条件分支和过程调用。该测试同时测量整数和浮点算术。
* `execl Throughput` 该测试测量进程创建和执行的速度。它使用了 `fork` 和 `exec` 系统调用，以及 C 库函数 `execl`。该测试测量每秒可执行的 `execl` 调用的数量。`execl` 是 `exec` 系列函数的一部分，它用一个新的进程图像替换当前的进程图像。它和许多其他类似的命令是函数 `execve()` 的前端。
* `File Copy` 这是测量数据从一个文件传输到另一个文件的速度，使用不同的缓冲区大小。文件读、写和复制测试捕捉在指定时间内可以写入、读和复制的字符数（默认为 10 秒）。
* `Pipe Throughput` 管道是进程间通信的最简单形式。管道吞吐量是一个进程向管道写入 512 Byte 并读回的次数（每秒）。在现实世界的编程中，管道吞吐量测试没有真正的对应物。
* `Pipe-based Context Switching` 这个测试测量两个进程通过管道交换一个增加的整数的次数。基于管道的上下文切换测试更像现实世界的应用。测试程序产生一个子进程，与之进行双向的管道对话。
* `Process Creation` 这个测试测量每秒可以创建的进程数和立即退出的子进程的次数。进程创建指的是为新进程实现创建进程控制块和进行内存分配，因此可以直接用于内存带宽测试。通常情况下，这个基准测试被用来比较各个操作系统进程创建调用的各种实现
* `Shell Scripts` shell 脚本测试了每分钟可以启动和退出的脚本个数。其中用的脚本对一个数据文件进行了一系列的数据转换
* `System Call Overhead` 这估计的是进入和离开操作系统内核的成本，即执行系统调用的开销。它由一个简单的程序重复调用 `getpid`（返回调用进程的进程 ID）系统调用组成。执行这种调用的时间被用来估计进入和退出内核的成本。

## microbench

### 简介

在分析 RISC-V Jump Label 的过程中，为了探究 Jump Label 的性能优化预期效果，[泰晓科技][006] 在一周内专门基于 Google benchmark 框架开发了一款面向处理器基础指令性能测试的工具 ——`microbench`。

microbench 的最初目标是测试 Jump Label 涉及到的几条基础指令，比如：

* nop
* unconditional jump
* conditional branch (include beqz and bnez)
* load + branch
* cache miss + load + branch
* branch miss + load + branch
* cache miss + branch miss + load + branch

目前已经支持 x86_64, riscv64，对其他架构的支持和数据测试工作也将陆续展开。

从长远来看，我们期待扩大每个架构的指令覆盖范围，并希望这款工具能帮助业界评估目标处理器的真实性能表现，当然，也希望最终能帮助处理器厂商发现并改进设计上的缺陷。

microbench 目前还很简陋，短期内将继续停留在 RISC-V Linux 内核剖析 协作仓库内的 test/microbench 目录下，未来将作为独立的项目发布和维护。

### 项目地址

[microbench][005]

### 使用方法

#### 下载 microbench

```bash
$ git clone https://gitee.com/tinylab/riscv-linux
```

#### 编译 microbench

##### 在标准 Linux 平台上编译

```bash
$ make
```

## 总结

在本文中，大致介绍了 Linux 一些常用的基准测试工具，在接下来的移植过程中，将使用这些工具中的部分来为全志 D1/D1s 系列芯片进行基准测试，并与 X86 平台进行性能对比。

## 参考资料

1. [SysBench][003]

2. [UnixBench][004]

3. [microbench][005]

4. [Linux Kernel Performance][006]

[001]: https://github.com/akopytov/sysbench/issues/333
[002]: https://groups.google.com/a/groups.riscv.org/g/sw-dev/c/m5Va9Z93b28
[003]: https://github.com/akopytov/sysbench
[004]: https://github.com/kdlucas/byte-unixbench
[005]: https://tinylab.org/
[006]: https://www.kernel.org/doc/html/latest/admin-guide/kernel-parameters.html
