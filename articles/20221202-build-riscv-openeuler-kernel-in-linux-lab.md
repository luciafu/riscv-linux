> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [spaces header urls]<br/>
> Author:    Falcon <falcon@tinylab.org><br/>
> Date:      2022/12/02<br/>
> Revisor:   Falcon<br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Environ:   [泰晓 Linux 实验盘](https://tinylab.org/linux-lab-disk/)<br/>
> Sponsor:   PLCT Lab, ISCAS

# 在 Linux Lab 下编译 OpenEuler RISC-V Linux 内核

## 前言

在 [开源之夏 2021][005] 的时候，泰晓社区有指导两位学生为 [Linux Lab][003] 添加了 X86_64 和 Aarch64 架构的 OpenEuler Linux 内核开发支持，可以大大简化 OpenEuler Linux 内核的学习与开发门槛。

往常需要自己从头准备 QEMU 或鲲鹏的机器，还需要准备编译器，撰写启动脚本等等，现在在 Linux Lab 下，就可以用非常简单的几条命令完成 OpenEuler Linux 内核的编译与运行。

这次我们也为 RISC-V 架构添加了 OpenEuler Linux 内核的开发支持，版本是：`5.10.0-132.0.0`，接下来做个简单介绍。

## 准备 Linux Lab 环境

省事一点的话，可以直接选购一支 [泰晓 Linux 实验盘][004]。该实验盘免安装，即插即跑，启动后桌面就有 Linux Lab Shell 图标，点击即可登陆。

如果想自行安装的话，可以参考 [Linux Lab][003] 文档。

如果手头有 Ubuntu 系统，也可以这样快速上手：

```
$ git clone https://gitee.com/tinylab/cloud-lab.git
$ cd cloud-lab
$ tools/docker/run linux-lab
ubuntu@linux-lab:/labs/linux-lab$
```

这个的效果跟在泰晓实验盘中执行 Linux Lab Shell 是一样的。

## 切换到 riscv64/virt 开发板

在 Linux Lab Shell 下面，可以非常轻松地切换所需的开发板，大家可以用 `make list-short` 查看支持的开发板列表，咱们这里直接选用目标开发板：

```
$ make BOARD=riscv64/virt
```

Linux Lab 目前已经支持 7 大主流处理器架构，支持 20 多款虚拟开发板和真实开发板。

## 配置 KERNEL_FORK 变量

OpenEuler 的内核是官方 Linux 内核的一个分叉，所以需要配置 `KERNEL_FORK` 参数，以便选择特定的内核仓库地址。

```
$ make local-config KERNEL_FORK=openeuler
```

做完本次文章的所有实验后，如果要用回 Linux 内核主线的分支，可以重置该变量：

```
$ make local-config KERNEL_FORK=
```

## 下载内核

配置完上述 `KERNEL_FORK` 以后，就可以直接下载 OpenEuler 的内核：

```
$ make kernel-download
```

OpenEuler 内核默认下载在 `src/openeuler`。

## 配置内核

在 Linux Lab 下，启用配置非常方便：

```
$ make kernel-defconfig
```

我们提前适配好的配置文件放在 `boards/riscv64/virt/bsp/configs/openeuler/`。

为了提升编译速度，我们在 OpenEuler 提供的 `arch/riscv/configs/defconfig` 的基础上，做了一些配置删减，也剔除了引起编译错误的 `EFI` 配置选项。

## 编译内核

Linux Lab 做了非常简洁的封装，不需要复杂的配置，也无需额外准备编译器，就可以直接编译内核：

```
$ make kernel
```

内核编译完成后保存在 `build/riscv64/virt/openeuler/linux/`。

## 启动内核

编译完成以后，Linux Lab 提供了极为简单的接口来直接调用 QEMU 模拟器，它会依次启动 OpenSBI、U-Boot 和 Linux 内核，并自动加载预编译好的文件系统：

```
LOG: Generating harddisk image with tools/root/rd2hd.sh ...
LOG: Rootfs size: 25600 (kilo bytes)
mke2fs 1.45.5 (07-Jan-2020)
Discarding device blocks: done
Creating filesystem with 6400 4k blocks and 6400 inodes

Allocating group tables: done
Writing inode tables: done
Writing superblocks and filesystem accounting information: done

sudo env PATH=/labs/linux-lab/build/riscv64/virt/qemu/v7.1.0/riscv64-softmmu/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin  qemu-system-riscv64 -bios /labs/linux-lab/boards/riscv64/virt/bsp/bios/opensbi/generic/fw_jump.elf -M virt  -m 256M -net nic,model=virtio -net tap,script=/etc/qemu-ifup,downscript=/etc/qemu-ifdown -device virtio-net-device,netdev=net0,mac=$(tools/qemu/macaddr.sh) -netdev tap,id=net0 -smp 4 -kernel /labs/linux-lab/boards/riscv64/virt/bsp/uboot/v2022.04/u-boot -no-reboot  -device loader,file=/labs/linux-lab/build/riscv64/virt/openeuler/linux/5.10.0-132.0.0/arch/riscv/boot/Image,addr=0x84000000 -device loader,file=tftpboot/env.img,addr=0x87300000 -drive if=none,file=/labs/linux-lab/build/riscv64/virt/bsp/root/2019.05/rootfs.ext2,format=raw,id=virtio-vda -device virtio-blk-device,drive=virtio-vda -nographic
qemu-system-riscv64: warning: hub port hub0port0 has no peer
qemu-system-riscv64: warning: hub 0 with no nics
qemu-system-riscv64: warning: netdev hub0port0 has no peer
qemu-system-riscv64: warning: requested NIC (#net083, model virtio) was not created (not supported by this machine?)

OpenSBI v0.9
   ____                    _____ ____ _____
  / __ \                  / ____|  _ \_   _|
 | |  | |_ __   ___ _ __ | (___ | |_) || |
 | |  | | '_ \ / _ \ '_ \ \___ \|  _ < | |
 | |__| | |_) |  __/ | | |____) | |_) || |_
  \____/| .__/ \___|_| |_|_____/|____/_____|
        | |
        |_|

Platform Name             : riscv-virtio,qemu
Platform Features         : timer,mfdeleg
Platform HART Count       : 4
Firmware Base             : 0x80000000
Firmware Size             : 124 KB
Runtime SBI Version       : 0.2

Domain0 Name              : root
Domain0 Boot HART         : 3
Domain0 HARTs             : 0*,1*,2*,3*
Domain0 Region00          : 0x0000000080000000-0x000000008001ffff ()
Domain0 Region01          : 0x0000000000000000-0xffffffffffffffff (R,W,X)
Domain0 Next Address      : 0x0000000080200000
Domain0 Next Arg1         : 0x0000000082200000
Domain0 Next Mode         : S-mode
Domain0 SysReset          : yes

Boot HART ID              : 3
Boot HART Domain          : root
Boot HART ISA             : rv64imafdcsuh
Boot HART Features        : scounteren,mcounteren,time
Boot HART PMP Count       : 16
Boot HART PMP Granularity : 4
Boot HART PMP Address Bits: 54
Boot HART MHPM Count      : 0
Boot HART MHPM Count      : 0
Boot HART MIDELEG         : 0x0000000000001666
Boot HART MEDELEG         : 0x0000000000f0b509

U-Boot 2022.04-dirty (Oct 02 2022 - 22:33:23 +0800)

CPU:   rv64imafdch_zicsr_zifencei_zba_zbb_zbc_zbs
Model: riscv-virtio,qemu
DRAM:  256 MiB
Core:  19 devices, 10 uclasses, devicetree: board
Flash: 32 MiB
Loading Environment from nowhere... OK
In:    uart@10000000
Out:   uart@10000000
Err:   uart@10000000
Net:   eth0: virtio-net#0
Hit any key to stop autoboot:  0
## Warning: defaulting to text format
Moving Image from 0x84000000 to 0x80200000, end=81119518
## Flattened Device Tree blob at 8f736630
   Booting using the fdt blob at 0x8f736630
   Using Device Tree in place at 000000008f736630, end 000000008f73adf4

Starting kernel ...

Linux version 5.10.0-20534-gfb0c3958f816 (ubuntu@linux-lab) (riscv64-linux-gnu-gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0, GNU ld (GNU Binutils for Ubuntu) 2.34) #1 SMP Fri Dec 2 14:02:56 CST 2022
OF: fdt: Ignoring memory range 0x80000000 - 0x80200000
Zone ranges:
  DMA32    [mem 0x0000000080200000-0x000000008fffffff]
  Normal   empty
Movable zone start for each node
Early memory node ranges
  node   0: [mem 0x0000000080200000-0x000000008fffffff]
Initmem setup node 0 [mem 0x0000000080200000-0x000000008fffffff]
software IO TLB: mapped [mem 0x000000008b736000-0x000000008f736000] (64MB)
SBI specification v0.2 detected
SBI implementation ID=0x1 Version=0x9
SBI v0.2 TIME extension detected
SBI v0.2 IPI extension detected
SBI v0.2 RFENCE extension detected
SBI v0.2 HSM extension detected
riscv: ISA extensions abcdefhimnrs
riscv: ELF capabilities acdfim
percpu: Embedded 17 pages/cpu s31384 r8192 d30056 u69632
Built 1 zonelists, mobility grouping on.  Total pages: 64135
Kernel command line: route=172.20.244.167 iface=eth0 rw fsck.repair=yes rootwait root=/dev/vda console=ttyS0
Dentry cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
Inode-cache hash table entries: 16384 (order: 5, 131072 bytes, linear)
Sorting __ex_table...
mem auto-init: stack:off, heap alloc:off, heap free:off
Memory: 174428K/260096K available (6218K kernel code, 4771K rwdata, 2048K rodata, 174K init, 317K bss, 85668K reserved, 0K cma-reserved)
SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
rcu: Hierarchical RCU implementation.
rcu: 	RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=4.
rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
riscv-intc: 64 local interrupts mapped
plic: plic@c000000: mapped 96 interrupts with 4 handlers for 8 contexts.
riscv_timer_init_dt: Registering clocksource cpuid [0] hartid [3]
clocksource: riscv_clocksource: mask: 0xffffffffffffffff max_cycles: 0x24e6a1710, max_idle_ns: 440795202120 ns
sched_clock: 64 bits at 10MHz, resolution 100ns, wraps every 4398046511100ns
Console: colour dummy device 80x25
Calibrating delay loop (skipped), value calculated using timer frequency.. 20.00 BogoMIPS (lpj=40000)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 512 (order: 0, 4096 bytes, linear)
Mountpoint-cache hash table entries: 512 (order: 0, 4096 bytes, linear)
rcu: Hierarchical SRCU implementation.
smp: Bringing up secondary CPUs ...
smp: Brought up 1 node, 4 CPUs
devtmpfs: initialized
clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
futex hash table entries: 1024 (order: 4, 65536 bytes, linear)
NET: Registered protocol family 16
HugeTLB registered 2.00 MiB page size, pre-allocated 0 pages
vgaarb: loaded
SCSI subsystem initialized
usbcore: registered new interface driver usbfs
usbcore: registered new interface driver hub
usbcore: registered new device driver usb
clocksource: Switched to clocksource riscv_clocksource
NET: Registered protocol family 2
IP idents hash table entries: 4096 (order: 3, 32768 bytes, linear)
tcp_listen_portaddr_hash hash table entries: 256 (order: 0, 4096 bytes, linear)
TCP established hash table entries: 2048 (order: 2, 16384 bytes, linear)
TCP bind hash table entries: 2048 (order: 3, 32768 bytes, linear)
TCP: Hash tables configured (established 2048 bind 2048)
UDP hash table entries: 256 (order: 1, 8192 bytes, linear)
UDP-Lite hash table entries: 256 (order: 1, 8192 bytes, linear)
NET: Registered protocol family 1
RPC: Registered named UNIX socket transport module.
RPC: Registered udp transport module.
RPC: Registered tcp transport module.
RPC: Registered tcp NFSv4.1 backchannel transport module.
PCI: CLS 0 bytes, default 64
kvm [1]: hypervisor extension available
kvm [1]: using Sv48x4 G-stage page table format
kvm [1]: VMID 14 bits available
workingset: timestamp_bits=62 max_order=16 bucket_order=0
NFS: Registering the id_resolver key type
Key type id_resolver registered
Key type id_legacy registered
nfs4filelayout_init: NFSv4 File Layout Driver Registering...
nfs4flexfilelayout_init: NFSv4 Flexfile Layout Driver Registering...
9p: Installing v9fs 9p2000 file system support
NET: Registered protocol family 38
Block layer SCSI generic (bsg) driver version 0.4 loaded (major 251)
io scheduler mq-deadline registered
io scheduler kyber registered
pci-host-generic 30000000.pci: host bridge /soc/pci@30000000 ranges:
pci-host-generic 30000000.pci:       IO 0x0003000000..0x000300ffff -> 0x0000000000
pci-host-generic 30000000.pci:      MEM 0x0040000000..0x007fffffff -> 0x0040000000
pci-host-generic 30000000.pci:      MEM 0x0400000000..0x07ffffffff -> 0x0400000000
pci-host-generic 30000000.pci: ECAM at [mem 0x30000000-0x3fffffff] for [bus 00-ff]
pci-host-generic 30000000.pci: PCI host bridge to bus 0000:00
pci_bus 0000:00: root bus resource [bus 00-ff]
pci_bus 0000:00: root bus resource [io  0x0000-0xffff]
pci_bus 0000:00: root bus resource [mem 0x40000000-0x7fffffff]
pci_bus 0000:00: root bus resource [mem 0x400000000-0x7ffffffff]
pci 0000:00:00.0: [1b36:0008] type 00 class 0x060000
Serial: 8250/16550 driver, 4 ports, IRQ sharing disabled
printk: console [ttyS0] disabled
10000000.uart: ttyS0 at MMIO 0x10000000 (irq = 2, base_baud = 230400) is a 16550A
printk: console [ttyS0] enabled
[drm] radeon kernel modesetting enabled.
loop: module loaded
virtio_blk virtio1: [vda] 51200 512-byte logical blocks (26.2 MB/25.0 MiB)
vda: detected capacity change from 0 to 26214400
e1000e: Intel(R) PRO/1000 Network Driver
e1000e: Copyright(c) 1999 - 2015 Intel Corporation.
ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
ehci-pci: EHCI PCI platform driver
ehci-platform: EHCI generic platform driver
ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
ohci-pci: OHCI PCI platform driver
ohci-platform: OHCI generic platform driver
usbcore: registered new interface driver uas
usbcore: registered new interface driver usb-storage
mousedev: PS/2 mouse device common for all mice
goldfish_rtc 101000.rtc: registered as rtc0
goldfish_rtc 101000.rtc: setting system clock to 2022-12-02T10:51:12 UTC (1669978272)
syscon-poweroff soc:poweroff: pm_power_off already claimed (____ptrval____) �;$����
syscon-poweroff: probe of soc:poweroff failed with error -16
sdhci: Secure Digital Host Controller Interface driver
sdhci: Copyright(c) Pierre Ossman
sdhci-pltfm: SDHCI platform and OF driver helper
usbcore: registered new interface driver usbhid
usbhid: USB HID core driver
NET: Registered protocol family 17
9pnet: Installing 9P2000 support
Key type dns_resolver registered
EXT4-fs (vda): mounting ext2 file system using the ext4 subsystem
EXT4-fs (vda): mounted filesystem without journal. Opts: (null)
ext2 filesystem being mounted at /root supports timestamps until 2038 (0x7fffffff)
VFS: Mounted root (ext2 filesystem) on device 254:0.
devtmpfs: mounted
Freeing unused kernel memory: 172K
Run /sbin/init as init process
EXT4-fs (vda): re-mounted. Opts: (null)
ext2 filesystem being remounted at / supports timestamps until 2038 (0x7fffffff)
Starting syslogd: OK
Starting klogd: OK
Initializing random number generator... random: dd: uninitialized urandom read (512 bytes read)
done.
Starting network: IP: 172.20.244.39
Route: 172.20.244.167
OK

Welcome to Linux Lab
linux-lab login: root
#
# uname -a
Linux linux-lab 5.10.0-20534-gfb0c3958f816 #1 SMP Fri Dec 2 14:02:56 CST 2022 riscv64 GNU/Linux
#
# poweroff
# Stopping network: OK
Saving random seed... random: dd: uninitialized urandom read (512 bytes read)
done.
Stopping klogd: OK
Stopping syslogd: OK
umount: devtmpfs busy - remounted read-only
EXT4-fs (vda): re-mounted. Opts: (null)
The system is going down NOW!
Sent SIGTERM to all processes
Sent SIGKILL to all processes
Requesting system poweroff
kvm: exiting hardware virtualization
reboot: Power down
```

可以看到，确实启动了一个我们刚刚编译好的内核版本。

启动以后，可以用 `poweroff` 命令关闭系统。

## 总结

泰晓社区近期为 Linux Lab 开源项目添加了 RISC-V 架构的 OpenEuler Linux 内核开发支持，本文对其用法做了简单演示，欢迎体验。

有了该支持，普通用户无需购买开发板，无需下载编译器，无需构建文件系统，无需配置复杂的 QEMU 启动参数，就可以快速开展国产 OpenEuler Linux 内核的开发，从而大大降低了此类国产软件的开发门槛，同时提升了学习与开发效率。

## 参考资料

* [Linux Lab][003]
* [泰晓 Linux 实验盘][004]
* [The openEuler kernel is the core of the openEuler OS][001]
* [Linux Kernel for openEuler RISC-V][002]

[001]: https://gitee.com/openeuler/kernel
[002]: https://gitee.com/src-openeuler/risc-v-kernel
[003]: https://tinylab.org/linux-lab
[004]: https://tinylab.org/linux-lab-disk
[005]: https://tinylab.org/summer2021/
