> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [comments codeinline urls pangu autocorrect epw]<br/>
> Author:    牛工 - 通天塔 985400330@qq.com<br/>
> Date:      2022/10/31<br/>
> Revisor:   Falcon <falcon@ruma.tech>; iOSDevLog <iosdevlog@iosdevlog.com><br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [【老师提案】Static Call 技术分析与 RISC-V 移植 · Issue #I5Y585 · 泰晓科技/RISCV-Linux - Gitee.com](https://gitee.com/tinylab/riscv-linux/issues/I5Y585)<br/>
> Sponsor:   PLCT Lab, ISCAS

# retpolines

## 前言

在文章 [20221020-missing-features-tools-for-riscv-part2.md][004] 中，提到了 RISC-V 架构下缺失的内核功能：Avoiding retpolines with static calls。

该功能的提出，是有历史原因的：

- 2018 年发现漏洞 Meltdown 和 Spectre
- 谷歌提出 Retpolines 解决了这个安全问题，但引入了 4% 的性能影响
- 开发者们不断寻求解决方法：[Relief for retpoline pain][002]
- 2020 年使用 static calls 方法避免使用 retpolines，性能影响降低至 1.6%：[Avoiding retpolines with static calls][003]

上一篇文章中，对两个 Meltdown 和 Spectre 漏洞原理进行了分析，本篇文章将讲述 retpolines 如何对该漏洞进行修补的。

## retpolines 分析

### retpolines 原理

[giles-retpoline (griffissinstitute.org)][007] 这篇文章中讲了很多关于 retpoline 的基本原理。

文章中写到的一个关键的概念：

> - The call instruction also pushes the  “Return Address” to a hidden stack called the Return Stack Buffer or RSB  (RAS on AMD)
> - The speculative execution engine gets its “guess” from the RSB when a “ret”  instruction is executed.
> - The RSB is a lot faster than the regular stack but might hold an incorrect  “guess”.

- 调用指令还将 “返回地址” 推入一个称为 “返回栈缓冲区”（RSB，在 AMD 上叫 RAS ) 的隐藏栈。
- 当 “ret” 指令执行时，分支预测器将从 RSB 中获取他的“预测”。
- RSB 将比 stack 的寄存器快的多，但是他可能是一个错误的“预测”。

分支预测的原理就是通过 RSB 预测将要执行的地址，从而实现 CPU 性能的提升。

通过以上分析可知，只要让其无法通过训练来“预测”执行不应该被执行的分支，就解决了 Spectre 通过分支预测造成的内存非法访问。

文中讲述的方法就是在执行 jmp 或者 call 命令时，跳转到一个函数中，将 RSB 的寄存器改为目标地址 address，使其执行 ret 指令时跳转至目标 address。

即使在多核情况下，分支预测也只会跳转至一个毫无意义的死循环当中。

无论怎么训练，在调用 call 或者 jmp 过程中，都会因为在 repotline 的反跳转中，陷入一个无效的死循环，而无法完成预测执行，从而无法将预测执行的数据放入缓存中。

### retpolines 内核代码分析

第一次 retpoline 被合入的补丁链接：[[tip:x86/pti] x86/retpoline: Add initial retpoline support - tip-bot for David Woodhouse (kernel.org)][006]

> Enable the use of -mindirect-branch=thunk-extern in newer GCC, and provide the corresponding thunks. Provide assembler macros for invoking the thunks in the same way that GCC does, from native and inline assembler.
>
> This adds X86_FEATURE_RETPOLINE and sets it by default on all CPUs. In some circumstances, IBRS microcode features may be used instead, and the retpoline can be disabled.
>
> On AMD CPUs if lfence is serialising, the retpoline can be dramatically simplified to a simple "lfence; jmp *\reg". A future patch, after it has been verified that lfence really is serialising in all circumstances, can enable this by setting the X86_FEATURE_RETPOLINE_AMD feature bit in addition to X86_FEATURE_RETPOLINE.
>
> Do not align the retpoline in the altinstr section, because there is no guarantee that it stays aligned when it's copied over the oldinstr during alternative patching.

在新的 GCC 中启用 `-mindirect-Branch = thunk-extern`，并提供相应的 thunk。提供汇编程序宏来调用 thunk，方式与 GCC 相同，从本机和内联汇编。

这将添加 `X86_FEATURE_RETPOLINE` 并在所有 CPU 上默认设置它。在某些情况下，可以使用 IBRS 微码特性来代替，并且可以禁用 retpoline。

在 AMD CPU 上，如果 lfence 是序列化的，那么可以将 repoline 戏剧性地简化为一个简单的 “lfence; jmp * reg”。未来的补丁，在确认 lfence 在所有情况下确实正在序列化之后，可以通过设置 `X86_FEATURE_RETPOLINE_AMD` 特性位以及 `X86_FEATURE_RETPOLINE` 来启用它。

不要对 altinstr 部分中的 retpoline 进行对齐，因为在替代修补期间将它复制到 oldinstr 上时，不能保证它保持对齐。

> config RETPOLINE
>         bool "Avoid speculative indirect branches in kernel"
>         default y
>         help
>           Compile kernel with the retpoline compiler options to guard against
>           kernel-to-user data leaks by avoiding speculative indirect
>           branches. Requires a compiler with -mindirect-branch=thunk-extern
>           support for full protection. The kernel may run slower.
>
>           Without compiler support, at least indirect branches in assembler
>           code are eliminated. Since this includes the syscall entry path,
>           it is not entirely pointless.

使用 retpoline 编译器选项编译内核，通过避免推测的间接分支来防止内核到用户的数据泄漏。要求编译器具有 `-mindirect-branch= unk-extern` 支持以实现完全保护。内核可能会运行得更慢。如果没有编译器支持，汇编程序代码中的间接分支至少会被消除。因为这包含了系统调用入口路径，所以它并非完全没有意义。

具体代码分析：

- 宏定义新增，外部声明间接调用函数。

```c
diff --git a/arch/x86/include/asm/asm-prototypes.h b/arch/x86/include/asm/asm-prototypes.h
index ff700d8..0927cdc 100644
--- a/arch/x86/include/asm/asm-prototypes.h
+++ b/arch/x86/include/asm/asm-prototypes.h
@@ -11,7 +11,32 @@
 #include <asm/pgtable.h>
 #include <asm/special_insns.h>
 #include <asm/preempt.h>
+#include <asm/asm.h>

 #ifndef CONFIG_X86_CMPXCHG64
 extern void cmpxchg8b_emu(void);
 #endif
+
+#ifdef CONFIG_RETPOLINE
+#ifdef CONFIG_X86_32
+#define INDIRECT_THUNK(reg) extern asmlinkage void __x86_indirect_thunk_e ## reg(void);
+#else
+#define INDIRECT_THUNK(reg) extern asmlinkage void __x86_indirect_thunk_r ## reg(void);
+INDIRECT_THUNK(8)
...
+INDIRECT_THUNK(15)
+#endif
+INDIRECT_THUNK(ax)
...
+INDIRECT_THUNK(sp)
+#endif /* CONFIG_RETPOLINE */
```

- 反跳转实现，关键的 repotlines 代码实现

以下代码存在大量的汇编指令，比较难理解，[X86 架构处理器汇编伪指令介绍（360doc.com）][008] 中存在很多伪指令的简介，[ARM 裸机开发篇 3：ARM 汇编语言程序设计（baidu.com）][005] 有一些汇编语言相关知识可以参考。

```c
diff --git a/arch/x86/include/asm/nospec-branch.h b/arch/x86/include/asm/nospec-branch.h
new file mode 100644
index 0000000..e20e92e
--- /dev/null
+++ b/arch/x86/include/asm/nospec-branch.h
@@ -0,0 +1,128 @@
+/* SPDX-License-Identifier: GPL-2.0 */
+
+#ifndef __NOSPEC_BRANCH_H__
+#define __NOSPEC_BRANCH_H__
+
+#include <asm/alternative.h>
+#include <asm/alternative-asm.h>
+#include <asm/cpufeatures.h>
+
+#ifdef __ASSEMBLY__
+
+/*
+ * This should be used immediately before a retpoline alternative.  It tells
+ * objtool where the retpolines are so that it can make sense of the control
+ * flow by just reading the original instruction(s) and ignoring the
+ * alternatives.
+ * 在 retpoline 替代品出来之前，它应该被应用，
+ * 它告诉了 objtool retpolines 在哪里，所以它可以通过读取原始指令来理解控制流，并且忽略替代品。
+ */
+.macro ANNOTATE_NOSPEC_ALTERNATIVE /* no spece 的替代方案 */
+	.Lannotate_\@:			/* 标号 */
+	.pushsection .discard.nospec	/* 入栈 */
+	.long .Lannotate_\@ - .			
+	.popsection			/* 出栈 */
+.endm
+
+/*
+ * These are the bare retpoline primitives for indirect jmp and call.
+ * Do not use these directly; they only exist to make the ALTERNATIVE
+ * invocation below less ugly.
+ * 这里是 retpoline 的基本元间接调用 jmp 和 call，不要再直接使用它们了，
+ * 它们的存在只是为了使替代品的调用不那么难看。
+ */
+.macro RETPOLINE_JMP reg:req
+	call	.Ldo_rop_\@		/* 调用 Ldo_rop_ */
+.Lspec_trap_\@:
+	pause
+	jmp	.Lspec_trap_\@
+.Ldo_rop_\@:				/* 标号 */
+	mov	\reg, (%_ASM_SP)	/* 将 _ASM_SP 赋值给 reg（SP 寄存器存放着栈顶地址，是栈指针寄存器）*/
+	ret				/* ret 指令和 call 指令配合使用，返回值 call 的位置 */
+.endm
+
+/*
+ * This is a wrapper around RETPOLINE_JMP so the called function in reg
+ * returns to the instruction after the macro.
+ * 这是 RETPOLINE_JMP 的包装器，因此在 reg 中被调用的函数返回宏之后的那条指令。
+ */
+.macro RETPOLINE_CALL reg:req
+	jmp	.Ldo_call_\@
+.Ldo_retpoline_jmp_\@:
+	RETPOLINE_JMP \reg
+.Ldo_call_\@:				/* 反蹦床，先跳转至此处，再跳转回上一步 */
+	call	.Ldo_retpoline_jmp_\@
+.endm
+
+/*
+ * JMP_NOSPEC and CALL_NOSPEC macros can be used instead of a simple
+ * indirect jmp/call which may be susceptible to the Spectre variant 2
+ * attack.
+ * JMP_NOSPEC 和 CALL_NOSPEC 宏可以被用来替代简单的间接调用 jmp/call（容易被 Spectre variant 2 攻击）的指令。
+ */
+.macro JMP_NOSPEC reg:req
+#ifdef CONFIG_RETPOLINE		/* 使用 retpoline 则走此处跳转 */
+	ANNOTATE_NOSPEC_ALTERNATIVE
+	ALTERNATIVE_2 __stringify(jmp *\reg),				\
+		__stringify(RETPOLINE_JMP \reg), X86_FEATURE_RETPOLINE,	\
+		__stringify(lfence; jmp *\reg), X86_FEATURE_RETPOLINE_AMD
+#else
+	jmp	*\reg
+#endif
+.endm
+
+.macro CALL_NOSPEC reg:req		/* RETPOLINE_CALL 比 RETPOLINE_JMP 多了 1 次反蹦床 */
+#ifdef CONFIG_RETPOLINE
+	ANNOTATE_NOSPEC_ALTERNATIVE
+	ALTERNATIVE_2 __stringify(call *\reg),				\
+		__stringify(RETPOLINE_CALL \reg), X86_FEATURE_RETPOLINE,\
+		__stringify(lfence; call *\reg), X86_FEATURE_RETPOLINE_AMD
+#else
+	call	*\reg
+#endif
+.endm
+
+#else /* __ASSEMBLY__ */
+
+#define ANNOTATE_NOSPEC_ALTERNATIVE				\
+	"999:\n\t"						\
+	".pushsection .discard.nospec\n\t"			\
+	".long 999b - .\n\t"					\
+	".popsection\n\t"
+
+#if defined(CONFIG_X86_64) && defined(RETPOLINE)
+
+/*
+ * Since the inline asm uses the %V modifier which is only in newer GCC,
+ * the 64-bit one is dependent on RETPOLINE not CONFIG_RETPOLINE.
+ * 由于内联 asm 使用%V 修饰符，这只在较新的 GCC 中存在，
+ * 64 位的依赖于 RETPOLINE 而不是 CONFIG_RETPOLINE。
+ * 以下实现的东西跟上边差不多，像是为了兼容性所写的一些代码。
+ */
+# define CALL_NOSPEC						\
+	ANNOTATE_NOSPEC_ALTERNATIVE				\
+	ALTERNATIVE(						\
+	"call *%[thunk_target]\n",				\
+	"call __x86_indirect_thunk_%V[thunk_target]\n",		\
+	X86_FEATURE_RETPOLINE)
+# define THUNK_TARGET(addr) [thunk_target] "r" (addr)
+
+#elif defined(CONFIG_X86_32) && defined(CONFIG_RETPOLINE)
+/*
+ * For i386 we use the original ret-equivalent retpoline, because
+ * otherwise we'll run out of registers. We don't care about CET
+ * here, anyway.
+ * 对于 i386，我们使用原始的 ret-equivalent retpoline，
+ * 否则我们将用完寄存器，我们在这里无论如何都不再关心 CET.
+ */
+# define CALL_NOSPEC ALTERNATIVE("call *%[thunk_target]\n",	\
+	"       jmp    904f;\n"					\
+	"       .align 16\n"					\
+	"901:	call   903f;\n"					\
+	"902:	pause;\n"					\
+	"       jmp    902b;\n"					\
+	"       .align 16\n"					\
+	"903:	addl   $4, %%esp;\n"				\
+	"       pushl  %[thunk_target];\n"			\
+	"       ret;\n"						\
+	"       .align 16\n"					\
+	"904:	call   901b;\n",				\
+	X86_FEATURE_RETPOLINE)
+
+# define THUNK_TARGET(addr) [thunk_target] "rm" (addr)
+#else /* No retpoline */
+# define CALL_NOSPEC "call *%[thunk_target]\n"
+# define THUNK_TARGET(addr) [thunk_target] "rm" (addr)
+#endif
+
+#endif /* __ASSEMBLY__ */
+#endif /* __NOSPEC_BRANCH_H__ */
```

- 调用 repolines 相关实现

以上分析了宏定义的相关实现，比较重要的就是 JMP_NOSPEC 的实现，以下则对 JMP_NOSPEC 进行了调用，对一些寄存器做了调用函数的定义。

```c
diff --git a/arch/x86/lib/retpoline.S b/arch/x86/lib/retpoline.S
new file mode 100644
index 0000000..cb45c6c
--- /dev/null
+++ b/arch/x86/lib/retpoline.S
@@ -0,0 +1,48 @@
+/* SPDX-License-Identifier: GPL-2.0 */
+
+#include <linux/stringify.h>
+#include <linux/linkage.h>
+#include <asm/dwarf2.h>
+#include <asm/cpufeatures.h>
+#include <asm/alternative-asm.h>
+#include <asm/export.h>
+#include <asm/nospec-branch.h>
+
+.macro THUNK reg
+	.section .text.__x86.indirect_thunk.\reg
+
+ENTRY(__x86_indirect_thunk_\reg)
+	CFI_STARTPROC
+	JMP_NOSPEC %\reg
+	CFI_ENDPROC
+ENDPROC(__x86_indirect_thunk_\reg)
+.endm
+
+/*
+ * Despite being an assembler file we can't just use .irp here
+ * because __KSYM_DEPS__ only uses the C preprocessor and would
+ * only see one instance of "__x86_indirect_thunk_\reg" rather
+ * than one per register with the correct names. So we do it
+ * the simple and nasty way...
+ */
+#define EXPORT_THUNK(reg) EXPORT_SYMBOL(__x86_indirect_thunk_ ## reg)
+#define GENERATE_THUNK(reg) THUNK reg ; EXPORT_THUNK(reg)
+
+GENERATE_THUNK(_ASM_AX)
...
+GENERATE_THUNK(_ASM_SP)
+#ifdef CONFIG_64BIT
+GENERATE_THUNK(r8)
...
+GENERATE_THUNK(r15)
+#endif

```

GENERATE_THUNK(reg) 宏展开为：

```c
.section .text.__x86.indirect_thunk.\reg
ENTRY(__x86_indirect_thunk_\reg)
CFI_STARTPROC
JMP_NOSPEC %\reg /* 使用反跳床 */
CFI_ENDPROC
ENDPROC(__x86_indirect_thunk_\reg)
EXPORT_SYMBOL(__x86_indirect_thunk_\reg) /* 声明全局函数，全内核可以调用 */
```

以上代码中调用 JMP_NOSPEC，实际执行以下代码：

```c
.macro RETPOLINE_JMP reg:req
	call	.Ldo_rop_\@		/* 调用 Ldo_rop_ */
.Lspec_trap_\@:
	pause
	jmp	.Lspec_trap_\@
.Ldo_rop_\@:				/* 标号 */
	mov	\reg, (%_ASM_SP)	/* 将 _ASM_SP 赋值给 reg（SP 寄存器存放着栈顶地址，是栈指针寄存器）*/
	ret				/* ret 指令和 call 指令配合使用，返回值 call 的位置 */
.endm
```

其中比较难理解的是通过何种方式，访问到了目标地址 reg。

原理是在 `Ldo_rop_` 中，通过 ret 指令，跳转到了栈顶的数据中，在执行 ret 之前，就用 reg 数值替换了 ret 将要跳转的地址。

CALL 指令与 RET 指令原理如下：

> CALL 指令调用一个过程，指挥处理器从新的内存地址开始执行。过程使用 RET（从过程返回）指令将处理器转回到该过程被调用的程序点上。
>
> 从物理上来说，CALL 指令将其返回地址压入栈，再把被调用过程的地址复制到指令指针寄存器。当过程准备返回时，它的 RET 指令从栈把返回地址弹回到指令指针寄存器。

跳转流程如下：

![image-20221123235854753](images/static-call/image-20221123235854753.png)

## 小结

通过代码分析，repotlines 的原理就是，通过使用 ret 跳转至 reg 地址，而不是以前直接通过 call 或者 jmp 跳转至 reg 地址。

如果存在分支预测，提前执行 `Lspec_trap_` 那也不会获取到任何结果，因为这个地址本身就是一个空循环。

retpoline 通过改造 jmp 和 call 指令，强制的令 RSB 的功能失效了，从而使攻击者无法训练 CPU，使其执行攻击者想缓存的数据，retpoline 的缺点也十分的明显，分支预测功能失效了，执行的指令也变多了，一定会带来性能上的损失。

## 参考资料

- [20221020-missing-features-tools-for-riscv-part2.md][004]

- [Relief for retpoline pain][002]
- [Avoiding retpolines with static calls][003]
- [giles-retpoline (griffissinstitute.org)][007]
- [[tip:x86/pti] x86/retpoline: Add initial retpoline support - tip-bot for David Woodhouse (kernel.org)][006]
- [X86 架构处理器汇编伪指令介绍（360doc.com）][008]
- [ARM 裸机开发篇 3：ARM 汇编语言程序设计（baidu.com）][005]

[002]: https://lwn.net/Articles/774743/
[003]: https://lwn.net/Articles/815908/
[004]: https://gitee.com/nfk1996/riscv-linux/blob/a6b5ac6a507c106bb9471cbc06f8d43e9f912411/articles/20221020-missing-features-tools-for-riscv-part2.md
[005]: https://baijiahao.baidu.com/s?id=1715205853592979532&wfr=spider&for=pc
[006]: https://lore.kernel.org/all/tip-76b043848fd22dbf7f8bf3a1452f8c70d557b860@git.kernel.org/
[007]: https://www.griffissinstitute.org/what-we-do/gi-lecture-education-series-presentations/giles-retpoline
[008]: http://www.360doc.com/content/22/0413/12/68492704_1026298110.shtml
