#include "os.h"
#include "csr.h"

/*
 * Following functions SHOULD be called ONLY ONE time here,
 * so just declared here ONCE and NOT included in file os.h.
 */
extern void uart_init(void);

void start_kernel(void)
{
    uart_init();
    uart_puts("Hello, RVOS!\n");
    print_misa();

    while (1) {}; // stop here!
}

